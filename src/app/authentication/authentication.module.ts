import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../core/shared.module';
import { LockEngineService } from '../core/lock-engine/lock.manager.service';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },

  { path: '**', redirectTo: 'login', pathMatch: 'full' },
];

@NgModule({
  declarations: [LoginComponent],
  imports: [RouterModule.forChild(routes), SharedModule],
  exports: [],
  providers: [LockEngineService],
})
export class AuthenticationModule {}
