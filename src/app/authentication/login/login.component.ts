import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from 'src/app/core/security/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LockEngineService } from 'src/app/core/lock-engine/lock.manager.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CheckSessionStorageService } from 'src/app/core/service/check-session-storage.service';
import { PositionRouterService } from '../../core/service/position-router.service';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { Title } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

declare var deeseal: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  isLoginNow = false;
  loginForm: any;
  loginModel: any;

  registerUrl = environment.registerUrl;
  forgetPasswordUrl = environment.forgetPasswordUrl;
  
  get username() {
    return this.loginForm.get('username');
  }
  get password() {
    return this.loginForm.get('password');
  }

  constructor(
    private authService: AuthService,
    private titleService: Title,
    private deviceService: DeviceDetectorService,
    private router: ActivatedRoute,
    private translate: TranslateService,
    private readonly lockManager: LockEngineService, //   @Inject(DOCUMENT) private readonly document: any
    private checkSessionStorageService: CheckSessionStorageService,
    private positionRouterService: PositionRouterService,
    private dataService: DataServiceService
  ) {}

  ngOnInit() {
    this.loginModel = { username: '', password: '' };
    this.loginForm = new FormGroup({
      username: new FormControl(this.loginModel.username, [
        Validators.required,
      ]),
      password: new FormControl(this.loginModel.password, [
        Validators.required,
      ]),
    });

   			
	 this.translate
	  .get("body.title.label")
	  .toPromise()
	  .then((r) => {
		this.titleService.setTitle(r);
	  });
  }

  async login() {
    this.isLoginNow = true;
    const data = this.loginForm.getRawValue();
    this.checkSessionStorageService.clearSession();

    this.dataService.getClosYmInfo().subscribe(
      (response) => {
        // console.log('res. getClosYmInfo: ', Object.values(response)[0]);
        var yearId = Object.values(response)[0].substring(0, 4);
        sessionStorage.setItem('calendarCurrentYear', yearId);
      },
      (error) => {
        // console.log('getPerformanceDaily: ', error);
      }
    );

    await this.authService
      .oauthLogin(data.username, data.password)
      .then(
        (resolve) => {
          if (!this.authService.isRedirectCms(this.router.snapshot.queryParams)) {
            const postData = { username: data.username };
            // DD-WAR CR Check User Roles for Create/Update E-Register Activity
            this.dataService.getUserRolesByUsername(postData).subscribe(
              (response) => {
                console.log('res. getUserRolesByUsername: ', Object.values(response));
                let tempRoles = Object.values(response);
                
                let roles = tempRoles.map(item => item.roleId);
                console.log('MAPPING RESULT: ', roles);
                this.authService.setUserRolesList(roles);
              },
              (error) => {
                // console.log('getPerformanceDaily: ', error);
              }
            );
			
			 this.translate
			  .get("body.title.label")
			  .toPromise()
			  .then((r) => {
				this.titleService.setTitle(r);
			  });
			  
            this.positionRouterService.gotoPerformanceDetail();
          }
        },
        (err) => {
          // console.log(""err"");
        }
      )
      .finally(() => {
        this.isLoginNow = false;
      });
  }
}
