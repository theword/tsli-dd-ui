import { ActivatedRoute } from '@angular/router';
import { DashboardBackRouteService } from './../../core/service/dashboard-back-route.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/security/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-register-upload-activity',
  templateUrl: './register-upload-activity.component.html',
  styleUrls: ['./register-upload-activity.component.scss']
})
export class RegisterUploadActivityComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private dataService: DataServiceService,
    private webAgentService: WebAgentService,
    private spinner: NgxSpinnerService,
    private location: Location,
    private dashboardBackRouteService: DashboardBackRouteService,
    private router: ActivatedRoute
  ) { }

  public history: any = [
    {
      "id": "275",
      "fileName": "IL2021 - 021 - Potential การแข่งขันคุณวุฒิ ดูไบ_DD.xlsx",
      "createDate": "7/21/21, 11:32",
      "createBy": "THATPONG (ธัชพงศ์ คำพิทักษ์)",
      "type": "POTENTIAL"
    }
  ]

  public activityType;
  public activityName;

  ngOnInit(): void {
    this.setActivityType();
    this.apiGetHistoryUploadFile();
  }

  setActivityType() {
    this.router.queryParams.subscribe(params => {
      switch (params['IHRFCBJT']) {
        case 'HETWRGQR':
          this.activityType = "1";
          break;
        case 'YHDNHRDJ':
          this.activityType = "2";
          break;
        case 'OKJHGFRB':
          this.activityType = "3";
          break;
        case 'PMGFDHFN':
          this.activityType = "4";
          break;
        case 'HMCBMDYK':
          this.activityType = "5";
          break;
        case 'MV<VFGHR':
          this.activityType = "6";
          break;
        default:
          break;
      }

      this.activityName = params['GASDGETG']
    })
  }

  updateHistory(){
    this.apiGetHistoryUploadFile()
  }

  apiGetHistoryUploadFile() {
    this.spinner.show();
    console.log("apiGetHistoryUploadFile", history);

    this.dataService.getUploadFileHistory(this.activityType).subscribe(
    // this.webAgentService.getUploadHistoryByType(this.activityType).subscribe(
      response => {
        console.log('RES: ', response);
        this.history = response;
        this.spinner.hide();
      },
      error => {
        console.log('getUploadHistoryByType Failed: ', error);
        this.spinner.hide();
      }
    );
    this.spinner.hide();
  }

  handlerActivateSpinner(e) {
    if (e) {
      this.spinner.show();
    } else {
      this.spinner.hide();
    }
  }

  goToHome() {
    let userRole = this.authService.getPosition();
    let agentCode = this.authService.getLoginedName();
    sessionStorage.removeItem('agentCodeStored');
    sessionStorage.removeItem('agentLevelStored');
    this.dashboardBackRouteService.backHome(userRole, agentCode);
  }

  goBack() {
    this.location.back();
  }

}
