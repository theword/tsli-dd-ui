import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterUploadActivityComponent } from './register-upload-activity.component';

describe('RegisterUploadActivityComponent', () => {
  let component: RegisterUploadActivityComponent;
  let fixture: ComponentFixture<RegisterUploadActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterUploadActivityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterUploadActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
