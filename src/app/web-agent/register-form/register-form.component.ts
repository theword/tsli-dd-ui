import { Component, OnInit, QueryList, ViewChildren, ElementRef, Input } from '@angular/core';
import { DatePipe, Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import { NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/security/auth.service';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { ModalOptions } from 'angular-bootstrap-md';
@Component({
    selector: 'app-register-form',
    templateUrl: './register-form.component.html',
    styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {
    // Declare Variable
    // public modelRegisAgent: MockRegisterAgent;
    smartCardUrl = 'http://localhost:8084/smartcard/data/';
    btnName = 'ตกลง';
    public agentCode: string = this.authService.getLoginedName();
    public isDataAvailable: boolean = false;
    public eventInfo: any = {};

    public disabledBtn = false;
    public prefixImagePath: string = `${environment.apiWebAgentUrl}/e-register/images/`;
    public imageId: any;
    public idUrl: any;
    public msgImageId: string;
    public imageIdName: string = '';

    public imagePaid: any;
    public paidUrl: any;
    public msgImagePaid: string;
    public imagePaidName: string = '';
    public requirePayinLocal = false;
    public requirePictureLocal = false;

    // Form Agent
    model: NgbDateStruct;
    date: { year: number, month: number };
    public maxDate = new Date();
    public isRepeat = '';

    public modalOption: ModalOptions  = {   class: "modal-status modal-dialog-centered",   backdrop : 'static', keyboard : false };
    public agentForm: any = {
        'id': '',
        'dob': '',
        'registerId': '',
        'attendanceResult': '',
        'title': '',
        'name': '',
        'surname': '',
        'gender': '',
        'age': '',
        'mobile': '',
        'email': '',
        'line': '',
        'payinLocal': '',
        'idcardLocal': '',
    };
    // Form AgentRecommend
    public recommendAgentCode: string = '';
    public recommendAgentDetail: any = {
        'agentName': '',
        'agentCategoryCode': '',
    };
    public agentRegis: any = {
        'activityId': '',
        'address1': '',
        'address2': '',
        'address3': '',
        'address4': '',
        'address5': '',
        'age': '',
        'agentCode': '',
        'attendanceResult': '',
        'buildingNumber': '',
        'dateOfBirth': '',
        'delIndi': '',
        'dob': '',
        'educationLevel': undefined,
        'email': '',
        'examResult': '',
        'gender': '',
        'id': '',
        'idcardLocal': '',
        'insertBy': '',
        'insertTime': '',
        'isProspect': '',
        'licenseDate': '',
        'licenseNumber': '',
        'line': '',
        'mobile': '',
        'moo': '',
        'name': '',
        'payinLocal': '',
        'pictureLocal': '',
        'recommenderCode': '',
        'refActivityId': '',
        'registerId': '',
        'registerType': '',
        'roadName': '',
        'sexDivisionCode': '',
        'soiName': '',
        'surname': '',
        'thaiName': '',
        'thaiSurname': '',
        'thaiTitleName': '',
        'title': '',
        'updateBy': '',
        'updateTime': '',
        'zip': ''
    }
    public agentDetail: any = {
        'agentCode': '',
        'thaiFullName': '',
        'agentCateCodeSales': '',
        'licenseNumber': '',
        'dateOfLicenseExpiration': ''
    };

    form: FormGroup
    submitted = false
    idSubmitted = false
    payinSubmitted = false

    dropdownActivitySubType: any;
    dropdownGender: any
    dropdownTitle: any
    dropdownBankCode: any
    dropdownEducation: any
    showPayInDate: any = "text"

    activityExpire: any = false
    // Sync Variable from Session
    public eventType: string = sessionStorage.getItem('webAgent-regis-form-type');
    public eventCode: string = sessionStorage.getItem('webAgent-regis-form-id');
    public agentLevel: string = sessionStorage.getItem('webAgentLevel');

    public msgSuccess: string = 'บันทึกข้อมูลสำเร็จ';
    public msgFail: string = 'เกิดข้อผิดพลาดในการบันทึกข้อมูล';
    public modalStatus: string = '';
    public exitToPath: string = '';

    public disableRegisPid: Boolean = false;
    public disableRegisPayin: Boolean = false;

    private ATTENDANCE: string;

    private REGISTER_TYPE: string;

    @ViewChildren('file1,file2', { read: ElementRef }) inputFiles: QueryList<any>;

    constructor(
        private spinner: NgxSpinnerService
        , private webAgentService: WebAgentService
        , private formBuilder: FormBuilder
        , private datePipe: DatePipe
        , private authService: AuthService
        , private modalService: NgbModal
        , private translate: TranslateService
        , private location: Location
        , private router: ActivatedRoute
    ) {
        this.createForm();
    }

    ngOnInit(): void {
        this.apiGetActivityInfo();
        this.apiGetDropDownChoice();
        this.ATTENDANCE = atob(this.router.snapshot.queryParamMap.get('YXR0ZW5kYW5jZQ'));
        this.REGISTER_TYPE = atob(this.router.snapshot.queryParamMap.get('cmVnaXN0ZXJUeXBl'));
    }

    onActivityExpire() {
        let dateNow = Date.now()
        if (dateNow > this.eventInfo["activityRegisTo"]) {
            this.activityExpire = true
            return true;
        } else {
            return false;
        }
    }

    onSubmit() {
        this.submitted = true
        this.idSubmitted = true

        if (this.form.invalid) {
            return
        }
    }
    get f() { return this.form.controls }

    ngAfterViewInit() {
        // this.inputFiles.forEach( item => console.log('ITEM: ', item));
    }

    createForm(id: string = '') {
        if (this.isRequirePaymentDetail()) {
            this.form = this.formBuilder.group({
                id: [id, [Validators.required, Validators.minLength(13), Validators.maxLength(13)]],
                repeat: '',
                registerId: '',
                registerType: '1',
                attendanceResult: '',
                dateCreate: '',
                dateExpire: '',
                title: ['', [Validators.required]],
                name: ['', [Validators.required]],
                surname: ['', [Validators.required]],
                gender: ['', [Validators.required]],
                age: '',
                adrCode: '',
                adrMoo: '',
                adrBranch: '',
                adrRoad: '',
                adrSubDistrict: '',
                adrDistrict: '',
                adrProvince: '',
                adrPostCode: '',
                mobile: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(12)]], // , Validators.pattern('[0-9]*')
                email: ['', [Validators.email]],
                line: '',
                recommendAgentCode: ['', [Validators.required]],
                thaiFullName: '',
                agentCateCodeSales: '',
                agentDetailAgentCode: '',
                agentDetailThisFullName: '',
                agentDetailAgentCateCodeSales: '',
                agentDetailLicenseNumber: '',
                agentDetailDateOfLicenseExpiration: '',
                pictureLocal: '',
                payinLocal: '',
                idcardLocal: '',
                bankcode: ['', [Validators.required]],
                payInNumber: ['', []],
                payInAmount: [0, [Validators.required]],
                payinDate: '',
                payInTime: '',
                payInRef: ''
            })
        } else {
            console.log('SET FORM BUILDING WITHOUT RequirePayment!');
            this.form = this.formBuilder.group({
                id: [id, [Validators.required, Validators.minLength(13), Validators.maxLength(13)]],
                repeat: '',
                registerId: '',
                registerType: '1',
                attendanceResult: '',
                dateCreate: '',
                dateExpire: '',
                title: ['', [Validators.required]],
                name: ['', [Validators.required]],
                surname: ['', [Validators.required]],
                gender: ['', [Validators.required]],
                age: '',
                adrCode: '',
                adrMoo: '',
                adrBranch: '',
                adrRoad: '',
                adrSubDistrict: '',
                adrDistrict: '',
                adrProvince: '',
                adrPostCode: '',
                mobile: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]], //, Validators.pattern('[0-9]*')
                email: ['', [Validators.email]],
                line: '',
                recommendAgentCode: ['', [Validators.required]],
                thaiFullName: '',
                agentCateCodeSales: '',
                agentDetailAgentCode: '',
                agentDetailThisFullName: '',
                agentDetailAgentCateCodeSales: '',
                agentDetailLicenseNumber: '',
                agentDetailDateOfLicenseExpiration: '',
                payinLocal: '',
                pictureLocal: '',
                idcardLocal: '',
                bankcode: '',
                payInNumber: '',
                payInAmount: 0,
                payinDate: '',
                payInTime: '',
                payInRef: ''
            })
        }

        if (this.isRequireDob()) {
            this.form.addControl('dob', new FormControl('', [Validators.required]));
        } else {
            this.form.addControl('dob', new FormControl(''));
        }
        if (this.isShowEducation) {
            this.form.addControl('educationLevel', new FormControl(undefined, [Validators.required]));
        } else {
            this.form.addControl('educationLevel', new FormControl(undefined, []));
        }
        this.form.addControl('recommendLicenseNumber', new FormControl(''));
        this.form.addControl('recommendDateOfLicenseExpiration', new FormControl(''));

        this.paidUrl = undefined;
        this.idUrl = undefined;
        this.imageIdName = "";
        this.imagePaidName = "";
        if (this.inputFiles) {
            this.inputFiles.forEach( item => {
                console.log(item.nativeElement.value);
                item.nativeElement.value = "";
            });
        }
    }

    // apiService
    async apiGetActivityInfo() {
        this.spinner.show();
        const _postData = {
            'activityId': this.eventCode
        };
        let dataRes = await this.webAgentService.getActivityInfoById(_postData).toPromise();
        console.log('res. getActivityInfoById: ', dataRes);
        if (dataRes) {
            this.eventInfo = dataRes;
            this.onActivityExpire();
            this.isDataAvailable = true;
        }
        this.spinner.hide();
        // this.webAgentService.getActivityInfoById(_postData).subscribe(
        //     response => {
        //         console.log('res. getActivityInfoById: ', response);
        //         this.eventInfo = response;
        //         this.onActivityExpire();
        //         this.isDataAvailable = true;
        //         this.spinner.hide();
        //     },
        //     error => {
        //         this.spinner.hide();
        //     }
        // );
    }



    apiGetAgentDetailDirect(recommendAgentCode: string = '') {


        if (recommendAgentCode) {
            this.spinner.show();

            const _postData = { 'agentCode': recommendAgentCode };
            const now = new Date();
            const year = new DatePipe("en-EN").transform(now, "yyyy");
            const md = new DatePipe("en-EN").transform(now, "MMdd");
            const sysDate = (year) + md;


            this.webAgentService.getAgentDetail(_postData).subscribe(
                response => {
                    if (!_postData) {
                        return;
                    }

                    this.recommendAgentDetail = response;
                    this.form.controls['recommendAgentCode'].setValue(this.recommendAgentDetail['agentCode']);
                    this.form.controls['thaiFullName'].setValue(this.recommendAgentDetail['thaiFullName']);
                    this.form.controls['agentCateCodeSales'].setValue(this.recommendAgentDetail['agentCateCodeSales']);
                    this.form.controls['recommendLicenseNumber'].setValue(this.recommendAgentDetail['licenseNumber']);
                    this.form.controls['recommendDateOfLicenseExpiration'].setValue(this.tranformTHDate(this.recommendAgentDetail['dateOfLicenseExpiration']));
                },
                error => {
                    //console.log('err. getAgentDetail: ', error);


                }
            ).add(() => {
                this.spinner.hide();
            });
        }
    }

    tranformTHDate(date) {
        if (!date) return null;
        return date.substring(6, 8) + '/' + date.substring(4, 6) + '/' + (+date.substring(0, 4) + 543);
    }

    apiGetAgentDetailByPid(contentResult, smartCardScan: any = undefined) {
        this.idSubmitted = true

        if (this.f.id.invalid) {
            return;
        }

        this.spinner.show();
        const _postData = {
            'pid': this.form.value['id'],
            'activityId': this.eventInfo['activityId'],
            'activityType': this.eventInfo['activityType']
        };
        // 'agentLogin' : this.authService.getLoginedName()
        this.webAgentService.getAgentDetailByPid(_postData).subscribe(
            response => {
                console.log(response);
                if (response['agentRegis']['registerId'] &&
                    (
                        response['agentRegis']['insertBy'] != this.authService.getLoginedName()
                        && response['agentRegis']['recommenderCode'] != this.authService.getLoginedName()
                        && !this.authService.isRegisterAdmin()
                    )
                ) {
                    Swal.fire('', this.translate.instant('web.agent.record.no.permission'), 'warning');
                    this.createForm();
                    return;
                }

                // if (response['agentRegis']?.delIndi == 'Y') {
                //     Swal.fire('', this.translate.instant('web.agent.record.isdelete'), 'warning');
                //     this.disableRegisPid = true;
                //     return;
                // }


                // SET Agent's Register Detail
                if (response && Object.keys(response['agentDetail']).length > 0) {
                    this.agentDetail = response['agentDetail'];

                    this.form.controls['title'].setValue(this.agentDetail.thaiTitleName);
                    this.form.controls['name'].setValue(this.agentDetail.thaiName);
                    this.form.controls['surname'].setValue(this.agentDetail.thaiSurname);
                    this.form.controls['gender'].setValue(this.agentDetail.sexDivisionCode);
                    if (this.agentDetail.dateOfLicenseRenewal) {
                        this.form.controls['dateCreate'].setValue(new DatePipe('en-US').transform(this.convertStrDate2Date(this.agentDetail.dateOfLicenseRenewal), 'yyyy-MM-dd'));
                    }
                    if (this.agentDetail.dateExpire) {
                        this.form.controls['dateExpire'].setValue(new DatePipe('en-US').transform(this.convertStrDate2Date(this.agentDetail.dateExpire), 'yyyy-MM-dd'));
                    }
                    try {
                        this.form.controls['dob'].setValue(new DatePipe('en-US').transform(this.agentDetail.dateOfBirth, 'yyyy-MM-dd'));
                    } catch (e) {
                        this.form.controls['dob'].setValue('');
                    }
                    // Calculate AGE by divide 
                    let dateNow = new Date();
                    let tempDob = new Date(this.agentDetail.dateOfBirth);
                    let tempAge = dateNow.getFullYear() - tempDob.getFullYear();
                    this.form.controls['age'].setValue(tempAge);
                    this.form.controls['adrCode'].setValue(this.agentDetail.houseNo);
                    this.form.controls['adrMoo'].setValue(this.agentDetail.moo);
                    this.form.controls['adrBranch'].setValue(this.agentDetail.soi);
                    this.form.controls['adrRoad'].setValue(this.agentDetail.road);
                    this.form.controls['adrSubDistrict'].setValue(this.agentDetail.subDistrict);
                    this.form.controls['adrDistrict'].setValue(this.agentDetail.district);
                    this.form.controls['adrProvince'].setValue(this.agentDetail.province);
                    this.form.controls['adrPostCode'].setValue(this.agentDetail.zipcode);
                    this.form.controls['mobile'].setValue(this.agentDetail.producerPhoneNo);
                    this.form.controls['email'].setValue(this.agentDetail.agentEmail);
                    this.form.controls['line'].setValue(this.agentDetail.agentLineId);
                    // SET AgentDetail
                
                    this.form.controls['agentDetailAgentCode'].setValue(this.agentDetail.agentCode);
                    this.form.controls['agentDetailThisFullName'].setValue(this.agentDetail.thaiFullName);
                    this.form.controls['agentDetailAgentCateCodeSales'].setValue(this.agentDetail.agentCateCodeSales);
                    this.form.controls['agentDetailLicenseNumber'].setValue(this.agentDetail.licenseNumber);
                    this.form.controls['agentDetailDateOfLicenseExpiration'].setValue(new DatePipe('en-US').transform(this.convertStrDate2Date(this.agentDetail.dateOfLicenseExpiration), 'yyyy-MM-dd'));


                    this.apiGetAgentDetailDirect(this.agentDetail.agentCode);
                } else {
                    this.agentDetail = {
                        'agentCode': '',
                        'thaiFullName': '',
                        'agentCateCodeSales': '',
                        'licenseNumber': '',
                        'dateOfLicenseExpiration': ''
                    }
                    this.createForm(this.form.value['id']);
                }

                // Check Agent's Repeat Activity
                if (response && Object.keys(response['repeat']).length > 0) {
                    this.form.controls['repeat'].setValue(response['repeat']['repeat']);
                }

                // Set Information of Agent's Register
                if (response && Object.keys(response['agentRegis']).length > 0) {
                    this.agentRegis = response['agentRegis']
                    this.form.controls['registerId'].setValue(response['agentRegis']['registerId']);
                    this.form.controls['attendanceResult'].setValue(response['agentRegis']['attendanceResult']);
                    this.form.controls['registerType'].setValue(response['agentRegis']['registerType']);
                    this.form.controls['idcardLocal'].setValue(this.agentRegis.idcardLocal);
                    this.form.controls['title'].setValue(this.agentRegis.title);
                    this.form.controls['name'].setValue(this.agentRegis.name);
                    this.form.controls['surname'].setValue(this.agentRegis.surname);
                    this.form.controls['gender'].setValue(this.agentRegis.gender);
                    try {
                        this.form.controls['dob'].setValue(new DatePipe('en-US').transform(this.agentRegis.dob, 'yyyy-MM-dd'));
                    } catch (e) {
                        this.form.controls['dob'].setValue('');
                    }


                    this.form.controls['educationLevel'].setValue(this.agentRegis.educationLevel);
                    this.form.controls['email'].setValue(this.agentRegis.email);
                    this.form.controls['line'].setValue(this.agentRegis.line);
                    this.form.controls['mobile'].setValue(this.agentRegis.mobile);
                    if (this.agentRegis.dateCreate) {
                        this.form.controls['dateCreate'].setValue(new DatePipe('en-US').transform(this.convertStrDate2Date(this.agentRegis.dateCreate), 'yyyy-MM-dd'));
                    }
                    if (this.agentRegis.dateExpire) {
                        this.form.controls['dateExpire'].setValue(new DatePipe('en-US').transform(this.convertStrDate2Date(this.agentRegis.dateExpire), 'yyyy-MM-dd'));
                    }


                    let dateNow = new Date();
                    let tempDob = new Date(this.agentRegis.dob);
                    let tempAge = dateNow.getFullYear() - tempDob.getFullYear();
                    this.form.controls['age'].setValue(tempAge);

                    this.form.controls['adrCode'].setValue(this.agentRegis.buildingNumber);
                    this.form.controls['adrMoo'].setValue(this.agentRegis.moo);
                    this.form.controls['adrBranch'].setValue(this.agentRegis.soiName);
                    this.form.controls['adrRoad'].setValue(this.agentRegis.roadName);
                    this.form.controls['adrSubDistrict'].setValue(this.agentRegis.address2);
                    this.form.controls['adrDistrict'].setValue(this.agentRegis.address3);
                    this.form.controls['adrProvince'].setValue(this.agentRegis.address4);
                    this.form.controls['adrPostCode'].setValue(this.agentRegis.zip);

                    this.form.controls['recommendAgentCode'].setValue(this.agentRegis.recommenderCode);

                    this.form.controls['bankcode'].setValue(response['agentRegis']['bankCode']);
                    this.form.controls['payInNumber'].setValue(response['agentRegis']['payinNum']);
                    this.form.controls['payInAmount'].setValue(response['agentRegis']['payinAmount']);

                    try {
                        this.form.controls['payinDate'].setValue(new DatePipe('en-US').transform(this.agentRegis.payinDate, 'yyyy-MM-dd'));
                    } catch (e) {
                        this.form.controls['payinDate'].setValue('');
                    }

                    this.form.controls['payInTime'].setValue(response['agentRegis']['payinTime']);
                    this.form.controls['payInRef'].setValue(response['agentRegis']['payinRef']);

                    // prefixImagePath
                    this.form.controls['pictureLocal'].setValue(response['agentRegis']['pictureLocal']);
                    this.form.controls['payinLocal'].setValue(response['agentRegis']['payinLocal']);
                    this.idUrl = this.prefixImagePath + response['agentRegis']['pictureLocal'];
                    this.paidUrl = this.prefixImagePath + response['agentRegis']['payinLocal'];


                    if (response['agentRegis']['registerId']) {
                        this.apiGetAgentDetailDirect(response['agentRegis']['recommenderCode']);
                    }


                } else {
                    this.form.controls['registerId'].reset();
                    this.form.controls['attendanceResult'].reset();
                    this.form.controls['pictureLocal'].reset();
                    this.form.controls['payinLocal'].reset();
                    this.idUrl = undefined;
                    this.paidUrl = undefined;
                }


                //  Check Already Register
                if (response && Object.keys(response['alreadyRegis']).length > 0) {
                    if (response['alreadyRegis']['repeat'] == 'Y' && !this.isAdminRegister()) {
                        this.disableRegisPid = true;
                        this.modalStatus = 'fail';
                        this.msgFail = 'เลขที่บัตรประชาชนดังกล่าวถูกใช้สมัครในกิจกรรมนี้แล้ว ต้องการแก้ไขรายละเอียดรายละเอียดการสมัคร';

                        Swal.fire({
                            title: '',
                            text: this.msgFail,
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'ตกลง',
                            cancelButtonText: 'ยกเลิก',
                        }).then((result) => {

                            if (result.isConfirmed && response['agentRegis']['insertBy'] == this.authService.getLoginedName()) {
                                this.disableRegisPid = false;
                            }

                        });

                    } else {
                        this.disableRegisPid = false;
                    }
                }


                if (this.agentRegis.agentCode && !response['agentRegis']['recommenderCode']) {
                    this.apiGetAgentDetailDirect(this.agentRegis.agentCode);
                }
            },
            error => {
                // console.log('err. getAgentDetailByPid: ', error);
            }
        ).add(() => {
            this.mapSmartCard(smartCardScan);
            this.spinner.hide();
        });
    }

    
    async apiUploadImage() {



        if (this.imageId ) {

            const _postData = {
                'fileBased64': this.idUrl,
                'fileType': this.imageId['type'],
                'fileName': this.imageId['name'],
                'fileSize': this.imageId['size'],
            };
            let response = await this.webAgentService.uploadImage(_postData).toPromise();

            // console.log("response", response);
            if (response) {
                //console.log('res. uploadImage - ID: ', response);
                if (response && response.toString()) {
                    let _x = response.toString();
                    let x = _x.split('data/');

                    this.form.controls['pictureLocal'].setValue(x[1]);
                    // //console.log('SET IDCARD LOCAL');
                }
            }
        }
        if (this.imagePaid) {
            this.spinner.show();
            const _postData = {
                'fileBased64': this.paidUrl,
                'fileType': this.imagePaid['type'],
                'fileName': this.imagePaid['name'],
                'fileSize': this.imagePaid['size'],
            };
            let response = await this.webAgentService.uploadImage(_postData).toPromise();
            if (response) {
                if (response && response.toString()) {
                    let _x = response.toString();
                    let x = _x.split('data/');
                    this.form.controls['payinLocal'].setValue(x[1]);

                }
            }
        }
    }
    apiGetDropDownChoice() {
        this.webAgentService.getBankMaster().subscribe(r => {
            // //console.log('res. getBankMaster: ', r);
            this.dropdownBankCode = r
        });
        this.webAgentService.getGenderMaster().subscribe(r => {
            // //console.log('res. getGenderMaster: ', r);
            this.dropdownGender = r
        });
        this.webAgentService.getTitleMaster().subscribe(r => {
            // //console.log('res. getTitleMaster: ', r);
            this.dropdownTitle = r
        });

        this.webAgentService.getEducationMaster().subscribe(r => {
            // //console.log('res. getTitleMaster: ', r);
            this.dropdownEducation = r
        });

    }
    apiCheckPayinNumAlreadyInUse(contentResult) {
        this.payinSubmitted = true;

        if (this.f.payInNumber.invalid) {
            return;
        }

        this.spinner.show();
        const _postData = {
            'payinNum': this.form.value['payInNumber'],
            'activityId': this.eventInfo['activityId'],
            'registerId': this.form.value['registerId'],
            'isAdmin': this.authService.isRegisterAdmin() ? 'Y' : 'N'
        };

        this.webAgentService.checkPayinNumber(_postData).subscribe(
            response => {
                //console.log('res. checkPayinNumber: ', response);
                if (response['repeat'] == 'Y') {
                    this.disableRegisPayin = true;
                    this.modalStatus = 'fail';
                    this.msgFail = 'หมายเลข Pay-in Number ถูกใช้งานแล้ว';
                    // this.modalService.open(contentResult, { centered: true, windowClass: "modal-status" });
                    Swal.fire('', this.msgFail, 'error');
                } else {
                    this.disableRegisPayin = false;
                }
            },
            error => {
                //console.log('err. checkPayinNumber: ', error);
            }
        ).add(() => {
            this.spinner.hide();
        });
    }

    // Handler button action
    async onSubmitRegister(contentResult) {
   
        await this.apiGetActivityInfo();
        if (!this.onActivityExpire() || this.authService.isRegisterAdmin() ) {
            this.submitted = true
            this.idSubmitted = true

            this.requirePictureLocal = (this.form.value['pictureLocal'] == '' || this.form.value['pictureLocal'] == undefined) && (this.idUrl == undefined || this.idUrl == '');
            this.requirePayinLocal = (this.form.value['payinLocal'] == '' || this.form.value['payinLocal'] == undefined) && (this.paidUrl == undefined || this.paidUrl == '');

            if (this.requirePayinLocal && this.requirePictureLocal && this.isShowImageAttach()) {
                return;
            }

            if (this.form.invalid) {
                return;
            }

            this.spinner.show()


            let _dob = null, _payinDate = null, dateExpire = null;
            if (this.form.value['dob']) {
                _dob = this.datePipe.transform(this.form.value['dob'], 'dd-MMM-yyyy').toUpperCase()
            }
            if (this.form.value['payinDate']) {
                _payinDate = this.datePipe.transform(this.form.value['payinDate'], 'dd-MMM-yyyy').toUpperCase()
            }
            if (this.form.value['dateExpire']) {
                dateExpire = this.datePipe.transform(this.form.value['dateExpire'], 'dd-MMM-yyyy').toUpperCase()
            }

            var _postData = {
                'registerId' : this.form.value['registerId'],
                'registerType': this.form.value['registerType'],
                'activityId': this.eventInfo['activityId'],
                'id': this.form.value['id'],
                'title': this.form.value['title'],
                'name': this.form.value['name'],
                'surname': this.form.value['surname'],
                'gender': this.form.value['gender'],
                'dob': _dob,
                'age': this.form.value['age'],
                'address1': this.isNullResEmpty(this.form.value['adrCode']) + '|' + this.isNullResEmpty(this.form.value['adrMoo']) + '|' +
                    this.isNullResEmpty(this.form.value['adrBranch']) + '|' + this.isNullResEmpty(this.form.value['adrRoad']),
                'address2': this.form.value['adrSubDistrict'],
                'address3': this.form.value['adrDistrict'],
                'address4': this.form.value['adrProvince'],
                'address5': null,
                'zip': this.form.value['adrPostCode'],
                'mobile': this.form.value['mobile'],
                'email': this.form.value['email'],
                'line': this.form.value['line'],
                'recommenderCode': this.form.value['recommendAgentCode'],
                'dateExpire': this.form.value['dateExpire'],
                'dateCreate': this.form.value['dateCreate'],
                'idcardLocal': this.form.value['idcardLocal'],
                'pictureLocal': this.form.value['pictureLocal'],
                'payinLocal': this.form.value['payinLocal'],
                'updateBy': this.agentCode,
                'agentCode': this.form.value['agentDetailAgentCode'],
                'bankCode': this.form.value['bankcode'],
                'payinNum': this.form.value['payInNumber'],
                'payinAmount': this.form.value['payInAmount'],
                'payinDate': _payinDate,
                'payinTime': this.form.value['payInTime'],
                'payinRef': this.form.value['payInRef'],
                'attendanceResult': this.ATTENDANCE,
                'educationLevel': this.form.value['educationLevel'],
            };


            if (this.imageId ) {
                _postData['filePictureLocal'] =  {
                                    'fileBased64': this.idUrl,
                                    'fileType': this.imageId['type'],
                                    'fileName': this.imageId['name'],
                                    'fileSize': this.imageId['size'],
                                };
            }
            if (this.imagePaid) {
                _postData['filePayinLocal'] = {
                                    'fileBased64': this.paidUrl,
                                    'fileType': this.imagePaid['type'],
                                    'fileName': this.imagePaid['name'],
                                    'fileSize': this.imagePaid['size'],
                                };
            }


            // Update and change attendanceStatus as Y
            if ((this.form.value['registerId'] != '' && this.form.value['registerId'])) {
                this.exitToPath = this.authService.getWebRole();

               
                // UPDATE
                this.webAgentService.eRegisterUpdateAgent(_postData).subscribe(
                    response => {
                        // console.log('res. eRegisterUpdateAgent: ', response);
                        if (response['message'] == 'success') {

                            this.modalStatus = 'success';

                            if (this.ATTENDANCE == 'Y') {
                                this.createForm();
                            }

                            this.submitted = false;
                        } else if (response['message'] == 'full') {
                            this.msgFail = 'ไม่สามารถสมัครได้ เนื่องจากมีผู้สมัครครบจำนวนแล้ว';
                            this.modalStatus = 'fail';
                        } else {
                            this.msgFail = 'เกิดข้อผิดพลาดในการบันทึกข้อมูล';
                            this.modalStatus = 'fail';
                        }
                    },
                    error => {
                        //console.log('err. eRegisterUpdateAgent: ', error);
                        this.msgFail = 'เกิดข้อผิดพลาดในการบันทึกข้อมูล';
                        this.modalStatus = 'fail';
                    }
                ).add(() => {
                    this.spinner.hide();
                    this.modalService.open(contentResult, this.modalOption);

                });
            } else {
                
                // Insert registeration
                _postData['insertBy'] = this.agentCode;
                this.exitToPath = 'REGISTER_AGENT';

                if( this.authService.isRegisterAdmin()){
                    _postData['registerType'] = '1';
                }
                if(this.ATTENDANCE == 'Y'){
                    _postData['registerType'] = '2';
                }
               
                // INSERT
                this.disabledBtn = true;
                this.webAgentService.eRegisterActivity(_postData).subscribe(
                    response => {
                        // console.log('res. eRegisterActivity: ', response);
                        if (response['message'] == 'success') {
                            this.msgSuccess = `ลงทะเบียนสำเร็จ (ลำดับที่ ${this.form.value['registerId']||response['registerId'] })`
                            this.modalStatus = 'success';
                            if (this.authService.isRegisterAdmin()) {
                                this.exitToPath = 'REGISTER_ADMIN';
                                this.createForm();
                                this.submitted = false;
                            }
                        } else if (response['message'] == 'full') {
                            this.msgFail = 'ไม่สามารถสมัครได้ เนื่องจากมีผู้สมัครครบจำนวนแล้ว';
                            this.modalStatus = 'fail';
                        } else {
                            this.msgFail = 'เกิดข้อผิดพลาดในการบันทึกข้อมูล';
                            this.modalStatus = 'fail';
                        }
                    },
                    error => {
                        this.msgFail = 'เกิดข้อผิดพลาดในการบันทึกข้อมูล';
                        this.modalStatus = 'fail';
                    }
                ).add(() => {
                    this.spinner.hide();
                    this.modalService.open(contentResult, this.modalOption);
                    this.disabledBtn = false;
                });
            }
        } else {
            this.msgFail = 'ไม่สามารถสมัครได้ เนื่องจากปิดรับสมัคร';
            this.modalStatus = 'fail';
            this.modalService.open(contentResult, this.modalOption );
        }
    }


    // Utils
    calculateFormAge() {
        let dateNow = new Date()
        try {
            this.form.value.age = dateNow.getFullYear() - this.form.value.dob.getFullYear()
        } catch (e) {
            this.form.value.age = undefined;
        }

    }
    isNullResEmpty(text: string) {
        if (text) {
            return text;
        } else {
            return '';
        }
    }
    convertStrDate2Date(dateString: string) {
        if (!dateString) return new Date();

        var year = dateString.substring(0, 4);
        var month = dateString.substring(4, 6);
        var day = dateString.substring(6, 8);
        var date = new Date(Number.parseInt(year), Number.parseInt(month) - 1, Number.parseInt(day));
        return date;
    }
    reformatDateTh(val = '') {
        if (!val) return null;
        let _date;
        _date = new DatePipe('en-US').transform(val, 'dd/MM/yyyy')
        _date = _date.substring(0, 6)
            + (Number.parseInt(_date.substring(6, 10)) + 543).toString()
        // //console.log("payInDateChange", this.form.value.payInDate);
        // //console.log("showPayInDate", _date);
        return _date;
    }

    // Preview image
    previewImage(files, type) {
        if (files.length === 0)
            return;

        let fileType = files[0].type;
        if (fileType.match(/image\/*/) == null) {
            if (type == 'ID')
                this.msgImageId = "ระบบไม่รองรับไฟล์นามสกุลที่เลือก";
            else if (type == 'PAID')
                this.msgImagePaid = "ระบบไม่รองรับไฟล์นามสกุลที่เลือก";
            return;
        }

        let reader = new FileReader();
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
            if (type == 'ID') {
                this.imageId = files[0];
                this.idUrl = reader.result;
                this.msgImageId = '';
                this.imageIdName = files[0].name;
                // //console.log("imageId: ", this.imageId);
                // //console.log("idUrl: ", this.idUrl);
                // //console.log("imageIdName: ", this.imageIdName);
            }
            else if (type == 'PAID') {
                this.imagePaid = files[0];
                this.paidUrl = reader.result;
                this.msgImagePaid = '';
                this.imagePaidName = files[0].name;
            }
        }
    }

    // Condition Form Dynamic
    get isInfoOfAdmin() {

        if (['3'].includes(this.eventType) || this.isAdminRegister()) return true;
        else return false;
    }
    isRequirePaymentDetail() {
        const _eventTypeList = ['2', '3', '5'];
        if (_eventTypeList.includes(this.eventType)) return true;
        else return false;
    }
    isRequireDob() {
        return ['1', '2', '3', '5'].includes(this.eventType);
    }
    isReadonlyAgentCode() {
        if (this.eventType != '2') return true;
        else return false;
    }

    isShowRepeatForm() {
        return ['4', '6'].includes(this.eventType);
    }

    get isShowEducation() {
        return ['3'].includes(this.eventType);
    }

    isShowImageAttach() {
        const _eventTypeList = ['2', '3', '5'];
        if (_eventTypeList.includes(this.eventType)) return true;
        else return false;
    }
    isAdminRegister() {
        return this.authService.isRegisterAdmin() && this.eventInfo['isRegister'] == 'true';
    }

    smartCardScan() {
        fetch(this.smartCardUrl, {
            method: 'get'
        })
            .then(response => response.json())
            .then(data => {

                if (data && (Object.keys(data).length === 0)) {
                    Swal.fire(
                        '',
                        this.translate.instant('web.agent.invalid-smartcard'),
                        'warning'
                    );
                    return;
                } else {
                    this.form.controls['id'].setValue(data['cid']);
                    this.apiGetAgentDetailByPid(undefined, data);
                }
            }).catch(error => {
                Swal.fire(
                    '',
                    this.translate.instant('web.agent.error-smartcard'),
                    'warning'
                );
            }).finally(() => {

            });
    }

    mapSmartCard(data: any = undefined) {

        if (!data) return;

        const dboDt = this.convertDob(data['dob'])
        this.form.controls['title'].setValue(data['prename']);
        this.form.controls['name'].setValue(data['fname']);
        this.form.controls['surname'].setValue(data['lname']);
        this.form.controls['adrCode'].setValue(data['address_no']);
        this.form.controls['adrMoo'].setValue(data['address_moo']);
        this.form.controls['adrBranch'].setValue(data['address_road']);
        this.form.controls['adrRoad'].setValue(data['address_soi']);
        this.form.controls['adrSubDistrict'].setValue(data['address_tambol']);
        this.form.controls['adrDistrict'].setValue(data['address_amphur']);
        this.form.controls['adrProvince'].setValue(data['address_province']);
        this.form.controls['dob'].setValue(dboDt);
        if (dboDt) {
            const age = new Date().getFullYear() - dboDt.getFullYear();
            this.form.controls['age'].setValue(age);
        }
        if (data['cid']) {
            this.apiGetAgentDetailDirect(data['cid']);
        }
        if (data['gender'] == 1) {
            this.form.controls['gender'].setValue('M');
        } else if (data['gender'] == 0) {
            this.form.controls['gender'].setValue('F');
        }

    }

    convertDob(dbo) {

        //"25621213"
        if (!dbo) {
            return;
        }
        const YEAR = dbo.substr(0, 4);
        const MONTH = dbo.substr(4, 2);
        const DAY = dbo.substr(6, 2);

        // //console.log( "YEAR",  YEAR);
        // //console.log( "MONTH",  MONTH-1);
        // //console.log( "DAY",  DAY);
        let date: Date = new Date();
        date.setDate(DAY);
        date.setMonth(MONTH - 1);
        date.setFullYear(YEAR - 543);
        return date;
    }

    goBack() {
        this.location.back()
    }
}

