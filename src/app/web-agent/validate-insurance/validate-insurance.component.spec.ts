import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidateInsuranceComponent } from './validate-insurance.component';

describe('ValidateInsuranceComponent', () => {
  let component: ValidateInsuranceComponent;
  let fixture: ComponentFixture<ValidateInsuranceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidateInsuranceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidateInsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
