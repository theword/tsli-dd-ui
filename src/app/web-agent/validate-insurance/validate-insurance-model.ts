export class MockInsuranceData {
  tableHeader = [
    'ลำดับ'
    , 'เลขที่กรมธรรม์'
    , 'แบบประกัน'
    , 'ชื่อผู้เอาประกัน'
    , 'นามสกุล'
    , 'สถานะ'
    , 'วันที่สถานะ'
    , 'ตัวแทน'
    , 'หนังสือรับรองชำระเบี้ย'
  ];

  tableData = [
    {
      id: '1'
      , insCode: '012150026072'
      , insDesc: 'ซัมซุงซูเปอร์เซฟวิง 10/5'
      , cusFirstName: 'นัฐพร'
      , cusLastName: 'สมบูรณ์'
      , status: 'อยู่ระหว่างการพิจารณา (Withdrawn)'
      , statusDate: '30/09/2558'
      , agentName: 'ซัมซุง ประกันชีวิต (ประเทศไทย) จำกัด (มหาชน)(สำนักงานใหญ่) 221599999'
      , bookmark: ''
    },
    {
      id: '2'
      , insCode: '012150016663'
      , insDesc: 'ซัมซุงซูเปอร์เซฟวิง 10/5'
      , cusFirstName: 'จารุวรรณ'
      , cusLastName: 'ชะรารัมย์'
      , status: 'อยู่ระหว่างการพิจารณา (Withdrawn)'
      , statusDate: '30/06/2558'
      , agentName: 'ซัมซุง ประกันชีวิต (ประเทศไทย) จำกัด (มหาชน)(สำนักงานใหญ่) 221599999'
      , bookmark: ''
    },
    {
      id: '3'
      , insCode: '012150015257'
      , insDesc: 'ทรัพย์ยืนยง 90'
      , cusFirstName: 'อำพร'
      , cusLastName: 'ศรีชานิล'
      , status: 'อยู่ระหว่างการพิจารณา (Withdrawn)'
      , statusDate: '30/05/2559'
      , agentName: 'ซัมซุง ประกันชีวิต (ประเทศไทย) จำกัด (มหาชน)(สำนักงานใหญ่) 221599999'
      , bookmark: ''
    }
  ];
}