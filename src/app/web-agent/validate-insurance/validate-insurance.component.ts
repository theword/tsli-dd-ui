import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/core/security/auth.service';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import { MockInsuranceData } from './validate-insurance-model';

@Component({
    selector: 'app-validate-insurance',
    templateUrl: './validate-insurance.component.html',
    styleUrls: ['./validate-insurance.component.scss']
})
export class ValidateInsuranceComponent implements OnInit {

    // Declare Variable
    public modelValInsTable: MockInsuranceData;
    public searchStatus: Boolean = false;
    public tableData: any = [];

    public curPage: number = 0;
    public pagingItem: any = {
        pageSize: 0
        , totalPage: 0
        , totalElements: 0
    };

    private agentCode: string = this.authService.getLoginedName();
    private searchParams: any = {
        'policyCode': ''
        , 'policyId': ''
        , 'fName': ''
        , 'lName': ''
        , 'pid': ''
        , 'agentFilter': ''
        , 'requestInsuranceCode': ''
    };

    constructor(
        private router: Router
        , private spinner: NgxSpinnerService
        , private authService: AuthService
        , private webAgentService: WebAgentService
    ) {
        this.modelValInsTable = new MockInsuranceData();
    }

    ngOnInit(): void {
        // this.apiGetInsuranceList();
    }

    // apiService Controller
    apiGetInsuranceList() {
        this.spinner.show();

        const _postData = {
            'agentCode': this.agentCode
            , 'policyCode': this.searchParams['policyCode']
            , 'policyId': this.searchParams['policyId']
            , 'fName': this.searchParams['fName']
            , 'lName': this.searchParams['lName']
            , 'pid': this.searchParams['pid']
            , 'curPage': this.curPage
            , 'agentFilter': this.searchParams['agentFilter']
            , 'requestInsuranceCode': undefined
        };
        // console.log('POST: ', _postData);

        this.webAgentService.searchInsurance(_postData).subscribe(
            response => {
                // console.log('_postData : ', response);
                this.searchStatus = true;
                this.tableData = response['detail'];
                this.pagingItem = {
                    pageSize: response['pageSize'],
                    totalPage: response['totalPage'],
                    totalElements: response['totalElements'],
                };
            },
            error => {
                console.log('err. searchInsurance: ', error);
            }
        ).add( () => {
            this.spinner.hide();
        });
    }

    // Handle child component emit
    handlerSearchValidateInsurance(searchParam) {
        this.curPage = 0;
        this.searchParams = {
            'policyCode': searchParam.insCode
            , 'policyId': searchParam.insRequestCode
            , 'fName': searchParam.insFirstName
            , 'lName': searchParam.insLastName
            , 'pid': searchParam.insPersonalId
            , 'agentFilter': searchParam['agentFilter']
            , 'requestInsuranceCode' : searchParam.requestInsuranceCode
        };
        this.apiGetInsuranceList();
    }
    handlerPaging(event) {
        this.curPage = event;
        this.apiGetInsuranceList();
    }

    // Navigation Controller
    handlerInsuranceSelect(emitData) {
        const path = '/web-agent/detail-insurance';
        const queryParams = [
            {
                'insuranceCode': emitData['policyCode'],
                'insuranceId': emitData['policyId']
            },
        ];
        this.router.navigate([path], {
            queryParams: {
                params: JSON.stringify(queryParams),
            },
        });
    }

}
