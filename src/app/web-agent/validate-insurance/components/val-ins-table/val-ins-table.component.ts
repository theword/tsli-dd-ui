import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-val-ins-table',
    templateUrl: './val-ins-table.component.html',
    styleUrls: ['./val-ins-table.component.scss']
})
export class ValInsTableComponent implements OnInit {

    // Declare Variables
    @Input() headers: any[] = [];
    @Input() data: any[] = [];

    @Input() pagingCount: number = 0;
    @Input() pagingItem: { pageSize: 0, totalPage: 0, totalElements: 0 };

    @Output() selectInsCode = new EventEmitter();
    @Output() clickPaging = new EventEmitter<any>();

    constructor(
        private translate: TranslateService,
        private webAgentService: WebAgentService
    ) { }

    ngOnInit(): void {
    }
    // HANDLER ACTION
    onClickInsCode(policyCode, policyId) {
        let _emitData = {
            'policyCode': policyCode,
            'policyId': policyId
        }
        this.selectInsCode.emit(_emitData);
    }
    onDownloadFile(policyCode) {
        this.webAgentService.downloadDocument( policyCode ).subscribe(response => {
  
            if(response['size'] == 0){
                Swal.fire(   '',   this.translate.instant('web.agent.file.notfound'),  'warning'  );
                return ;
            }
              const blob = new Blob([response], { type: 'application/octet-stream' });
              let downloadLink = document.createElement('a');
              downloadLink.href = window.URL.createObjectURL(blob);
              downloadLink.setAttribute('download', `${policyCode}` + '.pdf');
              document.body.appendChild(downloadLink);
              downloadLink.click();
              
              document.body.removeChild(downloadLink);
            }), error => console.log('Error downloading the file'),
                     () => console.info('File downloaded successfully');
    
    }

    canExportFile( file: string ){
        file = file?.replace(" ", "");
        return( file && file != '' );
    }

    // PAGINATION
    itemInfo(): string {
        const start = this.printSequense();
        const end = start + this.pagingItem.pageSize - 1;
        return 'Page ' + (this.pagingCount + 1) + ' : ' + start + '-'
            + (end > this.pagingItem?.totalElements ? this.pagingItem?.totalElements : end) + ' of ' + this.pagingItem?.totalElements;
    }
    printSequense() {
        return ((this.pagingCount) * this.pagingItem.pageSize) + 1;
    }
    paginationAction(type: string) {
    
        switch (type) {
            case 'firstPage':
                if (this.pagingCount == 0) return;
                else {
                    this.clickPaging.emit(0);
                }
                break;
            case 'previous':
                if (this.pagingCount >= 1) {
                    this.clickPaging.emit(this.pagingCount - 1);
                }
                break;
            case 'next':
              
                if (this.pagingCount < this.pagingItem.totalPage ) {
                    this.clickPaging.emit(this.pagingCount + 1);
                }
                else return;
                break;
            case 'endPage':
                if (this.pagingCount == this.pagingItem.totalPage) return;
                else {
                    this.clickPaging.emit(this.pagingItem.totalPage);
                }
                break;

            default:
                break;
        }
    }

}
