import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValInsTableComponent } from './val-ins-table.component';

describe('ValInsTableComponent', () => {
  let component: ValInsTableComponent;
  let fixture: ComponentFixture<ValInsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValInsTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValInsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
