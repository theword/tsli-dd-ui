import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValInsFormComponent } from './val-ins-form.component';

describe('ValInsFormComponent', () => {
  let component: ValInsFormComponent;
  let fixture: ComponentFixture<ValInsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValInsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValInsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
