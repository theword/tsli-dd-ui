import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/core/security/auth.service';
import { WebAgentService } from 'src/app/core/service/web-agent.service';

@Component({
  selector: 'app-val-ins-form',
  templateUrl: './val-ins-form.component.html',
  styleUrls: ['./val-ins-form.component.scss']
})
export class ValInsFormComponent implements OnInit {

  // Declare Variables
  @Output() onSearch = new EventEmitter();
  childAgent = [];
  filterInsuranceForm = {
    insCode: '',
    insRequestCode: '',
    insFirstName: '',
    insLastName: '',
    insPersonalId: '',
    agentFilter: '*',
    requestInsuranceCode: undefined
  };

  constructor( private webAgentService: WebAgentService, private authService: AuthService) { }

  ngOnInit(): void {
    const agentCode = this.authService.getLoginedName();
    this.webAgentService.getAgentChild(agentCode).subscribe( (res:any) =>{
      this.childAgent = res;
    });
  }

  onSearchValidateInsurance() {
    this.onSearch.emit(this.filterInsuranceForm);
  }

}
