import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterUploadResultComponent } from './register-upload-result.component';

describe('RegisterUploadResultComponent', () => {
  let component: RegisterUploadResultComponent;
  let fixture: ComponentFixture<RegisterUploadResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterUploadResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterUploadResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
