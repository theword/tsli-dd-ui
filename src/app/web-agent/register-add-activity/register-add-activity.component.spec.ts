import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterAddActivityComponent } from './register-add-activity.component';

describe('RegisterAddActivityComponent', () => {
  let component: RegisterAddActivityComponent;
  let fixture: ComponentFixture<RegisterAddActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterAddActivityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterAddActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
