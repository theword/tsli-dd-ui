import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/core/security/auth.service';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import { MockReportData } from './report-model';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  // Declare Variable
  public modelReportTable:any = null;
  public searchStatus: Boolean = false;
  tableHeader = new MockReportData().tableHeader;
  private params = { 'currentPage': 1, pageSize: 50 };

  constructor(
    private router: Router
    , private spinner: NgxSpinnerService
    , private webAgentService: WebAgentService
    , private authService : AuthService
  ) {
  
   }

  ngOnInit(): void {
    this.init();
  }

  init(){
    this.modelReportTable = {detail: []};
  }
  handlerPaggingSearch( pagging ){
    this.params.currentPage = pagging.currentPage;
    this.params.pageSize = pagging.pageSize;
    this.handlerSearchReport(null);
  }

  handlerSearchReport(searchParam) {
   this.spinner.show();
   if(searchParam){
    this.init();
      this.params = {
          ...searchParam
      };
    }
   this.params['agentCode'] = this.authService.getLoginedName();


    this.webAgentService.searchReport(this.params).subscribe( (r:any)=>{
      this.spinner.hide();
      if( this.searchStatus == false){
        this.modelReportTable = r;
      }else{
        this.modelReportTable.detail.push( ...r.detail );
        this.modelReportTable.pageSize = r.pageSize;
        this.modelReportTable.totalElements = r.totalElements;
        this.modelReportTable.totalPage =r.totalPage;
      }

      this.searchStatus = true;
      
    });
    

    //   setTimeout(() => {
  //     /** spinner ends after 2.8 seconds */
  //     this.searchStatus = true;
  //     this.spinner.hide();
  //   }, 1000);
  }

}
