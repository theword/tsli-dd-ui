import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { WebAgentService } from 'src/app/core/service/web-agent.service';

@Component({
  selector: 'app-report-filter',
  templateUrl: './report-filter.component.html',
  styleUrls: ['./report-filter.component.scss']
})
export class ReportFilterComponent implements OnInit {

  currentPage = 1;
  pageSize = 50;
  // Declare Variables
  @Output() searchReport = new EventEmitter();

  public filterReportForm = {
    closYm: '',
    reportCode: '',
    agentCode: '',
	  filterAgentCode:'',
    pageSize: 50,
    currentPage: 1
  };

  public closYm:any;
  public typeList:any;


  constructor(   private webAgentService: WebAgentService ) { }

  ngOnInit(): void {
    this.webAgentService.getReportClosYM().subscribe( r=>{
      this.closYm = r;
      this.filterReportForm.closYm = r[0]['code'];
    });
    this.webAgentService.getReportMaster().subscribe( r=>{
      this.typeList = r;
      this.filterReportForm.reportCode = r[0]['code']
    });
    
  }

  onSearchReport() {
    this.searchReport.emit(this.filterReportForm);
  }

}
