import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-report-table',
  templateUrl: './report-table.component.html',
  styleUrls: ['./report-table.component.scss']
})
export class ReportTableComponent implements OnInit {

  // Declare Variables
  @Input() headers: any[] = [];
  @Input() data: any = {};

  @Output() searchReport = new EventEmitter();

  // detail:[];
  pageSize= 50;
  currentPage=1;
  // totalPage:0;

  constructor(private webAgentService: WebAgentService,         private translate: TranslateService) { }

  ngOnInit(): void {
    // this.detail = this.data['detail'];
  }

	generateReportUrl( report ){

		this.webAgentService.downloadReportFile( report['reportPath'] ).subscribe(response => {
      if(response['size'] == 0){
        Swal.fire(   '',   this.translate.instant('web.agent.file.notfound'),  'warning'  );
        return ;
      }

      let element = report['reportPath'].split(".");
      let extension = element[element.length-1];

			const blob = new Blob([response], { type: 'application/octet-stream' });
			  let downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(blob);
          downloadLink.setAttribute('download', `${report['date']}-${report['reportName']}-${report['agentCode']}` + `.${extension}`);
          document.body.appendChild(downloadLink);
          downloadLink.click();
		  
		  document.body.removeChild(downloadLink);
		}), error => console.log('Error downloading the file'),
                 () => console.info('File downloaded successfully');
		
	}

  retriveReport() {
    this.searchReport.emit({ pageSize: this.pageSize, currentPage: ++this.currentPage});
  }

}
