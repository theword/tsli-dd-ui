export class MockReportData {
  tableHeader = [
    'ลำดับ'
    , 'วันที่'
    , 'ชื่อรายงาน'
    , 'VP/CM'
    , 'AL/FM/BK'
    , 'AGENT'
    , 'ตัวแทน'
  ];

  tableData = [
    {
      id: '1'
      , date: '08/02/2558'
      , reportPath: ''
      , reportName: 'รายงานกรมธรรม์รายเดือน'
      , avpCode: '0050025'
      , alCode: '005002500'
      , agentCode: '221599999'
      , agentName: 'ซัมซุง ประกันชีวิต (ประเทศไทย) จำกัด (มหาชน)(สำนักงานใหญ่)'
    },
  ];
}