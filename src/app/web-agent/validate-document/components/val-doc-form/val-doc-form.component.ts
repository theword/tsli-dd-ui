import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/core/security/auth.service';
import { WebAgentService } from 'src/app/core/service/web-agent.service';

@Component({
  selector: 'app-val-doc-form',
  templateUrl: './val-doc-form.component.html',
  styleUrls: ['./val-doc-form.component.scss']
})
export class ValDocFormComponent implements OnInit {

  // Declare Variables
  @Output() onSearchValDoc = new EventEmitter();

  childAgent = [];

  public filterInsuranceDocForm = {
    policyCode: '',
    fName: '',
    lName: '',
    docCategory: '',
    insPersonalId: '',
    agentFilter: '*'
  };

  public organizationList: any;

  constructor(
    private webAgentService: WebAgentService, 
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    // const agentCode = this.authService.getLoginedName();
    // this.webAgentService.getAgentChild(agentCode).subscribe( (res:any) =>{
    //   this.childAgent = res;
    // });

    this.apiGetListDocCategory();
    const agentCode = this.authService.getLoginedName();
    console.log("agentCode", agentCode);
    this.webAgentService.getAgentFilterSearchDocument(agentCode).subscribe( (res:any) =>{
      this.childAgent = res;
    });
  }

  onSearchValidateInsurance() {
    this.onSearchValDoc.emit(this.filterInsuranceDocForm);
  }

  apiGetListDocCategory() {
    this.webAgentService.getListDocCategory().subscribe(
      response => {
        this.organizationList = response;
        this.filterInsuranceDocForm.docCategory = response[0]['categoryId'];
      },
      error => {
        console.log('err. getListDocCategory: ', error);
      }
    );
  }

}
