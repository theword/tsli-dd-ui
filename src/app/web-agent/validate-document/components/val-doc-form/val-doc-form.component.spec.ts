import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValDocFormComponent } from './val-doc-form.component';

describe('ValDocFormComponent', () => {
  let component: ValDocFormComponent;
  let fixture: ComponentFixture<ValDocFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValDocFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValDocFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
