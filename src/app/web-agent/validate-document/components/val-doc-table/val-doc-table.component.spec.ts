import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValDocTableComponent } from './val-doc-table.component';

describe('ValDocTableComponent', () => {
  let component: ValDocTableComponent;
  let fixture: ComponentFixture<ValDocTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValDocTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValDocTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
