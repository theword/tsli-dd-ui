import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-val-doc-table',
    templateUrl: 'val-doc-table.component.html',
    styleUrls: ['val-doc-table.component.scss']
})
export class ValDocTableComponent implements OnInit {
    @ViewChild('downloadFile') tagDownloadFile: ElementRef;
    // Declare Variables
    @Input() headers: any[] = [];
    @Input() data: any[] = [];

    @Input() pagingCount: number = 0;
    @Input() pagingItem: { pageSize: 0, totalPage: 0, totalElements: 0 };

    @Output() selectInsCode = new EventEmitter();
    @Output() clickPaging = new EventEmitter<any>();

    pageSize = 25;
    constructor(
        private webAgentService: WebAgentService,
        private translate: TranslateService,
        private http: HttpClient
    ) { }

    ngOnInit(): void {
    }

    // HANDLE ACTION
    onClickInsCode(policyCode, policyId) {
        let _emitData = {
            'policyCode': policyCode,
            'policyId': policyId
        }
        this.selectInsCode.emit(_emitData);
    }
     onDownloadFile(filePath) {
        // console.log(`${environment.apiWebAgentUrl}/document/download?fileName=${filePath}`);
        let fileName = (""+filePath).replace("\\","/").replace("//","/");
    

        //  return `${environment.apiWebAgentUrl}/document/download?fileName=${fileName}`;
        // let fileName = (""+filePath).replace("\\","_").replace("/","_");
        this.webAgentService.downloadMemoDocument( fileName ).subscribe(response => {

          
            if(response['size'] == 0){
                Swal.fire(   '',   this.translate.instant('web.agent.file.notfound'),  'warning'  );
                return ;
            }

              const blob = new Blob([response], { type: 'application/octet-stream' });
              let downloadLink = document.createElement('a');
              downloadLink.href = window.URL.createObjectURL(blob);
              downloadLink.setAttribute('download', `${fileName}` );
              downloadLink.setAttribute('target', "_self" );
              document.body.appendChild(downloadLink);
              downloadLink.click();
              
              document.body.removeChild(downloadLink);
            }), error => console.log('Error downloading the file'),
                    () => console.info('File downloaded successfully');
    }

    // PAGINATION
    itemInfo(): string {
        const start = this.printSequense();
        const end = start + this.pagingItem.pageSize - 1;
        return 'Page ' + (this.pagingCount + 1) + ' : ' + start + '-'
            + (end > this.pagingItem?.totalElements ? this.pagingItem?.totalElements : end) + ' of ' + this.pagingItem?.totalElements;
    }
    printSequense() {
        return ((this.pagingCount) * this.pagingItem.pageSize) + 1;
    }
    paginationAction(type: string) {
        switch (type) {
            case 'firstPage':
                if (this.pagingCount == 0) return;
                else {
                    this.clickPaging.emit(0);
                }
                break;
            case 'previous':
                if (this.pagingCount >= 1) {
                    this.clickPaging.emit(this.pagingCount - 1);
                }
                break;
            case 'next':
                if (this.pagingCount < this.pagingItem.totalPage ) {
                    this.clickPaging.emit(this.pagingCount + 1);
                }
                else return;
                break;
            case 'endPage':
                if (this.pagingCount == this.pagingItem.totalPage) return;
                else {
                    this.clickPaging.emit(this.pagingItem.totalPage - 1);
                }
                break;

            default:
                break;
        }
    }

}
