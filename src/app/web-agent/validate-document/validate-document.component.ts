import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/core/security/auth.service';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import { MockInsuranceDocData } from './validate-insurance-doc-model';

@Component({
    selector: 'app-validate-document',
    templateUrl: './validate-document.component.html',
    styleUrls: ['./validate-document.component.scss']
})
export class ValidateDocumentComponent implements OnInit {

    // Declare Variable
    public modelValDocTable: MockInsuranceDocData;
    public tableData: any = [];
    public searchStatus: Boolean = false;

    agentCode: string = this.authService.getLoginedName();
    curPage: number = 0;
    public pagingItem: any = {
        pageSize: 0
        , totalPage: 0
        , totalElements: 0
    };
    searchParams: any = {
        'policyCode': ''
        , 'fName': ''
        , 'lName': ''
        , 'docCategory': ''
        , 'pid': ''
        , 'agentFilter': ''

    };

    // TEST ON AGENTCODE 121700011
    constructor(
        private router: Router
        , private spinner: NgxSpinnerService
        , private webAgentService: WebAgentService
        , private authService: AuthService
    ) {
        this.modelValDocTable = new MockInsuranceDocData();
    }

    ngOnInit(): void {
        // this.apiGetListDocCategory();
    }

    // API Controller
    apiGetDocumentList() {
        this.spinner.show();
        const _postData = {
            'agentCode': this.agentCode
            , 'policyCode': this.searchParams['policyCode']
            , 'fName': this.searchParams['fName']
            , 'lName': this.searchParams['lName']
            , 'docCategory': this.searchParams['docCategory']
            , 'curPage': this.curPage
            , agentFilter : this.searchParams['agentFilter']
			, 'pid': this.searchParams['pid']

        };

        console.log("_postData", _postData);
        

        this.webAgentService.searchDocument(_postData).subscribe(
            response => {
            
                this.searchStatus = true;
                this.tableData = response['detail'];
                this.pagingItem = {
                    pageSize: response['pageSize'],
                    totalPage: response['totalPage'],
                    totalElements: response['totalElements'],
                };
            },
            error => {
                console.log('err. searchDocument: ', error);
            }
        ).add( () => {
            this.spinner.hide();
        });
    }
    apiGetListDocCategory() {
        this.webAgentService.getListDocCategory().subscribe(
            response => {
                console.log('res. getListDocCategory: ', response);
                this.searchParams['docCategory'] = response[0]['categoryId'];
                this.apiGetDocumentList();
            },
            error => {
                console.log('err. getListDocCategory: ', error);
            }
        );
    }

    // Handler Controller
    handlerSearchValidateDocInsurance(searchParam) {
        this.searchParams = {
            'policyCode': searchParam.policyCode
            , 'fName': searchParam.fName
            , 'lName': searchParam.lName
            , 'docCategory': searchParam.docCategory
            , 'pid': searchParam.insPersonalId
            , 'agentFilter': searchParam.agentFilter
        };
        this.apiGetDocumentList();
    }

    handlerInsuranceSelect(emitData) {
        const path = '/web-agent/detail-insurance';
        const queryParams = [
            {
                'insuranceCode': emitData['policyCode'],
                'insuranceId': emitData['policyId']
            },
        ];
        this.router.navigate([path], {
            queryParams: {
                params: JSON.stringify(queryParams),
            },
        });
    }

    handlerPaging(event) {
        this.curPage = event;
        this.apiGetDocumentList();
    }

}
