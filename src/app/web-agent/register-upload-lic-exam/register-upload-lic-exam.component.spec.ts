import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterUploadLicExamComponent } from './register-upload-lic-exam.component';

describe('RegisterUploadLicExamComponent', () => {
  let component: RegisterUploadLicExamComponent;
  let fixture: ComponentFixture<RegisterUploadLicExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterUploadLicExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterUploadLicExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
