import { DashboardBackRouteService } from './../../core/service/dashboard-back-route.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/security/auth.service';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Location } from '@angular/common';
@Component({
  selector: 'app-register-upload-lic-exam',
  templateUrl: './register-upload-lic-exam.component.html',
  styleUrls: ['./register-upload-lic-exam.component.scss']
})
export class RegisterUploadLicExamComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private dataService: DataServiceService,
    private spinner: NgxSpinnerService,
    private dashboardBackRouteService: DashboardBackRouteService,
    private location: Location,
  ) { }

  public history: any = [
    {
      "id": "275",
      "fileName": "IL2021 - 021 - Potential การแข่งขันคุณวุฒิ ดูไบ_DD.xlsx",	
      "createDate": "7/21/21, 11:32",
      "createBy":	"THATPONG (ธัชพงศ์ คำพิทักษ์)",
      "type":	"POTENTIAL"
    }
  ]

  ngOnInit(): void {
    this.apiGetHistoryUploadFile();
  }

  updateHistory(){
    this.apiGetHistoryUploadFile();
  }

  apiGetHistoryUploadFile() {
    this.spinner.show();
    console.log("apiGetHistoryUploadFile", history);

    this.dataService.getUploadFileHistory("LIC-EXAM").subscribe(
    // this.webAgentService.getUploadHistoryByType(this.activityType).subscribe(
      response => {
        console.log('RES: ', response);
        this.history = response;
        this.spinner.hide();
      },
      error => {
        console.log('getUploadHistoryByType Failed: ', error);
        this.spinner.hide();
      }
    );
    this.spinner.hide();
  }

  handlerActivateSpinner(e) {
    if (e) {
      this.spinner.show();
    } else {
      this.spinner.hide();
    }
  }

  goToHome() {
    let userRole = this.authService.getPosition();
    let agentCode = this.authService.getLoginedName();
    sessionStorage.removeItem('agentCodeStored');
    sessionStorage.removeItem('agentLevelStored');
    this.dashboardBackRouteService.backHome(userRole, agentCode);
  }

  goBack() {
    this.location.back();
  }
}
