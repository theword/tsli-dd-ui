import Base64Url from 'base64url';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import * as CryptoJS from 'crypto-js';
@Component({
  selector: 'app-e-document',
  templateUrl: './e-document.component.html',
  styleUrls: ['./e-document.component.scss']
})
export class EDocumentComponent implements OnInit {

  private isDebug = true;
  public showSearchBox: Boolean = false;
  public masterEdocData: any = [];
  public edocData: any = [];
  public edocType: any = [];
  public checkShowType: any = [];

  constructor(
    private spinner: NgxSpinnerService,
    private webAgentService: WebAgentService,
    private route: ActivatedRoute,
    private router: Router
  ) { 
     
  }

  ngOnInit(): void {

    this.route.queryParams
    .subscribe(params => {
      if (params['key'] && params['key'].length > 0) {
        this.queryEdocument(params['key'], true);
      }
    });
  }

  handlerSearch(event) {
  
    let data = {
      "token": "tslI@2018",
      "id": event.pid,
      "dob": event.birthdate,
      "datenow": Date.now()
    }
    const _postData = Base64Url(JSON.stringify(data));
    this.queryEdocument(_postData, false);
  }

  // API Service
  queryEdocument(postBase64, isHash: boolean) {

    if(isHash){
      let base64 = atob(postBase64)
      if(this.isDebug){ console.log("base64", base64); }
      let data  = CryptoJS.AES.decrypt(base64, environment.edocumentSecretKey).toString(CryptoJS.enc.Utf8);
      if(this.isDebug){ console.log("data", data); }

      if( !data ) {
        this.router.navigate(['/e-document']);
      }
      postBase64 = Base64Url(data); 
    }

    this.webAgentService.searchEDocumentsByPid(postBase64).subscribe(
      response => {
        this.spinner.show();
        console.log("response", response);
        if (response['status'] == 'success') {
          this.showSearchBox = true;
          this.masterEdocData = [...response['detail']];
          this.edocData = [...response['detail']];
          this.edocType = [...response['type']];

          for (let index = 0; index < response['type'].length; index++) {
            this.checkShowType.push(true);
          }
          // let queryParams = { key: postBase64 };
          // this.router.navigate(['/e-document'], {
          //   queryParams: {
          //     key: queryParams.key,
          //   },
          // });
        } else {

          // console.log(response);
          this.showSearchBox = false;
          this.router.navigate(['/e-document']);
        }
        this.spinner.hide();
      }, error => {
        console.log('err. searchEDocumentsByPid: ', error);
        this.spinner.hide();
      }
    );
  }

  handlerCheckShowType(event) {
    let _type = [];
    for (let index = 0; index < event.length; index++) {
      if (event[index] == true) {
        _type.push(this.edocType[index]['templateNameTh']);
      }
    }
    this.edocData = this.masterEdocData.filter(element => _type.includes(element['templateNameTh']));
  }

}