import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriptionEDocComponent } from './description-e-doc.component';

describe('DescriptionEDocComponent', () => {
  let component: DescriptionEDocComponent;
  let fixture: ComponentFixture<DescriptionEDocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescriptionEDocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptionEDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
