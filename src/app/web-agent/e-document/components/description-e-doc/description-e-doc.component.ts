import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/core/security/auth.service';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-description-e-doc',
  templateUrl: './description-e-doc.component.html',
  styleUrls: ['./description-e-doc.component.scss']
})
export class DescriptionEDocComponent implements OnInit {

  @Input() edocData: any = [];
  @Input() edocType: any = [];
  @Input() checkShowType: any = [];

  @Output() isCheckType = new EventEmitter();

  public customerName: string = '';

  constructor(
    private webAgentService: WebAgentService
    , private translate: TranslateService
  ) {
    
  }

  ngOnInit(): void {
    try{
      if( this.edocData ){
        this.customerName  = this.edocData[0]['insuredName'];
      }
    }catch(e){

    }

  }


  onDownloadFile(policyCode, exportsPath ) {
   
    this.webAgentService.downloadEdocument( exportsPath ).subscribe(response => {

        if(response['size'] == 0){
            Swal.fire(   '',   this.translate.instant('web.agent.file.notfound'),  'warning'  );
            return ;
        }
          const blob = new Blob([response], { type: 'application/octet-stream' });
          let downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(blob);
          downloadLink.setAttribute('download', `${exportsPath}`);
          document.body.appendChild(downloadLink);
          downloadLink.click();
          
          document.body.removeChild(downloadLink);
        }), error => console.log('Error downloading the file'),
                 () => console.info('File downloaded successfully');

}

  onCheckShowType() {
    this.isCheckType.emit(this.checkShowType);
  }

}
