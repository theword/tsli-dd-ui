import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterEDocComponent } from './filter-e-doc.component';

describe('FilterEDocComponent', () => {
  let component: FilterEDocComponent;
  let fixture: ComponentFixture<FilterEDocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterEDocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterEDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
