import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-filter-e-doc',
    templateUrl: './filter-e-doc.component.html',
    styleUrls: ['./filter-e-doc.component.scss']
})
export class FilterEDocComponent implements OnInit {

    @Output() searchData = new EventEmitter();

    days = []
    listMonthName = [
        'JAN'
        , 'FEB'
        , 'MAR'
        , 'APR'
        , 'MAY'
        , 'JUN'
        , 'JUL'
        , 'AUG'
        , 'SEP'
        , 'OCT'
        , 'NOV'
        , 'DEC'
    ];
    months = [
        { id: 0, name: "มกราคม" },
        { id: 1, name: "กุมภาพันธ์" },
        { id: 2, name: "มีนาคม" },
        { id: 3, name: "เมษายน" },
        { id: 4, name: "พฤษภาคม" },
        { id: 5, name: "มิถุนายน" },
        { id: 6, name: "กรกฎาคม" },
        { id: 7, name: "สิงหาคม" },
        { id: 8, name: "กันยายน" },
        { id: 9, name: "ตุลาคม" },
        { id: 10, name: "พฤศจิกายน" },
        { id: 11, name: "ธันวาคม" }
    ]
    years = []
    isLeapYear: any
    today = new Date();
    currentYear = this.today.getFullYear() + 543;
    isDateValid = true;

    public pid;
    public day: any;
    public month: any;
    public year: any;

    constructor() { }

    ngOnInit(): void {
        this.checkLeapYear();
        this.setDays()
        this.setYears()
    }
    // public pid: string = '3550900248301';
    onSubmitSearch() {
        let _pid = '';
        if (this.pid) {
            _pid = this.pid.toString().replace('-', '');
        }

        if (_pid.length == 13 && this.day && this.month && this.year) {
            const packData = {
                'pid': _pid
                // , 'birthdate': this.day.padStart(2, "0") + (parseInt(this.month) + 1).toString().padStart(2, "0") + this.year
                , 'birthdate': this.day.padStart(2, "0") + '-' + this.listMonthName[parseInt(this.month)] + '-' + (parseInt(this.year) - 543)
            }
            this.searchData.emit(packData);
        }
        else {
            alert('โปรดระบุเลขที่บัตรประชาชนและวันเดือนปีเกิด');
        }
    }

    setDays() {
        for (let i = 1; i <= 31; i++) {
            this.days.push({ id: i.toString(), name: i.toString() });
        }
    }

    setYears() {
        for (let i = 1; i <= 120; i++) {
            this.years.push({ id: (this.currentYear - i).toString(), name: (this.currentYear - i).toString() })
        }
    }

    checkLeapYear() {
        // console.log("this.year", this.year);
        // console.log("this.month", this.month);
        // console.log("this.day", this.day);

        // LEAP YEAR CALCULATOR
        if (typeof (this.year) != 'undefined') {
            if (this.year % 4 != 0) {
                this.isLeapYear = false;
            } else if (this.year % 100 != 0) {
                this.isLeapYear = true;
            } else if (this.year % 400 != 0) {
                this.isLeapYear = false;
            } else {
                this.isLeapYear = true;
            }
        }
        this.setNewDate();
    };

    setNewDate() {

        if (this.day != "" && this.month != "" && this.year != "") {
            this.checkDateIsValid(this.day, this.month, this.year);
        } else {
            this.isDateValid = false;
        }
    };

    dateValidClass(isDateValid) {
        //$log.info(isDateValid);
        if (isDateValid) {
            return '';
        } else {
            return 'text-red';
        }
    };

    checkDateIsValid(day, month, year) {

        if (this.isLeapYear && month == '1') {
            if (day > 29) { // 29 Days in Feb in a leap year
                this.isDateValid = false;
            } else {
                this.isDateValid = true;
            }
        } else if (!this.isLeapYear && month == '1') {
            if (day > 28) { // 28 Days in Feb in a standard year
                this.isDateValid = false;
            } else {
                this.isDateValid = true;
            }
        } else {

            if (day > 30) { // 30 & 31 day rules
                if (month == '0' ||
                    month == '2' ||
                    month == '4' ||
                    month == '6' ||
                    month == '7' ||
                    month == '9' ||
                    month == '11') {
                    this.isDateValid = true;
                } else {
                    this.isDateValid = false;
                }
            } else {
                this.isDateValid = true;
            }
        }
    };
}
