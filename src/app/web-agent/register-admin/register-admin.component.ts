import { Component, Input, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/core/security/auth.service';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import { MockRegisterAgent } from '../register-agent/register-agent-model';

@Component({
    selector: 'app-register-admin',
    templateUrl: './register-admin.component.html',
    styleUrls: ['./register-admin.component.scss']
})
export class RegisterAdminComponent implements OnInit {

    @Input() isStandalone = false;

    // Declare Variable
    public agentLevel: string = 'ADMIN';
    public modelRegisAgent: MockRegisterAgent;
    public searchStatus: Boolean = false;
    public searchKeyword: string = '';

    public eventList: any = [];
    public viewEventData: any;

    public tableData: any;
    public curPage: number = 0;
    public pagingItem: any = {
        pageSize: 0
        , totalPage: 0
        , totalElements: 0
    };

    private sortingType: string = 'ACT_ID DESC';
    private searchParam = {
        'type': '',
        'province': ''
    };

    public activityType ;

    constructor(
          private spinner: NgxSpinnerService
        , private webAgentService: WebAgentService
        , private auth: AuthService
    ) {
        this.modelRegisAgent = new MockRegisterAgent();
        this.eventList = this.modelRegisAgent.tableData;
    }

    ngOnInit(): void {
        // sessionStorage.setItem('webAgentLevel', this.agentLevel);
        if (this.isStandalone) {
            this.eventList = this.modelRegisAgent.findByType('ทั้งหมด');
            this.searchStatus = true;
        }
    }

    // apiService
    apiGetActivityList() {
        this.spinner.show();
        const _postData = {
            'activityType': this.searchParam['type']
            , 'provinceId': this.searchParam['province']
            , 'keywords': this.searchKeyword
            , 'curPage': this.curPage
            , 'sortingType': this.sortingType
            , 'webRole': this.auth.getWebRole()
        };

        this.webAgentService.getActivityList(_postData).subscribe(
            response => {
                this.searchStatus = true;
                this.tableData = response['detail'];
                this.pagingItem = {
                    pageSize: response['pageSize'],
                    totalPage: response['totalPage'],
                    totalElements: response['totalElements'],
                };
            },
            error => {
                console.log('err. getActivityList: ', error);
            }
        ).add( () => {
            this.spinner.hide();
        });
    }

    // handlerController
    handlerSearchEvent(searchParam) {
        // console.log('SEARCH EVENT PARAM: ', searchParam, ' | ', this.searchParam);
        this.activityType = searchParam['type']
        if (searchParam['type'] != this.searchParam['type'] || searchParam['province'] != this.searchParam['province']) {
            this.curPage = 0;
        }
        this.searchParam = { ...searchParam };
        this.apiGetActivityList();
    }
    handlerPaging(event) {
        this.curPage = event;
        this.apiGetActivityList();
    }
    hanlderSorting(event) {
        this.sortingType = event;
        this.apiGetActivityList();
    }

}
