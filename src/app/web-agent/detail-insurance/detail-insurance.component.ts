import { Component, OnInit } from '@angular/core';
import {  Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/core/security/auth.service';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import { MockInsuranceDetail } from './ins-detail-model';
@Component({
  selector: 'app-detail-insurance',
  templateUrl: './detail-insurance.component.html',
  styleUrls: ['./detail-insurance.component.scss']
})
export class DetailInsuranceComponent implements OnInit {

  // Declare Variables
  public modelInsurance: MockInsuranceDetail;
  public insuranceCode: string = '';
  public insuranceId: string = '';

  public insuranceDetail: any;
  public insuranceInfo: any;
  public customerDetail: any;
  public productDetail: any;
  public paidDetail: any = [];

  public totalPremium: number = 0;
  public claimDetail: any;
  public benefit = [];
  public barcode = null;
  public isDataAvaliable: boolean = false;

  private agentCode: string = this.authService.getLoginedName();

  constructor(
    private route: ActivatedRoute
    , private spinner: NgxSpinnerService
    , private authService: AuthService
    , private webAgentService: WebAgentService
    , private location: Location
  ) { 
    this.modelInsurance = new MockInsuranceDetail();
  }

  ngOnInit(): void {
    // Get url parameter
    this.route.queryParams.subscribe(params => {
      let getParam = JSON.parse(params.params);
      this.insuranceCode = getParam[0]['insuranceCode'];
      this.insuranceId = getParam[0]['insuranceId'];
    });
    this.apiGetPolicyDetail();
  }
  goBack(){
    this.location.back()
}
  // apiService Controller
  apiGetPolicyDetail() {
    this.spinner.show();
    const _postData = {
      'agentCode': this.agentCode,
      'policyCode': this.insuranceCode,
      'policyId': this.insuranceId
    }
    this.webAgentService.getPolicyDetail(_postData).subscribe(
      response => {
        // console.log('res. searchInsurance: ', response);
        this.insuranceInfo = response['detail'];
        this.customerDetail = response['customer'];
        this.productDetail = response['product'];
        this.paidDetail = response['paid'];
        this.claimDetail = response['claim'];
        this.barcode = response['barcode'];
        this.benefit = response['benefit'];
        this.totalPremium = response['totalPremium'];
        this.isDataAvaliable = true;
        this.spinner.hide();
      },
      error => {
        console.log('err. searchInsurance: ', error);
        this.spinner.hide();
      }
    );
  }
  // getPolicyDetail

}
