import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/core/shared.module';
import { WebAgentControllerComponent } from './web-agent-controller/web-agent-controller.component';
import { DetailInsuranceComponent } from './detail-insurance/detail-insurance.component';
import { ValidateInsuranceComponent } from './validate-insurance/validate-insurance.component';
import { ValidateDocumentComponent } from './validate-document/validate-document.component';
import { DetailDocumentComponent } from './detail-document/detail-document.component';
import { ReportComponent } from './report/report.component';
import { ReportDetailComponent } from './report-detail/report-detail.component';
import { RegisterAdminComponent } from './register-admin/register-admin.component';
import { RegisterAgentComponent } from './register-agent/register-agent.component';
import { EDocumentComponent } from './e-document/e-document.component';
import { ActivityDetailComponent } from './activity-detail/activity-detail.component';

import { AgentHeaderComponent } from './components/agent-header/agent-header.component';
import { ValInsFormComponent } from './validate-insurance/components/val-ins-form/val-ins-form.component';
import { ValInsTableComponent } from './validate-insurance/components/val-ins-table/val-ins-table.component';
import { InsuranceDetailTableComponent } from './components/insurance-detail-table/insurance-detail-table.component';
import { InsuranceSubDetailTableComponent } from './components/insurance-sub-detail-table/insurance-sub-detail-table.component';
import { ValDocFormComponent } from './validate-document/components/val-doc-form/val-doc-form.component';
import { ValDocTableComponent } from './validate-document/components/val-doc-table/val-doc-table.component';
import { ReportFilterComponent } from './report/components/report-filter/report-filter.component';
import { ReportTableComponent } from './report/components/report-table/report-table.component';
import { ERegisterFilterComponent } from './components/e-register-filter/e-register-filter.component';
import { ERegisterTableComponent } from './components/e-register-table/e-register-table.component';
import { ERegisterDetailComponent } from './components/e-register-detail/e-register-detail.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { FilterEDocComponent } from './e-document/components/filter-e-doc/filter-e-doc.component';
import { DescriptionEDocComponent } from './e-document/components/description-e-doc/description-e-doc.component';
import { ActivityDetailListComponent } from './components/activity-detail-list/activity-detail-list.component';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';

import { DatePipe } from '@angular/common';
import { ModalStatusComponent } from './components/modal-status/modal-status.component';
import { ERegisterAdminGuards } from '../core/security/e-register.guards';
import { InsuranceBarcodeComponent } from './components/insurance-barcode/insurance-barcode.component';
import { RegisterAddActivityComponent } from './register-add-activity/register-add-activity.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import { GuestGuards } from '../core/security/guest.guards';
import { RegisterUploadActivityComponent } from './register-upload-activity/register-upload-activity.component';
import { RegisterUploadLicExamComponent } from './register-upload-lic-exam/register-upload-lic-exam.component';
import { RegisterUploadResultComponent } from './register-upload-result/register-upload-result.component';
import { UploadFileComponent } from './components/upload-file/upload-file.component';

const routes: Routes = [
  {
    path: '',
    component: WebAgentControllerComponent,
  },

  {
    path: 'validate-insurance',
    component: ValidateInsuranceComponent,
  },
  {
    path: 'detail-insurance',
    component: DetailInsuranceComponent,
  },
  {
    path: 'validate-document',
    component: ValidateDocumentComponent,
  },
  {
    path: 'detail-document',
    component: DetailDocumentComponent,
  },
  {
    path: 'report',
    component: ReportComponent,
  },
  {
    path: 'report-detail',
    component: ReportDetailComponent,
  },

  {
    path: 'e-register-admin',
    component: RegisterAdminComponent,
    canActivate: [ERegisterAdminGuards],
  },
  {
    path: 'e-register-agent',
    component: RegisterAgentComponent,
  },
  {
    path: 'register-form',
    component: RegisterFormComponent,
  },
  {
    path: 'register-add-activity',
    component: RegisterAddActivityComponent,
  },
  {
    path: 'register-upload-activity',
    component: RegisterUploadActivityComponent
  },
  {
    path: 'register-upload-lic-exam',
    component: RegisterUploadLicExamComponent
  },
  {
    path: 'register-upload-result',
    component: RegisterUploadResultComponent
  },
  {
    path: 'activity-detail',
    component: ActivityDetailComponent,
  },
  {
    path: '**',
    redirectTo: 'error',
    pathMatch: 'full',
  },
];

@NgModule({
  declarations: [
    WebAgentControllerComponent,
    DetailInsuranceComponent,
    ValidateInsuranceComponent,
    ValidateDocumentComponent,
    ReportComponent,
    ReportDetailComponent,
    RegisterAdminComponent,
    RegisterAgentComponent,
    InsuranceBarcodeComponent,
    AgentHeaderComponent,
    ValInsFormComponent,
    ValInsTableComponent,
    InsuranceDetailTableComponent,
    InsuranceSubDetailTableComponent,
    ValDocFormComponent,
    ValDocTableComponent,
    ReportFilterComponent,
    ReportTableComponent,
    ERegisterFilterComponent,
    ERegisterTableComponent,
    ERegisterDetailComponent,
    RegisterFormComponent,
    ActivityDetailComponent,
    ActivityDetailListComponent,
    ModalStatusComponent,
    RegisterAddActivityComponent,
    RegisterUploadActivityComponent,
    RegisterUploadLicExamComponent,
    RegisterUploadResultComponent,
    UploadFileComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    NgxBarcodeModule,
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'th' }, DatePipe],
})
export class WebAgentModule {}
