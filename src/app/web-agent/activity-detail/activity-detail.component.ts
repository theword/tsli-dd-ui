import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import Swal from 'sweetalert2';
import {  Location } from '@angular/common';

@Component({
    selector: 'app-activity-detail',
    templateUrl: './activity-detail.component.html',
    styleUrls: ['./activity-detail.component.scss']
})
export class ActivityDetailComponent implements OnInit {

    @Input() isShowDetailTable: boolean = true ;
    public activityItem: string = '';
    public activityDetail: any = {};
    public activityDetailList: any = [];
    public curPage: number = 0;
    public searchKeyword: string = '';
    public pagingItem: any = {
        pageSize: 0
        , totalPage: 0
        , totalElements: 0
    };

    public isDataAvailable: boolean = false;
    public activityType

    constructor(
        private route: ActivatedRoute
        , private router: Router
        , private spinner: NgxSpinnerService
        , private webAgentService: WebAgentService
        , private translate: TranslateService
        , private location: Location
  
    ) { }

    ngOnInit(): void {
        // Get url parameter
        this.route.queryParams.subscribe(params => {
            if( params.params ){
                let getParam = JSON.parse(params.params);
                //console.log(getParam);
                this.activityItem = getParam[0]['item'];
             
            }
            this.prepareQuery();
        });

      
    }

    goBack(){
        return this.location.back();
    }
    // apiController
    async prepareQuery() {
        this.spinner.show();
        await this.getActivityDetail();
        await this.getActivityDetailList();
        this.isDataAvailable = true;
        this.spinner.hide();
    }

    async deleteActivityDetail() {
    
        Swal.fire({
            title: this.translate.instant('alert.confirm.title'),
            text: '',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: this.translate.instant('alert.delete.title.cancel'),
            confirmButtonText: this.translate.instant('alert.delete.title.confirm'),
          }).then( async (result) => {
            if (result.value) {
                this.spinner.show();
                    const _postData = { 'activityId': this.activityItem };
                    let _res = await this.webAgentService.deleteActivityDetail(_postData).toPromise();
                    //console.log(_res);
                    if (_res > 0) {
                      
                        Swal.fire( '',this.translate.instant('alert.delete.title.success'),   'success'   )
                        .then(rs=>{
                            this.router.navigateByUrl('/web-agent/e-register-admin');
                            // this.getActivityDetail();
                        });

                    }else {
                        Swal.fire( '', this.translate.instant('web.agent.activity.delete-error'), 'error'  );
                    }
                    this.spinner.hide()
            }
          });
        
    }

    async getActivityDetail() {
        this.spinner.show()
        if (this.activityItem) {
            const _postData = { 'activityId': this.activityItem };
            let _res = await this.webAgentService.getActivityDetail(_postData).toPromise();
            if (_res && Object.keys(_res).length > 0) {
                
                this.activityDetail = _res;
                this.activityType = this.activityDetail.activityType
            }
            this.spinner.hide()
        }
    }

    async getActivityDetailList() {
        const _postData = {
            'activityId': this.activityItem,
            'keywords': this.searchKeyword,
            'curPage': this.curPage
        };
     
        let response = await this.webAgentService.getActivityDetailList(_postData).toPromise();
    
        if (response && Object.keys(response).length > 0) {
            this.activityDetailList = response['detail'];
            this.pagingItem = {
                pageSize: response['pageSize'],
                totalPage: response['totalPage'],
                totalElements: response['totalElements'],
            };
        }
    }

    // Handle child component emit
    handlerPaging(event) {
        console.log(event);
        this.curPage = event;
        this.getActivityDetailList();
    }

}
