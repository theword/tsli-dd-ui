import { Component, Input, Output, OnInit, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import { AuthService } from 'src/app/core/security/auth.service';
@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements OnInit {

  @Input() uploadType: any;
  @Input() tableData: any = [];
  @Input() activityType: any;

  @Output() onUpdateHistory = new EventEmitter();
  @Output() onClickUploadFile = new EventEmitter();

  public msgSuccess: string = '';
  public msgFail: string = '';

  public fileName: String = '';
  public fileSelected: any;
  public fileBased64: String;
  public msgUploadStatus: String = 'รอการอัปโหลด';
  public uploadStatus = 0;
  public filePromotionType: String = 'RESULT';
  public modalStatus: String = '';

  public tabSelectedId: Number = 0;

  public tableHeader: any = [
    'ID', 'ชื่อไฟล์', 'วันที่สร้าง', 'ผู้สร้าง', 'วันที่อัปเดตล่าสุด', 'ผู้อัปเดต'
  ];

  public fullname;
  public agentCode;

  @ViewChild('customFile') InputFile: ElementRef;

  constructor(
    private dataService: DataServiceService,
    private webService: WebAgentService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private authservice: AuthService,
  ) { }

  ngOnInit(): void {
    this.fullname = this.authservice.getName();
    this.agentCode = this.authservice.getLoginedName();

    this.spinner.show();
  }

  onChangeTab(tabId: number) {
    if (tabId != this.tabSelectedId) {
      this.tabSelectedId = tabId;
    }
  }

  // Handle File Selected
  onFileChange(fileInput) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      this.uploadStatus = 0;
      this.msgUploadStatus = 'รอการอัปโหลด';
      this.fileSelected = fileInput.target.files[0];
      this.fileName = fileInput.target.files[0].name;
      const file = fileInput.target.files[0];
      this.readFile(file).then((base64: string): any => {
        this.fileBased64 = base64;
      });
    }
  }

  readFile(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  onSubmitFile(content) {
    if (this.fileSelected && this.uploadStatus == 0) {
      this.onClickUploadFile.emit(true);
      var _postData = {
        fileName: this.fileSelected.name,
        fileSize: parseInt(this.fileSelected.size),
        fileBased64: this.fileBased64,
        fileType: this.fileSelected.type,
        activityTypeId: Number.parseInt(this.activityType),
        updateBy: (this.agentCode + " (" + this.fullname + ")")
      };
      console.log("_postData", _postData);

      if (this.uploadType == "LIC-Exam") {
        console.log("uploadType", this.uploadType);
        this.webService.postUploadLicExam(_postData).subscribe(
          response => {
            // console.log('RES: ', response[0]);
            this.msgSuccess = 'อัปโหลดไฟล์สำเร็จ';
            this.msgFail = 'อัปโหลดไฟล์ล้มเหลว';
            if (response[0] == 'success') {
              this.msgUploadStatus = 'สำเร็จ';
              this.uploadStatus = 2;
              this.resetFileSelect();
              this.modalStatus = 'success';
              this.onUpdateHistory.emit();
            }
            else {
              this.msgUploadStatus = 'ไม่สำเร็จ';
              this.uploadStatus = 1;
              this.modalStatus = 'fail';
            }
            this.modalService.open(content, { centered: true, windowClass: "modal-status" }).result.then((result) => {
              // this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
              // this.closeResult = `Dismissed`;
            });
            this.onClickUploadFile.emit(false);
            this.spinner.hide();
          },
          error => {
            console.log('post_uploadLIC-EXAM Failed: ', error);
          }
        );

      }
      // For API Upload File Activity 
      else if (this.uploadType == "Activity") {
        console.log("uploadType", this.uploadType);
        this.webService.postUploadActivity(_postData).subscribe(
          response => {
            // console.log('RES: ', response[0]);
            this.msgSuccess = 'อัปโหลดไฟล์สำเร็จ';
            this.msgFail = 'อัปโหลดไฟล์ล้มเหลว';
            if (response[0] == 'success') {
              this.msgUploadStatus = 'สำเร็จ';
              this.uploadStatus = 2;
              this.resetFileSelect();
              this.modalStatus = 'success';
              this.onUpdateHistory.emit();
            }
            else {
              this.msgUploadStatus = 'ไม่สำเร็จ';
              this.uploadStatus = 1;
              this.modalStatus = 'fail';
            }
            this.modalService.open(content, { centered: true, windowClass: "modal-status" }).result.then((result) => {
              // this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
              // this.closeResult = `Dismissed`;
            });
            this.onClickUploadFile.emit(false);
            this.spinner.hide();
          },
          error => {
            console.log('post_uploadActivity Failed: ', error);
          }
        );
      }
      // For API Upload File Activity Result
      else if (this.uploadType == "Result") {
        console.log("uploadType", this.uploadType);
        this.webService.postUploadResult(_postData).subscribe(
          response => {
            // console.log('RES: ', response[0]);
            this.msgSuccess = 'อัปโหลดไฟล์สำเร็จ';
            this.msgFail = 'อัปโหลดไฟล์ล้มเหลว';
            if (response[0] == 'success') {
              this.msgUploadStatus = 'สำเร็จ';
              this.uploadStatus = 2;
              this.resetFileSelect();
              this.modalStatus = 'success';
              this.onUpdateHistory.emit();
            }
            else {
              this.msgUploadStatus = 'ไม่สำเร็จ';
              this.uploadStatus = 1;
              this.modalStatus = 'fail';
            }
            this.modalService.open(content, { centered: true, windowClass: "modal-status" }).result.then((result) => {
              // this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
              // this.closeResult = `Dismissed`;
            });
            this.onClickUploadFile.emit(false);
            this.spinner.hide();
          },
          error => {
            console.log('post_uploadResult Failed: ', error);
          }
        );
      }

      this.fileSelected = ""
      this.fileName = ""
      this.InputFile.nativeElement.value = "";
    }
  }

  resetFileSelect() {
    this.fileName = '';
    this.fileBased64 = '';
  }

}
