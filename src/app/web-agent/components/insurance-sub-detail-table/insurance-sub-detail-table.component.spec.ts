import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsuranceSubDetailTableComponent } from './insurance-sub-detail-table.component';

describe('InsuranceSubDetailTableComponent', () => {
  let component: InsuranceSubDetailTableComponent;
  let fixture: ComponentFixture<InsuranceSubDetailTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsuranceSubDetailTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceSubDetailTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
