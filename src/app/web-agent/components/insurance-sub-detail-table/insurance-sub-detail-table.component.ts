import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-insurance-sub-detail-table',
  templateUrl: './insurance-sub-detail-table.component.html',
  styleUrls: ['./insurance-sub-detail-table.component.scss']
})
export class InsuranceSubDetailTableComponent implements OnInit {

  // Declare Variables
  @Input() title: string = '';
  @Input() type: string = '';
  @Input() headers: any = [];
  @Input() data: any = [];
  @Input() totalPremium: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
