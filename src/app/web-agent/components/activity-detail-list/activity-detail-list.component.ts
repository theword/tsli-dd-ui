import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/core/security/auth.service';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-activity-detail-list',
    templateUrl: './activity-detail-list.component.html',
    styleUrls: ['./activity-detail-list.component.scss']
})
export class ActivityDetailListComponent implements OnInit {

    @Input() data: any = [];
    @Input() activityId ;
    @Input() pagingCount: number = 0;
    @Input() pagingItem: { pageSize: 0, totalPage: 0, totalElements: 0 };
    @Input() activityType ;
    @Output() clickPaging = new EventEmitter<any>();

    public headers: any = [           
        'ID'
        , 'เลขที่บัตรประชาชน'
        , 'ชื่อ-นามสกุล'
        , 'ผู้แนะนำ'
        , 'RESULT'
    ];

    constructor( 
        private auth: AuthService, 
        private webAgentService: WebAgentService, 
        private translate: TranslateService
    ) { }

    ngOnInit(): void {
        if( this.isRegisterAdmin ){
            this.headers.push('ลบรายการ');
        }
    }

    get isRegisterAdmin (){
        // this.auth.isRegisterAdmin() || this.auth.isERegisterAccess(this.activityType)
        return (this.auth.isRegisterAdmin() || this.auth.isRegisterAdmin(this.activityType))
    }
    // PAGINATION
    itemInfo(): string {
        const start = this.printSequense();
        const end = start + this.pagingItem.pageSize - 1;
        return 'Page ' + (this.pagingCount + 1) + ' : ' + start + '-'
            + (end > this.pagingItem?.totalElements ? this.pagingItem?.totalElements : end) + ' of ' + this.pagingItem?.totalElements;
    }

    printSequense() {
        return ((this.pagingCount) * this.pagingItem.pageSize) + 1;
    }

    async flagDeleteERegister( registerId ){

        Swal.fire({
            title: this.translate.instant('alert.confirm.title'),
            text: '',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: this.translate.instant('alert.delete.title.cancel'),
            confirmButtonText: this.translate.instant('alert.delete.title.confirm'),
          }).then( async (result) => {
            if (result.value) {
                // this.spinner.show();
                
                    let _res = await this.webAgentService.flagDeleteERegister(registerId, this.activityId ).toPromise();
                   
                    if(_res['message']=='success'){
                      
                        Swal.fire( '',this.translate.instant('alert.delete.title.success'),   'success'   )
                        .then(rs=>{
                            this.clickPaging.emit( this.pagingCount );
                        });

                    }else {
                        Swal.fire( '', this.translate.instant('web.agent.activity.delete-error'), 'error'  );
                    }
                    // this.spinner.hide()
            }
          });

     
    
    }
    paginationAction(type: string) {
        switch (type) {
            case 'firstPage':
                if (this.pagingCount == 0) return;
                else {
                    this.clickPaging.emit(0);
                }
                break;
            case 'previous':
                if (this.pagingCount >= 1) {
                    this.clickPaging.emit(this.pagingCount - 1);
                }
                break;
            case 'next':
                if (this.pagingCount < this.pagingItem.totalPage ) {
                    this.clickPaging.emit(this.pagingCount + 1);
                }
                else return;
                break;
            case 'endPage':
                if (this.pagingCount == this.pagingItem.totalPage) return;
                else {
                    this.clickPaging.emit(this.pagingItem.totalPage );
                }
                break;

            default:
                break;
        }
    }
}
