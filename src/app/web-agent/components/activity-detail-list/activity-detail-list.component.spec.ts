import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityDetailListComponent } from './activity-detail-list.component';

describe('ActivityDetailListComponent', () => {
  let component: ActivityDetailListComponent;
  let fixture: ComponentFixture<ActivityDetailListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivityDetailListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
