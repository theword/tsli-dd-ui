import { Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-insurance-barcode',
  templateUrl: './insurance-barcode.component.html',
  styleUrls: ['./insurance-barcode.component.scss']
})
export class InsuranceBarcodeComponent implements OnInit {

  @Input() title: string = '';
  @Input() data: string = '';
  _CR = String.fromCharCode(10);
  elementType = 'svg';
  value = '';
  format = 'CODE128';
  lineColor = '#000000';
  width = 1;
  height = 40;
  displayValue = true;
  fontOptions = '';
  font = 'monospace';
  textAlign = 'center';
  textPosition = 'bottom';
  textMargin = 2;
  fontSize = 13;
  background = '#ffffff';
  margin = 0;
  marginTop = 0;
  marginBottom = 0;
  marginLeft = 0;
  marginRight = 0;


  constructor() { 

  }

  ngOnInit(): void {

  }
 get values(): string {
   if( !this.isDraw() ){
     return '';
   }
   const element = (''+this.data).split(' ');
   const barcode = `${element[0]}${this._CR}${element[1]}${this._CR}${element[2]}${this._CR}${element[3]}`;
  // const barcode = (''+this.data).split(' ').join('x')
  // console.log("Data -->", this.data+"<--");
  // console.log("Barcode -->", barcode+"<--");
  return  barcode;
  }

  isDraw(){
    return '' != this.data && this.data != undefined ;
  }
}
