import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-insurance-detail-table',
  templateUrl: './insurance-detail-table.component.html',
  styleUrls: ['./insurance-detail-table.component.scss']
})
export class InsuranceDetailTableComponent implements OnInit {

  @Input() insuranceInfo: any;

  constructor(
  ) { }

  ngOnInit(): void {
    try{
      if(this.insuranceInfo.dispatchDate){
        this.insuranceInfo.dispatchDate =  new DatePipe("en-US").transform(this.insuranceInfo.dispatchDate, "dd/MM/" )+(new Date(this.insuranceInfo.dispatchDate).getFullYear()+543) ;
      }
    }catch{
      this.insuranceInfo.dispatchDate= '';
    }

  }

  

}
