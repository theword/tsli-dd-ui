import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsuranceDetailTableComponent } from './insurance-detail-table.component';

describe('InsuranceDetailTableComponent', () => {
  let component: InsuranceDetailTableComponent;
  let fixture: ComponentFixture<InsuranceDetailTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsuranceDetailTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceDetailTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
