import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/core/security/auth.service';
import { ModelAgentDetail } from 'src/app/core/model/model-agent-detail';

@Component({
  selector: 'app-agent-header',
  templateUrl: './agent-header.component.html',
  styleUrls: ['./agent-header.component.scss']
})
export class AgentHeaderComponent implements OnInit {

  @Input() showBackBtn: Boolean = false;
  public modelAgent: ModelAgentDetail;

  constructor(
    private authService: AuthService
    , private location: Location
  ) {
    this.modelAgent = new ModelAgentDetail();
    this.modelAgent.setAgentDetail(
      sessionStorage.getItem('agentLevelName')
      , this.authService.getLoginedName()
      , this.authService.getName()
    )
  }

  ngOnInit(): void {
  }

  directBackward() {
    this.location.back();
  }

}
