import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-status',
  templateUrl: './modal-status.component.html',
  styleUrls: ['./modal-status.component.scss']
})
export class ModalStatusComponent implements OnInit {

  @Input() status: any;
  @Input() msgSuccess: string = 'บันทึกสำเร็จ';
  @Input() msgFail: string = 'บันทึกไม่สำเร็จ';
  @Input() dismissAsExit: Boolean = false;
  @Input() exitToPath: string = '';
  @Input() btnName: string = 'OK';

  constructor(
    private modalService: NgbModal
    , private router: Router
  ) { 

    
  }

  ngOnInit(): void {
  }

  closeModal() {
    console.log();
    this.modalService.dismissAll();
    const eventCode: string = sessionStorage.getItem('webAgent-regis-form-id');

    if (this.dismissAsExit && this.status == 'success') {
      switch (this.exitToPath) {
        case 'REGISTER_AGENT':
            // ROUTE SERVICE REDIRIECT HERE !!!
          const path = '/web-agent/activity-detail';
          this.router.navigate([path], {queryParams: {params: JSON.stringify([{ 'item': eventCode } ])}});
          break;
        
        case 'REGISTER_ADMIN':
          break;
      
        default:
          break;
      }
    } // END IF DISMISS AND EXIT
  }

}
