import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ERegisterFilterComponent } from './e-register-filter.component';

describe('ERegisterFilterComponent', () => {
  let component: ERegisterFilterComponent;
  let fixture: ComponentFixture<ERegisterFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ERegisterFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ERegisterFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
