import { filter } from 'rxjs/operators';
import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/core/security/auth.service';
import { WebAgentService } from 'src/app/core/service/web-agent.service';

@Component({
  selector: 'app-e-register-filter',
  templateUrl: './e-register-filter.component.html',
  styleUrls: ['./e-register-filter.component.scss']
})
export class ERegisterFilterComponent implements OnInit {

  // Declare Variables
  @Output() onSearchEvent = new EventEmitter();
  @Output() onChangeActivityType = new EventEmitter();

  public filterEventForm = {
    type: '',
    province: '',
  };

  public eventTypeList: any;
  public provinceList: any;
  public isAccess: any;
  public activityName: any;
  constructor(
    private route: Router,
    private webAgentService: WebAgentService,
    private authService: AuthService
    , private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.apiGetProvinceList();
    this.apiGetActivityList();
  }

  onOptionsSelected(){
    // console.log("eventTypeList", this.eventTypeList.filter(item => item.activityType == this.filterEventForm.type)[0].typeNameThai);
    this.activityName = this.eventTypeList.filter(item => item.activityType == this.filterEventForm.type)[0].typeNameThai
    // console.log("selected", this.filterEventForm);
    this.isAccess = this.authService.isRegisterAdmin() || this.authService.isRegisterAdmin(this.filterEventForm.type);
    // console.log('isAccess: ', this.isAccess);
    this.onChangeActivityType.emit(this.filterEventForm.type);
  }

  onSearchEventList() {
    this.onSearchEvent.emit(this.filterEventForm);
  }
  onAddNewEvent(activityType){
    switch (activityType) {
      case 1:
        this.route.navigate(['/web-agent/register-add-activity'], { queryParams: { "IHRFCBJT": 'HETWRGQR'} });
      break;
      case 2:
        this.route.navigate(['/web-agent/register-add-activity'], { queryParams: { "IHRFCBJT": 'YHDNHRDJ'} });
      break;
      case 3:
        this.route.navigate(['/web-agent/register-add-activity'], { queryParams: { "IHRFCBJT": 'OKJHGFRB'} });
      break;
      case 4:
        this.route.navigate(['/web-agent/register-add-activity'], { queryParams: { "IHRFCBJT": 'PMGFDHFN'} });
      break;
      case 5:
        this.route.navigate(['/web-agent/register-add-activity'], { queryParams: { "IHRFCBJT": 'HMCBMDYK'} });
      break;
      case 6:
        this.route.navigate(['/web-agent/register-add-activity'], { queryParams: { "IHRFCBJT": 'MV<VFGHR'} });
      break;
      default:
        break;
    }
  }
  onUploadEvent(activityType){
    switch (activityType) {
      case 1:
        this.route.navigate(['/web-agent/register-upload-activity'], { queryParams: { "IHRFCBJT": 'HETWRGQR', "GASDGETG": this.activityName } });
      break;
      case 2:
        this.route.navigate(['/web-agent/register-upload-activity'], { queryParams: { "IHRFCBJT": 'YHDNHRDJ', "GASDGETG": this.activityName } });
      break;
      case 3:
        this.route.navigate(['/web-agent/register-upload-activity'], { queryParams: { "IHRFCBJT": 'OKJHGFRB', "GASDGETG": this.activityName } });
      break;
      case 4:
        this.route.navigate(['/web-agent/register-upload-activity'], { queryParams: { "IHRFCBJT": 'PMGFDHFN', "GASDGETG": this.activityName } });
      break;
      case 5:
        this.route.navigate(['/web-agent/register-upload-activity'], { queryParams: { "IHRFCBJT": 'HMCBMDYK', "GASDGETG": this.activityName } });
      break;
      case 6:
        this.route.navigate(['/web-agent/register-upload-activity'], { queryParams: { "IHRFCBJT": 'MV<VFGHR', "GASDGETG": this.activityName } });
      break;
      default:
        break;
    }
  }
  apiGetProvinceList() {
    this.spinner.show();
      this.webAgentService.getProvinceListWAll().subscribe(
      response => {
        this.provinceList = response;
        this.filterEventForm['province'] = response[0]['provinceCd'];
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      }
    );
  }

  isRegisterAdmin(){
    return this.authService.isRegisterAdmin();
  }
  apiGetActivityList() {
    this.spinner.show();
    this.webAgentService.getActivityTypeList().subscribe(
      response => {
        this.eventTypeList = response;
        this.filterEventForm['type'] = response[0]['activityType'];
        this.onOptionsSelected()
        this.spinner.hide();
      },
      error => {
        console.log('err. getActivityTypeList: ', error);
        this.spinner.hide();
      }
    );
  }

}
