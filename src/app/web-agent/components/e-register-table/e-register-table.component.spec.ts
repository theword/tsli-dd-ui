import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ERegisterTableComponent } from './e-register-table.component';

describe('ERegisterTableComponent', () => {
  let component: ERegisterTableComponent;
  let fixture: ComponentFixture<ERegisterTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ERegisterTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ERegisterTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
