import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/security/auth.service';
@Component({
    selector: 'app-e-register-table',
    templateUrl: './e-register-table.component.html',
    styleUrls: ['./e-register-table.component.scss']
})
export class ERegisterTableComponent implements OnInit {

    // Declare Variables
    @Input() headers: any = [];
    @Input() data: any = [];
    @Input() agentType: string = 'AGENT';

    @Input() pagingCount: number = 0;
    @Input() pagingItem: { pageSize: 0, totalPage: 0, totalElements: 0 };
    @Input() activityType;

    @Output() eventAction = new EventEmitter();
    @Output() clickPaging = new EventEmitter<any>();
    @Output() clickSorting = new EventEmitter<any>();

    public sortingActIdStatus: boolean = false;
    public sortingActIdType: string = 'ACT_ID DESC';

    public sortingActDatetSatus: boolean = true;
    public sortingActDateType: string = 'ACT_DATE ASC';

    constructor(
        private router: Router,
        private authenService: AuthService
    ) { }

    ngOnInit(): void {
    }

    
    onShowDetail(actionType, eventCode, typeId){
        this.onClickAction(actionType, eventCode, typeId, 'Tg'); // N, registerType = '1'
    }
    onAdminRegister(actionType, eventCode, typeId){
        this.onClickAction(actionType, eventCode, typeId, 'Wc', 'Mg'); // Y, registerType = '2'
    }

    onUserRegister(actionType, eventCode, typeId){
        this.onClickAction(actionType, eventCode, typeId, 'Tg', 'MQ'); // N , registerType = '1'
    }


    onClickAction(actionType, eventCode, typeId, attendance, registerType='') {
        let path = '';
        let queryParams = [];
        
        switch (actionType) {
            case 'info':
                path = '/web-agent/activity-detail';
                queryParams = [
                    { 'item': eventCode },
                ];
                this.router.navigate([path], {
                    queryParams: {
                        params: JSON.stringify(queryParams),
                    },
                });
                break;

            case 'register':
                
                path = '/web-agent/register-form';
                sessionStorage.setItem('webAgent-regis-form-id', eventCode);
                sessionStorage.setItem('webAgent-regis-form-type', typeId);
                this.router.navigate([path], {
                    queryParams: {
                        'YXR0ZW5kYW5jZQ' : (attendance)
                        ,'cmVnaXN0ZXJUeXBl': (registerType)
                    },
                } );
                break;

            default:
                break;
        }
    }
    onClickSortingActId() {
        this.sortingActIdStatus = !this.sortingActIdStatus;
        if (this.sortingActIdStatus) {
            this.sortingActIdType = 'ACT_ID ASC';
        } else {
            this.sortingActIdType = 'ACT_ID DESC';
        }
        this.clickSorting.emit(this.sortingActIdType);
    }

    onClickSortingActDate() {
        this.sortingActDatetSatus = !this.sortingActDatetSatus;
        if (this.sortingActDatetSatus) {
            this.sortingActDateType = 'ACT_DATE ASC';
        } else {
            this.sortingActDateType = 'ACT_DATE DESC';
        }
        this.clickSorting.emit(this.sortingActDateType);
    }

    // UTILITIES
    isAdmin() {
        // || this.authenService.isERegisterAccess(this.activityType)
        // console.log('props.activityType: ', this.activityType);
        // console.log('isAdmin: ', this.authenService.isRegisterAdmin() || this.authenService.isRegisterAdmin(this.activityType));
        return (this.authenService.isRegisterAdmin() || this.authenService.isRegisterAdmin(this.activityType));
    }
    isCanRegister(status) {
        return status == 'true';
    }


    // PAGINATION
    itemInfo(): string {
        const start = this.printSequense();
        const end = start + this.pagingItem.pageSize - 1;
        return 'Page ' + (this.pagingCount + 1) + ' : ' + start + '-'
            + (end > this.pagingItem?.totalElements ? this.pagingItem?.totalElements : end) + ' of ' + this.pagingItem?.totalElements;
    }
    printSequense() {
        return ((this.pagingCount) * this.pagingItem.pageSize) + 1;
    }
    paginationAction(type: string) {
        switch (type) {
            case 'firstPage':
                if (this.pagingCount == 0) return;
                else {
                    this.clickPaging.emit(0);
                }
                break;
            case 'previous':
                if (this.pagingCount >= 1) {
                    this.clickPaging.emit(this.pagingCount - 1);
                }
                break;
            case 'next':
                if (this.pagingCount < this.pagingItem.totalPage) {
                    this.clickPaging.emit(this.pagingCount + 1);
                }
                else return;
                break;
            case 'endPage':
                if (this.pagingCount == this.pagingItem.totalPage) return;
                else {
                    this.clickPaging.emit(this.pagingItem.totalPage );
                }
                break;

            default:
                break;
        }
    }
}
