import { Router, ActivatedRoute } from '@angular/router';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebAgentService } from 'src/app/core/service/web-agent.service';
import { DatePipe } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/security/auth.service';


@Component({
  selector: 'app-e-register-detail',
  templateUrl: './e-register-detail.component.html',
  styleUrls: ['./e-register-detail.component.scss']
})
export class ERegisterDetailComponent implements OnInit {

  @Input() typeId: String = '1';
  @Input() data: any;

  @Output() rollback = new EventEmitter;
  @Output() delete = new EventEmitter;
  public filterEventForm = {
    type: '',
    province: '',
  };

  public eventTypeList: any;
  public provinceList: any;
  public dropdownActivitySubType: any = [];
  public agentLevel: string = sessionStorage.getItem('webAgentLevel');
  public _agentActivityDetail = {
    'activityName': '',
    'activitySubType': '',
    'activityType': '',
    'activityId': '',
    'provinceName': '',
    'location': '',
    'activityDate': '',
    'startTime': '',
    'endTime': '',
    'numofdays': '',
    'roomCapa': '',
    'maxCapa': '',
    'activityRegisFrom': '',
    'activityRegisTo': '',
    'activityRegisToTime': '',
    activityDateTo:'',
    remark: '',
    'delIndi': undefined
  }

  public msgSuccess: string = 'อัปเดตข้อมูลกิจกรรมสำเร็จ';
  public msgFail: string = 'เกิดข้อผิดพลาดในการอัปเดตข้อมูลกิจกรรม';
  public modalStatus: string = '';
  public isAccess 
  form: FormGroup
  submitted = false

  public activityType;
  public activityName;
  
  constructor(
    private webAgentService: WebAgentService
    , private spinner: NgxSpinnerService
    , private datePipe: DatePipe
    , private modalService: NgbModal
    , private formBuilder: FormBuilder
    , private authService: AuthService
    , private route: Router
    , private router: ActivatedRoute
  ) {
    this.createForm()
  }

  ngOnInit(): void {
    this.setActivityType()

    if (this.data.startTime) {
      var startTime = this.data.startTime.split(':');
      this.data.startTime = startTime[0].padStart(2, "0") + ":" + startTime[1].padStart(2, "0");
    }
    if (this.data.endTime) {
      var endTime = this.data.endTime.split(':');
      this.data.endTime = endTime[0].padStart(2, "0") + ":" + endTime[1].padStart(2, "0");
    }

    this.initForm()
    this.apiGetProvinceList();
    this.apiGetActivityList();
  }

  setActivityType(){
    this.router.queryParams.subscribe(params => {
      switch (params['IHRFCBJT']) {
        case 'HETWRGQR' :
          this.activityType = 1;
          break;
        case 'YHDNHRDJ' :
          this.activityType = 2;
          break;
        case 'OKJHGFRB' :
          this.activityType = 3;
          break;
        case 'PMGFDHFN' :
          this.activityType = 4;
          break;
        case 'HMCBMDYK' :
          this.activityType = 5;
          break;
        case 'MV<VFGHR' :
          this.activityType = 6;
          break;
        default:
          break;
      }
    })
  }

  get f() { return this.form.controls }

  createForm() {
    this.form = this.formBuilder.group({
      activityName: ['', [Validators.required]],
      activitySubType: [''],
      activityType: ['', [Validators.required]],
      activityId: [''],
      provinceName: ['', [Validators.required]],
      location: ['', [Validators.required]],
      activityDate: ['', [Validators.required]],
      startTime: ['', [Validators.required]],
      endTime: ['', [Validators.required]],
      numofdays: ['', [Validators.required]],
      roomCapa: ['', [Validators.required]],
      maxCapa: ['', [Validators.required]],
      activityRegisFrom: ['', [Validators.required]],
      activityRegisTo: ['', [Validators.required]],
      activityRegisToTime: ['', [Validators.required]],
      activityDateTo:[null],
      delIndi: [''],
      remark: ['']
    })
  }

  initForm(){
    this.form.controls['activityName'].setValue(this.data['activityName']);
    this.form.controls['activityType'].setValue(this.data.activityType);
    this.form.controls['activitySubType'].setValue(this.data.activityType);
    this.form.controls['activityId'].setValue(this.data.activityId);
    this.form.controls['provinceName'].setValue(this.data.provinceName);
    this.form.controls['location'].setValue(this.data.location);
    this.form.controls['activityDate'].setValue(new DatePipe('en-US').transform(this.data.activityDate, 'yyyy-MM-dd'));
    this.form.controls['startTime'].setValue(this.data.startTime);
    this.form.controls['endTime'].setValue(this.data.endTime);
    this.form.controls['numofdays'].setValue(this.data.numofdays);
    this.form.controls['roomCapa'].setValue(this.data.roomCapa);
    this.form.controls['maxCapa'].setValue(this.data.maxCapa);
    this.form.controls['activityRegisFrom'].setValue(new DatePipe('en-US').transform(this.data.activityRegisFrom, 'yyyy-MM-dd'));
    this.form.controls['activityRegisTo'].setValue(new DatePipe('en-US').transform(this.data.activityRegisTo, 'yyyy-MM-dd'));
    this.form.controls['activityRegisToTime'].setValue(this.data.activityRegisToTime);
    this.form.controls['delIndi'].setValue(this.data.delIndi);
    this.form.controls['remark'].setValue(this.data.remark);
    if(this.data.activityDateTo){
      this.form.controls['activityDateTo'].setValue( new DatePipe('en-US').transform(this.data.activityDateTo, 'yyyy-MM-dd') );
    }
    if( this.data.activityType ){
      this.getActivitySUbTypeList(this.data.activityType);
    }
  }

  clearActivityDateTo(){
    this.form.value['activityDateTo'] = null;
  }
  ngOnChanges(change: SimpleChanges){    
    if(change.data){
      this.initForm()
    }
  }

  async onRollDelete(){
    if(  this.isUpdate ){
      await this.delete.emit();
    }
  }

  async onRollback(){
    if( !this.isUpdate ){
      this.createForm();
    }else{
      await this.rollback.emit();
    }
  }

  onUploadResult(){
    this.activityName = this.eventTypeList.filter(item => item.activityType == this.data.activityType)[0].typeNameThai
    
    switch (this.data.activityType) {
      case 1:
        this.route.navigate(['/web-agent/register-upload-result'], { queryParams: { "IHRFCBJT": 'HETWRGQR', "GASDGETG": this.activityName } });
      break;
      case 2:
        this.route.navigate(['/web-agent/register-upload-result'], { queryParams: { "IHRFCBJT": 'YHDNHRDJ', "GASDGETG": this.activityName } });
      break;
      case 3:
        this.route.navigate(['/web-agent/register-upload-result'], { queryParams: { "IHRFCBJT": 'OKJHGFRB', "GASDGETG": this.activityName } });
      break;
      case 4:
        this.route.navigate(['/web-agent/register-upload-result'], { queryParams: { "IHRFCBJT": 'PMGFDHFN', "GASDGETG": this.activityName } });
      break;
      case 5:
        this.route.navigate(['/web-agent/register-upload-result'], { queryParams: { "IHRFCBJT": 'HMCBMDYK', "GASDGETG": this.activityName } });
      break;
      case 6:
        this.route.navigate(['/web-agent/register-upload-result'], { queryParams: { "IHRFCBJT": 'MV<VFGHR', "GASDGETG": this.activityName } });
      break;
      default:
        break;
    }
  }

  get isUpdate(): boolean{
    return this.form.value['activityId'] &&  this.form.value['activityId'] != '';
  }

  onSubmit(contentResult) {

    this.submitted = true

    // console.log("form invalid", this.form.value['activityDateTo']);
    if (this.form.invalid || ( this.form.value['activityDateTo'] =='' && this.form.value['numofdays'] > 1) ) {
      return
    }
    // console.log("form value", this.form.value);
    
    this.spinner.show();


    // Mapping ProvinceID
    let mapProvince = this.provinceList.filter(item => item['provinceName'] == this.form.value['provinceName']);
    let province ;
    if (mapProvince) {
      province = mapProvince[0]['provinceCd'];
    }

    let activityDate = this.datePipe.transform(this.form.value['activityDate'], 'dd-MMM-yyyy').toUpperCase()
    let activityRegisFrom = this.datePipe.transform(this.form.value['activityRegisFrom'], 'dd-MMM-yyyy').toUpperCase()
    let activityRegisTo = this.datePipe.transform(this.form.value['activityRegisTo'], 'dd-MMM-yyyy').toUpperCase()
    let activityDateTo = null;

    if(this.form.value['activityDateTo']){
       activityDateTo = this.datePipe.transform(this.form.value['activityDateTo'], 'dd-MMM-yyyy').toUpperCase()
    }
   

    const _postData = {
      'activityType':  +this.form.value['activityType'],
      'activitySubType':  this.form.value['activitySubType']  == undefined?'': undefined,
      'province': province,
      'activityName': this.form.value['activityName'],
      'activityDate': activityDate,
      'location': this.form.value['location'],
      'numOfDays': +this.form.value['numofdays'],
      'startTime': this.form.value['startTime'],
      'endTime': this.form.value['endTime'],
      'activityRegisFrom': activityRegisFrom,
      'activityRegisTo': activityRegisTo,
      'activityRegisToTime': this.form.value['activityRegisToTime'],
      'maxCapa': +this.form.value['maxCapa'],
      'roomCapa': +this.form.value['roomCapa'],
      'activityId': this.form.value['activityId'] == undefined?'': this.form.value['activityId'],
      'activityDateTo': activityDateTo,
      'remark' : this.form.value.remark
    };

    // console.log("_postData", _postData);
    
    // insert activity
    if( !this.form.value['activityId'] ){
      this.webAgentService.insertActivityDetail(_postData).subscribe(
        response => {
          // console.log('res. updateActivityDetail: ', response);
          if (response) {
            this.modalStatus = 'success';
            this.form.controls['activityId'].patchValue(response);
          } else {
            this.modalStatus = 'fail';
          }
          this.spinner.hide();
          this.modalService.open(contentResult, { centered: true, windowClass: "modal-status" });
        },
        error => {
          // console.log('err. updateActivityDetail: ', error);
          this.spinner.hide();
        }
      );
    // update activity
    }else{
      this.webAgentService.updateActivityDetail(_postData).subscribe(
        response => {
          // console.log('res. updateActivityDetail: ', response);
          if (response['message'] == 'success') {
            this.modalStatus = 'success';
          } else {
            this.modalStatus = 'fail';
          }
          this.spinner.hide();
          this.modalService.open(contentResult, { centered: true, windowClass: "modal-status" });
        },
        error => {
          // console.log('err. updateActivityDetail: ', error);
          this.spinner.hide();
        }
      );
    }

  }


  isSmartType() {
    if (this.typeId == '1') return true;
    else return false;
  }

  isReadOnlyForm() {
    // if (this.agentLevel === 'AGENT') return true;
    // else return false;
    return  !(this.authService.isRegisterAdmin() || this.authService.isRegisterAdmin(this.data.activityType) || this.authService.isRegisterAdmin(this.activityType))
  }

  getActivitySUbTypeList( activityType ){
    this.spinner.show();
    // console.log( activityType );
    this.webAgentService.getActivitySubTypeList(activityType).subscribe( r=>{
    
        this.dropdownActivitySubType = r;
        this.spinner.hide();
    });
  }


  apiGetProvinceList() {
    this.spinner.show();
    this.webAgentService.getProvinceList().subscribe(
      response => {

        this.provinceList = response;
        this.filterEventForm['province'] = response[0]['provinceCd'];
        this.spinner.hide();
      },
      error => {
        // console.log('err. getProvinceList: ', error);
        this.spinner.hide();
      }
    );
  }

  
  apiGetActivityList() {
    this.spinner.show();
    this.webAgentService.getActivityTypeList().subscribe(
      response => {
        // console.log('res. getActivityTypeList: ', response);
        this.eventTypeList = response;
        this.filterEventForm['type'] = response[0]['activityType'];
        this.spinner.hide();
      },
      error => {
        // console.log('err. getActivityTypeList: ', error);
        this.spinner.hide();
      }
    );
  }

  payInDateChange(val = ''){
    if (!val) return null;
    let _date;
    _date = new DatePipe('en-US').transform(val, 'dd/MM/yyyy')
    _date = _date.substring(0, 6) 
      + (Number.parseInt(_date.substring(6, 10)) + 543).toString()
    // console.log("payInDateChange", this.form.value.payInDate);
    // console.log("showPayInDate", _date);
    return _date;
  }
}
