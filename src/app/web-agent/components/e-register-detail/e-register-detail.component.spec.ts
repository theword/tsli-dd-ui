import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ERegisterDetailComponent } from './e-register-detail.component';

describe('ERegisterDetailComponent', () => {
  let component: ERegisterDetailComponent;
  let fixture: ComponentFixture<ERegisterDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ERegisterDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ERegisterDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
