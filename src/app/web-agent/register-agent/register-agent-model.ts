export class MockRegisterAgent {
  tableHeader = [
    'Item'
    , 'Name'
    , 'Location'
    , 'Date'
    , 'Time'
    , 'Qty'
    , 'Max qty'
    , 'Action'
  ];
  tableData = [
    {
      type: 'Smart Success'
      , typeId: '1'
      , item: '1'
      , name: 'Smart Success #34'
      , location: 'โรงแรมมิราเคิลแกรนด์'
      , province: 'กรุงเทพมหานคร'
      , date: '09/01/2564'
      , time: '11:00 - 16:00'
      , timeStart: '11:00'
      , timeEnd: '16:00'
      , qty: '350/601'
      , maxQty: '800'
      , duration: '1'
      , regisStartDate: '16/04/2564'
      , regisCloseDate: '30/04/2564'
    },
    {
      type: 'Smart Success'
      , typeId: '1'
      , item: '1002'
      , name: 'Smart Success #35'
      , location: 'Online'
      , province: ''
      , date: '09/01/2564'
      , time: '11:00 - 16:00'
      , timeStart: '11:00'
      , timeEnd: '16:00'
      , qty: '500/800'
      , maxQty: '2,000'
      , duration: '1'
      , regisStartDate: '16/04/2564'
      , regisCloseDate: '30/04/2564'
    },
    {
      type: 'อบรม (Training)'
      , typeId: '2'
      , item: '2001'
      , name: 'Standard Agent Course 1 (S.1)'
      , location: 'Online ผ่าน Program Zoom (Meeting ID 933 754 0699)'
      , province: ''
      , date: '07/05/2564'
      , time: '11:00 - 17:00'
      , timeStart: '11:00 '
      , timeEnd: '17:00'
      , qty: '0/50'
      , maxQty: '50'
      , duration: '1'
      , regisStartDate: '16/04/2564'
      , regisCloseDate: '30/04/2564'
    },
    {
      type: 'อบรมขอรับใบอนุญาตฯ (Licensing Training)'
      , typeId: '3'
      , item: '3001'
      , name: 'อบรมขอรับใบอนุญาตตัวแทนประกันชีวิต (L.1)'
      , location: 'Online ผ่าน Program Zoom (Meeting ID 523 192 5053)'
      , province: ''
      , date: '08/05/2564'
      , time: '08:30 - 16:30'
      , timeStart: '08:30'
      , timeEnd: '16:30'
      , qty: '0/50'
      , maxQty: '50'
      , duration: '1'
      , regisStartDate: '16/04/2564'
      , regisCloseDate: '30/04/2564'
    },
    {
      type: 'อบรมขอต่อใบอนุญาตฯ (Licensing Training)'
      , typeId: '3'
      , item: '4001'
      , name: 'อบรมขอต่อใบอนุญาตตัวแทนประกันชีวิต ครั้งที่ 2 (A.2)'
      , location: 'Online ผ่าน Program Zoom (Meeting ID 686 336 6891)'
      , province: ''
      , date: '08/05/2564'
      , time: '08:30 - 16:30'
      , timeStart: '08:30'
      , timeEnd: '16:30'
      , qty: '0/50'
      , maxQty: '50'
      , duration: '1'
      , regisStartDate: '16/04/2564'
      , regisCloseDate: '30/04/2564'
    },
    {
      type: 'สอบขอรับใบอนุญาตฯ (Licensing Exam)'
      , typeId: '4'
      , item: '5001'
      , name: 'สอบขอรับใบอนุญาตฯ รอบ 08.30 - 10.30 น. (เช้า) - In House'
      , location: 'สาขาบางกอกทาวเวอร์ ชั้น 15'
      , province: 'กรุงเทพมหานคร'
      , date: '15/05/2564'
      , time: '08:30 - 10:30'
      , timeStart: '08:30'
      , timeEnd: '10:30'
      , qty: '0/15'
      , maxQty: '15'
      , duration: '1'
      , regisStartDate: '16/04/2564'
      , regisCloseDate: '30/04/2564'
    },
  ];

  findByType(type: string) {
    if (type && type == 'ทั้งหมด') {
      return this.tableData;
    }
    else if (type && type.length > 0) {
      let _filter = this.tableData.filter(item => item.type == type);
      return _filter;
    } 
    else return this.tableData;
  }

  findByItem(id) {
    const _emptyData = {
      type: 'ไม่พบข้อมูล'
      , typeId: 'ไม่พบข้อมูล'
      , item: 'ไม่พบข้อมูล'
      , name: 'ไม่พบข้อมูล'
      , location: 'ไม่พบข้อมูล'
      , date: 'ไม่พบข้อมูล'
      , time: 'ไม่พบข้อมูล'
      , qty: 'ไม่พบข้อมูล'
      , maxQty: 'ไม่พบข้อมูล'
    }
    if (!id) return _emptyData;
    let _filter = this.tableData.filter( item => item.item == id);
    if (_filter.length < 1) return _emptyData;
    return _filter[0];
  }
}