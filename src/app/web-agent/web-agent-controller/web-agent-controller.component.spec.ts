import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebAgentControllerComponent } from './web-agent-controller.component';

describe('WebAgentControllerComponent', () => {
  let component: WebAgentControllerComponent;
  let fixture: ComponentFixture<WebAgentControllerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebAgentControllerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebAgentControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
