import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiRypComponent } from './kpi-ryp.component';

describe('KpiRypComponent', () => {
  let component: KpiRypComponent;
  let fixture: ComponentFixture<KpiRypComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiRypComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiRypComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
