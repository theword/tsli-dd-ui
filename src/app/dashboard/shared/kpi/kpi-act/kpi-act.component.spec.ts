import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiActComponent } from './kpi-act.component';

describe('KpiActComponent', () => {
  let component: KpiActComponent;
  let fixture: ComponentFixture<KpiActComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiActComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiActComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
