import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'displayAgentName' })
export class DisplayAgentNamePipe implements PipeTransform {
  transform(item: any, filterAgentLevel: any, currentColumnLevel, filterGroupType: string): any {
    if(!item) { return ''; }
    if(filterAgentLevel == null || filterAgentLevel === undefined || filterAgentLevel == null) return '';

    // WHOLE GROUP
    if (filterGroupType == 'wholeGroup') return item.sonName;

    // directGroup
    let agentName = '';
    if(currentColumnLevel == 'GM') agentName = item.gmName;
    else if(currentColumnLevel == 'AVP') agentName = item.avpName;
    else if(currentColumnLevel == 'AL') agentName = item.alName;
    else if(filterAgentLevel == currentColumnLevel ) return item.agentName;
    return agentName;
  }
}