import { Pipe, PipeTransform } from '@angular/core';
import { UtilitiesHelper } from 'src/app/core/utilities-helper';

@Pipe({ name: 'filterRetention' })
export class FilterRetentionPipe implements PipeTransform {
    transform(records: any, searchText: any): any {
        if(!records) { return []; }
        if(searchText == null || searchText === undefined || searchText == null) return records;

        return records.filter(function(data){
            return searchText == '' || searchText === undefined ||
            UtilitiesHelper.compareStr(data.chName,searchText) ||
            UtilitiesHelper.compareStr(data.cmRT,searchText) ||
            UtilitiesHelper.compareStr(data.lmRetention,searchText) ||
            UtilitiesHelper.compareStr(data.rtMonth01,searchText) ||
            UtilitiesHelper.compareStr(data.rtMonth02,searchText) ||
            UtilitiesHelper.compareStr(data.rtMonth03,searchText) ||
            UtilitiesHelper.compareStr(data.rtMonth04,searchText) ||
            UtilitiesHelper.compareStr(data.rtMonth05,searchText) ||
            UtilitiesHelper.compareStr(data.rtMonth06,searchText)
        })
      }
    }