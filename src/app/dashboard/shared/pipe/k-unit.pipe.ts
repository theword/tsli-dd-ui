import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'kUnit'
})
export class KUnitPipe implements PipeTransform {

  transform(value: any, args: any): any {
    if (!value && value != 0) return '';
    if (value == null || value == undefined) return '';

    // if ((parseInt(value) / 1000) == 0) return '0K';
    let _valKUnit = (parseInt(value) / 1000).toFixed(0);
    let _resString = _valKUnit.toString() + 'K';

    return _resString;
  }

}
