import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'displayAgentCode' })
export class DisplayAgentCodePipe implements PipeTransform {
  transform(item: any, filterAgentLevel: any, currentColumnLevel, filterGroupType: string): any {
    if(!item) { return ''; }
    if(filterAgentLevel == null || filterAgentLevel === undefined || filterAgentLevel == null) return '';

    // WHOLE GROUP
    if (filterGroupType == 'wholeGroup') return item.sonCode;
    
    // directGroup
    if(filterAgentLevel == currentColumnLevel ) return item.agentCode;

    let agentCode = '';
    if(currentColumnLevel == 'GM') agentCode = item.gmCode;
    else if(currentColumnLevel == 'AVP') agentCode = item.avpCode;
    else if(currentColumnLevel == 'AL') agentCode = item.alCode;
    return agentCode;
  }
}