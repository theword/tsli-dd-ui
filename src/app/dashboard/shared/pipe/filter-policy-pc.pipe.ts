import { Pipe, PipeTransform } from '@angular/core';
import { UtilitiesHelper } from 'src/app/core/utilities-helper';

@Pipe({ name: 'filterPolicyPc' })
export class FilterPolicyPcPipe implements PipeTransform {

  transform(records: any, searchText: any, masterLevel: any, accessLevel: any): any {
    if(!records) { return []; }
    if(searchText == null || searchText === undefined || searchText == null) return records;

    return records.filter(function(data){
      return searchText == '' || searchText === undefined ||
      ( 
        accessLevel.includes(masterLevel[0]) &&
        (UtilitiesHelper.compareStr(data.gmCode, searchText) || UtilitiesHelper.compareStr(data.gmName, searchText))
      ) ||
      ( 
        accessLevel.includes(masterLevel[1]) &&
        (UtilitiesHelper.compareStr(data.avpCode, searchText) || UtilitiesHelper.compareStr(data.avpName, searchText))
      ) ||
      ( 
        accessLevel.includes(masterLevel[2]) &&
        (UtilitiesHelper.compareStr(data.alCode, searchText) || UtilitiesHelper.compareStr(data.alName, searchText))
      ) ||
      ( 
        accessLevel.includes(masterLevel[3]) &&
        (UtilitiesHelper.compareStr(data.agentCode, searchText) || UtilitiesHelper.compareStr(data.agentName, searchText))
      ) ||
      UtilitiesHelper.compareStr(data.policyCode, searchText) ||
      UtilitiesHelper.compareStr(data.prodName, searchText) ||
      UtilitiesHelper.compareStr(data.pc, searchText) ||
      UtilitiesHelper.compareStr(data.submitDate, UtilitiesHelper.convertToRawDate(searchText)) ||
      UtilitiesHelper.compareStr(data.approveDate, UtilitiesHelper.convertToRawDate(searchText))
    })
  }

}
