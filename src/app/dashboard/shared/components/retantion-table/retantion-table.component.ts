import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { UtilitiesHelper } from 'src/app/core/utilities-helper';

@Component({
  selector: 'app-retantion-table',
  templateUrl: './retantion-table.component.html',
  styleUrls: ['./retantion-table.component.scss']
})
export class RetantionTableComponent implements OnInit {

  @Input() dataRetention: any;
  @Input() currentLevel: any;
  @Input() closYm: any;

  searchKeyword : any;
  lastMonth = 0;
  currentMonth = 0;
  monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  
  constructor(
  ) { }

  ngOnInit(): void {
  }
  ngOnChanges(changes: SimpleChanges) {
    var keys = Object.keys(this.closYm);
    if (keys.length > 0) {
      // console.log('CLOS_YM: ', this.closYm);
      
      this.lastMonth = parseInt(this.closYm.closYm_last) - 1;
      this.currentMonth = parseInt(this.closYm.closYm_current) - 1;
    }
  }



}
