import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetantionTableComponent } from './retantion-table.component';

describe('RetantionTableComponent', () => {
  let component: RetantionTableComponent;
  let fixture: ComponentFixture<RetantionTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetantionTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetantionTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
