import { Component, OnInit , NgModule, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  @Input() mode: string;
  @Input() listChannelBtn = [];
  @Input() listZoneBtn = [];
  @Input() listStatusBtn =[];
  @Input() listAgentLevelBtn =[];
  @Input() indexChannelSelected : any;
  @Input() ChannelFilterSelected : string;
  @Input() ZoneFilterSelected : string;

  @Input() agentLevel : string;
  @Input() filterKpiType : string;
  @Input() filterAgentLevel : string;

  @Input() listWholeGroupBtn = [];

  @Output() changeChannel = new EventEmitter();
  @Output() changeZone = new EventEmitter();
  @Output() changeStatus = new EventEmitter();
  @Output() changeAgentLevel = new EventEmitter();
  @Output() changeWholeGroup = new EventEmitter();

  constructor() {
    this.mode = 'company';
    this.ChannelFilterSelected = 'COMPANY';
    this.ZoneFilterSelected = 'COMPANY';
  }
  
  ngOnInit(): void {
  }
  

  onChangeChannel(code,id) {
    var params = {code : code, setId : id,zone :'COMPANY'};
    if (this.ChannelFilterSelected !== code || this.ZoneFilterSelected !== 'COMPANY') {
      this.ChannelFilterSelected = code;
      this.indexChannelSelected = id;
      this.ZoneFilterSelected = 'COMPANY';
    }
    this.changeChannel.emit(params);
  }

  onChangeZone(type) {
    if (this.ZoneFilterSelected !== type) {
      this.ZoneFilterSelected = type;
    }
    this.changeZone.emit(type);
  }

  onChangeStatus(status) {
    this.changeStatus.emit(status);
  }
  
  onChangeAgentLevel(agentLevel, type){
    const param = { agentLevel: agentLevel, type: type };
    this.changeAgentLevel.emit(param);
  }

  getFilterStatusStyle(item){
    if(item == 'PC')
      return this.filterKpiType == 'PC' ? 'btn-pcs' : 'btn-outline-pcs';
    if(item == 'ACTIVE')
      return this.filterKpiType == 'ACTIVE' ? 'btn-act' : 'btn-outline-act';
    if(item == 'NC')
      return this.filterKpiType == 'NC' ? 'btn-nc' : 'btn-outline-nc';
    if(item == 'RYP')
      return this.filterKpiType == 'RYP' ? 'btn-ryp' : 'btn-outline-ryp';
  }

  isAgentLevel () {
    if (this.agentLevel == 'AGENT' || this.agentLevel == 'AG') return true;
    else return false;
  }
  isZoneLevel () {
    if (this.agentLevel == 'ZONE') return true;
    else return false;
  }


}
