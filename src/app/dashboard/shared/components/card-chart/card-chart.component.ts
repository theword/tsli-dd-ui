import { log } from 'util';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CheckSessionStorageService } from 'src/app/core/service/check-session-storage.service';

@Component({
  selector: 'app-card-chart',
  templateUrl: './card-chart.component.html',
  styleUrls: ['./card-chart.component.scss']
})
export class CardChartComponent implements OnInit {

  @Input() kpiValueBox: any;
  @Input() renderChartType: string;
  @Input() dataCurrentYear = [];
  @Input() dataLastYear = [];
  @Input() dataTarget = [];
  @Input() isShowLastyear: boolean = true;
  @Input() selectLevel: string = '';
  @Input() isThousand: boolean = false;
  @Input() periodType: string = '';

  isAgent: boolean = false;
  subDetails : {}

  constructor(
    private router: Router,
    private checkSessionStorageService: CheckSessionStorageService) { }

  ngOnInit(): void {
    if (sessionStorage.getItem('agentLevelSelected') === 'AGENT') {
      this.isAgent = true;
      // console.log('SET isAgent');
    }

    // console.log('kpiValueBox',this.kpiValueBox);
    this.setButtonSubDetail(this.renderChartType);
    
  }

  setButtonSubDetail(nameKpiValueBox){
    if (nameKpiValueBox == 'PC'){
      this.subDetails = ['Approved', 'Pending']
    } else if (nameKpiValueBox == 'ACTIVE'){
      this.subDetails = []
    } else if (nameKpiValueBox == 'RYP'){
      this.subDetails = ['Approved', 'Pending', 'Lapse']
    } else if (nameKpiValueBox == 'RETENTION'){
      this.subDetails = ['Active', 'Non Active']
    } else if (nameKpiValueBox == 'NC'){
      this.subDetails = ['New Code', 'Temp', 'Trainee',  'Prospect']  
    }
  }

  // Redirect route method
  redirectToKPI(type: string) {
    let redirectionBasedOnKpiType = '/agent/kpidetail/main';
    let agentCode = sessionStorage.getItem('agentCodeSelected');
    let agentLevel = sessionStorage.getItem('agentLevelSelected');
    this.checkSessionStorageService.storeSessionChangePage(agentCode,agentLevel);

    switch (this.selectLevel) {
      case 'GM': case 'AVP': case 'AL': case 'AGENT':
        redirectionBasedOnKpiType = '/agent/kpidetail/main';
        break;
      
      case 'COMPANY': case 'CHANNEL': case 'ZONE':
        redirectionBasedOnKpiType = '/company/kpidetail/main';
        break;
    
      default:
        break;
    }
    
    let queryParams = [
      { kpiType: type}
    ];
    
    this.router.navigate([redirectionBasedOnKpiType], { 
      queryParams: {
        params: JSON.stringify(queryParams) 
      } 
    });
  }

  redirectToSubKPI(type: String, subDetailType: String){
    let redirectionBasedOnKpiType = '/kpi/subdetail/main';
    let agentCode = sessionStorage.getItem('agentCodeSelected');
    let agentLevel = sessionStorage.getItem('agentLevelSelected');
    this.checkSessionStorageService.storeSessionChangePage(agentCode, agentLevel);

    let queryParams = [
      { 
        kpiType: type, 
        subDetailType: subDetailType
      }
    ];
    
    this.router.navigate([redirectionBasedOnKpiType], { 
      queryParams: {
        params: JSON.stringify(queryParams) 
      } 
    });

  }

}
