import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-case-table',
  templateUrl: './case-table.component.html',
  styleUrls: ['./case-table.component.scss']
})
export class CaseTableComponent implements OnInit {

  @Input() noCaseData: any;
  @Input() caseSizeData: any;
  @Input() ytdData: any;

  @Input() isUseKUnit: boolean = false;
  
  constructor() { }

  ngOnInit(): void {
  }

}
