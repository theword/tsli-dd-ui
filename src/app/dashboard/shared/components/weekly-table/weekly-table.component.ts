import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-weekly-table',
  templateUrl: './weekly-table.component.html',
  styleUrls: ['./weekly-table.component.scss']
})
export class WeeklyTableComponent implements OnInit {

  @Input() data;
  @Input() monthName;
  @Input() year;

  datas = [{
    "actual1": 0,
    "actual2": 2324148,
    "actual3": 0,
    "actual4": null,
    "agentLevel": "ZONE",
    "gap1": -2250000,
    "gap2": 74148,
    "gap3": -2250000,
    "gap4": -2250000,
    "groupChannel": "SFC",
    "id": 1,
    "target1": null,
    "target2": 2250000,
    "target3": 2250000,
    "target4": 2250000,
    "zone": "0",
    "zoneName": "ZONE 0"
  }]

  constructor() {  }

  ngOnInit(): void {
       
  }

  displayValue(val) {
    if (val !== null) {
      let calculate = (val/1000).toFixed(0);
      let _value = parseInt(calculate).toLocaleString();
      return _value + 'K';
    } else {
      return ;
    }
  }

}
