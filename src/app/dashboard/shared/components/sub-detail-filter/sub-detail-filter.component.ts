import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-sub-detail-filter',
  templateUrl: './sub-detail-filter.component.html',
  styleUrls: ['./sub-detail-filter.component.scss']
})
export class SubDetailFilterComponent implements OnInit {

  @Input() kpiType: string = '';
  @Input() buttonList: any = [];
  @Input() filterKpiType: number = 0;
  @Output() clickFilter = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  handleFilterSelect(subDetailSelect: number) {
    if (this.filterKpiType != subDetailSelect) {
      this.filterKpiType = subDetailSelect;
      this.clickFilter.emit(subDetailSelect);
    }
  }

}
