import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubDetailFilterComponent } from './sub-detail-filter.component';

describe('SubDetailFilterComponent', () => {
  let component: SubDetailFilterComponent;
  let fixture: ComponentFixture<SubDetailFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubDetailFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubDetailFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
