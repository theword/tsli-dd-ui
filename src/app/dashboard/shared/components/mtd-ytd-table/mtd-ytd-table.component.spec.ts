import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MtdYtdTableComponent } from './mtd-ytd-table.component';

describe('MtdYtdTableComponent', () => {
  let component: MtdYtdTableComponent;
  let fixture: ComponentFixture<MtdYtdTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MtdYtdTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MtdYtdTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
