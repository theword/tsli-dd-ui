import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-mtd-ytd-table',
  templateUrl: './mtd-ytd-table.component.html',
  styleUrls: ['./mtd-ytd-table.component.scss']
})
export class MtdYtdTableComponent implements OnInit {

  @Input() kpiType: string = '';
  @Input() periodType: string = '';

  @Input() dataTarget: any;
  @Input() dataSubmit: any;
  @Input() dataCurrentApprove: any;
  @Input() dataLastApprove: any;
  @Input() dataPercent: any;
  @Input() dataYTD: any;
  @Input() isDyThousand: boolean = false;
  @Input() activeMonth: number = 0;
  @Input() groupType: string = 'INDIVIDUAL';

  // public currentYear = new Date().getFullYear();
  // public currentMonth = new Date().getMonth();

  constructor() { }

  ngOnInit(): void {
  }

}
