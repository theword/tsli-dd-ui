import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-daily-table',
  templateUrl: './daily-table.component.html',
  styleUrls: ['./daily-table.component.scss']
})
export class DailyTableComponent implements OnInit {

  @Input() agentLevel;
  @Input() data;
  @Input() totalLabel;
  
  constructor() { }

  ngOnInit(): void {
  }

}
