import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-minor-table',
  templateUrl: './minor-table.component.html',
  styleUrls: ['./minor-table.component.scss']
})
export class MinorTableComponent implements OnInit {

  @Input() targetSerie: any;
  @Input() showSubmit: boolean = true;
  @Input() submitSerie: any;
  @Input() approveCurrentSerie: any;
  @Input() approveLastSerie: any;
  @Input() approvePercentSerie: any;
  @Input() dataYTD: any;

  @Input() hideLastYear: boolean = true;
  @Input() tableType: string = 'normal';
  @Input() groupType: string = 'INDIVIDUAL';

  public currentYear = parseInt(sessionStorage.getItem('calendarCurrentYear'));

  constructor() { }

  ngOnInit(): void {
  }

}
