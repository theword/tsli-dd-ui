import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinorTableComponent } from './minor-table.component';

describe('MinorTableComponent', () => {
  let component: MinorTableComponent;
  let fixture: ComponentFixture<MinorTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinorTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinorTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
