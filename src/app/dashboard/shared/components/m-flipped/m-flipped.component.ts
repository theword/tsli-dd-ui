import { Component, OnInit } from '@angular/core';
import { LockEngineService } from 'src/app/core/lock-engine/lock.manager.service';

@Component({
  selector: 'app-m-flipped',
  templateUrl: './m-flipped.component.html',
  styleUrls: ['./m-flipped.component.scss']
})
export class MFlippedComponent implements OnInit {

  isVisible(): boolean {
    return this.lockEngineService.isDesktop;
  }
  
  constructor(
    private lockEngineService: LockEngineService
  ) { }

  ngOnInit(): void {
  }

}
