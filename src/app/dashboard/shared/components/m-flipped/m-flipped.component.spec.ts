import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MFlippedComponent } from './m-flipped.component';

describe('MFlippedComponent', () => {
  let component: MFlippedComponent;
  let fixture: ComponentFixture<MFlippedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MFlippedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MFlippedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
