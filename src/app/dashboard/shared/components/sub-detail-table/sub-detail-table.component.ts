import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AuthService } from 'src/app/core/security/auth.service';

@Component({
  selector: 'app-sub-detail-table',
  templateUrl: './sub-detail-table.component.html',
  styleUrls: ['./sub-detail-table.component.scss']
})
export class SubDetailTableComponent implements OnInit {

  @Input() data: any = [];
  @Input() first_headers: any = [];
  @Input() first_fieldNames: any = [];
  @Input() second_headers: any = [];

  @Input() agentLevel: any
  @Input() filterKpiType: number = 0;
  @Input() kpiType: any
  @Input() masterLevelName: any = [];

  @Input() pagingItem: {totalPage: 0, totalElements: 0, content: []};
  @Input() pagingCount: number = 1;
  @Input() isShowPaging: false;

  @Output() clickPaging = new EventEmitter<any>();
  @Output() clickMemo = new EventEmitter<any>();
  @Output() clickSorting = new EventEmitter<any>();

  itemPerPage: number = 500;

  public searchKeyword: string = '';

  private accessLevelList_GM: any = ['ZONE', 'GM'];
  private accessLevelList_AVP: any = ['ZONE', 'GM', 'AVP'];
  private accessLevelList_AL: any = ['ZONE', 'GM', 'AVP', 'AL'];
  private accessLevelList_AGENT: any = ['ZONE', 'GM', 'AVP', 'AL', 'AGENT'];

  prefixClosYm = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

  private sortTypeAsc: boolean = false;
  private sortTypeName: string = "DESC";

  constructor( private auth: AuthService) { 

    
  }

  get loginedName(){return this.auth.getLoginedName() };

  ngOnInit(): void {
    // console.log('kpiType',this.kpiType); 
  }

  // ngOnChanges(){
  //   console.log('first_headers',this.first_headers);
  //   console.log('first_fieldNames',this.first_fieldNames);
  //   console.log('second_headers',this.second_headers);
  //   console.log('kpiType',this.kpiType);
  // }

  convertStringToDate(date){
    if (date) {
      let formatDate = date.substring(0, 4) +'-'+ date.substring(4, 6) +'-'+ date.substring(6, 8);
      return formatDate;
    }
  }

  checkShowColumn(accessLevel: string) {
    switch (accessLevel) {
      case 'ZONE':
        if (this.agentLevel === 'ZONE') return true;
        else return false;

      case 'GM':
        return this.accessLevelList_GM.includes(this.agentLevel);
      
      case 'AVP':
        return this.accessLevelList_AVP.includes(this.agentLevel);

      case 'AL':
        return this.accessLevelList_AL.includes(this.agentLevel);

      case 'AGENT':
        return this.accessLevelList_AGENT.includes(this.agentLevel);
    }
  }

  checkKpiColumn(kpiType: string){
    if(kpiType=='NC' || kpiType=='RETENTION'){
      return true
    }
    else
      return false
  }

  toCrosYm(res){
   return this.prefixClosYm[res.substring(4,6) - 1] + '-' + res.substring(2,4);
  }

  toMonthFormat(month){
    if(month == 1){
      return 'st'
    } else if(month == 2){
      return 'nd'
    } else if (month == 3) {
      return 'rd'
    } else 
      return 'th'
  }

  itemInfo(): string {
    const start = this.printSequense();
    const end = start + this.itemPerPage - 1;
    return '' + start + '-' 
      + (end > this.pagingItem?.totalElements ? this.pagingItem?.totalElements : end ) + ' of ' + this.pagingItem?.totalElements;
  }

  printSequense() {
    return ((this.pagingCount) * this.itemPerPage) + 1;
  }

  paginationAction(type: string) {
    switch (type) {
      case 'firstPage':
        if (this.pagingCount == 0) return;
        else {
          this.clickPaging.emit(0);
        }
        break;
      case 'previous':
        if (this.pagingCount >= 1) {
          this.clickPaging.emit(this.pagingCount - 1);
        }
        break;
      case 'next':
        if (this.pagingCount < this.pagingItem.totalPage - 1) {
          this.clickPaging.emit(this.pagingCount + 1);
        } 
        else return;
        break;
      case 'endPage':
        if (this.pagingCount == this.pagingItem.totalPage) return;
        else {
          this.clickPaging.emit(this.pagingItem.totalPage - 1);
        }
        break;
    
      default:
        break;
    }
  }

  handleMemo(code: string, name: string, memoType: string) {
    let postData = {
      "agentCode": code,
      "memoType": memoType,
      "agentName": name
    }
    this.clickMemo.emit(postData);
  }

  handleSorting(colName: string) {
    this.swapSortingType();
    let postData = {
      // sortColName: this.convertToFieldName(colName),
      sortColName: this.convertToFieldName(colName),
      sortOrder: this.sortTypeName
    }
    // console.log('SORT.POST: ', postData);
    this.clickSorting.emit(postData);
  }

  swapSortingType() {
    if (this.sortTypeAsc) {
      this.sortTypeName = "ASC";
      this.sortTypeAsc = !this.sortTypeAsc;
    } else {
      this.sortTypeName = "DESC";
      this.sortTypeAsc = !this.sortTypeAsc;
    }
  }

  convertToFieldName(fieldName: string) {
    let _s = fieldName.replace(/([A-Z])/g, '_$1').trim().toUpperCase();
    return _s;
  }

  isCompOrChannel() {
    if (sessionStorage.getItem('agentLevelSelected') === 'ZONE') {
      let tempChannel = sessionStorage.getItem('channelFilterSelected');
      let tempZone = sessionStorage.getItem('zoneFilterSelected');
      if (tempChannel) {
        // Channel
        if (tempChannel !== 'COMPANY' && tempZone === 'COMPANY') {
          return true;
        } 
        // Company
        else if (tempChannel === 'COMPANY' && tempZone === 'COMPANY') {
          return true;
        }
        else {
          return false;
        }
      }
    }
  }

}
