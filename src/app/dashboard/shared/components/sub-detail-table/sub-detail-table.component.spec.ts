import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubDetailTableComponent } from './sub-detail-table.component';

describe('SubDetailTableComponent', () => {
  let component: SubDetailTableComponent;
  let fixture: ComponentFixture<SubDetailTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubDetailTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubDetailTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
