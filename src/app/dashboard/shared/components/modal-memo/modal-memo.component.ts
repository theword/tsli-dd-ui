import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-memo',
  templateUrl: './modal-memo.component.html',
  styleUrls: ['./modal-memo.component.scss']
})
export class ModalMemoComponent implements OnInit {

  @Input() memoOwner: any;
  @Input() memoDetail: any = [];

  constructor() { }

  ngOnInit(): void {
  }

  headerTypeConvert () {
    if (this.memoOwner?.memoType === 'MEMO_AGENT') return 'Memo แต่งตั้ง -';
    else return 'Memo OIC -';
  }

}
