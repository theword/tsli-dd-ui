import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KPIDetailTableComponent } from './kpidetail-table.component';

describe('KPIDetailTableComponent', () => {
  let component: KPIDetailTableComponent;
  let fixture: ComponentFixture<KPIDetailTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KPIDetailTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KPIDetailTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
