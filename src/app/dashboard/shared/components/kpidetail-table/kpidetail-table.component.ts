import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { KPIDetailTableModel } from 'src/app/core/model/model-kpi-detail-table';

@Component({
  selector: 'app-kpidetail-table',
  templateUrl: './kpidetail-table.component.html',
  styleUrls: ['./kpidetail-table.component.scss']
})
export class KPIDetailTableComponent implements OnInit {

  filterHeader  = '';
  @Input() table: KPIDetailTableModel;
  @Input() agentLevel: string = "AL";
  @Input() filterAgentLevel: string = 'AG/FC';
  @Input() filterKpiType :string = '';
  @Input() filterGroupType: string = '';

  @Output() onclickAgent = new EventEmitter();
  @Output() onClickTeam = new EventEmitter();

  constructor() { }

  setFilterHeader(header){
    // if ( this.filterHeader === header){
    //   this.filterHeader = undefined;
    // } else {
    this.filterHeader = header;
    // }
  }

  ngOnInit(): void {
  }

  selectAgent(item,level){
    // console.log('selectAgent: ACTIVATED...', item);
    var param = { item: item, level: level, groupType: this.filterGroupType};
    this.onclickAgent.emit(param);
  }

  selectTeam(event){
    // console.log('selectTeam: ACTIVATED...', event);
    // console.log('TYPE:...', this.filterGroupType);
    const _resEvent = {
      type: this.filterGroupType,
      detail: event
    };
    this.onClickTeam.emit(_resEvent);
  }

  checkIFShow(thisColLevel){
    /* use agent name for level filter that is selected, others normally use their name by level */
    let showColsLevel = ['AG'];
    if (this.filterAgentLevel == 'GM') {
      if(this.agentLevel == 'ZONE' || this.agentLevel == 'GM') showColsLevel = ['GM','VIEW'];
      else showColsLevel = [];
    } else if (this.filterAgentLevel == 'AVP') {
      if(this.agentLevel == 'ZONE') showColsLevel = ['GM','AVP','VIEW'];
      else if(this.agentLevel == 'GM' || this.agentLevel == 'AVP') showColsLevel = ['AVP','VIEW'];
      else showColsLevel = [];
    } else if (this.filterAgentLevel == 'AL') {
      if(this.agentLevel == 'ZONE') showColsLevel = ['GM','AVP','AL','VIEW'];
      else if(this.agentLevel == 'GM') showColsLevel = ['AVP','AL','VIEW'];
      else if (this.agentLevel == 'AVP' || this.agentLevel == 'AG') showColsLevel = ['AL','VIEW'];
      else showColsLevel = [this.filterAgentLevel];
    } else if (this.filterAgentLevel == 'AG') {
      if(this.agentLevel == 'ZONE') showColsLevel = ['GM','AVP','AL','AG'];
      else if(this.agentLevel == 'GM') showColsLevel = ['AVP','AL','AG'];
      else if(this.agentLevel == 'AVP') showColsLevel = ['AL','AG'];
      else showColsLevel = [this.filterAgentLevel];
    }
    return showColsLevel.includes(thisColLevel);
  }
  checkGroupShow() {
    // if (this.filterGroupType == 'wholeGroup') return true;
    // else return false;
    return false;
  }

  getTargetVal(item){
    if(this.filterKpiType == 'PC') return item.targetPc;
    if(this.filterKpiType == 'ACTIVE') return item.targetAct;
    if(this.filterKpiType == 'NC') return item.targetNc;
    if(this.filterKpiType == 'RYP') return item.targetRyp;
  }

  getSubmitVal(item){
    if(this.filterKpiType == 'PC') return item.pcSubmit;
  }

  getApproveVal(item){
    if(this.filterKpiType == 'PC') return item.pcApprove;
    if(this.filterKpiType == 'ACTIVE') return item.active;
    if(this.filterKpiType == 'NC') return item.nc;
    if(this.filterKpiType == 'RYP') return item.ryp;
  }

  getPercentVal(item){
    if(this.filterKpiType == 'PC') return item.pcPercent;
    if(this.filterKpiType == 'ACTIVE') return item.activePercent;
    if(this.filterKpiType == 'NC') return item.ncPercent;
    if(this.filterKpiType == 'RYP') return item.rypPercent;
  }

  getPers(item){
    if(this.filterKpiType == 'RYP') return item.prstRte;
  }

}
