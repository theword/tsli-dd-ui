import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { DataServiceService } from 'src/app/core/service/data-service.service';

@Component({
  selector: 'app-master-table',
  templateUrl: './master-table.component.html',
  styleUrls: ['./master-table.component.scss']
})
export class MasterTableComponent implements OnInit {

  isCollapsed = false;
  private REMARK_PARAMETER = "MASTER_TABLE_REMARK";
  remark = "";
  @Input() agentLevel;
  @Input() data;
  @Input() totalLabel;

  // isCollapsed: boolean = false;
  // tslint:disable-next-line:variable-name
  isCollapsed_arr: any = [];
  constructor( private dataServiceService: DataServiceService ) { 
    this.totalLabel = 'Total Comp';

    this.dataServiceService.getParameter(this.REMARK_PARAMETER).subscribe( (r:any)=>{
      this.remark = r;
    });
  }

  ngOnInit(): void {
    this.setInitailCollapsed(this.data);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnChanges(changes: SimpleChanges) {
    if (this.data.length > 1) {
      this.setInitailCollapsed(this.data);
    }
  }

  setInitailCollapsed(data) {
    this.isCollapsed_arr = [];
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < data.length; index++) {
      // tslint:disable-next-line:no-inferrable-types
      let id: number = 0;
      // tslint:disable-next-line:no-inferrable-types
      let status: boolean = true;
      if (data[index].id === undefined) {
        id = 0;
      } else {
        id = data[index].id;
      }
      if (data[index].staffLevel === 'COMPANY') {
        status = false;
      }
      // tslint:disable-next-line:prefer-const
      let tempCollap = {
        // tslint:disable-next-line:object-literal-shorthand
        id: id,
        isCollapsed: status
      };
      this.isCollapsed_arr.push(tempCollap);
    }
  }

  handleClickCollapsed(index) {
    this.isCollapsed_arr[index].isCollapsed = !this.isCollapsed_arr[index].isCollapsed;

  }

}


