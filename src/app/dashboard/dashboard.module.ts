import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../core/shared.module';
import { MainpageComponent } from '../theme/mainpage/mainpage.component';
import { AgDashboardComponent } from '../theme/ag-dashboard/ag-dashboard.component';
import { GmAlDashboardComponent } from '../theme/gm-al-dashboard/gm-al-dashboard.component';
import { ZoneAlKpiDetailComponent } from '../theme/zone-al-kpi-detail/zone-al-kpi-detail.component';
import { CompanyZoneDashboardComponent } from '../theme/company-zone-dashboard/company-zone-dashboard.component';
import { FilterComponent } from './shared/components/filter/filter.component';
import { BlockComponent } from './shared/components/block/block.component';
import { CardChartComponent } from './shared/components/card-chart/card-chart.component';
import { KPIComponent } from './shared/kpi/kpi.component';
import { KpiActComponent } from './shared/kpi/kpi-act/kpi-act.component';
import { KpiPcComponent } from './shared/kpi/kpi-pc/kpi-pc.component';
import { KpiRypComponent } from './shared/kpi/kpi-ryp/kpi-ryp.component';
import { MasterTableComponent } from './shared/components/master-table/master-table.component';
import { ProducerGuards } from '../core/security/producer.guards';
import { KPIDetailTableComponent } from './shared/components/kpidetail-table/kpidetail-table.component';
import { AuthGuards } from '../core/security/auth.guards';
import { GmGuards } from '../core/security/gm.guards';
import { AvpGuards } from '../core/security/avp.guards';
import { AlGuards } from '../core/security/al.guards';
import { StaffGuards } from '../core/security/staff.guards';
import { DailyTableComponent } from './shared/components/daily-table/daily-table.component';
import { CommonModule } from '@angular/common';
import { WeeklyTableComponent } from './shared/components/weekly-table/weekly-table.component';


const routes: Routes = [

  // { path: 'home', component: MainpageComponent, canActivate: [] },
  // START Agent DASHBOARD
  { path: 'producer', component: GmAlDashboardComponent, canActivate: [AuthGuards, ProducerGuards] },
  { path: 'staff', component: CompanyZoneDashboardComponent, canActivate: [AuthGuards, StaffGuards] },
  {
    path: 'producer/kpidetail',
    component: ZoneAlKpiDetailComponent,
    canActivate: [AuthGuards],
  },
  {
    path: 'producer/kpidetail/ag',
    component: ZoneAlKpiDetailComponent,
    canActivate: [AuthGuards, ProducerGuards],
  },
  {
    path: 'producer/kpidetail/al',
    component: ZoneAlKpiDetailComponent,
    canActivate: [AuthGuards, AlGuards],
  },
  {
    path: 'producer/kpidetail/avp',
    component: ZoneAlKpiDetailComponent,
    canActivate: [AuthGuards, GmGuards, AvpGuards],
  },
  {
    path: 'producer/kpidetail/gm',
    component: ZoneAlKpiDetailComponent,
    canActivate: [AuthGuards, GmGuards],
  },
  { path: 'agent', component: AgDashboardComponent, canActivate: [AuthGuards] },

  { path: '**', redirectTo: 'staff', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    AgDashboardComponent,
    MainpageComponent,
    GmAlDashboardComponent,
    ZoneAlKpiDetailComponent,
    CompanyZoneDashboardComponent,
    FilterComponent,
    BlockComponent,
    CardChartComponent,
    KPIComponent,
    KpiActComponent,
    KpiPcComponent,
    KpiRypComponent,
    MasterTableComponent,
    KPIDetailTableComponent,
    DailyTableComponent,
    WeeklyTableComponent,
  ],

  imports: [RouterModule.forChild(routes), SharedModule, CommonModule],
  exports: [],
  providers: [],
})
export class DashboardModule {}
