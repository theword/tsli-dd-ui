import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetYearControllerComponent } from './target-year-controller.component';

describe('TargetYearControllerComponent', () => {
  let component: TargetYearControllerComponent;
  let fixture: ComponentFixture<TargetYearControllerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TargetYearControllerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetYearControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
