import { Component, OnInit , Input, Output} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModelUser } from 'src/app/core/model/model-user';
import { AuthService } from 'src/app/core/security/auth.service';
import { DashboardBackRouteService } from 'src/app/core/service/dashboard-back-route.service';
import { Location } from '@angular/common';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesHelper } from 'src/app/core/utilities-helper';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-target-year-controller',
  templateUrl: './target-year-controller.component.html',
  styleUrls: ['./target-year-controller.component.scss']
})
export class TargetYearControllerComponent implements OnInit {

  // Cores
  public agentName: string;
  public agentLevel: string;
  public agentCode: string;
  public yearCalendar: string;
  public lastYearCalendar: string;

  // Paginated and search
  public searchKeyword: string = '';
  public pagingCount: number = 0;
  public totalPage: number = 0;

  // Master Target
  public masterCurPc: any = [];
  public masterLastPc: any = [];
  public masterCurActive: any = [];
  public masterLastActive: any = [];
  public masterCurNc: any = [];
  public masterLastNc: any = [];
  public masterCurRETENTION: any = [];
  public masterLastRETENTION: any = [];
  public masterCurRYP: any = [];
  public masterLastRYP: any = [];
  public UpdateBy: any = [];

  _resData;
  agCode : any;
  displayLvName: any = '';
  titleCode: string;
  closeResult = '';
  dataModal :any = [];
  data :any = [];
  status: any;
  prstRte:any
  
  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private dataService: DataServiceService,
    private dashboardBackRouteService: DashboardBackRouteService,
    private location: Location,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
  ) {
  }

  ngOnInit(): void {
    // Get localstorage value from service
    this.agentCode = this.authService.getLoginedName();
    this.agentLevel = this.authService.getPosition();
    this.agentName = this.authService.getName();

    // Get AgentList
    let sortDesc = { sortColName: '', sortOrder: '' };
    this.apiService_getTargetAgentListStructure(sortDesc);
    this.apiService_getClosCalendar();

    this.prstRte = sessionStorage.getItem('prstRte')
  }

  // API Services
  apiService_getClosCalendar() {
    this.dataService.getClosYmInfo().subscribe(
      response => {
        this.yearCalendar = Object.values(response)[0].toString().substring(0, 4);
        this.lastYearCalendar = (parseInt(this.yearCalendar) - 1).toString();
      },
      error => {
        console.log('getPerformanceDaily: ', error);
      }
    );
  }
  apiService_getTargetAgentListStructure(sort) {
    this.spinner.show();
    const _postData = {
      "agentCode": this.agentCode,
      "paginationPage": this.pagingCount,
      "sortColName": sort['sortColName'],
      "sortOrderType": sort['sortOrder']
    };
    
    switch (this.agentLevel) {
      case 'AGENT':
        this.dataService.postTargetAgentListAg(_postData).subscribe(
          response => {
            this._resData = response
            this.data = this._resData.content;
            this.spinner.hide();
            console.log('res. postTargetAgentListAg: ', response);
          },
          error => {
            console.log('err. postTargetAgentListAg: ', error);
          });
        break;
      case 'AL':
        this.dataService.postTargetAgentListAl(_postData).subscribe(
          response => {
            this._resData = response
            this.data = this._resData.content;
            this.spinner.hide();
            console.log('res. postTargetAgentListAl: ', response);
          },
          error => {
            console.log('err. postTargetAgentListAl: ', error);
          });
        break;
      case 'AVP':
        this.dataService.postTargetAgentListAvp(_postData).subscribe(
          response => {
            this._resData = response
            this.data = this._resData.content;
            this.spinner.hide();
            console.log('res. postTargetAgentListAvp: ', response);
          },
          error => {
            console.log('err. postTargetAgentListAvp: ', error);
          });
        break;
      case 'GM':
        this.dataService.postTargetAgentListGm(_postData).subscribe(
          response => {
            this._resData = response
            this.data = this._resData.content;
            this.spinner.hide();
            console.log('res. postTargetAgentListGm: ', response);
          },
          error => {
            console.log('err. postTargetAgentListGm: ', error);
          });
        break;
      case 'ZONE':
        this.dataService.postTargetAgentListZone(_postData).subscribe(
          response => {
            this._resData = response
            this.data = this._resData.content;
            this.spinner.hide();
            console.log('res. postTargetAgentListZone: ', response);
          },
          error => {
            console.log('err. postTargetAgentListZone: ', error);
          });
        break;
      default:
        break;
    }
  }
  apiService_searchAgentList(sort={sortColName: '', sortOrder: ''}) {
    if (this.searchKeyword.toString().length > 0) {
      this.spinner.show();
      var _prefixSearch = '%' + this.searchKeyword + '%';
      var _resSearchData;
      this.data = [];

      // Declare postData (searchInfomation)
      const _postData = {
        "agentCode": this.agentCode,
        "searchKeyword": _prefixSearch,
        "paginationPage": this.pagingCount,
        "sortColName": sort['sortColName'],
        "sortOrderType": sort['sortOrder']
      };

      switch (this.agentLevel.toString()) {
        case 'AGENT':
          this.dataService.searchTargetAgentListAg(_postData).subscribe(
            response => {
              this.spinner.hide();
              this._resData = response;
              _resSearchData = response;
              this.data = _resSearchData.content;
              console.log('res. searchTargetAgentListAg: ', response);
            },
            error => {
              console.log('err. searchTargetAgentListAg: ', error);
            }
          );
          break;
        case 'AL':
          this.dataService.searchTargetAgentListAl(_postData).subscribe(
            response => {
              this.spinner.hide();
              this._resData = response;
              _resSearchData = response;
              this.data = _resSearchData.content;
              console.log('res. searchTargetAgentListAl: ', response);
            },
            error => {
              console.log('err. searchTargetAgentListAl: ', error);
            }
          );
          break;
        case 'AVP':
          this.dataService.searchTargetAgentListAvp(_postData).subscribe(
            response => {
              this.spinner.hide();
              this._resData = response;
              _resSearchData = response;
              this.data = _resSearchData.content;
              console.log('res. searchTargetAgentListAvp: ', response);
            },
            error => {
              console.log('err. searchTargetAgentListAvp: ', error);
            }
          );
          break;
        case 'GM':
          this.dataService.searchTargetAgentListGm(_postData).subscribe(
            response => {
              this.spinner.hide();
              this._resData = response;
              _resSearchData = response;
              this.data = _resSearchData.content;
              console.log('res. searchTargetAgentListGm: ', response);
            },
            error => {
              console.log('err. searchTargetAgentListGm: ', error);
            }
          );
          break;
        case 'ZONE':
          const _zonePostData = {
            "searchKeyword": _prefixSearch,
            "paginationPage": this.pagingCount,
            "sortColName": sort['sortColName'],
            "sortOrderType": sort['sortOrder']
          };
          this.dataService.searchTargetAgentListZone(_zonePostData).subscribe(
            response => {
              this.spinner.hide();
              this._resData = response;
              _resSearchData = response;
              this.data = _resSearchData.content;
              console.log('res. searchTargetAgentListZone: ', response);
            },
            error => {
              console.log('err. searchTargetAgentListZone: ', error);
            }
          );
          break;
      
        default:
          break;
      }
    } else {
      let sortDesc = { sortColName: '', sortOrder: '' };
      this.apiService_getTargetAgentListStructure(sortDesc);
    }
  }
  apiService_getTargetByAgent(content, agentCode: string, agentLevel: string) {
    var _resData;
    if (agentLevel.length > 0) {
      this.spinner.show();
      const _postData = {
        "agentCode": agentCode,
        "agentLevel": agentLevel,
        "closYm": this.yearCalendar
      };
      this.dataService.postTargetYearByAgentCode(_postData).subscribe(
        response => {
          this.spinner.hide();
          this.masterCurPc = this.getKpiCrossYear(response['currentYear'], 'PC', this.yearCalendar);
          this.masterLastPc = response['pcLastYear'];
          this.masterCurActive = this.getKpiCrossYear(response['currentYear'], 'NEW_ACTIVE_AGENT', this.yearCalendar);
          this.masterLastActive = response['activeLastYear'];
          this.masterCurNc = this.getKpiCrossYear(response['currentYear'], 'NEW_CODE', this.yearCalendar);
          this.masterLastNc = response['ncLastYear'];

          this.masterCurRETENTION = this.getKpiCrossYear(response['currentYear'], 'RETENTION', this.yearCalendar);
          this.masterLastRETENTION = response['retentionLastYear'];
          this.masterCurRYP = this.getKpiCrossYear(response['currentYear'], 'RYP', this.yearCalendar);
          this.masterLastRYP = response['rypLastYear'];
          
          // this.masterCurRETENTION = [0,0,0,0,0,0,0,0,0,0,0,0]
          // this.masterLastRETENTION = [0,0,0,0,0,0,0,0,0,0,0,0]
          // this.masterCurRYP = [0,0,0,0,0,0,0,0,0,0,0,0]
          // this.masterLastRYP = [0,0,0,0,0,0,0,0,0,0,0,0]

          _resData = response['currentYear'][0];
          // UpdateBy
          this.UpdateBy = _resData;

          // console.log('res. postTargetYearByAgentCode: ', response);

          // Open Modal
          this.modalService.open(content, {centered: true, windowClass : "my-class" }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
          }, (reason) => {
            this.closeResult = `Dismissed`;
          });
        },
        error => {
          console.log('err. postTargetYearByAgentCode: ', error);
        });
    }
  }
  apiService_updatePcTargetYear(content,agentCode: string, listPcTargetYear: any = []) {
    if (listPcTargetYear.length > 0) {
      const _sumPcTarget = listPcTargetYear.reduce((a, b) => a + b, 0);
      const _postData = {
        "agentCode": agentCode,
        "agentLevel": 'AGENT',
        "closYm": this.yearCalendar,
        "pcSumYearTarget": _sumPcTarget,
        "pcYearTarget": listPcTargetYear
      };
      this.dataService.postUpdateTargetYear(_postData).subscribe(
        response => {
          this.spinner.hide();
          console.log('res. postUpdateTargetYear: ', response[0]);
          if(response[0] =='success'){
            this.status = 'success'
          }
          else{
            this.status = 'fail'
          }
          this.modalService.open(content, {centered: true, windowClass : "modal-status" }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
          }, (reason) => {
            this.closeResult = `Dismissed`;
          });
        },
        error => {
          console.log('err. postUpdateTargetYear: ', error);
        }
      );
    }
  }
  apiService_updateAllTargetYear(content, agentCode: string, agentLevel: string
    , listPcTargetYear: any = [], listActiveTargetYear: any = [], listNcTargetYear: any = []
    , listRetentionTargetYear: any = [], listRypTargetYear: any = []
  ) {
    if (listPcTargetYear.length > 0 && listActiveTargetYear.length > 0 && listNcTargetYear.length > 0 && listRypTargetYear.length > 0 && listRetentionTargetYear.length > 0) {
      const _sumPcTarget = listPcTargetYear.reduce((a, b) => a + b, 0);
      const _sumActiveTarget = listActiveTargetYear.reduce((a, b) => a + b, 0);
      const _sumNcTarget = listNcTargetYear.reduce((a, b) => a + b, 0);
      const _sumRypTarget = listRypTargetYear.reduce((a, b) => a + b, 0);
      const _sumRetentionTarget = listRetentionTargetYear.reduce((a, b) => a + b, 0);
      const _postData = {
        "agentCode": agentCode,
        "agentLevel": agentLevel,
        "closYm": this.yearCalendar,
        "pcSumYearTarget": _sumPcTarget,
        "activeSumYearTarget": _sumActiveTarget,
        "ncSumYearTarget": _sumNcTarget,
        "rypSumYearTarget": _sumRypTarget,
        "retentionSumYearTarget": _sumRetentionTarget,
        "pcYearTarget": listPcTargetYear,
        "activeYearTarget": listActiveTargetYear,
        "ncYearTarget": listNcTargetYear,
        "rypYearTarget": listRypTargetYear,
        "retentionYearTarget": listRetentionTargetYear,
      };
      console.log('POST DATA: ', _postData);
      
      this.dataService.postUpdateAllTargetYear(_postData).subscribe(
        response => {
          this.spinner.hide();
          console.log('res. postUpdateAllTargetYear: ', response[0]);
          if(response[0] =='success'){
            this.status = 'success'
          }
          else{
            this.status = 'fail'
          }
          this.modalService.open(content, {centered: true, windowClass : "modal-status" }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
          }, (reason) => {
            this.closeResult = `Dismissed`;
          });
        },
        error => {
          console.log('err. postUpdateAllTargetYear: ', error);
        }
      );
    }
  }

  // Utilities
  getKpiCrossYear(list, kpiName: string, productionYear: string) {
    var _listKpiVal = [];
    if (list) {
      for (let index = 0; index < list.length; index++) {
        if (list[index].kpiName == kpiName && list[index].productionYear == productionYear) {
          var _scopeKpi = list[index];
          // console.log('FOUND: ', kpiName, ' IN ', productionYear);
          var mCount = 1;
          for (const key in _scopeKpi) {
            var mName = 'targetM' + mCount;
            if (key == mName) {
              // console.log('# ', mCount, ' = ', _scopeKpi[mName]);
              _listKpiVal.push(_scopeKpi[mName]);
              mCount++;
            }
          }
        } // End if
      } // End for
    }
    return _listKpiVal;
  }

  // Route Controllers
  goToHome() {
    let userRole = this.authService.getPosition();
    let agentCode = this.authService.getLoginedName();
    this.dashboardBackRouteService.backHome(userRole, agentCode);
  }
  goToOverview() {
    this.location.back();
  }

  // Modal Controller
  openModal(content, valEmitted){
    this.agCode = valEmitted;
    this.apiService_getTargetByAgent(content, valEmitted.agentCode, valEmitted.agentLevel);
  }

  // Handler Emit
  handle_Page(valEmitted){
    this.pagingCount = valEmitted
    let sortDesc = { sortColName: '', sortOrder: '' };
    if (this.searchKeyword.toString().length > 0) {
      this.apiService_searchAgentList(sortDesc);
    } else {
      this.apiService_getTargetAgentListStructure(sortDesc);
    }
  }
  handle_target(content,valEmitted) {
    this.modalService.dismissAll();
    if (valEmitted.TYPE === 'AGENT') {
      this.apiService_updatePcTargetYear(content, this.agCode.agentCode, valEmitted.PC);
    } else {
      this.apiService_updateAllTargetYear(content, this.agCode.agentCode
        , valEmitted.TYPE
        , valEmitted.PC
        , valEmitted.Active
        , valEmitted.NC
        , valEmitted.RETENTION
        , valEmitted.RYP);
    }
  }
  handlerSorting(sortDesc) {
    if (this.searchKeyword.length > 0) {
      this.apiService_searchAgentList(sortDesc);
    } else {
      this.apiService_getTargetAgentListStructure(sortDesc);
    }
  }
  

}