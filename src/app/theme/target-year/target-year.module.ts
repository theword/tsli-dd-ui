import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TargetYearControllerComponent } from './target-year-controller/target-year-controller.component';
import { TargetYearTableComponent } from './components/target-year-table/target-year-table.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/core/shared.module';
import { ModalTargetYearComponent } from './components/modal-target-year/modal-target-year.component';

const routes: Routes = [
  {
    path: '',
    component: TargetYearControllerComponent
  },
  {
    path: '**', redirectTo: 'error', pathMatch: 'full'
  }
]

@NgModule({
  declarations: [
    TargetYearControllerComponent, 
    TargetYearTableComponent, 
    ModalTargetYearComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    CommonModule,
  ]
})
export class TargetYearModule { }
