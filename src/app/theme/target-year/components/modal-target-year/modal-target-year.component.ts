import { Component, Input, OnInit, SimpleChanges , Output , EventEmitter} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-target-year',
  templateUrl: './modal-target-year.component.html',
  styleUrls: ['./modal-target-year.component.scss']
})
export class ModalTargetYearComponent implements OnInit {

 

  @Input() id: string;
  @Input() name: string;
  @Input() agCode: any;
  @Input() first_header = [];
  @Input() second_header: any = [];
  @Input() target_Agent_Level = '';
  @Input() masterCurPc: any = [];
  @Input() masterLastPc: any = [];
  @Input() masterCurActive: any = [];
  @Input() masterLastActive: any = [];
  @Input() masterCurNc: any = [];
  @Input() masterLastNc: any = [];
  @Input() masterCurRYP: any = [];
  @Input() masterLastRYP: any = [];
  @Input() masterCurRETENTION: any = [];
  @Input() masterLastRETENTION: any = [];
  @Input() lastYearCalendar: string;
  @Input() yearCalendar: string;
  sumCurPC: any = 0;
  sumLastPC: any = 0;
  sumCurActive: any = 0;
  sumLastActive: any = 0;
  sumCurNc: any = 0;
  sumLastNc: any = 0;
  sumCurRYP: any = 0;
  sumLastRYP: any = 0;
  sumCurRETENTION: any = 0;
  sumLastRETENTION: any = 0;
  @Input() updateBy: any;

  public isActiveError: boolean = false;
  @Output() postData = new EventEmitter<any>();

  month = ['January','February','March','April','May','June','July','August','September','October','November','December'];
  header_Modal = [
    [["PC","ACTIVE","NC","RETENTION","RYP"],
    []], 
    [["PC"],
    []]
  ]
  _Data = [];
  isDisable = true

  public validatePC = new Array(12);
  public tempPC = [];
  
  public validateAT = new Array(12);
  public tempAT = [];

  public validateNC = new Array(12);
  public tempNC = [];

  public validateRYP = new Array(12);
  public tempRYP = [];

  public validateRETENTION = new Array(12);
  public tempRETENTION = [];

  constructor(   private modalService: NgbModal   ) { }

  ngOnInit(): void {
    // console.log("agentCode-Modal",this.agCode.agentLevel);
    //console.log('NAME: ', this.name);
    this.callAPI_tragetFilterTest(this.agCode)
    // this.targetValidate(100)
    this.setInitValidateKpi();
    // console.log('VALIDATE-STATUS-PC ', this.validatePC);
    //console.log('MASTER PC ', this.tempPC);
    this.sumCurPC = this.calSumTarget(this.sumCurPC,this.masterCurPc);
    this.sumLastPC = this.calSumTarget(this.sumLastPC,this.masterLastPc);
    this.sumCurActive = this.calSumTarget(this.sumCurActive,this.masterCurActive);
    this.sumLastActive = this.calSumTarget(this.sumLastActive,this.masterLastActive);
    this.sumCurNc = this.calSumTarget(this.sumCurNc,this.masterCurNc);
    this.sumLastNc = this.calSumTarget(this.sumLastNc,this.masterLastNc);
    this.sumCurRYP = this.calSumTarget(this.sumCurRYP,this.masterCurRYP);
    this.sumLastRYP = this.calSumTarget(this.sumLastRYP,this.masterLastRYP);
    this.sumCurRETENTION = this.calSumTarget(this.sumCurRETENTION,this.masterCurRETENTION);
    this.sumLastRETENTION = this.calSumTarget(this.sumLastRETENTION,this.masterLastRETENTION);
    // console.log("xxxx",this.agCode);
    
  }
  
  ngOnChanges(changes: SimpleChanges): void {
    if (this.masterCurPc.length > 0) {
      this.tempPC = [...this.masterCurPc];
    }
    if (this.masterCurActive.length > 0) {
      this.tempAT = [...this.masterCurActive];
    }
    if (this.masterCurNc.length > 0) {
      this.tempNC = [...this.masterCurNc];
    }
    if (this.masterCurRETENTION.length > 0) {
      this.tempRETENTION = [...this.masterCurRETENTION];
    }
    if (this.masterCurRYP.length > 0) {
      this.tempRYP = [...this.masterCurRYP];
    }
  }

  callAPI_tragetFilterTest(agCode){
    // this.dataService.getButtonName_agentLevel().subscribe(
    //   response => {
    //     // console.log('res. getButtonName_agentLevel: ', response);
    //     if (Object.keys(response).length > 0) {
    //       let _levelArr = [];
    //       let _agentLevel = this.modelUser.getAgentLevel();
    //target_Agent_Level = parameter
    this.target_Agent_Level = agCode.agentLevel
    if (this.target_Agent_Level == 'AGENT'){
      this.first_header = this.header_Modal[1][0];
      this.header_Modal[1][1].push(this.lastYearCalendar,this.yearCalendar);
      this.second_header = this.header_Modal[1][1];
    } else if (this.target_Agent_Level != 'AGENT'){
      this.first_header = this.header_Modal[0][0];
      this.header_Modal[0][1].push(this.lastYearCalendar,this.yearCalendar,this.lastYearCalendar,this.yearCalendar,this.lastYearCalendar,this.yearCalendar,this.lastYearCalendar,this.yearCalendar,this.lastYearCalendar,this.yearCalendar);
      this.second_header = this.header_Modal[0][1];
      console.log("second_header", this.second_header);
      
    }
  }

  checkNewTarget(newVal, type, index) {
    newVal = (''+newVal).replace(/\D/g, '');
    console.log(newVal);
    if(newVal.length == 0){
      // console.log("Emthy");
      if(type=='PC'){
        this.tempPC[index] = this.masterCurPc[index]
      }
      if(type=='AT'){
        this.tempAT[index] = this.masterCurActive[index]
      }
      if(type=='NC'){
        this.tempNC[index] = this.masterCurNc[index]
      }
      if(type=='RYP'){
        this.tempRYP[index] = this.masterCurRYP[index]
      }
      if(type=='RETENTION'){
        this.tempRETENTION[index] = this.masterCurRETENTION[index]
      }
      this.isDisable = false
      // console.log("isDisable",this.isDisable);
      
    }
    switch(type.toString()) {
      case 'PC':
        if (/*this.calMorethan(newVal, this.masterCurPc[index]) && */newVal.substr(0,1)!='0') {
          this.tempPC[index] = parseInt(newVal);;
          this.validatePC[index] = true;
          // console.log('ACCEPT PC...# ', index, ' = ', newVal);
          this.isDisable = false
          this.sumCurPC = this.totalSumTarget(this.tempPC);
        } else {
          this.validatePC[index] = false;
          this.isDisable = true
        }
        break;
      case 'AT':
        if (/*this.calMorethan(newVal, this.masterCurActive[index]) &&*/ newVal.substr(0,1)!='0') {
          this.tempAT[index] = parseInt(newVal);;
          this.validateAT[index] = true;
          // console.log('ACCEPT AT...# ', index, ' = ', newVal);
          this.isDisable = false
          this.sumCurActive = this.totalSumTarget(this.tempAT);
        } else {
          this.validateAT[index] = false;
          this.isDisable = true;
        }
        break;
      case 'NC':
        if (/*this.calMorethan(newVal, this.masterCurNc[index]) &&*/ newVal.substr(0,1)!='0') {
          this.tempNC[index] = parseInt(newVal);
          if (this.tempNC[index] == NaN){
            this.tempNC[index] = 0
          }
          console.log("this.tempNC[index] : ", this.tempNC);
          
          this.validateNC[index] = true;
          // console.log('ACCEPT NC...# ', index, ' = ', newVal);
          this.isDisable = false
          this.sumCurNc = this.totalSumTarget(this.tempNC);
        } else {
          this.validateNC[index] = false;
          this.isDisable = true
        }
        break;
      case 'RYP':
        if (/*this.calMorethan(newVal, this.masterCurNc[index]) &&*/ newVal.substr(0,1)!='0') {
          this.tempRYP[index] = parseInt(newVal);
          if (this.tempRYP[index] == NaN){
            this.tempRYP[index] = 0
          }
          console.log("this.tempRYP[index] : ", this.tempRYP);
          
          this.validateRYP[index] = true;
          // console.log('ACCEPT NC...# ', index, ' = ', newVal);
          this.isDisable = false
          this.sumCurRYP = this.totalSumTarget(this.tempRYP);
          // this.sumCurRYP = 0
        } else {
          this.validateRYP[index] = false;
          this.isDisable = true
        }
        break;
      case 'RETENTION':
        if (/*this.calMorethan(newVal, this.masterCurNc[index]) &&*/ newVal.substr(0,1)!='0') {
          this.tempRETENTION[index] = parseInt(newVal);
          if (this.tempRETENTION[index] == NaN){
            this.tempRETENTION[index] = 0
          }
          console.log("this.tempRETENTION[index] : ", this.tempRETENTION);
          
          this.validateRETENTION[index] = true;
          // console.log('ACCEPT NC...# ', index, ' = ', newVal);
          this.isDisable = false
          this.sumCurRETENTION = this.totalSumTarget(this.tempRETENTION);
          // this.sumCurRETENTION = 0
        } else {
          this.validateRETENTION[index] = false;
          this.isDisable = true
        }
        break;
      default:
        break;
    }

    if(newVal.length == 0){
      console.log("Empty");
      
      this.isDisable = false
      console.log("isDisable",this.isDisable);
      
    }
  }

  // Calculate Function
  calMorethan(newVal, oldVal) {
    if (parseFloat(newVal) >= parseFloat(oldVal)) return true;
    else return false;
  }

  // Utilities
  setInitValidateKpi() {
    this.validatePC.fill(true);
    this.validateAT.fill(true);
    this.validateNC.fill(true);
  }

  setMyStyle(f_header :string){
    let styles = {
      'background-color' : f_header=='PC' ? '#1b5e20' : f_header == 'ACTIVE' ? '#b71c1c ' :  f_header == 'NC' ? '#d18c00' : f_header == 'RYP' ? '#6A1B9A' : '#12366D'
    }
    return styles
  }

  compareSave(){
    var _postData = {
      "TYPE": this.agCode.agentLevel,
      "PC": this.tempPC,
      "Active": this.tempAT,
      "NC": this.tempNC,
      "RETENTION": this.tempRETENTION,
      "RYP": this.tempRYP,
    }
    // console.log("POST DATA", _postData);
    
    this.postData.emit(_postData);
  }

  isInvalid(){
    let word = false
    console.log("PC",this.validatePC.filter(x => x == false).length);
    console.log("AT",this.validateAT.filter(x => x == false).length);
    console.log("NC",this.validateNC.filter(x => x == false).length);
    if(
      this.validatePC.filter(x => x == false).length > 0 ||
      this.validateAT.filter(x => x == false).length > 0 ||
      this.validateNC.filter(x => x == false).length > 0
     ) {
      this.isDisable = true
    } else{
      this.isDisable = false
    }
  }
  calSumTarget(sumTarget,master){
    for (let i = 0; i < 12; i++){
      sumTarget += parseInt(master[i]);
    }
    return sumTarget;
  }
  totalSumTarget(Temp){
    let sum = 0;
    for (let i = 0; i < 12; i++){
      if (!isNaN(Temp[i])) {
        sum += parseInt(Temp[i]);
      } else {
        sum += 0;
      }
    }
    return sum;
  }

  closeModal(){
    this.modalService.dismissAll();
  }
}
