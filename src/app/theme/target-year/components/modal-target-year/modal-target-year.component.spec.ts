import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTargetYearComponent } from './modal-target-year.component';

describe('ModalTargetYearComponent', () => {
  let component: ModalTargetYearComponent;
  let fixture: ComponentFixture<ModalTargetYearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalTargetYearComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTargetYearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
