import { Component, EventEmitter,Input, OnInit, Output } from '@angular/core';
//import { ServicesService } from '../../services.service';

@Component({
  selector: 'app-target-year-table',
  templateUrl: './target-year-table.component.html',
  styleUrls: ['./target-year-table.component.scss']
})
export class TargetYearTableComponent implements OnInit {


  @Input() headers = [];
  @Input() tragets = [];
  @Input() _agentLevel :string;
  
  @Input() first_header = [];
  @Input() second_header = [];
  @Input() target_Agent_Level = '';
  @Input() pagingCount: number = 0;
  @Input() pagingItem: {totalPage: 0, totalElements: 0, content: []};
  @Input() data = [];

  @Output() clickModal = new EventEmitter<any>();
  @Output() clickPaging = new EventEmitter<any>();

  private sortTypeAsc: boolean = false;
  private sortTypeName: string = "DESC";

  @Output() clickSorting = new EventEmitter();
// header_Nam.zonee
  itemPerPage: number = 100; 
  header_Name = [
    {
      "zone": ["ZONE", "GM/BM", "VP/CM", "AL/FM/BK", "AG/FC"],
      "gm":["GM/BM", "VP/CM", "AL/FM/BK", "AG/FC"],
      "vp":["VP/CM", "AL/FM/BK", "AG/FC"],
      "al":["AL/FM/BK", "AG/FC"],
      "ag":["AG/FC"],
    }
  ]

  header_Modal = [
          [["PC","ACTIVE","NC"],
          ["Last Year","Current Year","Last Year","Current Year","Last Year","Current Year"]], 
          [["PC"],
          ["Last Year","Current Year"]]
  ]

  tragets_Name = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน",
                  "พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม",
                  "กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"]

  first_fieldNames = [];

  @Input() tragetYear_data = [
    {
      "MONTH" : "มกราคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "กุมภาพันธ์",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "มีนาคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "มีนาคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "มีนาคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "มีนาคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "มีนาคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "มีนาคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "มีนาคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "มีนาคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "มีนาคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "มีนาคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "มีนาคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "มีนาคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    },
    {
      "MONTH" : "มีนาคม",
      "PC_LY" : "100,000",
      "PC_CY" : "100,000",
      "AT_LY" : "20",
      "AT_CY" : "22",
      "NC_LY" : "6",
      "NC_CY" : "6"
    }
  ]

  constructor() { }

  ngOnInit(): void {
    this.callAPI_levelFilterTest();
  }

  callAPI_levelFilterTest(){
    if( this._agentLevel == 'AL'){
      this.headers = this.header_Name[0].al;
      this.first_fieldNames = ['alCode','agentCode'];
    } else if (this._agentLevel == 'VP' || this._agentLevel == 'AVP'){
      this.headers = this.header_Name[0].vp;
      this.first_fieldNames = ['avpCode','alCode','agentCode'];
    } else if (this._agentLevel == 'GM'){
      this.headers = this.header_Name[0].gm;
      this.first_fieldNames = ['gmCode','avpCode','alCode','agentCode'];
    } else if (this._agentLevel == 'ZONE'){
      this.headers = this.header_Name[0].zone;
      this.first_fieldNames = ['zone','gmCode','avpCode','alCode','agentCode'];
    } else if (this._agentLevel == 'AGENT'){
      this.headers = this.header_Name[0].ag;
      this.first_fieldNames = ['agentCode'];
    }
  }

  handleModal(code: string, name: string, level : string){
    let postData = {
      "agentCode" : code,
      "agentName" : name,
      "agentLevel" : level
    }
    this.clickModal.emit(postData);

  }

  isAG(Level: string){
    var masterLevel = 'AGENT AL AVP GM ZONE'
    return masterLevel.includes(Level);
  }

  isAL(Level: string){
    var masterLevel = 'AL AVP GM ZONE'
    return masterLevel.includes(Level);
  }

  isAVP(Level: string){
    var masterLevel = 'AVP GM ZONE'
    return masterLevel.includes(Level);
  }

  isGM(Level: string){
    var masterLevel = 'GM ZONE'
    return masterLevel.includes(Level);
  }

  isZONE(Level: string){
    var masterLevel = 'ZONE'
    return masterLevel.includes(Level);
  }

  itemInfo(): string {
    const start = this.printSequense();
    const end = start + this.itemPerPage - 1;
    return '' + start + '-' 
      + (end > this.pagingItem?.totalElements ? this.pagingItem?.totalElements : end ) + ' of ' + this.pagingItem?.totalElements;
  }

  printSequense() {
    return ((this.pagingCount) * this.itemPerPage) + 1;
  }

  paginationAction(type: string) {
    switch (type) {
      case 'firstPage':
        if (this.pagingCount == 0) return;
        else {
          this.clickPaging.emit(0);
        }
        break;
      case 'previous':
        if (this.pagingCount >= 1) {
          this.clickPaging.emit(this.pagingCount - 1);
        }
        break;
      case 'next':
        if (this.pagingCount < this.pagingItem.totalPage - 1) {
          this.clickPaging.emit(this.pagingCount + 1);
        } 
        else return;
        break;
      case 'endPage':
        if (this.pagingCount == this.pagingItem.totalPage) return;
        else {
          this.clickPaging.emit(this.pagingItem.totalPage - 1);
        }
        break;
    
      default:
        break;
    }
  }

  handleSorting(fieldName) {
    this.swapSortingType();
    let postData = {
      sortColName: this.convertToFieldName(fieldName),
      sortOrder: this.sortTypeName
    }
    this.clickSorting.emit(postData);
  }

  convertToFieldName(fieldName: string) {
    let _s = fieldName.replace(/([A-Z])/g, '_$1').trim().toUpperCase();
    return _s;
  }

  swapSortingType() {
    if (this.sortTypeAsc) {
      this.sortTypeName = "ASC";
      this.sortTypeAsc = !this.sortTypeAsc;
    } else {
      this.sortTypeName = "DESC";
      this.sortTypeAsc = !this.sortTypeAsc;
    }
  }

}
