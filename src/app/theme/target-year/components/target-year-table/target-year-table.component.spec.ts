import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetYearTableComponent } from './target-year-table.component';

describe('TargetYearTableComponent', () => {
  let component: TargetYearTableComponent;
  let fixture: ComponentFixture<TargetYearTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TargetYearTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetYearTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
