import { Component, Input, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-edit-status',
  templateUrl: './modal-edit-status.component.html',
  styleUrls: ['./modal-edit-status.component.scss']
})
export class ModalEditStatusComponent implements OnInit {

  @Input() status: any;
  @Input() msgSuccess: String = 'บันทึกสำเร็จ';
  @Input() msgFail: String = 'บันทึกไม่สำเร็จ';
  id1:String;

  constructor(
    private modalService: NgbModal
    ) { }

  ngOnInit(): void {
  }
  closeModal(){
    this.modalService.dismissAll();
  }

}
