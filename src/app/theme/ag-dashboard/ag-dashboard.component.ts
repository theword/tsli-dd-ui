import { Component, OnInit, HostListener } from '@angular/core';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/security/auth.service';
import { ModelUser } from 'src/app/core/model/model-user';
import { ModelDashboard } from 'src/app/core/model/model-dashboard';
import { ModelTempKpi } from 'src/app/core/model/model-tempKpi';
import { DashboardBackRouteService } from 'src/app/core/service/dashboard-back-route.service';
import { SetFormDataService } from 'src/app/core/service/set-form-data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { CheckSessionStorageService } from 'src/app/core/service/check-session-storage.service';
import $ from 'jquery';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-ag-dashboard',
  templateUrl: './ag-dashboard.component.html',
  styleUrls: ['./ag-dashboard.component.scss'],
  providers: [DecimalPipe]
})
export class AgDashboardComponent implements OnInit {
  
  // ManualRun_URL: dashboard/agent/
  // Data variables
  public modelUser: ModelUser;
  public model_PC: ModelDashboard;
  public model_Active: ModelDashboard;
  public model_RYP: ModelDashboard;
  public model_tempKpi: ModelTempKpi;
  public isDailyCollapsed = true;
  // Utilities
  // currentYear = new Date().getFullYear();
  currentYear = parseInt(sessionStorage.getItem('calendarCurrentYear'));
  hideLastYear: boolean = true;
  showBtnHome: boolean = false;
  loadingStatus: boolean = true;

  selectPerformanceDaily: any = [];
  calendarList: any [];
  masterClosYm: string = '';
  selectCalendar: string = '';
  closeResult = '';

  kpiPeriodMode_status: boolean = true;
  kpiPeriodMode_name: string = 'MTD';

  prstRte:any
  channelFilterSelected:any

  constructor(
    private modalService: NgbModal,
    private dataService: DataServiceService,
    private authService: AuthService,
    private router: Router,
    private setFormDataService: SetFormDataService,
    private dashboardBackRouteService: DashboardBackRouteService,
    private checkSessionStorageService: CheckSessionStorageService,
    private spinner: NgxSpinnerService,
    private _decimalPipe: DecimalPipe
  ) {
    this.modelUser = new ModelUser();
    this.model_PC = new ModelDashboard();
    this.model_Active = new ModelDashboard();
    this.model_RYP = new ModelDashboard();
    this.model_tempKpi = new ModelTempKpi();
  }

  ngOnInit(): void {
    /** spinner starts on init */
     this.spinner.show();

     setTimeout(() => {
     /** spinner ends after 2.8 seconds */
       this.spinner.hide();
    }, 2800);

    // Home Button display status base on login user's Role
     if (this.authService.getPosition() != 'AGENT') {
      this.showBtnHome = true;
      this.modelUser.setAgentCode(sessionStorage.getItem('agentCodeSelected'));
    } else {
      // Login as AGENT role
    //   console.log('as AGENT - ', sessionStorage.getItem('agentCodeSelected'));
        let curAgentCode = null;
        if (sessionStorage.getItem('agentCodeSelected') && this.authService.getLoginedName() != sessionStorage.getItem('agentCodeSelected')) {
            // console.log('prepare wholeGroup!!!');
            curAgentCode = sessionStorage.getItem('agentCodeSelected');
        }
        else {
            // console.log('prepare directAgent!!!');
            curAgentCode = this.authService.getLoginedName();
        }
      
      this.modelUser.setAgentCode(curAgentCode);
      sessionStorage.setItem('agentCodeSelected', curAgentCode);
      sessionStorage.setItem('agentLevelSelected', 'AGENT');
    }
    this.modelUser.setAgentLevel('AGENT');
    
    // Set masterClosYm
    this.dataService.getClosYmInfo().subscribe(
      response => {
        // console.log('res. getClosYmInfo: ', Object.values(response)[0]);
        this.masterClosYm = Object.values(response)[0];
        this.selectCalendar = Object.values(response)[0];
      },
      error => {
        // console.log('getPerformanceDaily: ', error);
      }
    );
    this.getAllData_API();

    this.prstRte = sessionStorage.getItem('prstRte')
    this.channelFilterSelected = sessionStorage.getItem('channelFilterSelected')
    console.log("channelFiterSelected", this.channelFilterSelected);
  }
  
  handleViewTeam() {
    console.log('Redirect to team detail');
    let redirectionBasedOnKpiType = '/dashboard/producer/kpidetail';
    let queryParams = [
      {kpiType: 'PC', lvName: this.modelUser.getLevelName()}
    ];

    this.checkSessionStorageService.storeSessionChangePage(
      this.modelUser.getAgentCode(),
      this.modelUser.getAgentLevel()
    );
        
    this.router.navigate([redirectionBasedOnKpiType], { 
      queryParams: {
        params: JSON.stringify(queryParams) 
      } 
    });
  }

  scrollTo(id){
    console.log(this.isDailyCollapsed);
    
    if(this.isDailyCollapsed == true){
      this.isDailyCollapsed = !this.isDailyCollapsed
      // console.log(`scrolling to ${id}`);
      let el = document.getElementById(id);
      el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }
    else {
      this.isDailyCollapsed = !this.isDailyCollapsed
    }
  }

  handleSelectCalendar(){
    let result = this.setFormDataService.transform_performanceData(
        this.modelUser.getTablePerformanceDaily(), this.selectCalendar);
    this.selectPerformanceDaily = result.list;
    this.modelUser.setPerformanceDailyTotal(result.total);
    // console.log('NEW LIST: ', this.selectPerformanceDaily);
  }

  // receive all Data from api
  getAllData_API() {
    this.callAPI_getAgentStructure();
    this.callAPI_getAgentOverview();
    this.callAPI_getAgentPerformance();
    this.callAPI_getAgentTarget();
    this.callAPI_getPerformanceDaily();
    // -- CHANGE YTD to MTD -- //
    this.callAPI_getAgentYtdPc();
    this.callAPI_getAgentYtdActive();
    this.callAPI_getAgentYtdRyp();
    // == END CHANGE == //
  }

  // Calling API Service methods
  callAPI_getAgentOverview() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    this.dataService.getAgentOverview(data).subscribe(
      response => {
        // console.log('res. getAgentOverview: ', response);
        if (Object.keys(response).length > 0) {
          let tempUser = [];
          tempUser.push(response[0]);
          this.modelUser.setTableOverview(tempUser);
          this.loadingStatus = false;
          // sessionStorage.setItem('agent_tableOverview', JSON.stringify(response[0]));

          // ====== ADD NEW PHASE 2 ====== //
          this.model_PC.setKpiBoxValue('PC', response[0].targetPc, response[0].pcApprove, response[0].pcPercent);
          this.model_Active.setKpiBoxValue('ACTIVE', response[0].targetAct, response[0].active, response[0].activePercent);
          this.model_RYP.setKpiBoxValue('RYP', response[0].targetRyp, response[0].ryp, response[0].rypPercent);
          this.model_tempKpi.setMasterPcMtd(response[0].targetPc, response[0].pcApprove, response[0].pcPercent);
          this.model_tempKpi.setMasterActiveMtd(response[0].targetAct, response[0].active, response[0].activePercent);
          this.model_tempKpi.setMasterRypMtd(response[0].targetRyp, response[0].ryp, response[0].rypPercent);
          // ====== END NEW PHASE 2 ====== //

          // prstRte
          let [y, m] = this.parseMonth(response[0].closYm)
          let month = this.closYm2Month(m-1)
          let year;
          if(m == 1){
            y = y-1
            year = (y+543).toString().substring(2, 4);
          } else {
            year = (y+543).toString().substring(2, 4);
          }
          
          if(response[0].prstRte == -1 || response[0].prstRte == null) {
            this.prstRte = `: Pers. ณ เดือน ${month} ${year} : N/A`
          } else {
            let p = this._decimalPipe.transform(response[0].prstRte, '1.2-2');
            this.prstRte = `: Pers. ณ เดือน ${month} ${year} : ${p}%`
          }


          sessionStorage.setItem('prstRte', this.prstRte);

        } else {
          this.loadingStatus = false;
          // this.callAPI_getAgentStructure();
          this.modelUser.setDefaultTableOverview(this.modelUser.getAgentCode());

          // ====== ADD NEW PHASE 2 ====== //
          this.model_PC.setKpiBoxValue('PC', 0, 0, 0);
          this.model_Active.setKpiBoxValue('ACTIVE', 0, 0, 0);
          this.model_RYP.setKpiBoxValue('RYP', 0, 0, 0);
          // ====== END NEW PHASE 2 ====== //
        }
      },
      error => {
        // console.log('getAgentOverview: ', error);
      }
    );
  }
  callAPI_getAgentPerformance() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    this.dataService.getAgentPerformance(data).subscribe(
      response => {
        // console.log('res. getAgentPerformance: ', response);
        if (Object.keys(response).length > 0) {
          let result = this.setFormDataService.transform_kpiChartSeries(response, 'pcApprove');
          this.model_PC.setCurrentAndLast(result.current, result.last);

          result = this.setFormDataService.transform_kpiChartSeries(response, 'active');
          this.model_Active.setCurrentAndLast(result.current, result.last);

          result = this.setFormDataService.transform_kpiChartSeries(response, 'ryp');
          this.model_RYP.setCurrentAndLast(result.current, result.last);
        }
      },
      error => {
        // console.log('getAgentPerformance: ', error);
      }
    );
  }
  callAPI_getAgentTarget() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    this.dataService.getAgentTarget(data).subscribe(
      response => {
        // console.log('res. getAgentTarget: ', response);
        if (response && Object.keys(response).length > 0) {
          this.model_PC.setSeriesTarget(
            this.setFormDataService.transform_kpiTargetSeries(response, 'PC'));
          this.model_Active.setSeriesTarget(
            this.setFormDataService.transform_kpiTargetSeries(response, 'NEW_ACTIVE_AGENT'));
          this.model_RYP.setSeriesTarget(
            this.setFormDataService.transform_kpiTargetSeries(response, 'RYP'));
        } else {
          let series_target = [1,1,1,1,1,1,1,1,1,1,1,1];
          this.model_Active.setSeriesTarget(series_target);
        }
      },
      error => {
        // console.log('err. getAgentTarget: ', error);
      }
    );
  }
  callAPI_getAgentYtdPc() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    this.dataService.getAgentYtdPc(data).subscribe(
      response => {
        // console.log('res. getAgentYtdPc: ', response);
        if (Object.keys(response).length > 0) {
          this.model_tempKpi.setMasterPcYtd(response[0].pcTarget, response[0].pcApprove, response[0].percent);
          // this.model_PC.setKpiBoxValue('PC', response[0].pcTarget, response[0].pcApprove, response[0].percent);
        } else {
          this.model_PC.setKpiBoxValue('PC', 0, 0, 0);
        }
      },
      error => {
        // console.log('err. getAgentYtdPc: ', error);
      }
    );
  }
  callAPI_getAgentYtdActive() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    this.dataService.getAgentYtdActive(data).subscribe(
      response => {
        // console.log('res. getAgentYtdActive: ', response);
        if (Object.keys(response).length > 0) {
          this.model_tempKpi.setMasterActiveYtd(response[0].activeTarget, response[0].active, response[0].percent);
          // this.model_Active.setKpiBoxValue('ACTIVE', response[0].activeTarget, response[0].active, response[0].percent);
        } else {
          this.model_Active.setKpiBoxValue('ACTIVE', 0, 0, 0);
        }
      },
      error => {
        // console.log('err. getAgentYtdActive: ', error);
      }
    );
  }
  callAPI_getAgentYtdRyp() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    this.dataService.getAgentYtdRyp(data).subscribe(
      response => {
        // console.log('res. getAgentYtdRyp: ', response);
        if (Object.keys(response).length > 0) {
          this.model_tempKpi.setMasterRypYtd(response[0].rypTarget, response[0].ryp, response[0].percent);
          // this.model_RYP.setKpiBoxValue('RYP', response[0].rypTarget, response[0].ryp, response[0].percent);
        } else {
          this.model_RYP.setKpiBoxValue('RYP', 0, 0, 0);
        }
      },
      error => {
        // console.log('err. getAgentYtdRyp: ', error);
      }
    );
  }
  callAPI_getAgentStructure() {
    let data = {
      "agentCode": this.modelUser.getAgentCode()
    };
    this.dataService.getAgentStructure(data).subscribe(
      response => {
        if (Object.keys(response).length > 0) {
          let agentName = response[0]['thaiName'] + ' ' + response[0]['thaiSurname'];
          this.modelUser.setAgentName(agentName);
          this.modelUser.setAgentLevel('AGENT');
          sessionStorage.setItem('agentLevelSelected', 'AGENT');
          sessionStorage.setItem('agentLevelName', response[0]['levelName']);
          if (response[0]['agentLevel'] !== 'AGENT') {
            if(response[0]['levelName'] == 'BK') {
              this.modelUser.setLevelName('AL/BROKER' + ' (ผลงานส่วนตัว)');
              sessionStorage.setItem('agentLevelName', 'AL/BROKER' + ' (ผลงานส่วนตัว)');
            } else {
              this.modelUser.setLevelName(response[0]['levelName'] + ' (ผลงานส่วนตัว)');
              sessionStorage.setItem('agentLevelName', response[0]['levelName'] + ' (ผลงานส่วนตัว)');
            }
          } else {
            this.modelUser.setLevelName(response[0]['levelName']);
          }
          
        }
      },
      error => {
        // console.log('getAgentStructure: ', error);
      }
    );
  }
  // ====== ADD NEW PHASE 2 ====== //
  callAPI_getPerformanceDaily() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    this.dataService.getPerformanceDaily(data).subscribe(
      response => {
        // console.log('res. getPerformanceDaily: ', response);

        if (Object.keys(response).length > 0) {
          for (let index = 0; index < Object.keys(response).length; index++) {
            let date = new Date(response[index].dayOfMonth);
            let dd = String(date.getDate()). padStart(2, '0');
            response[index].dayOfMonth = dd;
          }
          this.modelUser.setTablePerformanceDaily(response);
          let result = this.setFormDataService.transform_performanceData(response, this.masterClosYm);
          this.selectPerformanceDaily = result.list;
          this.modelUser.setPerformanceDailyTotal(result.total);
          this.calendarList = this.setFormDataService.getSelectionCalendarYM(this.masterClosYm);       
          // console.log('TRANSFORM: ', this.calendarList);
        } else {
          //
        }
      },
      error => {
        // console.log('getPerformanceDaily: ', error);
      }
    );
  }
  // ====== END NEW PHASE 2 ====== //
  
  // Controller methods
  compareYear() {
    this.hideLastYear = !this.hideLastYear;
  }
  goToHome() {
    let userRole = this.authService.getPosition();
    let agentCode = this.authService.getLoginedName();
    this.dashboardBackRouteService.backHome(userRole, agentCode);
    sessionStorage.removeItem('agentCodeStored');
    sessionStorage.removeItem('agentLevelStored');
  }

  // Back from browser
  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    this.dashboardBackRouteService.onClickBack();
  }

  // Redirect to Persistency
  onClickPersistency() {
    let redirectionToPer = '/persistency';
    let queryParams = [
      {lvName: this.modelUser.getLevelName() },
    ];
    this.checkSessionStorageService.storeSessionChangePage(
      this.modelUser.getAgentCode(),
      this.modelUser.getAgentLevel()
    );

    this.router.navigate([redirectionToPer], {
      queryParams: {
        params: JSON.stringify(queryParams),
      },
    });
  }
  onClickCompensation() {
    // console.log('compensation');
    
    let redirectionToCompensation = '/compensation';
    let queryParams = [
      {lvName: this.modelUser.getLevelName() },
    ];
    this.checkSessionStorageService.storeSessionChangePage(
      this.modelUser.getAgentCode(),
      this.modelUser.getAgentLevel()
    );

    this.router.navigate([redirectionToCompensation], {
      queryParams: {
        params: JSON.stringify(queryParams),
      },
    });
  }
  onClickPromotion(mode) {
    let redirectionToPromotion = '/promotion';
    let queryParams = [
      {lvName: this.modelUser.getLevelName() },
      {filterPromotion: mode}
    ];
    this.checkSessionStorageService.storeSessionChangePage(
      this.modelUser.getAgentCode(),
      this.modelUser.getAgentLevel()
    );

    this.router.navigate([redirectionToPromotion], {
      queryParams: {
        params: JSON.stringify(queryParams),
      },
    });
  }
  
  // Handle open modal
  open(content) {
    this.modalService.open(content, {centered: true, windowClass : "xxl" }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed`;
    });
  }
  scrollTop() {
    $('body,html').animate({
      scrollTop: 0
    }, 400);
  }

  // ====== ADD NEW PHASE 3 ====== //
  // Switch KPI summary between MTD and YTD
  handlerKpiPeriodMode(type: string) {
    if (type == 'MTD') {
      this.kpiPeriodMode_status = true;
      this.kpiPeriodMode_name = 'MTD';
      this.setKpiMtd();
    }
    else {
      this.kpiPeriodMode_status = false;
      this.kpiPeriodMode_name = 'YTD';
      this.setKpiYtd();
    }
  }
  setKpiMtd() {
    this.model_PC.setKpiBoxValue(
      'PC',
      this.model_tempKpi.masterPcMtd.target,
      this.model_tempKpi.masterPcMtd.value,
      this.model_tempKpi.masterPcMtd.percent
    );
    this.model_Active.setKpiBoxValue(
      'ACTIVE',
      this.model_tempKpi.masterActiveMtd.target,
      this.model_tempKpi.masterActiveMtd.value,
      this.model_tempKpi.masterActiveMtd.percent
    );
    this.model_RYP.setKpiBoxValue(
      'RYP',
      this.model_tempKpi.masterRypMtd.target,
      this.model_tempKpi.masterRypMtd.value,
      this.model_tempKpi.masterRypMtd.percent
    );
  }
  setKpiYtd() {
    this.model_PC.setKpiBoxValue(
      'PC',
      this.model_tempKpi.masterPcYtd.target,
      this.model_tempKpi.masterPcYtd.value,
      this.model_tempKpi.masterPcYtd.percent
    );
    this.model_Active.setKpiBoxValue(
      'ACTIVE',
      this.model_tempKpi.masterActiveYtd.target,
      this.model_tempKpi.masterActiveYtd.value,
      this.model_tempKpi.masterActiveYtd.percent
    );
    this.model_RYP.setKpiBoxValue(
      'RYP',
      this.model_tempKpi.masterRypYtd.target,
      this.model_tempKpi.masterRypYtd.value,
      this.model_tempKpi.masterRypYtd.percent
    );
  }
  // ====== END NEW PHASE 3 ====== //

    // Utilities
    closYm2Month(closYm) {
      switch (closYm) {
        case 0:
          return 'ธ.ค.'
        case 1:
          return 'ม.ค.'
        case 2:
          return 'ก.พ.'
        case 3:
          return 'มี.ค.'
        case 4:
          return 'เม.ย.'
        case 5:
          return 'พ.ค.'
        case 6:
          return 'มิ.ย.'
        case 7:
          return 'ก.ค.'
        case 8:
          return 'ส.ค.'
        case 9:
          return 'ก.ย.'
        case 10:
          return 'ต.ค.'
        case 11:
          return 'พ.ย.'
        case 12:
          return 'ธ.ค.'
      }
    }
    parseMonth(str) {
      return str.match(/(\d{4})(\d{2})/).splice(1).map(Number)
    }
}
