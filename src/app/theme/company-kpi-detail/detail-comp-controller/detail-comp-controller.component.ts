import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/core/security/auth.service';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { ModelUser } from 'src/app/core/model/model-user';
import { ModelDashboard } from 'src/app/core/model/model-dashboard';
import { SetFormDataService } from 'src/app/core/service/set-form-data.service';
import { Location } from '@angular/common';
import { DashboardBackRouteService } from 'src/app/core/service/dashboard-back-route.service';


@Component({
  selector: 'app-detail-comp-controller',
  templateUrl: './detail-comp-controller.component.html',
  styleUrls: ['./detail-comp-controller.component.scss']
})
export class DetailCompControllerComponent implements OnInit {

  // Data variables
  public modelUser: ModelUser;
  public model_PC: ModelDashboard;
  public model_PCsubmit: ModelDashboard;
  public model_Active: ModelDashboard;
  public model_NC: ModelDashboard;
  public model_Retention: ModelDashboard;
  public model_RYP: ModelDashboard;

  // Utilities
  public currentYear = parseInt(sessionStorage.getItem('calendarCurrentYear'));
  public hideLastYear: boolean = true;
  public filterKpiType: string = 'PC';
  public showBtnHome: boolean = false;

  public LevelFilterSelected: string = 'COMPANY';
  queryLevel: string = 'COMPANY';
  
  titleLevel: string = 'Company';
  titleSubName: string = '';
  mtd_rt = []; activeClosYm_RT = {};

  public noCaseData: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public caseSizeData: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public ytdCaseData: any = {
    noCase: 0,
    caseSize: 0
  };

  public activeMonthNo: number = 0;
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataServiceService,
    private authService: AuthService,
    private setFormDataService: SetFormDataService,
    private dashboardBackRouteService : DashboardBackRouteService,
    private location : Location
  ) { 
    this.modelUser = new ModelUser();
    this.model_PC = new ModelDashboard();
    this.model_PCsubmit = new ModelDashboard();
    this.model_Active = new ModelDashboard();
    this.model_NC = new ModelDashboard();
    this.model_Retention = new ModelDashboard();
    this.model_RYP = new ModelDashboard();
  }

  ngOnInit(): void {
    this.modelUser.setAgentName(this.authService.getName());
    this.modelUser.setAgentCode(this.authService.getLoginedName());
    // Get KPI filter type from AgentOverview by params URL. 
    this.route.queryParams.subscribe(params => {
      let getParam = JSON.parse(params.params);
      this.filterKpiType = getParam[0]['kpiType'];
    });
    
    let tempChannel = sessionStorage.getItem('channelFilterSelected');
    let tempZone = sessionStorage.getItem('zoneFilterSelected');
    // console.log(tempChannel, ' | ', tempZone);
    if (tempChannel) {
      this.LevelFilterSelected = tempChannel;
      if (tempChannel !== 'COMPANY' && tempZone === 'COMPANY') {
        this.titleLevel = 'Channel';
        this.queryLevel = 'CHANNEL';
        this.titleSubName = tempChannel;
      } 
      else if (tempChannel !== 'COMPANY' && tempZone !== 'COMPANY') {
        this.LevelFilterSelected = tempZone;
        this.titleLevel = 'Zone';
        this.queryLevel = 'ZONE';
        this.titleSubName = ('0' + tempZone).slice(-2);
      }
      else {
        this.titleLevel = 'Company';
        this.queryLevel = 'COMPANY';
        this.titleSubName = '';
      }
    } else {
      this.LevelFilterSelected = '';
    }
    this.getBasicData_API();
    this.getRetention_API();
  }

  async getBasicData_API() {
    const resPerformance = await this.callAPI_getPerformance(this.queryLevel);
    const resTarget = await this.callAPI_getTarget(this.queryLevel);
    this.callAPI_getYTD(this.queryLevel);

    // Check [PERFORMANCE] data available or not missing
    if (Object.keys(resPerformance).length > 0) {
      this.setPackPerformance(resPerformance);
    }

    // Check [TARGET] data available or not missing
    if (Object.keys(resTarget).length > 0 ) {
      this.setPackTarget(resTarget);
    }

    // Check [PERFORMANCE] and [TARGET], for set percent
    if (Object.keys(resPerformance).length > 0 && Object.keys(resTarget).length > 0) {
      let closYmLastData = resPerformance[Object.keys(resPerformance).length - 1];
      // console.log('ACTIVE #:', closYmLastData);
      let activeMonth = parseInt(closYmLastData.closYm.substring(4, 6));
      // console.log('MONTH #: ', activeMonth);
      this.activeMonthNo = activeMonth - 1;
      this.setPackPercentage();
    } 
  }

  async getRetention_API() {
    const resMtdRT = await this.callAPI_getRetention(this.queryLevel);
    const resYtdRT = await this.callAPI_getYtdRetention(this.queryLevel);
    const resOverview = await this.callAPI_getCompanyOverview();
    if (Object.keys(resMtdRT).length > 0 && Object.keys(resYtdRT).length > 0 && Object.keys(resOverview).length > 0) {
      var result  = this.setFormDataService.transform_MtdRetention(resMtdRT, resYtdRT, resOverview);
      this.mtd_rt = result.dataRes;
      this.activeClosYm_RT = result.closYm;
    } else {
      let defaultMTD = {
        chName: 'NA',
        lmRetention: 0,
        rtMonth01: 0,
        rtMonth02: 0,
        rtMonth03: 0,
        rtMonth04: 0,
        rtMonth05: 0,
        rtMonth06: 0,
        cmRT: 0
      };
      this.mtd_rt.push(defaultMTD);
    }
  }

  // [API Calling]
  async callAPI_getPerformance(type: string) {
    let dataPost;
    let dataRes;
    switch (type) {
      case 'COMPANY':
        dataRes = await this.dataService.getCompanyPerformance().toPromise();
        // console.log('DATA: PERF.Comp: ', dataRes);
        break;
      case 'CHANNEL':
        dataPost = {
          "channelCode": this.LevelFilterSelected
        }
        dataRes = await this.dataService.getChannelPerformance(dataPost).toPromise();
        // console.log('DATA: PERF.Channel: ', dataRes);
        break;
      case 'ZONE':
        dataPost = {
          "zoneCode": this.LevelFilterSelected
        };
        dataRes = await this.dataService.getZonePerformance(dataPost).toPromise();
        // console.log('DATA: PERF.ZONE: ', dataRes);
        break;
      default:
        break;
    }
    return dataRes;
  }
  async callAPI_getTarget(type: string) {
    let data = {
      "agentCode": 'EMPTY',
    };
    switch (type) {
      case 'COMPANY':
        data = {
          "agentCode": 'COMPANY',
        };
        break;
      case 'CHANNEL': case 'ZONE':
        data = {
          "agentCode": this.LevelFilterSelected
        };
        break;
      default:
        break;
    }
    let dataRes = await this.dataService.getCompanyTarget(data).toPromise();
    // console.log('TARGET: ', this.LevelFilterSelected, ' : ', dataRes);
    
    return dataRes;
  }
  callAPI_getYTD(type: string) {
    let data = {
      "agentLevel": '',
      "zoneCode": ''
    }
    switch (type) {
      case 'COMPANY':
        data = {
          "agentLevel": 'COMPANY',
          "zoneCode": this.LevelFilterSelected
        }
        break;
      case 'CHANNEL':
        data = {
          "agentLevel": 'CHANNEL',
          "zoneCode": this.LevelFilterSelected
        }
        break;
      case 'ZONE':
        data = {
          "agentLevel": 'ZONE',
          "zoneCode": this.LevelFilterSelected
        }
        break;
    
      default:
        break;
    }
    this.dataService.getCompanyYTD(data).subscribe(
      response => {
        // console.log('res. getCompanyYTD: ', response);
        if (Object.keys(response).length > 0) {
          this.model_PC.setSumYtdValue(
            this.setFormDataService.transform_kpiYtd_forSTAFF(response, 'PC')
          );
          this.model_Active.setSumYtdValue(
            this.setFormDataService.transform_kpiYtd_forSTAFF(response, 'ACTIVE')
          );
          this.model_NC.setSumYtdValue(
            this.setFormDataService.transform_kpiYtd_forSTAFF(response, 'NC')
          );
          this.model_RYP.setSumYtdValue(
            this.setFormDataService.transform_kpiYtd_forSTAFF(response, 'RYP')
          );

          let indexRes = Object.values(response);
          let indexCurrentYear = indexRes.findIndex(x => x.closYm === this.currentYear.toString());
          this.ytdCaseData = {
            'noCase': response[indexCurrentYear]['noCase'],
            'caseSize': response[indexCurrentYear]['caseSize']
          };
        } 
      },
      error => {
        // console.log('err. getCompanyYTD: ', error);
      }
    );
  }
  // KPI Detail - Retention only
  async callAPI_getRetention(type: string) {
    let dataPost;
    let dataRes;
    switch (type) {
      case 'COMPANY':
        dataRes = await this.dataService.getCompanyRetention().toPromise();
        break;
      case 'CHANNEL':
        dataPost = {
          "channelCode": this.LevelFilterSelected
        };
        dataRes = await this.dataService.getChannelRetention(dataPost).toPromise();
        break;
      case 'ZONE':
        dataPost = {
          "zoneCode": this.LevelFilterSelected
        };
        dataRes = await this.dataService.getZoneRetention(dataPost).toPromise();
        break;
    
      default:
        break;
    }
    // console.log('RETENTION: ', dataRes);
    
    return dataRes;
  }
  async callAPI_getYtdRetention(type: string) {
    let dataPost;
    let dataRes;

    switch (type) {
      case 'COMPANY':
        dataPost = {
          "filter" : this.currentYear.toString()
        };
        dataRes = await this.dataService.getCompanyYtdRetention(dataPost).toPromise();
        break;
      case 'CHANNEL':
        dataPost = {
          "filter": this.currentYear,
          "channelCode": this.LevelFilterSelected
        };
        dataRes = await this.dataService.getChannelYtdRetention(dataPost).toPromise();
        break;
      case 'ZONE':
        dataPost = {
          "filter": this.currentYear,
          "channelCode": sessionStorage.getItem('channelFilterSelected'),
          "zoneCode": this.LevelFilterSelected
        };
        dataRes = await this.dataService.getZoneYtdRetention(dataPost).toPromise();
        break;
    
      default:
        break;
    }
    // console.log('RETENTION YTD: ', dataRes);
    return dataRes;
  }
  async callAPI_getCompanyOverview() {
    // Need ZoneName from this
    let dataRes;
    dataRes = await this.dataService.getCompanyAllOverview().toPromise();
    return dataRes;
  }

  // [Controller methods]
  handleFilterSelect(kpiSelect: string) {
    if (this.filterKpiType != kpiSelect) {
      this.filterKpiType = kpiSelect;
    }
  }
  goToHome() {
    let userRole = this.authService.getPosition();
    let agentCode = this.authService.getLoginedName();
    sessionStorage.removeItem('agentCodeStored');
    sessionStorage.removeItem('agentLevelStored');
    this.dashboardBackRouteService.backHome(userRole, agentCode);    
  }
  goToOverview() {
    // let redirectionBasedOnRole = '/dashboard/staff/';
    // this.router.navigate([redirectionBasedOnRole]);
    this.location.back();
  }
  compareYear() {
    this.hideLastYear = !this.hideLastYear;
  }

  // [PackSet DataForm]
  setPackPerformance(response){

    let result = this.setFormDataService.transform_kpiChartSeries_dyThousand(response, 'pcApprove');
    this.model_PC.setCurrentAndLast(result.current, result.last);

    result = this.setFormDataService.transform_kpiChartSeries_dyThousand(response, 'pcSubmit');
    this.model_PCsubmit.setCurrentAndLast(result.current, result.last);

    result = this.setFormDataService.transform_kpiChartSeries(response, 'active');
    this.model_Active.setCurrentAndLast(result.current, result.last);
    
    result = this.setFormDataService.transform_kpiChartSeries(response, 'nc');
    this.model_NC.setCurrentAndLast(result.current, result.last);

    result = this.setFormDataService.transform_kpiChartSeries(response, 'retention');
    this.model_Retention.setCurrentAndLast(result.current, result.last);

    result = this.setFormDataService.transform_kpiChartSeries_dyThousand(response, 'ryp');
    this.model_RYP.setCurrentAndLast(result.current, result.last);

    result = this.setFormDataService.transform_kpiChartSeries(response, 'noCase');
    this.noCaseData = result.current;
    result = this.setFormDataService.transform_kpiChartSeries(response, 'caseSize');
    this.caseSizeData = result.current;
      
  }
  setPackTarget(response){
    this.model_PC.setSeriesTarget(
      this.setFormDataService.transform_kpiTargetSeries_dyThousand(response, 'PC'));
    this.model_Active.setSeriesTarget(
      this.setFormDataService.transform_kpiTargetSeries(response, 'NEW_ACTIVE_AGENT'));
    this.model_NC.setSeriesTarget(
      this.setFormDataService.transform_kpiTargetSeries(response, 'NEW_CODE'));
    this.model_Retention.setSeriesTarget(
      this.setFormDataService.transform_kpiTargetSeries(response, 'RETENTION'));
    this.model_RYP.setSeriesTarget(
      this.setFormDataService.transform_kpiTargetSeries_dyThousand(response, 'RYP'));
  }
  setPackPercentage() {
    this.model_PC.setSeriesPercent(
      this.setFormDataService.cal_kpiPercent(
        this.model_PC.getSeriesCurrent(), this.model_PC.getSeriesTarget())
    );
    this.model_Active.setSeriesPercent(
      this.setFormDataService.cal_kpiPercent(
        this.model_Active.getSeriesCurrent(), this.model_Active.getSeriesTarget())
    );
    this.model_NC.setSeriesPercent(
      this.setFormDataService.cal_kpiPercent(
        this.model_NC.getSeriesCurrent(), this.model_NC.getSeriesTarget())
    );
    this.model_Retention.setSeriesPercent(
      this.setFormDataService.cal_kpiPercent(
        this.model_Retention.getSeriesCurrent(), this.model_Retention.getSeriesTarget())
    );
    this.model_RYP.setSeriesPercent(
      this.setFormDataService.cal_kpiPercent(
        this.model_RYP.getSeriesCurrent(), this.model_RYP.getSeriesTarget())
    );
  }

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    this.dashboardBackRouteService.onClickBack();
  }

}
