import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailCompControllerComponent } from './detail-comp-controller.component';

describe('DetailCompControllerComponent', () => {
  let component: DetailCompControllerComponent;
  let fixture: ComponentFixture<DetailCompControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailCompControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailCompControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
