import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-kpi-nc',
  templateUrl: './kpi-nc.component.html',
  styleUrls: ['./kpi-nc.component.scss']
})
export class KpiNcComponent implements OnInit {

  // Data variables
  // currentYear = new Date().getFullYear();
  // currentMonth = new Date().getMonth();
  chartZoom: boolean = true;

  // Input
  @Input() series_target = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() series_cur = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() series_last = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() series_percent = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() sumYTD: object = {
    target: 0,
    approveCur: 0,
    approveLast: 0,
    percent: 0
  };
  @Input() hideLastYear: boolean = true;
  @Input() activeMonth: number = 0;
  
  constructor() { }

  ngOnInit(): void {
  }

}
