import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/core/shared.module';
import { KpiPcComponent } from './kpi-pc/kpi-pc.component';
import { KpiActComponent } from './kpi-act/kpi-act.component';
import { KpiNcComponent } from './kpi-nc/kpi-nc.component';
import { KpiRtComponent } from './kpi-rt/kpi-rt.component';
import { KpiRypComponent } from './kpi-ryp/kpi-ryp.component';
import { DetailCompControllerComponent } from './detail-comp-controller/detail-comp-controller.component';

const routes: Routes = [
  {
    path: 'main',
    component: DetailCompControllerComponent,
  },
  { path: '**', redirectTo: 'error', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    DetailCompControllerComponent,
    KpiPcComponent, 
    KpiActComponent, 
    KpiNcComponent, 
    KpiRtComponent, 
    KpiRypComponent
    ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule
  ]
})
export class CompanyKpiDetailModule { }
