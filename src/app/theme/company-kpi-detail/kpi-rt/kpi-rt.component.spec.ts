import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiRtComponent } from './kpi-rt.component';

describe('KpiRtComponent', () => {
  let component: KpiRtComponent;
  let fixture: ComponentFixture<KpiRtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiRtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiRtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
