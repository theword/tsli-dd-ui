import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-kpi-rt',
  templateUrl: './kpi-rt.component.html',
  styleUrls: ['./kpi-rt.component.scss']
})
export class KpiRtComponent implements OnInit {

  // Data variables
  // currentYear = new Date().getFullYear();
  // currentMonth = new Date().getMonth();
  chartZoom: boolean = true;
  monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  // Input
  @Input() series_target = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() series_cur = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() series_last = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() series_percent = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() mtd_rt: object = {
    chName: 'X',
    lmRetention: 0,
    rtMonth01: 0,
    rtMonth02: 0,
    rtMonth03: 0,
    rtMonth04: 0,
    rtMonth05: 0,
    rtMonth06: 0,
    cmRT: 0
  };
  @Input() hideLastYear: boolean = true;
  @Input() currentLevel: any;
  @Input() closYm: any;

  constructor() { }

  ngOnInit(): void {
  }
  
  ngOnChanges(changes: SimpleChanges) {
  }

}
