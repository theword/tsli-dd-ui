import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GmAlDashboardComponent } from './gm-al-dashboard.component';

describe('GmAlDashboardComponent', () => {
  let component: GmAlDashboardComponent;
  let fixture: ComponentFixture<GmAlDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GmAlDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GmAlDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
