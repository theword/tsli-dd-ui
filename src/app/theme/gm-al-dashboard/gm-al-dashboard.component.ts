import { Component, OnInit, HostListener, Renderer2 } from '@angular/core';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/security/auth.service';
import { ModelUser } from 'src/app/core/model/model-user';
import { ModelDashboard } from 'src/app/core/model/model-dashboard';
import { ModelTempKpi } from 'src/app/core/model/model-tempKpi';
import { DashboardBackRouteService } from 'src/app/core/service/dashboard-back-route.service';
import { SetFormDataService } from 'src/app/core/service/set-form-data.service';
import { CheckSessionStorageService } from 'src/app/core/service/check-session-storage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import $ from 'jquery';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-gm-al-dashboard',
  templateUrl: './gm-al-dashboard.component.html',
  styleUrls: ['./gm-al-dashboard.component.scss'],
  providers: [DecimalPipe]
})
export class GmAlDashboardComponent implements OnInit {
  // Data variables
  public modelUser: ModelUser;
  public model_PC: ModelDashboard;
  public model_Active: ModelDashboard;
  public model_NC: ModelDashboard;
  public model_RYP: ModelDashboard;
  public model_tempKpi: ModelTempKpi;
  public isDailyCollapsed = true;
  // Utilities
  currentYear = parseInt(sessionStorage.getItem('calendarCurrentYear'));
  hideLastYear: boolean = true;
  showBtnHome: boolean = false;
  loadingStatus: boolean = true;

  selectPerformanceDaily: any = [];
  calendarList: any [];
  masterClosYm: string = '';
  selectCalendar: string = '';
  closeResult = '';

  kpiPeriodMode_status: boolean = true;
  kpiPeriodMode_name: string = 'MTD';

  prstRte:any
  channelFilterSelected:any
  
  constructor(
    private modalService: NgbModal,
    private dataService: DataServiceService,
    private authService: AuthService,
    private router: Router,
    private setFormDataService: SetFormDataService,
    private dashboardBackRouteService: DashboardBackRouteService,
    private checkSessionStorageService: CheckSessionStorageService,
    private spinner: NgxSpinnerService,
    private renderer: Renderer2,
    private _decimalPipe: DecimalPipe
  ) {
    this.modelUser = new ModelUser();
    this.model_PC = new ModelDashboard();
    this.model_Active = new ModelDashboard();
    this.model_NC = new ModelDashboard();
    this.model_RYP = new ModelDashboard();
    this.model_tempKpi = new ModelTempKpi();
  }

  ngOnInit(): void {
    /** spinner starts on init */
    this.spinner.show();

    setTimeout(() => {
      /** spinner ends after 2.8 seconds */
      this.spinner.hide();
    }, 2800);

    // Set agentCode for Query
    var tempAgentCode = sessionStorage.getItem('agentCodeSelected');
    var tempAgentLevel = sessionStorage.getItem('agentLevelSelected');
    if (
      tempAgentCode &&
      tempAgentCode != 'null' &&
      tempAgentLevel &&
      tempAgentLevel != 'null'
    ) {
      // Click back to home or drill by above agent
      this.modelUser.setAgentCode(tempAgentCode);
      this.modelUser.setAgentLevel(tempAgentLevel);

      if (tempAgentLevel != this.authService.getPosition()) {
        this.showBtnHome = true;
      }
    } else {
      // login at first
      this.modelUser.setAgentCode(this.authService.getLoginedName());
      this.modelUser.setAgentLevel(this.authService.getPosition());
      sessionStorage.setItem(
        'agentCodeSelected',
        this.authService.getLoginedName()
      );
      sessionStorage.setItem(
        'agentLevelSelected',
        this.authService.getPosition()
      );
    }
    
    // Set masterClosYm
    this.dataService.getClosYmInfo().subscribe(
      response => {
        // console.log('res. getClosYmInfo: ', Object.values(response)[0]);
        this.masterClosYm = Object.values(response)[0];
        this.selectCalendar = Object.values(response)[0];
      },
      error => {
        // console.log('getPerformanceDaily: ', error);
      }
    );

    this.getAllData_API();

    this.channelFilterSelected = sessionStorage.getItem('channelFilterSelected')
    console.log("channelFiterSelected", this.channelFilterSelected);
  }

  scrollTo(id){
    console.log(this.isDailyCollapsed);
    
    if(this.isDailyCollapsed == true){
      this.isDailyCollapsed = !this.isDailyCollapsed
      console.log(`scrolling to ${id}`);
      let el = document.getElementById(id);
      el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }
    else {
      this.isDailyCollapsed = !this.isDailyCollapsed
    }
  }

  handleSelectCalendar(){
    let result = this.setFormDataService.transform_performanceData(
        this.modelUser.getTablePerformanceDaily(), this.selectCalendar);
    this.selectPerformanceDaily = result.list;
    this.modelUser.setPerformanceDailyTotal(result.total);
    console.log('NEW LIST: ', this.selectPerformanceDaily);
  }
  
  // Sync Data
  getAllData_API() {
    this.callAPI_getAgentOverview();
    // ====== ADD NEW PHASE 2 ====== //
    this.callAPI_getAgentYtdPc();
    this.callAPI_getAgentYtdActive();
    this.callAPI_getAgentYtdRyp();
    this.callAPI_getAgentYtdNc();
    this.callAPI_getPerformanceDaily();
    // ====== END NEW PHASE 2 ====== //
    this.callAPI_getAgentPerformance();
    this.callAPI_getAgentTarget();
    
  }

  // [Calling API Service methods]
  callAPI_getAgentOverview() {
    let data = {
      agentCode: this.modelUser.getAgentCode(),
      agentLevel: this.modelUser.getAgentLevel(),
    };
    this.dataService.getAgentOverview(data).subscribe(
      (response) => {
        console.log('res. getAgentOverview: ', response);
        if (Object.keys(response).length > 0) {
          let agentName =
            response[0]['agentName'] + ' ' + response[0]['agentSurname'];
          this.modelUser.setAgentName(agentName);
          this.modelUser.setAgentLevel(response[0]['agentLevel']);
          // sessionStorage.setItem('agentLevelName', response[0]['levelName']);
          if(response[0]['levelName'] == 'BK') {
            this.modelUser.setLevelName('AL/BROKER');
            sessionStorage.setItem('agentLevelName', 'AL/BROKER');
          } else {
            this.modelUser.setLevelName(response[0]['levelName']);
            sessionStorage.setItem('agentLevelName', response[0]['levelName']);
          }
          
          let tempUser = [];
          tempUser.push(response[0]);
          this.modelUser.setTableOverview(tempUser);
          this.loadingStatus = false;

          // ====== ADD NEW PHASE 2 ====== //
          this.model_PC.setKpiBoxValue(
            'PC', 
            response[0].targetPc, 
            response[0].pcApprove, 
            response[0].pcPercent
          );
          this.model_tempKpi.setMasterPcMtd(response[0].targetPc, response[0].pcApprove, response[0].pcPercent);
          this.model_Active.setKpiBoxValue(
            'ACTIVE',
            response[0].targetAct,
            response[0].active,
            response[0].activePercent
          );
          this.model_tempKpi.setMasterActiveMtd(response[0].targetAct, response[0].active, response[0].activePercent);
          this.model_NC.setKpiBoxValue(
            'NC',
            response[0].targetNc,
            response[0].nc,
            response[0].ncPercent
          );
          this.model_tempKpi.setMasterNcMtd(response[0].targetNc, response[0].nc, response[0].ncPercent);
          this.model_RYP.setKpiBoxValue(
            'RYP',
            response[0].targetRyp,
            response[0].ryp,
            response[0].rypPercent
          );
          this.model_tempKpi.setMasterRypMtd(response[0].targetRyp, response[0].ryp, response[0].rypPercent);
          // ====== END NEW PHASE 2 ====== //

          // prstRte
          // response[0].closYm = "202201"
          // console.log("response[0].closYm", response[0].closYm);
          let [y, m] = this.parseMonth(response[0].closYm)
          let month = this.closYm2Month(m-1)
          let year;
          if(m == 1){
            y = y-1
            year = (y+543).toString().substring(2, 4);
          } else {
            year = (y+543).toString().substring(2, 4);
          }
          
          if( response[0].prstRte == -1 || response[0].prstRte == null ) {
            this.prstRte = `: Pers. ณ เดือน ${month} ${year} : N/A`
          } else {
            let p = this._decimalPipe.transform(response[0].prstRte, '1.2-2');
            this.prstRte = `: Pers. ณ เดือน ${month} ${year} : ${p}%`
          }
         
          sessionStorage.setItem('prstRte', this.prstRte);

        } else {
          this.loadingStatus = false;

          // ====== ADD NEW PHASE 2 ====== //
          this.model_PC.setKpiBoxValue('PC', 0, 0, 0);
          this.model_Active.setKpiBoxValue('ACTIVE', 0, 0, 0);
          this.model_NC.setKpiBoxValue('NC', 0, 0, 0);
          this.model_RYP.setKpiBoxValue('RYP', 0, 0, 0);
          // ====== END NEW PHASE 2 ====== //

        }
      },
      (error) => {
        // console.log('getAgentOverview: ', error);
      }
    );
  }
  callAPI_getAgentPerformance() {
    let data = {
      agentCode: this.modelUser.getAgentCode(),
      agentLevel: this.modelUser.getAgentLevel(),
    };
    this.dataService.getAgentPerformance(data).subscribe(
      (response) => {
        // console.log('res. getAgentPerformance: ', response);
        if (Object.keys(response).length > 0) {
          let result = this.setFormDataService.transform_kpiChartSeries(
            response,
            'pcApprove'
          );
          this.model_PC.setCurrentAndLast(result.current, result.last);

          result = this.setFormDataService.transform_kpiChartSeries(
            response,
            'active'
          );
          this.model_Active.setCurrentAndLast(result.current, result.last);

          result = this.setFormDataService.transform_kpiChartSeries(
            response,
            'nc'
          );
          this.model_NC.setCurrentAndLast(result.current, result.last);

          result = this.setFormDataService.transform_kpiChartSeries(
            response,
            'ryp'
          );
          this.model_RYP.setCurrentAndLast(result.current, result.last);
        }
      },
      (error) => {
        // console.log('getAgentPerformance: ', error);
      }
    );
  }
  callAPI_getAgentTarget() {
    let data = {
      agentCode: this.modelUser.getAgentCode(),
      agentLevel: this.modelUser.getAgentLevel(),
    };
    this.dataService.getAgentTarget(data).subscribe(
      (response) => {
        // // console.log('res. getAgentTarget: ', response);
        if (response && Object.keys(response).length > 0) {
          this.model_PC.setSeriesTarget(
            this.setFormDataService.transform_kpiTargetSeries(response, 'PC')
          );
          this.model_Active.setSeriesTarget(
            this.setFormDataService.transform_kpiTargetSeries(
              response,
              'NEW_ACTIVE_AGENT'
            )
          );
          this.model_NC.setSeriesTarget(
            this.setFormDataService.transform_kpiTargetSeries(
              response,
              'NEW_CODE'
            )
          );
          this.model_RYP.setSeriesTarget(
            this.setFormDataService.transform_kpiTargetSeries(response, 'RYP')
          );
        }
      },
      (error) => {
        // console.log('err. getAgentTarget: ', error);
      }
    );
  }
  callAPI_getAgentYtdPc() {
    let data = {
      agentCode: this.modelUser.getAgentCode(),
      agentLevel: this.modelUser.getAgentLevel(),
    };
    this.dataService.getAgentYtdPc(data).subscribe(
      (response) => {
        // console.log('res. getAgentYtdPc: ', response);
        if (Object.keys(response).length > 0) {
          // this.model_PC.setKpiBoxValue(
          //   'PC',
          //   response[0].pcTarget,
          //   response[0].pcApprove,
          //   response[0].percent
          // );
          this.model_tempKpi.setMasterPcYtd(response[0].pcTarget, response[0].pcApprove, response[0].percent);
        }
      },
      (error) => {
        // console.log('err. getAgentYtdPc: ', error);
      }
    );
  }
  callAPI_getAgentYtdActive() {
    let data = {
      agentCode: this.modelUser.getAgentCode(),
      agentLevel: this.modelUser.getAgentLevel(),
    };
    this.dataService.getAgentYtdActive(data).subscribe(
      (response) => {
        // console.log('res. getAgentYtdActive: ', response);
        if (Object.keys(response).length > 0) {
          // this.model_Active.setKpiBoxValue(
          //   'ACTIVE',
          //   response[0].activeTarget,
          //   response[0].active,
          //   response[0].percent
          // );
          this.model_tempKpi.setMasterActiveYtd(response[0].activeTarget, response[0].active, response[0].percent);
        }
      },
      (error) => {
        // console.log('err. getAgentYtdActive: ', error);
      }
    );
  }
  callAPI_getAgentYtdNc() {
    let data = {
      agentCode: this.modelUser.getAgentCode(),
      agentLevel: this.modelUser.getAgentLevel(),
    };
    this.dataService.getAgentYtdNc(data).subscribe(
      (response) => {
        // console.log('res. getAgentYtdNc: ', response);
        if (Object.keys(response).length > 0) {
          // this.model_NC.setKpiBoxValue(
          //   'NC',
          //   response[0].ncTarget,
          //   response[0].nc,
          //   response[0].percent
          // );
          this.model_tempKpi.setMasterNcYtd(response[0].ncTarget, response[0].nc, response[0].percent);
        }
      },
      (error) => {
        // console.log('err. getAgentYtdNc: ', error);
      }
    );
  }
  callAPI_getAgentYtdRyp() {
    let data = {
      agentCode: this.modelUser.getAgentCode(),
      agentLevel: this.modelUser.getAgentLevel(),
    };
    this.dataService.getAgentYtdRyp(data).subscribe(
      (response) => {
        // console.log('res. getAgentYtdRyp: ', response);
        if (Object.keys(response).length > 0) {
          // this.model_RYP.setKpiBoxValue(
          //   'RYP',
          //   response[0].rypTarget,
          //   response[0].ryp,
          //   response[0].percent
          // );
          this.model_tempKpi.setMasterRypYtd(response[0].rypTarget, response[0].ryp, response[0].percent);
        }
      },
      (error) => {
        // console.log('err. getAgentYtdRyp: ', error);
      }
    );
  }
  // ====== ADD NEW PHASE 2 ====== //
  callAPI_getPerformanceDaily() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    this.dataService.getPerformanceDaily(data).subscribe(
      response => {
        // console.log('res. getPerformanceDaily: ', response);

        if (Object.keys(response).length > 0) {
          for (let index = 0; index < Object.keys(response).length; index++) {
            let date = new Date(response[index].dayOfMonth);
            let dd = String(date.getDate()). padStart(2, '0');
            response[index].dayOfMonth = dd;
          }
          this.modelUser.setTablePerformanceDaily(response);
          let result = this.setFormDataService.transform_performanceData(response, this.masterClosYm);
          this.selectPerformanceDaily = result.list;
          this.modelUser.setPerformanceDailyTotal(result.total);
          this.calendarList = this.setFormDataService.getSelectionCalendarYM(this.masterClosYm);       
          // console.log('TRANSFORM: ', this.selectPerformanceDaily);
        } else {
          //
        }
      },
      error => {
        // console.log('getPerformanceDaily: ', error);
      }
    );
  }
  // ====== END NEW PHASE 2 ====== //

  // [Controller methods]
  // :: call from HTML, button 'compareYear'
  compareYear() {
    this.hideLastYear = !this.hideLastYear;
  }
  // :: call from HTML, button 'HOME'
  goToHome() {
    if (
      this.authService.getPosition() == 'GM' ||
      this.authService.getPosition() == 'AVP' ||
      this.authService.getPosition() == 'AL'
    ) {
      sessionStorage.setItem(
        'agentCodeSelected',
        this.authService.getLoginedName()
      );
      sessionStorage.setItem(
        'agentLevelSelected',
        this.authService.getPosition()
      );
      window.location.reload();
    } else {
      let userRole = this.authService.getPosition();
      let agentCode = this.authService.getLoginedName();
      this.dashboardBackRouteService.backHome(userRole, agentCode);
    }
    sessionStorage.removeItem('agentCodeStored');
    sessionStorage.removeItem('agentLevelStored');
  }

  // Back from browser
  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    this.dashboardBackRouteService.onClickBack();
  }

  // Redirect to TeamDetail
  handleViewTeam() {
    // console.log('Redirect to team detail');
    let redirectionBasedOnKpiType = '/dashboard/producer/kpidetail';
    let queryParams = [
      { kpiType: 'PC', lvName: this.modelUser.getLevelName() },
    ];
    this.checkSessionStorageService.storeSessionChangePage(
      this.modelUser.getAgentCode(),
      this.modelUser.getAgentLevel()
    );
    this.router.navigate([redirectionBasedOnKpiType], {
      queryParams: {
        params: JSON.stringify(queryParams),
      },
    });
  }
  
  // Redirect to Persistency
  onClickPersistency() {
    let redirectionToPer = '/persistency';
    let queryParams = [
      {lvName: this.modelUser.getLevelName() },
    ];
    this.checkSessionStorageService.storeSessionChangePage(
      this.modelUser.getAgentCode(),
      this.modelUser.getAgentLevel()
    );

    this.router.navigate([redirectionToPer], {
      queryParams: {
        params: JSON.stringify(queryParams),
      },
    });
  }
  onClickCompensation() {
    let redirectionToPer = '/compensation';
    let queryParams = [
      {lvName: this.modelUser.getLevelName() },
    ];
    this.checkSessionStorageService.storeSessionChangePage(
      this.modelUser.getAgentCode(),
      this.modelUser.getAgentLevel()
    );

    this.router.navigate([redirectionToPer], {
      queryParams: {
        params: JSON.stringify(queryParams),
      },
    });
  }
  onClickPromotion(mode) {
    let redirectionToPromotion = '/promotion';
    let queryParams = [
      {lvName: this.modelUser.getLevelName() },
      {filterPromotion: mode}
    ];
    this.checkSessionStorageService.storeSessionChangePage(
      this.modelUser.getAgentCode(),
      this.modelUser.getAgentLevel()
    );

    this.router.navigate([redirectionToPromotion], {
      queryParams: {
        params: JSON.stringify(queryParams),
      },
    });
  }

  // Handle open modal
  open(content) {
    this.modalService.open(content, {centered: true, windowClass : "xxl" }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed`;
    });
  }
  scrollTop() {
    $('body,html').animate({
      scrollTop: 0
    }, 400);
  }

  // ====== ADD NEW PHASE 3 ====== //
  // Switch KPI summary between MTD and YTD
  handlerKpiPeriodMode(type: string) {
    if (type == 'MTD') {
      this.kpiPeriodMode_status = true;
      this.kpiPeriodMode_name = 'MTD';
      this.setKpiMtd();
    }
    else {
      this.kpiPeriodMode_status = false;
      this.kpiPeriodMode_name = 'YTD';
      this.setKpiYtd();
    }
  }
  setKpiMtd() {
    this.model_PC.setKpiBoxValue(
      'PC',
      this.model_tempKpi.masterPcMtd.target,
      this.model_tempKpi.masterPcMtd.value,
      this.model_tempKpi.masterPcMtd.percent
    );
    this.model_Active.setKpiBoxValue(
      'ACTIVE',
      this.model_tempKpi.masterActiveMtd.target,
      this.model_tempKpi.masterActiveMtd.value,
      this.model_tempKpi.masterActiveMtd.percent
    );
    this.model_NC.setKpiBoxValue(
      'NC',
      this.model_tempKpi.masterNcMtd.target,
      this.model_tempKpi.masterNcMtd.value,
      this.model_tempKpi.masterNcMtd.percent
    );
    this.model_RYP.setKpiBoxValue(
      'RYP',
      this.model_tempKpi.masterRypMtd.target,
      this.model_tempKpi.masterRypMtd.value,
      this.model_tempKpi.masterRypMtd.percent
    );
  }
  setKpiYtd() {
    this.model_PC.setKpiBoxValue(
      'PC',
      this.model_tempKpi.masterPcYtd.target,
      this.model_tempKpi.masterPcYtd.value,
      this.model_tempKpi.masterPcYtd.percent
    );
    this.model_Active.setKpiBoxValue(
      'ACTIVE',
      this.model_tempKpi.masterActiveYtd.target,
      this.model_tempKpi.masterActiveYtd.value,
      this.model_tempKpi.masterActiveYtd.percent
    );
    this.model_NC.setKpiBoxValue(
      'NC',
      this.model_tempKpi.masterNcYtd.target,
      this.model_tempKpi.masterNcYtd.value,
      this.model_tempKpi.masterNcYtd.percent
    );
    this.model_RYP.setKpiBoxValue(
      'RYP',
      this.model_tempKpi.masterRypYtd.target,
      this.model_tempKpi.masterRypYtd.value,
      this.model_tempKpi.masterRypYtd.percent
    );
  }
  // ====== END NEW PHASE 3 ====== //
  // Utilities
  closYm2Month(closYm) {
    switch (closYm) {
      case 0:
        return 'ธ.ค.'
      case 1:
        return 'ม.ค.'
      case 2:
        return 'ก.พ.'
      case 3:
        return 'มี.ค.'
      case 4:
        return 'เม.ย.'
      case 5:
        return 'พ.ค.'
      case 6:
        return 'มิ.ย.'
      case 7:
        return 'ก.ค.'
      case 8:
        return 'ส.ค.'
      case 9:
        return 'ก.ย.'
      case 10:
        return 'ต.ค.'
      case 11:
        return 'พ.ย.'
      case 12:
        return 'ธ.ค.'
    }
  }
  parseMonth(str) {
    return str.match(/(\d{4})(\d{2})/).splice(1).map(Number)
  }
}
