import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartRtComponent } from './chart-rt.component';

describe('ChartRtComponent', () => {
  let component: ChartRtComponent;
  let fixture: ComponentFixture<ChartRtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartRtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartRtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
