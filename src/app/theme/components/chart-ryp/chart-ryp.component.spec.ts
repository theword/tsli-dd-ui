import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartRypComponent } from './chart-ryp.component';

describe('ChartRypComponent', () => {
  let component: ChartRypComponent;
  let fixture: ComponentFixture<ChartRypComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartRypComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartRypComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
