import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartActComponent } from './chart-act.component';

describe('ChartActComponent', () => {
  let component: ChartActComponent;
  let fixture: ComponentFixture<ChartActComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartActComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartActComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
