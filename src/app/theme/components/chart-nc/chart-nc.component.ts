import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Chart } from 'node_modules/chart.js';

@Component({
  selector: 'app-chart-nc',
  templateUrl: './chart-nc.component.html',
  styleUrls: ['./chart-nc.component.scss']
})
export class ChartNcComponent implements OnInit {

  @Input() dataCurrentYear = [];
  @Input() dataLastYear = [];
  @Input() dataTarget = [];
  @Input() isShowLastyear: boolean = true;
  @Input() chartZoom: boolean = false;

  private mixedChart;
  private gradientCurrent;
  private gradientLast;

  currentYear = parseInt(sessionStorage.getItem('calendarCurrentYear'));
  
  constructor() { }

  ngOnInit(): void {
    var canvas = <HTMLCanvasElement> document.getElementById('chartNC');
    var ctx = canvas.getContext("2d");
    this.gradientCurrent = ctx.createLinearGradient(0, 0, 0, 200);
    this.gradientCurrent.addColorStop(0, 'rgba(0,166,227,1)');   
    this.gradientCurrent.addColorStop(1, 'rgba(2,77,161,1)');

    this.gradientLast = ctx.createLinearGradient(0, 0, 0, 200);
    this.gradientLast.addColorStop(0, 'rgba(255, 222, 168, 1)');   
    this.gradientLast.addColorStop(1, 'rgba(254, 200, 78, 1)');

    if (ctx) {
      if (this.mixedChart) {
        this.mixedChart.destroy();
      }
      this.renderChart();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.mixedChart) {
      this.mixedChart.destroy();
      this.renderChart();
    }
  }

  renderChart() {
    let label_last = (this.currentYear-1).toString()
    let label_cur = this.currentYear.toString();
    
    this.mixedChart = new Chart('chartNC', {
      type: 'bar',
      data: {
        datasets: [
          // Last Year
          {
            hidden: this.isShowLastyear,
            label: label_last,
            data: this.dataLastYear,
            backgroundColor: this.gradientLast,
            order: 2
          },
          // Current Year
          {
            label: label_cur,
            data: this.dataCurrentYear,
            backgroundColor: this.gradientCurrent,
            order: 2
          },
          {
            label: 'target',
            data: this.dataTarget,
            type: 'line',
            backgroundColor: '#012e5c',
            borderColor: '#012e5c',
            pointBorderColor: '#012e5c',
            pointBorderWidth: 2,
            pointStyle: 'star',
            fill : false,
            order: 1,
            lineTension: 0
          }
        ],
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          xAxes: [{
            gridLines: {
              display: false
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              precision:0,
              min: 0,
              callback: function(label, index, labels) { return label.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            },
            gridLines: {
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false,
          labels: {
            fontColor: '#000',
            defaultFontSize : 5
          },
          position: 'top',
          align: 'center',
        },
        tooltips: { 
          mode: 'label', 
          label: 'mylabel', 
          callbacks: { 
            label: function(tooltipItem, data) { 
              return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
            },
            labelColor: function(tooltipItem, chart) {
              var dataset = chart.config.data.datasets[tooltipItem.datasetIndex];
              return {
                  backgroundColor : dataset.backgroundColor
              }
          },
        }
      }
    }
    });

  }

}
