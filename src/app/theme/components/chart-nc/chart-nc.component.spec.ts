import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartNcComponent } from './chart-nc.component';

describe('ChartNcComponent', () => {
  let component: ChartNcComponent;
  let fixture: ComponentFixture<ChartNcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartNcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartNcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
