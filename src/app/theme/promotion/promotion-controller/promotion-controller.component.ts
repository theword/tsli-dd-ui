import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModelUser } from 'src/app/core/model/model-user';
import { AuthService } from 'src/app/core/security/auth.service';
import { DashboardBackRouteService } from 'src/app/core/service/dashboard-back-route.service';
import { Location } from '@angular/common';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesHelper } from 'src/app/core/utilities-helper';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Variable } from '@angular/compiler/src/render3/r3_ast';

@Component({
  selector: 'app-promotion-controller',
  templateUrl: './promotion-controller.component.html',
  styleUrls: ['./promotion-controller.component.scss']
})
export class PromotionControllerComponent implements OnInit {

  public modelUser: ModelUser;

  _resData;
  displayLvName: any = '';
  agentName: string;
  agentLevel: string;
  agentCode: string;
  titleCode: string;
  detailArray:any;
  headerArray:any;

  listFilterPromotion: any[] = ['สรุปการแข่งขัน', 'คาดการณ์การแข่งขัน']
  filterPromotion: string;
  dataList :any = [];
  dataDetail :any = [];
  closeResult = '';
  headerDetail = [];
  data :any =[];
  prstRte:any

  hilight = 0;
  public headersList: any = [];

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private dataService: DataServiceService,
    private dashboardBackRouteService: DashboardBackRouteService,
    private location: Location,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) { 
    this.modelUser = new ModelUser();
  }

  ngOnInit(): void {
    // Get KPI filter type from AgentOverview by params URL. 
    this.route.queryParams.subscribe(params => {
      try{
        let getParam = JSON.parse(params.params);
        if (getParam[0]['lvName'] !== 'ZONE') {
          this.displayLvName = getParam[0]['lvName'];
          this.filterPromotion = getParam[1]['filterPromotion']
        }
      }catch(e){
        /**/
        this.displayLvName = this.authService.getPosition();
         this.filterPromotion = this.listFilterPromotion[0];
      }
    });

    var tempAgentCode = sessionStorage.getItem('agentCodeSelected') || this.authService.getLoginedName();
    var tempAgentLevel = sessionStorage.getItem('agentLevelSelected')|| this.authService.getPosition();
    this.agentCode = tempAgentCode;
    this.agentLevel = tempAgentLevel;
    // var tempAgentLevel = sessionStorage.getItem('agentLevelSelected');
    this.titleCode = tempAgentCode;
    this.callAPI_getAgentPersonalInfo();
    this.setHeadersPromotionList();
    this.apiService_promotionList();

    this.prstRte = sessionStorage.getItem('prstRte')
  }

  // [Calling API Service methods]
  callAPI_getAgentPersonalInfo() {
    let data = {
      "agentCode": this.agentCode
    };
    this.dataService.getAgentStructure(data).subscribe(
      response => {
        // console.log('res. getAgentStructure: ', response);
        this.agentName = response[0].thaiName + ' ' + response[0].thaiSurname;
        this.agentLevel = response[0].agentLevel;
      },
      error => {
        // console.log('err. getAgentStructure: ', error);
      });
  }
  apiService_promotionList() {
    this.spinner.show();
    this.dataList = [];
    const _postData = {
      "filter": this.translateFilterMode(),
      "agentCode": this.agentCode,
      "agentLevel": this.agentLevel,
      "channelCode": sessionStorage.getItem('channelFilterSelected'),
      "zoneCode": sessionStorage.getItem('zoneFilterSelected')
    };
    this.dataService.postPromotionList(_postData).subscribe(
      response => {
        this.spinner.hide();
        this.dataList = response;
        console.log('res. postPromotionList: ', response);  
      },
      error => {
        console.log('err. postPromotionList: ', error);
      });
  }
  apiService_promotionDetail(content,contestCode: string = '') {
    this.headerDetail=[];
    this.setHeaderAgentLevel();
    this.spinner.show();
    const _postData = {
      "filter": contestCode,
      "filterType": this.translateFilterMode(),
      "agentCode": this.agentCode,
      "agentLevel": this.agentLevel,
      "channelCode": sessionStorage.getItem('channelFilterSelected'),
      "zoneCode": sessionStorage.getItem('zoneFilterSelected')
    };
    this.dataService.postPromotionDetail(_postData).subscribe(
      response => {
        this.dataDetail = response;
        this.spinner.hide();
        // console.log('res. postPromotionDetail: ', response);

        var res = response;

        // CONVERT 2 ARRAY : HEADER
        this.headerArray = this.cvStr2Arr(res[0].header);
        // console.log('as array: ', this.headerArray, ' | ', this.headerArray.length);

        // CONVERT 2 ARRAY : DETAIL
        this.setDetailData(res);
        this.detailArray = res[0].detail;
        this.data =  res;
        // console.log('as array: ', this.detailArray, ' | ', this.detailArray.length);

        if (this.headerArray.length != this.detailArray.length) {
          console.log('[PromotionDetail] MIS-MATCH NUMBER OF HEADER AND DETAIL');
        }
        
        this.setHeaderPromotionDetail();
        // Open Modal
        this.modalService.open(content, {centered: true, windowClass : "", size: "xl", scrollable: true}).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          this.closeResult = `Dismissed`;
        });
      },
      error => {
        console.log('err. postPromotionDetail: ', error);
      });
  }

  // Route Controller
  goToHome() {
    let userRole = this.authService.getPosition();
    let agentCode = this.authService.getLoginedName();
    sessionStorage.removeItem('agentCodeStored');
    sessionStorage.removeItem('agentLevelStored');
    this.dashboardBackRouteService.backHome(userRole, agentCode);
  }
  goToOverview() {
    this.location.back();
  }

  // Utilities
  translateFilterMode() {
    if (this.filterPromotion == 'สรุปการแข่งขัน') return 'RESULT';
    else if (this.filterPromotion == 'คาดการณ์การแข่งขัน') return 'POTENTIAL';
  }
  setHeadersPromotionList() {
    if(this.filterPromotion == 'สรุปการแข่งขัน')
    {
      this.headersList = ['เดือน','การแข่งขัน','สรุปรายชื่อผู้รับรางวัล'];
    }
    else if(this.filterPromotion == 'คาดการณ์การแข่งขัน')
    {
      this.headersList = ['เดือน','การแข่งขัน','คาดการณ์'];
    }
  }
  handleFilterPromotion(mode){
    this.filterPromotion = mode;
    this.setHeadersPromotionList();
    this.apiService_promotionList();
  }
  handleQueryDetail(content,data) {
    this.apiService_promotionDetail(content,data);
  }

  setHeaderPromotionDetail(){
    if(this.dataDetail[0].type == "RESULT")
    {
      this.insertDetailHeader();
    }
    else if(this.dataDetail[0].type == "POTENTIAL")
    {
      // this.headerDetail.push('Level','ชื่อการแข่งขัน','รหัสการแข่งขัน','Description')
      this.headerDetail.push('Level')
      this.insertDetailHeader();
    }
  }

  insertDetailHeader(){
    var i;
    for(i =0; i<this.headerArray.length; i++)
    {
      this.headerDetail.push(this.headerArray[i]);
    }
  }
  // Re-Format data
  cvStr2Arr(data: string = ''){
    console.log(data);
    var _string2Array = data;
    _string2Array = _string2Array.substring(1, _string2Array.length - 1);
    return _string2Array.split('||');
  }

  setDetailData(res){
    var i;
    for(i =0; i<res.length; i++)
    {
      res[i].detail = this.cvStr2Arr(res[i].detail)
    }
  }
  setHeaderAgentLevel(){
    if(this.agentLevel == 'GM' || this.agentLevel == 'ZONE'){
      this.headerDetail.push('GM','VP','AL','AG');
      
    }
    else if(this.agentLevel == 'AVP'){
      this.headerDetail.push('VP','AL','AG');
    }
    else if(this.agentLevel == 'AL'){
      this.headerDetail.push('AL','AG');
    }
    else if(this.agentLevel == 'AGENT'){
      this.headerDetail.push('AG');
    }

    this.hilight = this.headerDetail.length;

    
  }
}
