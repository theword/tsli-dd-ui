import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionControllerComponent } from './promotion-controller.component';

describe('PromotionControllerComponent', () => {
  let component: PromotionControllerComponent;
  let fixture: ComponentFixture<PromotionControllerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromotionControllerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
