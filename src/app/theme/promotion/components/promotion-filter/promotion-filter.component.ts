import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-promotion-filter',
  templateUrl: './promotion-filter.component.html',
  styleUrls: ['./promotion-filter.component.scss']
})
export class PromotionFilterComponent implements OnInit {

  @Input() listFilterPromotion = []
  @Input() filterPromotion: string
  
  @Output() changeFilterPromotion = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onChangeFilterPromotion(mode){
    this.changeFilterPromotion.emit(mode);
  }

}
