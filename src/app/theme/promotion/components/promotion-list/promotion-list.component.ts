import { Component, OnInit, Input, Output, EventEmitter, HostListener, ViewChild } from '@angular/core';
import { ClosYm2Month } from '../../../persistency/services/closYm2Month';
import {MdbTableDirective} from 'angular-bootstrap-md';

@Component({
  selector: 'app-promotion-list',
  templateUrl: './promotion-list.component.html',
  styleUrls: ['./promotion-list.component.scss']
})
export class PromotionListComponent implements OnInit {
  @ViewChild(MdbTableDirective, {static: true}) mdbTable: MdbTableDirective;

  @Input() filterPromotion: string;
  @Input() headers: any[];
  @Input() dataList: any[];
  @Output() onClickDetail = new EventEmitter();

  searchText: string = '';
  previous: string;

  constructor(
    private ClosYm2Month: ClosYm2Month
  ) { }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  ngOnInit(): void {

  }

  ngOnChanges() {
    this.mdbTable.setDataSource(this.dataList);
    this.previous = this.mdbTable.getDataSource();
  }

  handelClickDetail(contestCode) {
    this.onClickDetail.emit(contestCode);
  }

  // Services
  convertClosYm(closYm){
    return this.ClosYm2Month.langTH(closYm)
  }
  convertYear(closYm){
    return this.ClosYm2Month.cd2bd(closYm)
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();
    console.log("prev", prev);
    console.log("searchText", this.searchText);
    
    if (!this.searchText) {
        this.mdbTable.setDataSource(this.previous);
        this.dataList = this.mdbTable.getDataSource();
    }
    if (this.searchText) {
        this.dataList = this.mdbTable.searchLocalDataBy(this.searchText);
        this.mdbTable.setDataSource(prev);
    }
  }

}
