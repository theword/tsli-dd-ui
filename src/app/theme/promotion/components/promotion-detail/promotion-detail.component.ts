import { Component, OnInit, Input, ViewChild, HostListener } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {MdbTableDirective} from 'angular-bootstrap-md';

@Component({
  selector: 'app-promotion-detail',
  templateUrl: './promotion-detail.component.html',
  styleUrls: ['./promotion-detail.component.scss']
})
export class PromotionDetailComponent implements OnInit {
  @ViewChild(MdbTableDirective, {static: true}) mdbTable: MdbTableDirective;

  @Input() dataDetail: any[];
  @Input() headerDetail: any[];
  @Input() data: any[];
  @Input() agentLevel:any;
  @Input() filterPromotion: string;
  @Input() hilight: number;
  
  searchText: string = '';
  previous: string;

  constructor(    private modalService: NgbModal ) { }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  ngOnInit(): void {
    this.mdbTable.setDataSource(this.data);
    this.previous = this.mdbTable.getDataSource();
  }

  isAG(Level: string){
    var masterLevel = 'AGENT AL AVP GM ZONE'
    return masterLevel.includes(Level);
  }

  isAL(Level: string){
    var masterLevel = 'AL AVP GM ZONE'
    return masterLevel.includes(Level);
  }

  isAVP(Level: string){
    var masterLevel = 'AVP GM ZONE'
    return masterLevel.includes(Level);
  }

  isGM(Level: string){
    var masterLevel = 'GM ZONE'
    return masterLevel.includes(Level);
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();
    if (!this.searchText) {
        this.mdbTable.setDataSource(this.previous);
        this.data = this.mdbTable.getDataSource();
    }
    if (this.searchText) {
        this.data = this.mdbTable.searchLocalDataBy(this.searchText);
        this.mdbTable.setDataSource(prev);
    }
  }

  dismiss(){
    this.modalService.dismissAll("Cross click");
  }
   

}
