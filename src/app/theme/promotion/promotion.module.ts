import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromotionControllerComponent } from './promotion-controller/promotion-controller.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/core/shared.module';
import { PromotionFilterComponent } from './components/promotion-filter/promotion-filter.component';
import { PromotionListComponent } from './components/promotion-list/promotion-list.component';
import { PromotionDetailComponent } from './components/promotion-detail/promotion-detail.component';

const routes: Routes = [
  {
    path: '',
    component: PromotionControllerComponent
  },
  {
    path: '**', redirectTo: 'error', pathMatch: 'full'
  }
]

@NgModule({
  declarations: [
    PromotionControllerComponent, 
    PromotionFilterComponent, 
    PromotionListComponent,
    PromotionDetailComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule
  ]
})
export class PromotionModule { }
