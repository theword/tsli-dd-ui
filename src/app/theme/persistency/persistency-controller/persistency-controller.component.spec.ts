import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersistencyControllerComponent } from './persistency-controller.component';

describe('PersistencyControllerComponent', () => {
  let component: PersistencyControllerComponent;
  let fixture: ComponentFixture<PersistencyControllerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersistencyControllerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersistencyControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
