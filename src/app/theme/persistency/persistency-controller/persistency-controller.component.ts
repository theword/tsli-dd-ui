import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModelUser } from 'src/app/core/model/model-user';
import { AuthService } from 'src/app/core/security/auth.service';
import { DashboardBackRouteService } from 'src/app/core/service/dashboard-back-route.service';
import { Location } from '@angular/common';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesHelper } from 'src/app/core/utilities-helper';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-persistency-controller',
  templateUrl: './persistency-controller.component.html',
  styleUrls: ['./persistency-controller.component.scss']
})
export class PersistencyControllerComponent implements OnInit {

  public modelUser: ModelUser;
  
  displayLvName: any = '';
  agentName: string;
  agentLevel: string = "AL";
  agentCode: string;
  titleCode: string;

  listFilterGroup: any[] = ['Per ส่วนตัว', 'Per สายงาน']
  listFilterPersistency: any[] = ['13', '25']
  listFilterType: any[] = ['Pers ปัจจุบัน', 'ที่ต้องเก็บ']

  filterGroup: string = 'Per ส่วนตัว'
  filterPersistency: string = "13"
  filterType: string = "Pers ปัจจุบัน"

  summaryData: any = {}
  summaryTableData: any = []
  _summaryTableData: any = []
  toCollectTableData: any = []
  checkBlockStatus:any = []

  public searchKeyword: string = '';
  public pagingCount: number = 0;
  public totalPage: number = 0;

  public countSearchLoad: number = 0;
  public countNormLoad: number = 0;
  public countCheckCal: number = 0;

  public masterSummary: any = {};
  public masterToCollectData: any = [];

  public masterClosYm: string = '';
  public showPrstRte: number = 0;

  private sortColName: string = '';
  private sortOrder: string = '';
  
  prstRte:any

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private dataService: DataServiceService,
    private dashboardBackRouteService: DashboardBackRouteService,
    private location: Location,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
  ) { 
    this.modelUser = new ModelUser();
  }

  ngOnInit(): void {
    // Get KPI filter type from AgentOverview by params URL. 
    this.route.queryParams.subscribe(params => {
      let getParam = JSON.parse(params.params);
      if (getParam[0]['lvName'] !== 'ZONE') {
        this.displayLvName = getParam[0]['lvName'];
      }
    });

    var tempAgentCode = sessionStorage.getItem('agentCodeSelected');
    var tempAgentLevel = sessionStorage.getItem('agentLevelSelected');
    this.agentCode = tempAgentCode;
    this.agentLevel = tempAgentLevel;
    // var tempAgentLevel = sessionStorage.getItem('agentLevelSelected');
    this.titleCode = tempAgentCode;
    this.callAPI_setMasterClosYm();
    this.callAPI_getAgentPersonalInfo();
    this.handlerOnChangeQuery();
    this.prstRte = sessionStorage.getItem('prstRte')
  }

  // [Calling API Service methods]
  callAPI_getAgentPersonalInfo() {
    let data = {
      "agentCode": this.agentCode
    };
    this.dataService.getAgentStructure(data).subscribe(
      response => {
        // console.log('res. getAgentStructure: ', response);
        this.agentName = response[0].thaiName + ' ' + response[0].thaiSurname;
        this.agentLevel = response[0].agentLevel;
      },
      error => {
        // console.log('err. getAgentStructure: ', error);
      });
  }
  callAPI_setMasterClosYm() {
    // Set masterClosYm
    this.dataService.getClosYmInfo().subscribe(
      response => {
        // console.log('res. getClosYmInfo: ', Object.values(response)[0]);
        this.masterClosYm = Object.values(response)[0];
      },
      error => {
        // console.log('getPerformanceDaily: ', error);
      }
    );
  }

  // Child components emit handler
  handleFilterGroup(group: string){
    console.log("group", group);
    
    this.filterGroup = group;
    if (this.filterType == 'ที่ต้องเก็บ') {
      this.resetDataToCollect();
    }
    this.handlerOnChangeQuery();
  }
  handleFilterPersistency(persistency: string){
    console.log("persistency", persistency);

    this.filterPersistency = persistency;
    if (this.filterType == 'ที่ต้องเก็บ') {
      this.resetDataToCollect();
    }
    this.handlerOnChangeQuery();
  }
  handleFilterType(type: string){
    console.log("type", type);

    this.filterType = type;
    this.handlerOnChangeQuery();
  }

  // API Service
  apiService_persistencySummary() {
    this.spinner.show();
    const _postData = {
      // filter = persistency type [STRING] --> 13 OR 25
      "filter": this.filterPersistency,
      // "filter": this.translatePersistencyType(),
      // agentCode = current agent code
      "agentCode": this.agentCode,
      // agentLevel = depends on filter group
      //    if filterGroup == 'individual' then always set as 'AG'
      //    else based on agent level
      // Please use method --> translateAgentLevel() //
      "agentLevel": this.translateAgentLevel(),
    };
    
    this.dataService.postPersistencySummary(_postData).subscribe(
      response => {
        this.spinner.hide();
        this.summaryTableData = response;
        // this._summaryTableData = this.summaryTableData;
        this.summaryData = { ...response[0] } ;
        
        if(this.summaryTableData.length > 0){
          let sumTargetPC: number = this.summaryTableData.map(a => a.targetPc).reduce(function(a, b){
            return a + b;
          });
        
          let sumPlusPc: number = this.summaryTableData.map(a => a.plusPc).reduce(function(a, b){
            return a + b;
          });
        
          let sumCollectPc: number = this.summaryTableData.map(a => a.collectPc).reduce(function(a, b){
            return a + b;
          });

          this.summaryData.targetPc = sumTargetPC
          this.summaryData.plusPc = sumPlusPc
          this.summaryData.collectPc = sumCollectPc
          this.summaryData.prstRte = ((sumPlusPc + sumCollectPc)/sumTargetPC) * 100
          console.log("this.summaryData.prstRte", this.summaryData.prstRte);
          
          if (this.summaryData.prstRte >= 100){
            this.showPrstRte = 100;
          } else {
            this.showPrstRte = this.summaryData.prstRte;
          }

          this.masterSummary = { ...this.summaryData};
        } 

        // clone response (unchange data)
        // console.log('res. postPersistencySummary: ', this.masterSummary);
      },
      error => {
        console.log('err. postPersistencySummary: ', error);
    }); 
  }
  apiService_persistencyDetail(sortingDetail = {sortColName: '', sortOrder: ''}) {
    if (this.countSearchLoad > 0) {
      this.resetDataToCollect();
      this.countSearchLoad = 0;
    }
    if (this.pagingCount == 0 || 
      (this.sortColName.length > 0 && this.sortOrder.length > 0)
    ) {
      this.resetDataToCollectWOPagingCount();
    }
    if (sortingDetail.sortColName.length > 0 && sortingDetail.sortOrder.length > 0) {
      this.sortColName = sortingDetail.sortColName;
      this.sortOrder = sortingDetail.sortOrder;
      this.resetDataToCollectWOPagingCount();
    }
    this.countNormLoad += 1;
    this.spinner.show();

    // CHECK input as data?
    var _prefixSearch = '%' + this.searchKeyword + '%';
    if (this.searchKeyword.length == 10 && this.searchKeyword.includes('/')) {
      _prefixSearch = '%' + UtilitiesHelper.convertToRawDate(this.searchKeyword) + '%';
    }

    const _postData = {
      "filter": this.filterPersistency,
      "agentCode": this.agentCode,
      "searchKeyword": _prefixSearch,
      "paginationPage": this.pagingCount,
      "sortColName": this.sortColName,
      "sortOrderType": this.sortOrder
    };
    // console.log(':POST: ', _postData);
    
    let _resData;
    if (this.filterGroup !== 'Per ส่วนตัว') {
      switch (this.agentLevel.toString()) {
        case 'AGENT':
          this.dataService.postPersistencyDetailAg(_postData).subscribe(
            response => {
              this.spinner.hide();
              _resData = response;
              if (_resData.content.length == 0 && (
                this.sortColName.length > 0 && this.sortOrder.length > 0
              )) {
                this.toCollectTableData = [];
                this.masterToCollectData = [];
              } else {
                this.toCollectTableData.push(..._resData.content);
                this.masterToCollectData.push(...JSON.parse(JSON.stringify(_resData.content)));
              }
              this.totalPage = _resData.totalPage
            
              // console.log('res. postPersistencyDetailAg: ', response);

              this.apiService_persistencySummary()
            },
            error => {
              console.log('err. postPersistencyDetailAg: ', error);
            }
          );
          break;
        case 'AL':
          this.dataService.postPersistencyDetailAl(_postData).subscribe(
            response => {
              this.spinner.hide();
              _resData = response;
              if (_resData.content.length == 0 && (
                this.sortColName.length > 0 && this.sortOrder.length > 0
              )) {
                this.toCollectTableData = [];
                this.masterToCollectData = [];
              } else {
                this.toCollectTableData.push(..._resData.content);
                this.masterToCollectData.push(...JSON.parse(JSON.stringify(_resData.content)));
              }
              this.totalPage = _resData.totalPage
            
              // console.log('res. postPersistencyDetailAl: ', response);

              this.apiService_persistencySummary()
            },
            error => {
              console.log('err. postPersistencyDetailAl: ', error);
          });
          break;
        case 'AVP':
          this.dataService.postPersistencyDetailAvp(_postData).subscribe(
            response => {
              this.spinner.hide();
              _resData = response;
              if (_resData.content.length == 0 && (
                this.sortColName.length > 0 && this.sortOrder.length > 0
              )) {
                this.toCollectTableData = [];
                this.masterToCollectData = [];
              } else {
                this.toCollectTableData.push(..._resData.content);
                this.masterToCollectData.push(...JSON.parse(JSON.stringify(_resData.content)));
              }
              this.totalPage = _resData.totalPage
            
              // console.log('res. postPersistencyDetailAvp: ', response);

              this.apiService_persistencySummary()
            },
            error => {
              console.log('err. postPersistencyDetailAvp: ', error);
          });
          break;
        case 'GM':
          this.dataService.postPersistencyDetailGm(_postData).subscribe(
            response => {
              this.spinner.hide();
              _resData = response;
              if (_resData.content.length == 0 && (
                this.sortColName.length > 0 && this.sortOrder.length > 0
              )) {
                this.toCollectTableData = [];
                this.masterToCollectData = [];
              } else {
                this.toCollectTableData.push(..._resData.content);
                this.masterToCollectData.push(...JSON.parse(JSON.stringify(_resData.content)));
              }
              this.totalPage = _resData.totalPage
              
              // console.log('res. postPersistencyDetailGm: ', response);
          
              this.apiService_persistencySummary()
            },
            error => {
              console.log('err. postPersistencyDetailGm: ', error);
          });
          break;
        default:
          break;
      } // End SwitchCase
    } // End Filter Group != Individual
    else {
      this.dataService.postPersistencyDetailAg(_postData).subscribe(
        response => {
          this.spinner.hide();
          _resData = response;
          if (_resData.content.length == 0 && (
            this.sortColName.length > 0 && this.sortOrder.length > 0
          )) {
            this.toCollectTableData = [];
            this.masterToCollectData = [];
          } else {
            this.toCollectTableData.push(..._resData.content);
            this.masterToCollectData.push(...JSON.parse(JSON.stringify(_resData.content)));
          }
          this.totalPage = _resData.totalPage
            
          // console.log('res. postPersistencyDetailAg: ', response);

          this.apiService_persistencySummary()
        },
        error => {
          console.log('err. postPersistencyDetailAg: ', error);
        }
      );
    }
  }
  apiService_searchPerDetail() {
    if (this.searchKeyword.length < 1) {
      this.apiService_persistencyDetail();
    } else {
      if (this.countNormLoad > 0) {
        this.resetDataToCollect();
        this.countNormLoad = 0;
      }
      if (this.countSearchLoad > 0) {
        this.resetDataToCollect();
        this.countSearchLoad = 0;
      }
      this.countSearchLoad += 1;
      this.spinner.show();
      var _prefixSearch = '%' + this.searchKeyword + '%';
      var _resData;

      // CHECK input as data?
      if (this.searchKeyword.length == 10 && this.searchKeyword.includes('/')) {
        _prefixSearch = '%' + UtilitiesHelper.convertToRawDate(this.searchKeyword) + '%';
      }
      
      // Declare postData (searchInfomation)
      const _postData = {
        "filter": this.filterPersistency,
        "searchKeyword": _prefixSearch,
        "agentCode": this.agentCode,
        "paginationPage": this.pagingCount
      };

      if (this.filterGroup !== 'Per ส่วนตัว') {
        switch (this.agentLevel.toString()) {
          case 'AGENT': 
            this.dataService.searchPersistencyDetailAg(_postData).subscribe(
              response => {
                // console.log('res. searchPersistencyDetailAg: ', response);
                _resData = response;
                this.spinner.hide();
                if (_resData.content.length == 0) {
                  this.toCollectTableData = [];
                  this.masterToCollectData = [];
                } else {
                  this.toCollectTableData.push(..._resData.content);
                  this.masterToCollectData.push(...JSON.parse(JSON.stringify(_resData.content)));
                }
                this.totalPage = _resData.totalPage;
                this.apiService_persistencySummary();
              },
              error => {
                console.log('err. searchPersistencyDetailAg: ', error);
              }
            );
            break;
          case 'AL':
            this.dataService.searchPersistencyDetailAl(_postData).subscribe(
              response => {
                // console.log('res. searchPersistencyDetailAl: ', response);
                _resData = response;
                this.spinner.hide();
                if (_resData.content.length == 0) {
                  this.toCollectTableData = [];
                  this.masterToCollectData = [];
                } else {
                  this.toCollectTableData.push(..._resData.content);
                  this.masterToCollectData.push(...JSON.parse(JSON.stringify(_resData.content)));
                }
                this.totalPage = _resData.totalPage;
                this.apiService_persistencySummary();
              },
              error => {
                console.log('err. searchPersistencyDetailAl: ', error);
              }
            );
            break;
          case 'AVP':
            this.dataService.searchPersistencyDetailAvp(_postData).subscribe(
              response => {
                // console.log('res. searchPersistencyDetailAvp: ', response);
                _resData = response;
                this.spinner.hide();
                if (_resData.content.length == 0) {
                  this.toCollectTableData = [];
                  this.masterToCollectData = [];
                } else {
                  this.toCollectTableData.push(..._resData.content);
                  this.masterToCollectData.push(...JSON.parse(JSON.stringify(_resData.content)));
                }
                this.totalPage = _resData.totalPage;
                this.apiService_persistencySummary();
              },
              error => {
                console.log('err. searchPersistencyDetailAvp: ', error);
              }
            );
            break;
          case 'GM':
            this.dataService.searchPersistencyDetailGm(_postData).subscribe(
              response => {
                // console.log('res. searchPersistencyDetailGm: ', response);
                _resData = response;
                this.spinner.hide();
                if (_resData.content.length == 0) {
                  this.toCollectTableData = [];
                  this.masterToCollectData = [];
                } else {
                  this.toCollectTableData.push(..._resData.content);
                  this.masterToCollectData.push(...JSON.parse(JSON.stringify(_resData.content)));
                }
                this.totalPage = _resData.totalPage;
                this.apiService_persistencySummary();
              },
              error => {
                console.log('err. searchPersistencyDetailGm: ', error);
              }
            );
            break;
          default:
            break;
        }
      }
      // FilterGroup == 'Per ส่วนตัว' Search on level AG
      else {
        this.dataService.searchPersistencyDetailAg(_postData).subscribe(
          response => {
            // console.log('res. searchPersistencyDetailAg: ', response);
            _resData = response;
            this.spinner.hide();
            if (_resData.content.length == 0) {
              this.toCollectTableData = [];
              this.masterToCollectData = [];
            } else {
              this.toCollectTableData.push(..._resData.content);
              this.masterToCollectData.push(...JSON.parse(JSON.stringify(_resData.content)));
            }
            this.totalPage = _resData.totalPage;
            this.apiService_persistencySummary();
          },
          error => {
            console.log('err. searchPersistencyDetailAg: ', error);
          }
        );
      }
    }
  }

  // Utilities
  handlerOnChangeQuery() {
    if (this.filterType == 'Pers ปัจจุบัน') {
      this.apiService_persistencySummary();
    } else {
      this.apiService_persistencyDetail();
    }
  }
  translateAgentLevel() {
    if (this.filterGroup == 'Per ส่วนตัว') {
      return 'AGENT';
    } else {
      return this.agentLevel;
    }
  }
  handleSumPC(event: string){
    this.summaryData.collectPc = this.summaryData.collectPc + event
    this.summaryData.prstRte = ((this.summaryData.collectPc + this.summaryData.plusPc)/this.summaryData.targetPc*100).toFixed(2)
    // console.log("this.summaryData.prstRte: ",this.summaryData.prstRte);
    
    if (this.summaryData.prstRte >= 100){
      this.showPrstRte = 100;
    } else {
      this.showPrstRte = this.summaryData.prstRte;
    }
  }
  handlerLoadMoreData(event) {
    if (event) {
      this.pagingCount += 1;
      if (this.pagingCount <= this.totalPage) {
        // Loadmore by search
        if (this.searchKeyword.length > 0 && (
          this.sortColName.length < 1 && this.sortOrder.length < 1
        )) {
          this.apiService_searchPerDetail();
        } 
        // Loadmore by Cont. element
        else {
          this.apiService_persistencyDetail();
        }
      }
    }
  }
  handlerCheckCal(event) {
    this.countCheckCal = event;
  }
  resetDataToCollect() {
    this.toCollectTableData = [];
    this.masterToCollectData = [];
    this.totalPage = 0;
    this.pagingCount = 0;
    this.countCheckCal = 0;
  }
  resetDataToCollectWOPagingCount() {
    this.toCollectTableData = [];
    this.masterToCollectData = [];
    this.totalPage = 0;
    this.countCheckCal = 0;
  }

  // Route Controller
  goToHome() {
    let userRole = this.authService.getPosition();
    let agentCode = this.authService.getLoginedName();
    sessionStorage.removeItem('agentCodeStored');
    sessionStorage.removeItem('agentLevelStored');
    this.dashboardBackRouteService.backHome(userRole, agentCode);
  }
  goToOverview() {
    this.location.back();
  }
  
  clearAll(){
    this.summaryData = {...this.masterSummary};
    this.toCollectTableData = [...JSON.parse(JSON.stringify(this.masterToCollectData))];
    this.countCheckCal = 0;
    this.showPrstRte = this.summaryData.prstRte;
  }
}
