import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersistencyFilterComponent } from './persistency-filter.component';

describe('PersistencyFilterComponent', () => {
  let component: PersistencyFilterComponent;
  let fixture: ComponentFixture<PersistencyFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersistencyFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersistencyFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
