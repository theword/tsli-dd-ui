import { Component, OnInit , NgModule, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-persistency-filter',
  templateUrl: './persistency-filter.component.html',
  styleUrls: ['./persistency-filter.component.scss']
})
export class PersistencyFilterComponent implements OnInit {

  @Input() listFilterGroup = []
  @Input() listFilterPersistency = []
  @Input() listFilterType = []

  @Input() filterGroup: string
  @Input() filterPersistency: string
  @Input() filterType: string

  @Output() changeFilterGroup = new EventEmitter();
  @Output() changeFilterPersistency = new EventEmitter();
  @Output() changeFilterType = new EventEmitter();

  agentLevel: string;

  constructor() { }

  ngOnInit(): void {
    

    var tempAgentLevel = sessionStorage.getItem('agentLevelSelected');
    this.agentLevel = tempAgentLevel;

    if(this.agentLevel == 'AGENT'){
      this.listFilterGroup = ['Per ส่วนตัว']
    }

    // console.log("listFilterGroup", this.listFilterGroup);
    // console.log("listFilterPersistency", this.listFilterPersistency);
    // console.log("listFilterType", this.listFilterType);
  }
  
  onChangeFilterGroup(group) {
    this.changeFilterGroup.emit(group);
  }

  onChangeFilterPersistency(persistency) {
    this.changeFilterPersistency.emit(persistency);
  }

  onChangeFilterType(type) {
    this.changeFilterType.emit(type);
  }
}
