import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersistencySummaryComponent } from './persistency-summary.component';

describe('PersistencySummaryComponent', () => {
  let component: PersistencySummaryComponent;
  let fixture: ComponentFixture<PersistencySummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersistencySummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersistencySummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
