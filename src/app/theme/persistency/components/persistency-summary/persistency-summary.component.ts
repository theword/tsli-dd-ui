import {
  Component,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  Pipe,
  PipeTransform,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { ClosYm2Month } from '../../services/closYm2Month';

@Component({
  selector: 'app-persistency-summary',
  templateUrl: './persistency-summary.component.html',
  styleUrls: ['./persistency-summary.component.scss'],
})
export class PersistencySummaryComponent {
  elementPosition: any;

  @Input() data;
  @Input() prstRte: number = 0;
  @Input() filterPersistency;
  @Input() filterType;
  @Input() countCheckCal: number = 0;
  @Input() closYm: string = '';

  constructor(
    private ClosYm2Month: ClosYm2Month,
    private renderer: Renderer2
  ) {}

  ngOnInit(): void {}

  convertClosYm(closYm) {
    return this.ClosYm2Month.langTH(closYm);
  }

  convertYear(closYm) {
    return this.ClosYm2Month.cd2bd(closYm);
  }

}


@Pipe({
  name: 'maxPersist'
})
export class MaxPersistency implements PipeTransform {

  transform(per: number): any {
    return per > 100 ? 100: per;
  }

}
