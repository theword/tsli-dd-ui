import { Input, Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-collect-table',
  templateUrl: './collect-table.component.html',
  styleUrls: ['./collect-table.component.scss']
})
export class CollectTableComponent implements OnInit {

  @Input() data
  @Input() filterGroup
  @Input() filterPersistency
  @Input() agentCode
  @Input() agentLevel
  @Input() pagingCount: number
  @Input() checkBlockStatus
  @Input() totalPage
  @Input() countCheckCal: number = 0;
  @Output() changeValue = new EventEmitter();
  @Output() loadMoreData = new EventEmitter();
  @Output() sumCheckCal = new EventEmitter();

  @Output() clickSorting = new EventEmitter();

  private sortTypeAsc: boolean = false;
  private sortTypeName: string = "DESC";

  headers = [
    'GM/BM', 'VP/CM', 'AL/FM/BK', 'AG/FC', 
    'Policy/Customer', 'PC', 
    'Effective Date',  'ประมาณการ'
  ];

  fieldNames = [
    'gmCode', 'avpCode', 'alCode', 'agentCode',
    'policyCode', 'targetPc', 'contYmd', 'status'
  ];

  pcValue = [];
  sumPc = 0;

  flagClosym = true
  // public countCheckCal : number = 0;
  public currentLevel = sessionStorage.getItem('agentLevelSelected');

  constructor(
    private spinner: NgxSpinnerService,
    private dataService: DataServiceService,
  ) { }

  ngOnInit(): void {
    this.checkShowHeader();
  }

  onLoadData() {
    this.loadMoreData.emit(true);
  }

  onClick(index){
    if (this.data[index].status == 0){
      this.countCheckCal += 1;
      this.data[index].status = 1      
      this.onChangeValue(this.data[index].targetPc)
    }
    else {
      this.countCheckCal -= 1;
      this.data[index].status = 0
      this.onChangeValue(this.data[index].targetPc*(-1))    
    }
    this.sumCheckCal.emit(this.countCheckCal);
  }

  onChangeValue(value){
    this.changeValue.emit(value);
  }

  convertStringToDate(date){
    if (date) {
      let formatDate = date.substring(0, 4) +'-'+ date.substring(4, 6) +'-'+ date.substring(6, 8);
      return formatDate;
    }
  }

  // CheckShow Column
  isShowGmCol() {
    if (this.currentLevel == 'GM') return true;
    else return false;
  }
  isShowAvpCol() {
    if (this.currentLevel == 'GM' || this.currentLevel == 'AVP') return true;
    else return false;
  }
  isShowAlCol() {
    if (this.currentLevel == 'GM' || this.currentLevel == 'AVP'
      || this.currentLevel == 'AL') return true;
    else return false;
  }

  // checkShow Header
  checkShowHeader() {
    if (this.currentLevel == 'AVP') this.headers.splice(0, 1);
    else if (this.currentLevel == 'AL') this.headers.splice(0, 2);
    else if (this.currentLevel == 'AGENT') this.headers.splice(0,3)
  }

  // Controller of SORTING QUERY
  handleSorting(colName: string) {
    this.swapSortingType();
    let postData = {
      // sortColName: this.convertToFieldName(colName),
      sortColName: colName,
      sortOrder: this.sortTypeName
    }
    this.clickSorting.emit(postData);
  }

  swapSortingType() {
    if (this.sortTypeAsc) {
      this.sortTypeName = "ASC";
      this.sortTypeAsc = !this.sortTypeAsc;
    } else {
      this.sortTypeName = "DESC";
      this.sortTypeAsc = !this.sortTypeAsc;
    }
  }

  normalizeIndexOfFieldNames(index) {
    let _increase = 0;
    switch (this.currentLevel) {
      case 'AVP':
        _increase = 1;
        break;

      case 'AL':
        _increase = 2;
        break;

      case 'AG':
      case 'AGENT':
        _increase = 3;
        break;

      default:
        _increase = 0;
        break;
    }
    return this.fieldNames[index + _increase];
  }

  convertToFieldName(fieldName: string) {
    if (!fieldName) return '';
    let _s = fieldName.replace(/([A-Z])/g, '_$1').trim().toUpperCase();
    return _s;
  }
}
