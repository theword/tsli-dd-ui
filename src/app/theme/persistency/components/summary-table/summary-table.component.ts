import { Component, Input, OnInit } from '@angular/core';
import { ClosYm2Month } from '../../services/closYm2Month'

@Component({
  selector: 'app-summary-table',
  templateUrl: './summary-table.component.html',
  styleUrls: ['./summary-table.component.scss']
})
export class SummaryTableComponent implements OnInit {

  headers = ['บล็อค', 'เดือนที่ไปขายกรมธรรม์มา', 'PC เริ่มต้น', 'PC ที่เก็บได้', 'PC ที่ต่ออายุ']
  @Input() data


  constructor(
    private ClosYm2Month: ClosYm2Month
  ) { }

  ngOnInit(): void {
    // console.log('Summary Table :',this.data);
    
  }

  convertClosYm(closYm){
    return this.ClosYm2Month.langTH(closYm)
  }

  convertYear(closYm){
    return this.ClosYm2Month.cd2bd(closYm)
  }
  

}
