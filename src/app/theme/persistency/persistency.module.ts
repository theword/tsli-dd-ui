import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersistencyControllerComponent } from './persistency-controller/persistency-controller.component';
import { PersistencyFilterComponent } from './components/persistency-filter/persistency-filter.component';
import { MaxPersistency, PersistencySummaryComponent } from './components/persistency-summary/persistency-summary.component';
import { SummaryTableComponent } from './components/summary-table/summary-table.component';
import { CollectTableComponent } from './components/collect-table/collect-table.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/core/shared.module';

const routes: Routes = [
  {
    path: '',
    component: PersistencyControllerComponent
  },
  {
    path: '**', redirectTo: 'error', pathMatch: 'full'
  }
]

@NgModule({
  declarations: [
    PersistencyControllerComponent, 
    PersistencyFilterComponent, 
    PersistencySummaryComponent, 
    SummaryTableComponent, 
    CollectTableComponent,
    MaxPersistency
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule
  ]
})
export class PersistencyModule { }
