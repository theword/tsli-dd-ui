import { Component, OnInit } from '@angular/core';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { AuthService } from 'src/app/core/security/auth.service';
import { ModelUser } from 'src/app/core/model/model-user';
import { ModelDashboard } from 'src/app/core/model/model-dashboard';
import { ModelTempKpi } from 'src/app/core/model/model-tempKpi';
import { SetFormDataService } from 'src/app/core/service/set-form-data.service';
import {FormsModule} from '@angular/forms';
import { Router } from '@angular/router';
import { CheckSessionStorageService } from 'src/app/core/service/check-session-storage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { LockEngineService } from 'src/app/core/lock-engine/lock.manager.service';
import $ from 'jquery';

@Component({
  selector: 'app-company-zone-dashboard',
  templateUrl: './company-zone-dashboard.component.html',
  styleUrls: ['./company-zone-dashboard.component.scss']
})
export class CompanyZoneDashboardComponent implements OnInit {

  // Data variables
  public modelUser: ModelUser;
  public tmpModelUser: ModelUser;
  public model_PC: ModelDashboard;
  public model_Active: ModelDashboard;
  public model_NC: ModelDashboard;
  public model_Retention: ModelDashboard;
  public model_RYP: ModelDashboard;
  public model_tempKpi: ModelTempKpi;
  public isDailyCollapsed = true;
  public isWeeklyCollapsed = true;
  // Utilities
  // currentYear = new Date().getFullYear();
  currentYear = parseInt(sessionStorage.getItem('calendarCurrentYear'));

  hideLastYear: boolean = true;

  ChannelFilterSelected: string = 'COMPANY';
  ZoneFilterSelected: string = 'COMPANY';
  titleLevel: string = 'Company';
  currentLevel: string = 'COMPANY';
  indexChannelSelected: number;
  searchKeyword : string = '';
  totalCompLabel: string = 'Total Comp';

  selectPerformanceDaily: any = [];
  calendarList: any [];
  masterClosYm: string = '';
  selectCalendar: string = '';

  monthName: any = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  monthId: string = '';
  yearId: string = '';

  // ButtonSet Channel and Zone
  buttonSet_Channel = [];
  buttonSet_Zone = [];

  closeResult = '';

  kpiPeriodMode_status: boolean = true;
  kpiPeriodMode_name: string = 'MTD';

  isVisible(): boolean {
    return this.lockEngineService.isDesktop;
  }
  
  constructor(
    private dataService: DataServiceService,
    private authService: AuthService,
    private router: Router,
    private setFormDataService: SetFormDataService,
    private checkSessionStorageService: CheckSessionStorageService,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private lockEngineService: LockEngineService
  ) {
    this.modelUser = new ModelUser();
    this.tmpModelUser = new ModelUser();
    this.model_PC = new ModelDashboard();
    this.model_Active = new ModelDashboard();
    this.model_NC = new ModelDashboard();
    this.model_Retention = new ModelDashboard();
    this.model_RYP = new ModelDashboard();
    this.model_tempKpi = new ModelTempKpi();
   }

  ngOnInit(): void {
    /** spinner starts on init */
    this.spinner.show();

    setTimeout(() => {
    /** spinner ends after 2.8 seconds */
      this.spinner.hide();
   }, 2800);

    this.modelUser.setAgentName(this.authService.getName());
    // Set agentCode for Query
    var tempAgentCode = sessionStorage.getItem('agentCodeSelected');
    var tempAgentLevel = sessionStorage.getItem('agentLevelSelected');
    if (tempAgentCode && tempAgentLevel) {
      // Click back to home or drill by above agent
      this.modelUser.setAgentCode(tempAgentCode);
      this.modelUser.setAgentLevel(tempAgentLevel);
    } else {
      // login at staff
      let agentCode = this.authService.getLoginedName()
      let agentLevel = this.authService.getPosition()
      this.modelUser.setAgentCode(agentCode);
      this.modelUser.setAgentLevel(agentLevel);
      sessionStorage.setItem('agentCodeSelected', agentCode);
      sessionStorage.setItem('agentLevelSelected', agentLevel);
      sessionStorage.setItem('channelFilterSelected', this.currentLevel);
      sessionStorage.setItem('zoneFilterSelected', this.ZoneFilterSelected);
    }

    // ADD IN PHASE 2 //
    // Set masterClosYm
    this.dataService.getClosYmInfo().subscribe(
      response => {
        // console.log('res. getClosYmInfo: ', Object.values(response)[0]);
        this.masterClosYm = Object.values(response)[0];
        this.selectCalendar = Object.values(response)[0];

        this.monthId = this.monthName[parseInt(Object.values(response)[0].substring(4, 6))-1];
        this.yearId = Object.values(response)[0].substring(0, 4);
      },
      error => {
        // console.log('getPerformanceDaily: ', error);
      }
    );
    //  END PHASE 2  //

    var tempChannelFilter = sessionStorage.getItem('channelFilterSelected');
    var tempZoneFilter = sessionStorage.getItem('zoneFilterSelected');
    var tempChannelIndex = parseInt(sessionStorage.getItem('channelSelectedIndex'));
    if (tempChannelFilter && (tempChannelFilter !== 'COMPANY')) {
      this.ChannelFilterSelected = tempChannelFilter;
      this.ZoneFilterSelected = undefined;
      this.indexChannelSelected = tempChannelIndex;
      this.callAPI_getCompanyOverview();
      var zoneFilter = tempZoneFilter && tempZoneFilter != null ? tempZoneFilter : tempChannelIndex;
      var param = {code:tempChannelFilter,setId:tempChannelIndex,zone:zoneFilter};
      this.handleChannelFilter(param);
    } else {
      this.getAllData_API();
    }
    sessionStorage.setItem('prstRte', "");
  }

  scrollTo(id){
    if(id == 'dailyTable'){
      if(this.isDailyCollapsed == true){
        this.isDailyCollapsed = !this.isDailyCollapsed
        console.log(`scrolling to ${id}`);
        let el = document.getElementById(id);
        el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
      }
      else {
        this.isDailyCollapsed = !this.isDailyCollapsed
      }
    }

    if(id == 'weeklyTable'){
      if(this.isWeeklyCollapsed == true){
        this.isWeeklyCollapsed = !this.isWeeklyCollapsed
        console.log(`scrolling to ${id}`);
        let el = document.getElementById(id);
        el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
      }
      else {
        this.isWeeklyCollapsed = !this.isWeeklyCollapsed
      }
    }
      
   
  }

  async getAllData_API() {
    await this.callAPI_getCompanyOverview();
    await this.callAPI_getPerformance(this.currentLevel);
    await this.callAPI_getTarget(this.currentLevel);
    await this.callAPI_getYTD(this.currentLevel, this.currentLevel);
    await this.callAPI_getPerformanceDaily(this.currentLevel);
    await this.callAPI_getPerformanceWeekly(this.currentLevel);
  }

  
  handleSelectCalendar(){
    let result = this.setFormDataService.transform_performanceData(
        this.modelUser.getTablePerformanceDaily(), this.selectCalendar);
    this.selectPerformanceDaily = result.list;
    this.modelUser.setPerformanceDailyTotal(result.total);
    // console.log('NEW LIST: ', this.selectPerformanceDaily);
  }

  // [Calling API Service methods]
  callAPI_getCompanyOverview() {
    this.dataService.getCompanyAllOverview().subscribe(
      (response: any[]) => {
        if (response && Object.keys(response).length > 0) {
          // console.log("callAPI_getCompanyOverview", response);
          for (let i = 0; i < Object.keys(response).length; i++) {
            switch (response[i].code) {
              case '1':
                response[i].newId = 1
                break;
              case '2':
                response[i].newId = 2
                break;
              case '3':
                response[i].newId = 3
                break;
              case '4':
                response[i].newId = 4
                break;
              case '5':
                response[i].newId = 5
                break;
              case '6':
                response[i].newId = 6
                break;
              case '7':
                response[i].newId = 7
                break;
              case '8':
                response[i].newId = 8
                break;
              case '9':
                response[i].newId = 9
                break;
              case '90':
                response[i].newId = 10
                break;
              case 'AD':
                response[i].newId = 13
                break;
              case 'GA':
                response[i].newId = 11
                break;
              case 'SFC':
                response[i].newId = 12
                break;
              case 'COMPANY':
                response[i].newId = 14
                break;
              default:
                break;
            }
          }
          response.sort((a,b) => a.newId - b.newId)
          this.modelUser.setTableOverview(response);
          this.tmpModelUser.setTableOverview(response);
          this.filterTable(this.ChannelFilterSelected,this.ZoneFilterSelected);
          this.set_buttonChannel(response);
          this.set_buttonZone(response);
          this.setCardChartMTD(sessionStorage.getItem('channelFilterSelected'));
        }
      },
      error => {
        // console.log('getCompanyAllOverview: ', error);
      }
    );
  }
  callAPI_getPerformance(type: string) {
    let data = {};
    switch (type) {
      case 'COMPANY':
        this.dataService.getCompanyPerformance().subscribe(
          response => {
            // console.log('res. getCompanyPerformance: ', response);
            if (response && Object.keys(response).length> 0) {
              this.setPackChart(response);
            }
          },
          error => {
            // console.log('getCompanyPerformance: ', error);
          }
        );
        break;
      case 'CHANNEL':
        data = {
          "channelCode": this.ChannelFilterSelected
        };
        this.dataService.getChannelPerformance(data).subscribe(
          response => {
            // console.log('res. getChannelPerformance: ', response);
            if (response && Object.keys(response).length> 0) {
              this.setPackChart(response);
            }
          },
          error => {
            // console.log('getChannelPerformance: ', error);
          }
        );
        break;
      case 'ZONE':
        data = {
          "zoneCode": this.ZoneFilterSelected
        };
        this.dataService.getZonePerformance(data).subscribe(
          response => {
            // console.log('res. getZonePerformance: ', response);
            if (response && Object.keys(response).length> 0) {
              this.setPackChart(response);
            }
          },
          error => {
            // console.log('getZonePerformance: ', error);
          }
        );
        break;
      default:
        break;
    }
  }
  callAPI_getYTD(level: string, code: string) {
    let data = {
      "agentLevel": level,
      "zoneCode": code
    };
    this.dataService.getCompanyYTD(data).subscribe(
      response => {
        // console.log('res. getCompanyYTD: ', response);
        if (response && Object.keys(response).length > 0) {
          this.model_tempKpi.setMasterPcYtd(response[0].pcTarget, response[0].pcApprove, response[0].pcPercent);
          this.model_tempKpi.setMasterActiveYtd(response[0].activeTarget, response[0].active, response[0].activePercent);
          this.model_tempKpi.setMasterNcYtd(response[0].ncTarget, response[0].nc, response[0].ncPercent);
          this.model_tempKpi.setMasterRetentionYtd(response[0].retentionTarget, response[0].retentionYtd, 0);
          this.model_tempKpi.setMasterRypYtd(response[0].rypTarget, response[0].ryp, response[0].rypPercent);
          // this.model_PC.setKpiBoxValue('PC', response[0].pcTarget, response[0].pcApprove, response[0].pcPercent);
          // this.model_Active.setKpiBoxValue('ACTIVE', response[0].activeTarget, response[0].active, response[0].activePercent);
          // this.model_NC.setKpiBoxValue('NC', response[0].ncTarget, response[0].nc, response[0].ncPercent);
          // this.model_Retention.setKpiBoxValue('RETENTION', response[0].retentionTarget, response[0].retentionYtd, 0);
          // this.model_RYP.setKpiBoxValue('RYP', response[0].rypTarget, response[0].ryp, response[0].rypPercent);
        }
      },
      error => {
        // console.log('err. getCompanyYTD: ', error);
      }
    );
  }
  callAPI_getTarget(type) {
    let tempAgentCode = type;
    switch (type) {
      case 'CHANNEL':
        tempAgentCode = this.ChannelFilterSelected;
        break;
      case 'ZONE':
        tempAgentCode = this.ZoneFilterSelected;
        break;
      default:
        break;
    }
    let data = {
      "agentCode": tempAgentCode,
    };
    this.dataService.getCompanyTarget(data).subscribe(
      response => {
        // console.log('res. getCompanyTarget: ', response);
        if (response && Object.keys(response).length > 0) {
          this.model_PC.setSeriesTarget(
            this.setFormDataService.transform_kpiTargetSeries_dyThousand(response, 'PC'));
          this.model_Active.setSeriesTarget(
            this.setFormDataService.transform_kpiTargetSeries(response, 'NEW_ACTIVE_AGENT'));
          this.model_NC.setSeriesTarget(
            this.setFormDataService.transform_kpiTargetSeries(response, 'NEW_CODE'));
          this.model_Retention.setSeriesTarget(
            this.setFormDataService.transform_kpiTargetSeries(response, 'RETENTION'));
          this.model_RYP.setSeriesTarget(
            this.setFormDataService.transform_kpiTargetSeries_dyThousand(response, 'RYP'));
        }
      },
      error => {
        // console.log('err. getCompanyTarget: ', error);
      }
    );
  }
  setPackChart(data) {
    let result = this.setFormDataService.transform_kpiChartSeries_dyThousand(data, 'pcApprove');
    this.model_PC.setCurrentAndLast(result.current, result.last);

    result = this.setFormDataService.transform_kpiChartSeries(data, 'active');
    this.model_Active.setCurrentAndLast(result.current, result.last);

    result = this.setFormDataService.transform_kpiChartSeries(data, 'nc');
    this.model_NC.setCurrentAndLast(result.current, result.last);

    result = this.setFormDataService.transform_kpiChartSeries(data, 'retention');
    this.model_Retention.setCurrentAndLast(result.current, result.last);

    result = this.setFormDataService.transform_kpiChartSeries_dyThousand(data, 'ryp');
    this.model_RYP.setCurrentAndLast(result.current, result.last);
  }
  // ====== ADD NEW PHASE 2 ====== //
  setCardChartMTD(type) {
    let tempMTD = this.modelUser.tableOverview.filter(item => item.code === type);
    let dataMTD = tempMTD[0];
    // console.log(type, ' -> SET CardChart MTD: ', dataMTD);
    try {
      this.model_PC.setKpiBoxValue('PC',
        dataMTD.targetPc,
        dataMTD.pcApprove,
        dataMTD.pcPercent);
      this.model_Active.setKpiBoxValue('ACTIVE',
        dataMTD.targetAct,
        dataMTD.active,
        dataMTD.activePercent);
      this.model_NC.setKpiBoxValue('NC',
        dataMTD.targetNc,
        dataMTD.nc,
        dataMTD.ncPercent);
      this.model_Retention.setKpiBoxValue('RETENTION',
        dataMTD.targetRt,
        dataMTD.retention,
        0);
      this.model_RYP.setKpiBoxValue('RYP',
        dataMTD.targetRyp,
        dataMTD.ryp,
        dataMTD.rypPercent);
      this.model_tempKpi.setMasterPcMtd(dataMTD.targetPc, dataMTD.pcApprove, dataMTD.pcPercent);
      this.model_tempKpi.setMasterActiveMtd(dataMTD.targetAct, dataMTD.active, dataMTD.activePercent);
      this.model_tempKpi.setMasterNcMtd(dataMTD.targetNc, dataMTD.nc, dataMTD.ncPercent);
      this.model_tempKpi.setMasterRetentionMtd(dataMTD.targetRt, dataMTD.retention, 0);
      this.model_tempKpi.setMasterRypMtd(dataMTD.targetRyp, dataMTD.ryp, dataMTD.rypPercent);
    } catch (e) {
      // console.log('ERR. ', e);
    }
  }
  callAPI_getPerformanceDaily(type: string) {
    let data = {};

    switch (type) {
      case 'COMPANY':
        data = {
          "agentLevel": 'COMPANY'
        };
        this.dataService.getPerformanceDailyCompany(data).subscribe(
          response => {
            // console.log('res. getPerformanceDailyCompany: ', response);
            if (Object.keys(response).length > 0) {
              for (let index = 0; index < Object.keys(response).length; index++) {
                let date = new Date(response[index].dayOfMonth);
                let dd = String(date.getDate()). padStart(2, '0');
                response[index].dayOfMonth = dd;
              }
              this.modelUser.setTablePerformanceDaily(response);
              let result = this.setFormDataService.transform_performanceData(response, this.masterClosYm);
              this.selectPerformanceDaily = result.list;
              this.modelUser.setPerformanceDailyTotal(result.total);
              this.calendarList = this.setFormDataService.getSelectionCalendarYM(this.masterClosYm);
              
              // this.calendarList = this.setFormDataService.getSelectionCalendar(response);      
              // console.log('TRANSFORM: ', this.selectPerformanceDaily);
            }
          },
          error => {
            // console.log('getPerformanceDaily: ', error);
          }
        );
        break;
      case 'CHANNEL':
        data = {
          "channelCode": this.ChannelFilterSelected,
          "agentLevel": 'CHANNEL'
        };
        this.dataService.getPerformanceDailyChannel(data).subscribe(
          response => {
            // console.log('res. getPerformanceDailyCompany: ', response);
            if (Object.keys(response).length > 0) {
              for (let index = 0; index < Object.keys(response).length; index++) {
                let date = new Date(response[index].dayOfMonth);
                let dd = String(date.getDate()). padStart(2, '0');
                response[index].dayOfMonth = dd;
              }
              this.modelUser.setTablePerformanceDaily(response);
              let result = this.setFormDataService.transform_performanceData(response, this.masterClosYm);
              this.selectPerformanceDaily = result.list;
              this.modelUser.setPerformanceDailyTotal(result.total);
              this.calendarList = this.setFormDataService.getSelectionCalendarYM(this.selectCalendar);       
              // console.log('TRANSFORM: ', this.selectPerformanceDaily);
            }
          },
          error => {
            // console.log('getPerformanceDaily: ', error);
          }
        );
        break;
      case 'ZONE':
        data = {
          "zoneCode": this.ZoneFilterSelected,
          "agentLevel": 'ZONE'
        };
        this.dataService.getPerformanceDailyZone(data).subscribe(
          response => {
            // console.log('res. getPerformanceDailyCompany: ', response);
            if (Object.keys(response).length > 0) {
              for (let index = 0; index < Object.keys(response).length; index++) {
                let date = new Date(response[index].dayOfMonth);
                let dd = String(date.getDate()). padStart(2, '0');
                response[index].dayOfMonth = dd;
              }
              this.modelUser.setTablePerformanceDaily(response);
              let result = this.setFormDataService.transform_performanceData(response, this.masterClosYm);
              this.selectPerformanceDaily = result.list;
              this.modelUser.setPerformanceDailyTotal(result.total);
              this.calendarList = this.setFormDataService.getSelectionCalendarYM(this.selectCalendar);
              // console.log('TRANSFORM: ', this.selectPerformanceDaily);
            }
          },
          error => {
            // console.log('getPerformanceDaily: ', error);
          }
        );
        break;
    }
  }
  callAPI_getPerformanceWeekly(type: string) {
    let data = {};
    switch (type) {
      case 'COMPANY':
        data = {
          "agentLevel": 'COMPANY'
        };
        this.dataService.getPerformanceWeeklyCompany(data).subscribe(
          response => {
            // console.log('res. getPerformanceWeeklyCompany: ', response);
            if (Object.keys(response).length > 0) {
              this.modelUser.setPerformanceWeekly(response);
            }
          },
          error => {
            // console.log('getPerformanceWeeklyCompany: ', error);
          }
        );
        break;
      case 'CHANNEL':
        data = {
          "channelCode": this.ChannelFilterSelected,
          "agentLevel": 'CHANNEL'
        };
        this.dataService.getPerformanceWeeklyChannel(data).subscribe(
          response => {
            // console.log('res. getPerformanceWeeklyChannel: ', response);
            if (Object.keys(response).length > 0) {
              this.modelUser.setPerformanceWeekly(response);
            }
          },
          error => {
            // console.log('getPerformanceWeeklyChannel: ', error);
          }
        );
        break;
      case 'ZONE':
        data = {
          "zoneCode": this.ZoneFilterSelected,
          "agentLevel": 'ZONE'
        };
        this.dataService.getPerformanceWeeklyZone(data).subscribe(
          response => {
            // console.log('res. getPerformanceWeeklyZone: ', response);
            if (Object.keys(response).length > 0) {
              this.modelUser.setPerformanceWeekly(response);
            }
          },
          error => {
            // console.log('getPerformanceWeeklyZone: ', error);
          }
        );
        break;
    }
  }
  // ====== END NEW PHASE 2 ====== //

  // Set Button Set
  set_buttonChannel(data) {
    if (data.length > 0) {
      let channelElement = data.filter(x => x.staffLevel === 'CHANNEL');
      for (let index = 0; index < channelElement.length; index++) {
        let newKey = index
        switch (channelElement[index].groupChannel) {
          case 'AD':
            newKey = 2
            break;
          case 'GA':
            newKey = 0
            break;
          case 'SFC':
            newKey = 1
            break;
          default:
            break;
        }
        let buttonSet = {
          setId: newKey,
          groupChannel: channelElement[index].groupChannel,
          code: channelElement[index].code,
          name: channelElement[index].groupChannel,
          staffLevel: channelElement[index].staffLevel
        };
        this.buttonSet_Channel.push(buttonSet);
      }
      this.buttonSet_Channel.sort((b,a) => a.groupChannel - b.groupChannel)
      console.log('set.buttonChannelSet: ', this.buttonSet_Channel);
    }
  }
  set_buttonZone(data) {
    console.log("set_buttonZone", data);
    
    if (data.length > 0) {
      let packZoneButton = [];
      for (let index = 0; index < this.buttonSet_Channel.length; index++) {
        let zoneElement = data.filter(
          x => x.groupChannel === this.buttonSet_Channel[index].groupChannel
          && x.staffLevel === 'ZONE');
        let tempZoneSet = [];
        console.log("zoneElement", zoneElement);
        for (let j = 0; j < zoneElement.length; j++) {
          let buttonSet = {
            groupChannel: zoneElement[j].groupChannel,
            code: zoneElement[j].code,
            zoneName: zoneElement[j].zoneName,
            staffLevel: zoneElement[j].staffLevel
          };
          tempZoneSet.push(buttonSet);
        } // End for j
        packZoneButton.push(tempZoneSet);
      } // End For index
      this.buttonSet_Zone = packZoneButton;
      console.log('set.buttonZoneSet: ',this.buttonSet_Zone)
    }
  }

  // Handle click button
  handleChannelFilter(param) {
    console.log("handleChannelFilter(param)", param);
    var type = param.code, setId = param.setId, zone =param.zone;
    this.ChannelFilterSelected = type;
    this.indexChannelSelected = setId;
    this.ZoneFilterSelected = 'COMPANY';
    this.currentLevel = 'CHANNEL';
    this.callAPI_getYTD(this.currentLevel, type);
    this.callAPI_getPerformance(this.currentLevel);
    this.callAPI_getTarget(this.currentLevel);
    this.callAPI_getPerformanceDaily(this.currentLevel);
    this.callAPI_getPerformanceWeekly(this.currentLevel);
    if(zone != 'COMPANY'){
      setTimeout(() => {
        this.ZoneFilterSelected = zone;
        this.handleZoneFilter(zone);
        sessionStorage.setItem('channelFilterSelected', this.ChannelFilterSelected);
        sessionStorage.setItem('zoneFilterSelected', this.ZoneFilterSelected);
        sessionStorage.setItem('channelSelectedIndex', this.indexChannelSelected.toString());
        this.setTitleLevel();
      }, 500);
    }else{
      this.filterTable(param.code,zone);
      sessionStorage.setItem('channelFilterSelected', this.ChannelFilterSelected);
      sessionStorage.setItem('zoneFilterSelected', this.ZoneFilterSelected);
      sessionStorage.setItem('channelSelectedIndex', this.indexChannelSelected.toString());
      this.setTitleLevel();
    }
    this.setCardChartMTD(param.code);
  }
  handleZoneFilter(type) {
    if (this.ZoneFilterSelected !== type) {
      this.ZoneFilterSelected = type;
      this.currentLevel = 'ZONE';
      this.callAPI_getYTD(this.currentLevel, type);
      this.callAPI_getPerformance(this.currentLevel);
      this.callAPI_getTarget(this.currentLevel);
      this.callAPI_getPerformanceDaily(this.currentLevel);
      this.callAPI_getPerformanceWeekly(this.currentLevel);
      
      sessionStorage.setItem('zoneFilterSelected', this.ZoneFilterSelected);
      let _tempChannelIndex = sessionStorage.getItem('channelSelectedIndex');

      let _tempButtonZone = this.buttonSet_Zone[_tempChannelIndex];

      let filterZone = _tempButtonZone.filter( item => item.code == this.ZoneFilterSelected)
      console.log("filterZone", filterZone[0]);
      
      // let decreaseIndex = 0;

      // if (parseInt(_tempChannelIndex) >= 0) {
      //   for (let index = 0; index < parseInt(_tempChannelIndex); index++) {
      //     decreaseIndex = decreaseIndex + this.buttonSet_Zone[index].length;
      //   }
      // }
      console.log("_tempChannelIndex", _tempChannelIndex);
      console.log("this.ZoneFilterSelected", this.ZoneFilterSelected);
      // console.log("decreaseIndex", decreaseIndex);
      
      // this.titleLevel = this.buttonSet_Zone[parseInt(_tempChannelIndex)][parseInt(this.ZoneFilterSelected) - decreaseIndex].zoneName;
      this.titleLevel = filterZone[0].zoneName
      sessionStorage.setItem('zoneTitleName', this.titleLevel);
    }
    this.filterTable(this.ChannelFilterSelected,type);
    this.setCardChartMTD(sessionStorage.getItem('zoneFilterSelected'));
  }
  setTitleLevel(){
    let _tempChannelIndex = sessionStorage.getItem('channelSelectedIndex');
    if(this.ZoneFilterSelected !== undefined && this.ZoneFilterSelected != '' && this.ZoneFilterSelected != null && this.ZoneFilterSelected.toUpperCase() != 'COMPANY'){
      this.titleLevel = this.buttonSet_Zone[parseInt(_tempChannelIndex)][parseInt(this.ZoneFilterSelected) - 1].zoneName;
    }else {
      // this.titleLevel = 'Channel' + ' ' + this.ChannelFilterSelected;
      console.log("this.buttonSet_Channel", this.buttonSet_Channel);
      this.titleLevel = 'Channel' + ' ' + this.buttonSet_Channel[parseInt(_tempChannelIndex)].name;
    }
  }
  filterByChannelZone(channel,zone){
    var allData = this.tmpModelUser.tableOverview;
    return allData.filter(data => 
      (channel == 'COMPANY' && zone == 'COMPANY') || ((data.groupChannel == channel && data.code == zone || data.groupChannel == channel && 'COMPANY' == zone || 'COMPANY' == channel && data.code == zone))
    )
  }
  filterTable(channel,zone){
    var filteredList = this.filterByChannelZone(channel,zone).filter(data => 
      this.searchKeyword == null || this.searchKeyword == '' 
      || (
        (data.code != null && data.code.toLowerCase().includes(this.searchKeyword.toLowerCase())) 
          || (data.zoneName !=null && data.zoneName.toLowerCase().includes(this.searchKeyword.toLowerCase())) 
          || (this.totalCompLabel.toLowerCase().includes(this.searchKeyword.toLowerCase()) && data.staffLevel == 'COMPANY')
      )
    );
    this.modelUser.setTableOverview(filteredList);
  }

  // Controller
  resetToCompany() {
    this.ChannelFilterSelected = 'COMPANY';
    this.indexChannelSelected = undefined; // trick ng
    this.ZoneFilterSelected = 'COMPANY';
    this.currentLevel = 'COMPANY';
    this.titleLevel = 'Company';
    this.callAPI_getPerformance(this.currentLevel);
    this.callAPI_getTarget(this.currentLevel);
    this.callAPI_getPerformanceDaily(this.currentLevel);
    this.callAPI_getPerformanceWeekly(this.currentLevel);
    this.callAPI_getYTD(this.currentLevel, this.currentLevel);
    this.filterTable(this.ChannelFilterSelected,this.ZoneFilterSelected);
    sessionStorage.setItem('channelFilterSelected', this.currentLevel);
    sessionStorage.setItem('zoneFilterSelected', this.currentLevel);
    this.setCardChartMTD(sessionStorage.getItem('channelFilterSelected'));
  }
  compareYear() {
    this.hideLastYear = !this.hideLastYear;
  }
  // Redirect to TeamDetail
  handleViewTeam() {
    // console.log('Redirect to team detail');
    let redirectionBasedOnKpiType = '/dashboard/producer/kpidetail';
    let queryParams = [];
    if (sessionStorage.getItem('zoneFilterSelected') === 'COMPANY') {
      let paramLv = sessionStorage.getItem('zoneFilterSelected');
      if (sessionStorage.getItem('channelFilterSelected') !== 'COMPANY') {
        paramLv = 'Channel';
      }
      queryParams = [
        { kpiType: 'PC', lvName: paramLv}
      ];
    } else {
      queryParams = [
        { kpiType: 'PC', lvName: this.modelUser.getAgentLevel()}
      ];
    }
    
    this.router.navigate([redirectionBasedOnKpiType], { 
      queryParams: {
        params: JSON.stringify(queryParams) 
      } 
    });
  }

  // Handle open modal
  open(content) {
    this.modalService.open(content, {centered: true, windowClass : "xxl" }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed`;
    });
  }
  onClickPromotion(mode) {
    let redirectionToPromotion = '/promotion';
    let queryParams = [
      {lvName: this.titleLevel},
      {filterPromotion: mode}
    ];
   
    this.router.navigate([redirectionToPromotion], {
      queryParams: {
        params: JSON.stringify(queryParams),
      },
    });
  }
  scrollTop() {
    $('body,html').animate({
      scrollTop: 0
    }, 400);
  }

  // ====== ADD NEW PHASE 3 ====== //
  // Switch KPI summary between MTD and YTD
  handlerKpiPeriodMode(type: string) {
    if (type == 'MTD') {
      this.kpiPeriodMode_status = true;
      this.kpiPeriodMode_name = 'MTD';
      this.setKpiMtd();
    }
    else {
      this.kpiPeriodMode_status = false;
      this.kpiPeriodMode_name = 'YTD';
      this.setKpiYtd();
    }
  }
  setKpiMtd() {
    this.model_PC.setKpiBoxValue(
      'PC',
      this.model_tempKpi.masterPcMtd.target,
      this.model_tempKpi.masterPcMtd.value,
      this.model_tempKpi.masterPcMtd.percent
    );
    this.model_Active.setKpiBoxValue(
      'ACTIVE',
      this.model_tempKpi.masterActiveMtd.target,
      this.model_tempKpi.masterActiveMtd.value,
      this.model_tempKpi.masterActiveMtd.percent
    );
    this.model_NC.setKpiBoxValue(
      'NC',
      this.model_tempKpi.masterNcMtd.target,
      this.model_tempKpi.masterNcMtd.value,
      this.model_tempKpi.masterNcMtd.percent
    );
    this.model_Retention.setKpiBoxValue(
      'RETENTION',
      this.model_tempKpi.masterRetentionMtd.target,
      this.model_tempKpi.masterRetentionMtd.value,
      0
    );
    this.model_RYP.setKpiBoxValue(
      'RYP',
      this.model_tempKpi.masterRypMtd.target,
      this.model_tempKpi.masterRypMtd.value,
      this.model_tempKpi.masterRypMtd.percent
    );
  }
  setKpiYtd() {
    this.model_PC.setKpiBoxValue(
      'PC',
      this.model_tempKpi.masterPcYtd.target,
      this.model_tempKpi.masterPcYtd.value,
      this.model_tempKpi.masterPcYtd.percent
    );
    this.model_Active.setKpiBoxValue(
      'ACTIVE',
      this.model_tempKpi.masterActiveYtd.target,
      this.model_tempKpi.masterActiveYtd.value,
      this.model_tempKpi.masterActiveYtd.percent
    );
    this.model_NC.setKpiBoxValue(
      'NC',
      this.model_tempKpi.masterNcYtd.target,
      this.model_tempKpi.masterNcYtd.value,
      this.model_tempKpi.masterNcYtd.percent
    );
    this.model_Retention.setKpiBoxValue(
      'RETENTION',
      this.model_tempKpi.masterRetentionYtd.target,
      this.model_tempKpi.masterRetentionYtd.value,
      0
    );
    this.model_RYP.setKpiBoxValue(
      'RYP',
      this.model_tempKpi.masterRypYtd.target,
      this.model_tempKpi.masterRypYtd.value,
      this.model_tempKpi.masterRypYtd.percent
    );
  }
  // ====== END NEW PHASE 3 ====== //

}
