import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyZoneDashboardComponent } from './company-zone-dashboard.component';

describe('CompanyZoneDashboardComponent', () => {
  let component: CompanyZoneDashboardComponent;
  let fixture: ComponentFixture<CompanyZoneDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyZoneDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyZoneDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
