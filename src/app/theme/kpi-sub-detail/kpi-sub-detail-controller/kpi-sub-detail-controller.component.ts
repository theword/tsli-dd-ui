import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModelUser } from 'src/app/core/model/model-user';
import { AuthService } from 'src/app/core/security/auth.service';
import { DashboardBackRouteService } from 'src/app/core/service/dashboard-back-route.service';
import { Location } from '@angular/common';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesHelper } from 'src/app/core/utilities-helper';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-kpi-sub-detail-controller',
  templateUrl: './kpi-sub-detail-controller.component.html',
  styleUrls: ['./kpi-sub-detail-controller.component.scss']
})
export class KpiSubDetailControllerComponent implements OnInit {
  
  public filterKpiType: string = 'PC';
  public filterSubDetailIndex: number = 0;
  public filterSubDetailType: string = '';
  public modelUser: ModelUser;

  public data : {};
  public first_headers: any;
  public first_fieldNames: any
  public second_headers: any;
  public setDetailList: any;
  public titleForStaff: string = '';
  public masterLevelName: any = [];

  public searchKeyword: string = '';

  public topRecords: number = 0;
  public pagingCount: number = 0;
  public pagingItem: any;
  private _data: any;

  sumKpi: any
  prstRte:any

  private subDetailList : any = {
    PC: [
      {id: 0, postName: 'APPROVE', label: 'Approved'},
      {id: 1, postName: 'PENDING', label: 'Pending'}
    ],
    NC: [
      {id: 0, postName: 'NEWCODE', label: 'New Code'},
      {id: 1, postName: 'TEMP', label: 'Temp'},
      {id: 2, postName: 'TRAINEE', label: 'Trainee'},
      {id: 3, postName: 'PROSPECT', label: 'Prospect'},
    ],
    RETENTION: [
      {id: 0, postName: 'ACTIVE', label: 'Active'},
      {id: 1, postName: 'NON_ACTIVE', label: 'Non Active'},
    ],
    RYP: [
      {id: 0, postName: 'APPROVE', label: 'Approved'},
      {id: 1, postName: 'PENDING', label: 'Pending'},
      {id: 2, postName: 'LAPSE', label: 'Lapse'},
    ],
  };
  public queryLevel: string = '';
  private queryStaffLevel: string = '';
  closeResult = '';

  public memoOwner: any;
  public memoDetail: any;
  
  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private dataService: DataServiceService,
    private dashboardBackRouteService: DashboardBackRouteService,
    private location: Location,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
  ) { 
    this.modelUser = new ModelUser();
  }

  ngOnInit(): void {

    this.spinner.show();
    /** spinner ends after 1.5 seconds */
    setTimeout(() => {
      this.spinner.hide();
    }, 2200); 
    
    this.route.queryParams.subscribe(params => {
      let getParam = JSON.parse(params.params);
      this.filterKpiType = getParam[0]['kpiType'];
      this.filterSubDetailIndex = parseInt(getParam[0]['subDetailType']);
    });
    this.modelUser.setAgentCode(sessionStorage.getItem('agentCodeSelected'));
    this.modelUser.setAgentLevel(sessionStorage.getItem('agentLevelSelected'));
    this.setHeader();
    this.callAPI_getAgentStructure();
    this.callAPI_levelFilterBtn();
    this.prstRte = sessionStorage.getItem('prstRte')
  }
  // Set Role
  setViewerRole() {
    if (this.modelUser.getAgentLevel() === 'ZONE') {
      let tempChannel = sessionStorage.getItem('channelFilterSelected');
      let tempZone = sessionStorage.getItem('zoneFilterSelected');
      if (tempChannel) {
        if (tempChannel !== 'COMPANY' && tempZone === 'COMPANY') {
          this.queryLevel = 'CHANNEL';
          this.titleForStaff = 'Channel ' + tempChannel;
          this.queryStaffLevel = tempChannel;
        } 
        else if (tempChannel !== 'COMPANY' && tempZone !== 'COMPANY') {
          this.queryLevel = 'ZONE';
          this.titleForStaff = sessionStorage.getItem('zoneTitleName');
          // this.titleForStaff = 'Zone ' + ('0' + tempZone).slice(-2);
          this.queryStaffLevel = tempZone;
        }
        else {
          this.queryLevel = 'COMPANY';
          this.titleForStaff = 'Company';
          this.queryStaffLevel = 'COMPANY';
        }
      }
    }
  }

  // Converter
  convertSubDetailName(index: number) {
    this.setDetailList = this.subDetailList[this.filterKpiType];
    this.filterSubDetailType = this.subDetailList[this.filterKpiType][index].postName;
    switch (this.filterKpiType.toLocaleUpperCase()) {
      case 'PC':
        this.callAPI_getPolicyPC();
        break;
      case 'NC':
        this.callAPI_getPolicyNC();
        break;
      case 'RETENTION':
        this.callAPI_getRetentionDetail();
        break;
      case 'RYP':
        this.callAPI_getPolicyRYP();
        break;
    }
  }

  // Call services
  callAPI_getAgentStructure() {
    let data = {
      "agentCode": this.modelUser.getAgentCode()
    };
    this.dataService.getAgentStructure(data).subscribe(
      response => {
        // console.log('res. getAgentStructure: ', response);
        if (Object.keys(response).length > 0) {
          let agentName = response[0]['thaiName'] + ' ' + response[0]['thaiSurname'];
          this.modelUser.setAgentName(agentName);
          this.modelUser.setAgentLevel('AGENT');
          if (response[0]['agentLevel'] !== 'AGENT' && sessionStorage.getItem('agentLevelSelected') === 'AGENT') {
            this.modelUser.setLevelName(response[0]['levelName'] + ' (ผลงานส่วนตัว)');
          } else {
            this.modelUser.setLevelName(response[0]['levelName']);
            this.modelUser.setAgentLevel(response[0]['agentLevel']);
          }
          this.setViewerRole();
          this.convertSubDetailName(this.filterSubDetailIndex);
        }
      },
      error => {
        console.log('getAgentStructure: ', error);
      }
    );
  }
  callAPI_levelFilterBtn(){
    this.dataService.getButtonName_agentLevel().subscribe(
      response => {
        // console.log('res. getButtonName_agentLevel: ', response);
        if (Object.keys(response).length > 0) {
          let _levelArr = [];
          let _agentLevel = this.modelUser.getAgentLevel();

          if( _agentLevel == 'AL'){
            this.first_fieldNames = ['agentCode']
          } else if (_agentLevel == 'AVP'){
            this.first_fieldNames = ['alCode','agentCode']
          } else if (_agentLevel == 'GM'){
            this.first_fieldNames = ['avpCode','alCode','agentCode']
          } else if (_agentLevel == 'ZONE'){
            this.first_fieldNames = ['gmCode','avpCode','alCode','agentCode']
          }

          for (let index = 0; index < Object.keys(response).length; index++) {
            _levelArr.push(response[index]);
            this.masterLevelName.push(response[index]?.buttonName);
          }
          let _first_headers = _levelArr.filter(data => 
            _agentLevel == 'ZONE' ||
            (_agentLevel == 'GM' && ['AVP','AL','AGENT'].includes(data?.levelName)) ||
            (_agentLevel == 'AVP' && ['AL','AGENT'].includes(data?.levelName)) ||
            (_agentLevel == 'AL' && ['AGENT'].includes(data?.levelName))
          )
          this.first_headers = _first_headers.map(data => data?.buttonName);
        }
      },
      error => {
        console.log('getButtonName_agentLevel: ', error);
      }
    );
  }
  callAPI_getPolicyPC() {
    // CheckAgentLevel -> postData and pathAPI
    let postData = {};
    let caseQuery = this.modelUser.getAgentLevel();

    if (this.modelUser.getAgentLevel() === 'ZONE') {
      caseQuery = this.queryLevel;
    } 

    switch (caseQuery) {
      case 'AGENT':
        postData = {
          "agentCode": this.modelUser.getAgentCode(),
          "agentLevel": this.modelUser.getAgentLevel(),
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyPcAgent(postData).subscribe(
          response => {
            // console.log('res. postPolicyPcAgent: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("sumPc", this._data.map(item => item.pc).reduce((sum, pc) => sum + pc));
              this.sumKpi = this._data.map(item => item.pc).reduce((sum, pc) => sum + pc)
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyPcAgent: ', error);
          }
        );
        break;

      case 'AL':
        postData = {
          "alCode": this.modelUser.getAgentCode(),
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyPcAl(postData).subscribe(
          response => {
            // console.log('res. postPolicyPcAl: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("sumPc", this._data.map(item => item.pc).reduce((sum, pc) => sum + pc));
              this.sumKpi = this._data.map(item => item.pc).reduce((sum, pc) => sum + pc)
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyPcAl: ', error);
          }
        );
        break;

      case 'AVP':
        postData = {
          "avpCode": this.modelUser.getAgentCode(),
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyPcAvp(postData).subscribe(
          response => {
            // console.log('res. postPolicyPcAvp: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("sumPc", this._data.map(item => item.pc).reduce((sum, pc) => sum + pc));
              this.sumKpi = this._data.map(item => item.pc).reduce((sum, pc) => sum + pc)
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyPcAvp: ', error);
          }
        );
        break;

      case 'GM':
        postData = {
          "gmCode": this.modelUser.getAgentCode(),
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyPcGm(postData).subscribe(
          response => {
            // console.log('res. postPolicyPcGm: ', response[0]);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("sumPc", this._data.map(item => item.pc).reduce((sum, pc) => sum + pc));
              this.sumKpi = this._data.map(item => item.pc).reduce((sum, pc) => sum + pc)
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyPcGm: ', error);
          }
        );
        break;
      
      case 'COMPANY':
        postData = {
          "filter": this.filterSubDetailType,
          "paginationPage": this.pagingCount
        };
        this.dataService.postPolicyPcCompany(postData).subscribe(
          response => {
            // console.log('res. postPolicyPcCompany: ', response);
            if (Object.keys(response['content']).length > 0) {
              this.data = response['content'];
              this.pagingItem = response;
              this.topRecords = Object.keys(response).length;
              this._data = response['content'];
              // console.log("sumPc", this._data.map(item => item.pc).reduce((sum, pc) => sum + pc));
              this.sumKpi = this._data.map(item => item.pc).reduce((sum, pc) => sum + pc)
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyPcCompany: ', error);
          }
        );
        break;

      case 'CHANNEL':
        postData = {
          "channelCode": this.queryStaffLevel,
          "filter": this.filterSubDetailType,
          "paginationPage": this.pagingCount
        };
        this.dataService.postPolicyPcChannel(postData).subscribe(
          response => {
            // console.log('res. postPolicyPcChannel: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response['content'];
              this.pagingItem = response;
              this.topRecords = Object.keys(response).length;
              this._data = response['content'];
              // console.log("sumPc", this._data.map(item => item.pc).reduce((sum, pc) => sum + pc));
              this.sumKpi = this._data.map(item => item.pc).reduce((sum, pc) => sum + pc)
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyPcChannel: ', error);
          }
        );
        break;

      case 'ZONE':
        postData = {
          "zoneCode": this.queryStaffLevel,
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyPcZone(postData).subscribe(
          response => {
            // console.log('res. postPolicyPcZone: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("sumPc", this._data.map(item => item.pc).reduce((sum, pc) => sum + pc));
              this.sumKpi = this._data.map(item => item.pc).reduce((sum, pc) => sum + pc)
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyPcZone: ', error);
          }
        );
        break;
    }
    
  }
  callAPI_getPolicyNC() {
    // CheckAgentLevel -> postData and pathAPI
    let postData = {};
    let caseQuery = this.modelUser.getAgentLevel();

    if (this.modelUser.getAgentLevel() === 'ZONE') {
      caseQuery = this.queryLevel;
    } 

    switch (caseQuery) {

      case 'AL':
        postData = {
          "alCode": this.modelUser.getAgentCode(),
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyNcAl(postData).subscribe(
          response => {
            // console.log('res. postPolicyNcAl: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("countNc", this._data.length);
              this.sumKpi = this._data.length
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyNcAl: ', error);
          }
        );
        break;

      case 'AVP':
        postData = {
          "avpCode": this.modelUser.getAgentCode(),
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyNcAvp(postData).subscribe(
          response => {
            // console.log('res. postPolicyNcAvp: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("countNc", this._data.length);
              this.sumKpi = this._data.length
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyNcAvp: ', error);
          }
        );
        break;

      case 'GM':
        postData = {
          "gmCode": this.modelUser.getAgentCode(),
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyNcGm(postData).subscribe(
          response => {
            // console.log('res. postPolicyNcGm: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("countNc", this._data.length);
              this.sumKpi = this._data.length
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyNcGm: ', error);
          }
        );
        break;
      
      case 'COMPANY':
        postData = {
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyNcCompany(postData).subscribe(
          response => {
            // console.log('res. postPolicyNcCompany: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("countNc", this._data.length);
              this.sumKpi = this._data.length
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyNcCompany: ', error);
          }
        );
        break;

      case 'CHANNEL':
        postData = {
          "channelCode": this.queryStaffLevel,
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyNcChannel(postData).subscribe(
          response => {
            // console.log('res. postPolicyNcChannel: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("countNc", this._data.length);
              this.sumKpi = this._data.length
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyNcChannel: ', error);
          }
        );
        break;

      case 'ZONE':
        postData = {
          "zoneCode": this.queryStaffLevel,
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyNcZone(postData).subscribe(
          response => {
            // console.log('res. postPolicyNcZone: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("countNc", this._data.length);
              this.sumKpi = this._data.length
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyNcZone: ', error);
          }
        );
        break;
    }
    
  }
  callAPI_getRetentionDetail() {
    let postData = {};
    let caseQuery = this.modelUser.getAgentLevel();

    if (this.modelUser.getAgentLevel() === 'ZONE') {
      caseQuery = this.queryLevel;
    } 

    switch (caseQuery) {

      case 'COMPANY':
        postData = {
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyRetentionCompany(postData).subscribe(
          response => {
            // console.log('res. postPolicyRetentionCompany: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;

            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyRetentionCompany: ', error);
          }
        );
        break;

      case 'CHANNEL':
        postData = {
          "channelCode": this.queryStaffLevel,
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyRetentionChannel(postData).subscribe(
          response => {
            // console.log('res. postPolicyRetentionChannel: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyRetentionChannel: ', error);
          }
        );
        break;

      case 'ZONE':
        postData = {
          "zoneCode": this.queryStaffLevel,
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyRetentionZone(postData).subscribe(
          response => {
            // console.log('res. postPolicyRetentionZone: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyRetentionZone: ', error);
          }
        );
        break;
    }
  }
  callAPI_getPolicyRYP() {
    // CheckAgentLevel -> postData and pathAPI
    let postData = {};
    let caseQuery = this.modelUser.getAgentLevel();

    if (this.modelUser.getAgentLevel() === 'ZONE') {
      caseQuery = this.queryLevel;
    } 

    switch (caseQuery) {
      case 'AGENT':
        postData = {
          "agentCode": this.modelUser.getAgentCode(),
          "agentLevel": this.modelUser.getAgentLevel(),
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyRypAgent(postData).subscribe(
          response => {
            // console.log('res. postPolicyRypAgent: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("sumPremium", this._data.map(item => item.premium).reduce((sum, premium) => sum + premium));
              this.sumKpi = this._data.map(item => item.premium).reduce((sum, premium) => sum + premium)
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyRypAgent: ', error);
          }
        );
        break;

      case 'AL':
        postData = {
          "alCode": this.modelUser.getAgentCode(),
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyRypAl(postData).subscribe(
          response => {
            // console.log('res. postPolicyRypAl: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("sumPremium", this._data.map(item => item.premium).reduce((sum, premium) => sum + premium));
              this.sumKpi = this._data.map(item => item.premium).reduce((sum, premium) => sum + premium)
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyRypAl: ', error);
          }
        );
        break;

      case 'AVP':
        postData = {
          "avpCode": this.modelUser.getAgentCode(),
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyRypAvp(postData).subscribe(
          response => {
            // console.log('res. postPolicyRypAvp: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("sumPremium", this._data.map(item => item.premium).reduce((sum, premium) => sum + premium));
              this.sumKpi = this._data.map(item => item.premium).reduce((sum, premium) => sum + premium)
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyRypAvp: ', error);
          }
        );
        break;

      case 'GM':
        postData = {
          "gmCode": this.modelUser.getAgentCode(),
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyRypGm(postData).subscribe(
          response => {
            // console.log('res. postPolicyRypGm: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("sumPremium", this._data.map(item => item.premium).reduce((sum, premium) => sum + premium));
              this.sumKpi = this._data.map(item => item.premium).reduce((sum, premium) => sum + premium)
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyRypGm: ', error);
          }
        );
        break;
      
      case 'COMPANY':
        postData = {
          "filter": this.filterSubDetailType,
          "paginationPage": this.pagingCount
        };
        this.dataService.postPolicyRypCompanyPagination(postData).subscribe(
          response => {
            // console.log('res. postPolicyRypCompany: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response['content'];
              this.pagingItem = response;
              this.topRecords = Object.keys(response).length;
              this._data = response['content'];
              // console.log("sumPremium", this._data.map(item => item.premium).reduce((sum, premium) => sum + premium));
              this.sumKpi = this._data.map(item => item.premium).reduce((sum, premium) => sum + premium)
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyRypCompany: ', error);
          }
        );
        break;

      case 'CHANNEL':
        postData = {
          "channelCode": this.queryStaffLevel,
          "filter": this.filterSubDetailType,
          "paginationPage": this.pagingCount
        };
        this.dataService.postPolicyRypChannelPagination(postData).subscribe(
          response => {
            // console.log('res. postPolicyRypChannel: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response['content'];
              this.pagingItem = response;
              this.topRecords = Object.keys(response).length;
              this._data = response['content'];
              // console.log("sumPremium", this._data.map(item => item.premium).reduce((sum, premium) => sum + premium));
              this.sumKpi = this._data.map(item => item.premium).reduce((sum, premium) => sum + premium)
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyRypChannel: ', error);
          }
        );
        break;

      case 'ZONE':
        postData = {
          "zoneCode": this.queryStaffLevel,
          "filter": this.filterSubDetailType
        };
        this.dataService.postPolicyRypZone(postData).subscribe(
          response => {
            // console.log('res. postPolicyRypZone: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response;
              this._data = response;
              // console.log("sumPremium", this._data.map(item => item.premium).reduce((sum, premium) => sum + premium));
              this.sumKpi = this._data.map(item => item.premium).reduce((sum, premium) => sum + premium)
            
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postPolicyRypZone: ', error);
          }
        );
        break;
    }
  }  

  setHeader(){
    if (this.filterKpiType == 'PC') {
      if (this.filterSubDetailIndex == 0) {
      
        this.second_headers = [
          { label: 'Policy/', fieldName: 'policyCode', secLabel:"Customer"},
          { label: 'Product Name', fieldName: 'prodName'},
          { label: 'PC', fieldName: 'pc'},
          { label: 'Submitted', fieldName: 'submitDate'},
          { label: 'Approved', fieldName: 'approveDate'},
        ]
        
      }
      else if (this.filterSubDetailIndex == 1) {

        this.second_headers = [
          { label: 'Policy/', fieldName: 'policyCode', secLabel:"Customer"},
          { label: 'Product Name', fieldName: 'prodName'},
          { label: 'PC', fieldName: 'pc'},
          { label: 'Submitted', fieldName: 'submitDate'},
          { label: 'Memo', fieldName: 'memo'},
          { label: 'Counter', fieldName: 'counter'},
        ]

      }
    }
    else if (this.filterKpiType == 'NC') {
      if (this.filterSubDetailIndex == 0) {
      
        this.second_headers = [
          { label: 'Recruiter', fieldName: 'recruiter'},
          { label: 'NC', fieldName: 'agentCode'},
          { label: 'Memo OIC', fieldName: 'memoOic'},
          { label: 'Memo แต่งตั้ง', fieldName: 'memo'},
        ]
        
      }
      else if (this.filterSubDetailIndex == 1) {

        this.second_headers = [
          { label: 'Recruiter', fieldName: 'recruiter'},
          { label: 'Temp', fieldName: 'agentCode'},
          { label: 'Memo OIC', fieldName: 'memoOic'},
          { label: 'Memo แต่งตั้ง', fieldName: 'memo'},
        ]
      }
      else if (this.filterSubDetailIndex == 2) {

        this.second_headers = [
          { label: 'Recruiter', fieldName: 'recruiter'},
          { label: 'Trainee', fieldName: 'agentCode'},
          { label: 'Memo OIC', fieldName: 'memoOic'},
          { label: 'Memo แต่งตั้ง', fieldName: 'memo'},
        ]
      }
      else if (this.filterSubDetailIndex == 3) {

        this.second_headers = [
          { label: 'Recruiter', fieldName: 'recruiter'},
          { label: 'Prospect', fieldName: 'agentCode'},
          { label: 'Memo OIC', fieldName: 'memoOic'},
          { label: 'Memo แต่งตั้ง', fieldName: 'memo'},
        ]
      }
    }
    else if (this.filterKpiType == 'RETENTION') {
      if (this.filterSubDetailIndex == 0) {
      
        this.second_headers = [
          { label: 'AG/FC', fieldName: 'agentCode'},
          { label: 'New Code Month', fieldName: 'newCodeYm'},
          { label: 'Retention Month', fieldName: 'retentionMonth'},
        ]
        
      }
      else if (this.filterSubDetailIndex == 1) {

        this.second_headers = [
          { label: 'AG/FC', fieldName: 'agentCode'},
          { label: 'New Code Month', fieldName: 'newCodeYm'},
          { label: 'Retention Month', fieldName: 'retentionMonth'},
        ]
      }
    }
    else if (this.filterKpiType == 'RYP') {
      if (this.filterSubDetailIndex == 0) {
      
        this.second_headers = [
          { label: 'Policy/', fieldName: 'policyCode', secLabel:"Customer"},
          { label: 'Premium', fieldName: 'premium'},
          { label: 'Due Date', fieldName: 'dueDate'},
          { label: 'Year/Term', fieldName: 'yearTerm'},
        ]
        
      }
      else if (this.filterSubDetailIndex == 1) {

        this.second_headers = [
          { label: 'Policy/', fieldName: 'policyCode', secLabel:"Customer"},
          { label: 'Premium', fieldName: 'premium'},
          { label: 'Due Date', fieldName: 'dueDate'},
          { label: 'Year/Term', fieldName: 'yearTerm'},
          { label: 'Memo', fieldName: 'memo'}
        ]
      }
      else if (this.filterSubDetailIndex == 2) {

        this.second_headers = [
          { label: 'Policy/', fieldName: 'policyCode', secLabel:"Customer"},
          { label: 'Premium', fieldName: 'premium'},
          { label: 'Due Date', fieldName: 'dueDate'},
          { label: 'Year/Term', fieldName: 'yearTerm'},
        ]
      }
    }
  }

  // Controller
  handleFilterSelected(valEmitted) {
    this.spinner.show();
    console.log("valEmitted", valEmitted);
    
    setTimeout(() => {
      this.spinner.hide();
    }, 2200); 
    this.filterSubDetailIndex = valEmitted;
    this.convertSubDetailName(valEmitted);
    this.setHeader()
  }

  goToHome() {
    let userRole = this.authService.getPosition();
    let agentCode = this.authService.getLoginedName();
    sessionStorage.removeItem('agentCodeStored');
    sessionStorage.removeItem('agentLevelStored');
    this.dashboardBackRouteService.backHome(userRole, agentCode);
  }

  goToOverview() {
    this.location.back();
  }

  handlePagination(valEmitted) {
    this.pagingCount = valEmitted;
    this.queryKpiCondition();
  }

  queryKpiCondition() {
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 2200);
    if (this.filterKpiType === 'PC') {
      this.callAPI_getPolicyPC();
    }
    else if (this.filterKpiType === 'RYP') {
      this.callAPI_getPolicyRYP();
    }
  }

  // Utilities
  checkIsStaff() {
    if (this.queryLevel === 'COMPANY' || this.queryLevel === 'CHANNEL' || this.queryLevel === 'ZONE') return true;
    else return false; 
  }

  filterTable(){
    var allData = this._data;
    var filteredData = allData.filter(data => this.filteredData(data));
    // this.table.setData(filteredData);
    this.data = filteredData;
  }

  filterTable_search(valEmitted = {sortColName: '', sortOrder: ''}) {
    
    if (this.searchKeyword.length < 1 && valEmitted.sortColName.length < 1 && valEmitted.sortOrder.length < 1) {
      this.handlePagination(0);
    } else { 

      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide();
      }, 2200);

      let _prefixSearch = '%' + this.searchKeyword + '%';
      let _channelCode = '%' + this.queryStaffLevel + '%';
      if (this.queryStaffLevel === 'COMPANY') {
        _channelCode = '%';
      }
      
      // CHECK input as data?
      if (this.searchKeyword.length == 10 && this.searchKeyword.includes('/')) {
        _prefixSearch = '%' + UtilitiesHelper.convertToRawDate(this.searchKeyword) + '%';
      }
      
      let postData = {
        "filter": this.filterSubDetailType,
        "searchKeyword": _prefixSearch,
        "channelCode": _channelCode,
        "paginationPage": this.pagingCount,
        "sortColName": valEmitted.sortColName,
        "sortOrderType": valEmitted.sortOrder
      };
      
      if (this.filterKpiType === 'PC') {
        this.dataService.postSearchPolicyPC(postData).subscribe(
          response => {
            // console.log('res. postSearchPolicyRyp: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response['content'];
              this.pagingItem = response;
              // this.pagingCount = 0;
              this.topRecords = Object.keys(response).length;
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postSearchPolicyRyp: ', error);
          }
        );
      }
      else if (this.filterKpiType === 'RYP') {
        this.dataService.postSearchPolicyRyp(postData).subscribe(
          response => {
            // console.log('res. postSearchPolicyRyp: ', response);
            if (Object.keys(response).length > 0) {
              this.data = response['content'];
              this.pagingItem = response;
              // this.pagingCount = 0;
              this.topRecords = Object.keys(response).length;
            }
            else {
              this.data = []
              this.sumKpi = null
            }
          },
          error => {
            console.log('postSearchPolicyRyp: ', error);
          }
        );
      }
    }
  }

  filteredData(data) {

    return this.searchKeyword == '' || 
      ( 
        this.first_headers.includes(this.masterLevelName[0]) &&
        (UtilitiesHelper.compareStr(data.gmCode, this.searchKeyword) || UtilitiesHelper.compareStr(data.gmName, this.searchKeyword))
      ) ||
      ( 
        this.first_headers.includes(this.masterLevelName[1]) &&
        (UtilitiesHelper.compareStr(data.avpCode, this.searchKeyword) || UtilitiesHelper.compareStr(data.avpName, this.searchKeyword))
      ) ||
      ( 
        this.first_headers.includes(this.masterLevelName[2]) &&
        (UtilitiesHelper.compareStr(data.alCode, this.searchKeyword) || UtilitiesHelper.compareStr(data.alName, this.searchKeyword))
      ) ||
      ( 
        this.first_headers.includes(this.masterLevelName[3]) &&
        (UtilitiesHelper.compareStr(data.agentCode, this.searchKeyword) || UtilitiesHelper.compareStr(data.agentName, this.searchKeyword))
      ) ||
      UtilitiesHelper.compareStr(data.policyCode, this.searchKeyword) ||
      UtilitiesHelper.compareStr(data.prodName, this.searchKeyword) ||
      UtilitiesHelper.compareStr(data.pc, this.searchKeyword) ||
      UtilitiesHelper.compareStr(data.premium, this.searchKeyword) ||
      UtilitiesHelper.compareStr(data.yearTerm, this.searchKeyword) || 
      UtilitiesHelper.compareStr(data.name, this.searchKeyword) ||
      UtilitiesHelper.compareStr(data.counter, this.searchKeyword) ||
      UtilitiesHelper.compareStr(data.memo, this.searchKeyword) ||
      UtilitiesHelper.compareStr(data.memoOic, this.searchKeyword) ||
      UtilitiesHelper.compareStr(data.recruiter, this.searchKeyword) ||
      UtilitiesHelper.compareStr(data.recruiterName, this.searchKeyword) || 
      UtilitiesHelper.compareStr(data.retentionMonth, this.searchKeyword) ||
      UtilitiesHelper.compareStr(data.newCodeYm, this.searchKeyword) || 
      UtilitiesHelper.compareStr(data.dueDate, UtilitiesHelper.convertToRawDate(this.searchKeyword)) ||
      UtilitiesHelper.compareStr(data.submitDate, UtilitiesHelper.convertToRawDate(this.searchKeyword)) ||
      UtilitiesHelper.compareStr(data.approveDate, UtilitiesHelper.convertToRawDate(this.searchKeyword))
  }

  isShowRecords() {
    if (this.queryLevel === 'COMPANY' || this.queryLevel === 'CHANNEL') {
      if (this.filterKpiType === 'PC' ) { // || this.filterKpiType === 'RYP'
        return true;
      }
    }
    return false;
  }

  isShowPaging() {
    if (this.queryLevel === 'COMPANY' || this.queryLevel === 'CHANNEL') {
      if (this.filterKpiType === 'PC' || this.filterKpiType === 'RYP') {
        return true;
      }
    }
    return false;
  }

  // Handle open modal
  openMemo(content, valEmitted) {
    this.memoOwner = valEmitted;

    let postData = {
      "agentCode": valEmitted.agentCode, // code
      "memoType": valEmitted.memoType,
    }
    
    this.dataService.postNcMemoDetail(postData).subscribe(
      response => {
        // console.log('res. postNcMemoDetail: ', response);
        if (Object.keys(response).length > 0) {
          this.memoDetail = response
        }
      },
      error => {
        console.log('postNcMemoDetail: ', error);
      }
    );

    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);

    this.modalService.open(content, {centered: true, windowClass : "xxl" }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed`;
    });
  }

  isCompOrChannel() {
    if (this.queryLevel === 'COMPANY' || this.queryLevel === 'CHANNEL') return true;
    else return false;
  }
  
}
