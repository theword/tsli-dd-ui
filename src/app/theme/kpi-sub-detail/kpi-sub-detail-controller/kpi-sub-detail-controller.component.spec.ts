import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiSubDetailControllerComponent } from './kpi-sub-detail-controller.component';

describe('KpiSubDetailControllerComponent', () => {
  let component: KpiSubDetailControllerComponent;
  let fixture: ComponentFixture<KpiSubDetailControllerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KpiSubDetailControllerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiSubDetailControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
