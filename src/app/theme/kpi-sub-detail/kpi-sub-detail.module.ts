import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KpiSubDetailControllerComponent } from './kpi-sub-detail-controller/kpi-sub-detail-controller.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/core/shared.module';

const routes: Routes = [
  {
    path: 'main',
    component: KpiSubDetailControllerComponent,
  },
  { path: '**', redirectTo: 'error', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    KpiSubDetailControllerComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule
  ]
})
export class KpiSubDetailModule { }
