import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoneAlKpiDetailComponent } from './zone-al-kpi-detail.component';

describe('ZoneAlKpiDetailComponent', () => {
  let component: ZoneAlKpiDetailComponent;
  let fixture: ComponentFixture<ZoneAlKpiDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoneAlKpiDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneAlKpiDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
