import { take } from 'rxjs/operators';
import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { AuthService } from 'src/app/core/security/auth.service';
import { UtilitiesHelper } from 'src/app/core/utilities-helper';
import { KPIDetailTableModel, HeaderKPIDetailModel } from 'src/app/core/model/model-kpi-detail-table';
import { DashboardBackRouteService } from 'src/app/core/service/dashboard-back-route.service';
import { Location } from '@angular/common';
import { CheckSessionStorageService } from 'src/app/core/service/check-session-storage.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-zone-al-kpi-detail',
  templateUrl: './zone-al-kpi-detail.component.html',
  styleUrls: ['./zone-al-kpi-detail.component.scss']
})
export class ZoneAlKpiDetailComponent implements OnInit {
  searchKeyword : any = '';
  table : KPIDetailTableModel = new KPIDetailTableModel();
  listAgentLevelBtn : any;
  listStatusBtn : any[] = ['PC','ACTIVE','NC','RYP'];
  tmpListAgentLevelBtn : any;
  mapAgentLevelName;
  displayLvName: any = '';

  listWholeGroupBtn: any = [];
  mtdPeriodMode_status: Boolean = true;
  closYm: string = '';
  currentPeriodType: string = 'MTD';

  // Data variables
  // public currentYear = new Date().getFullYear();
  public hideLastYear: boolean = true;
  public topRecords: number = 0;

  // Agent's Info.
  agentName: string;
  agentLevel: string;
  agentCode: string;
  titleCode: string;

  // Table Agent underLine
  loadingStatus: boolean = false;
  tmpTableAgent : any;
  filterKpiType: string = 'PC';
  filterGroupType: string = '';

  totalSubmitPc:any
  totalActualPc:any
  numberActive:any 
  numberActiveGt0:any 
  numberActivePct:any 
  numberNc:any
  numberNcGt0:any
  numberNcPct:any
  totalSubmitRyp:any
  totalActualRyp:any
  prstRte:any

  constructor(
    private dataService: DataServiceService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private dashboardBackRouteService: DashboardBackRouteService,
    private checkSessionStorageService : CheckSessionStorageService,
    private location : Location,
    private spinner: NgxSpinnerService
  ) { 
    this.getAgentLevelFilterBtn();
  }

  ngOnInit(): void {
    
    this.table.setFilterAgentLevel('AG/FC','AG');
    // Get KPI filter type from AgentOverview by params URL. 
    this.route.queryParams.subscribe(params => {
      let getParam = JSON.parse(params.params);
      this.table.setKpiFilter(getParam[0]['kpiType']);
      if (getParam[0]['lvName'] !== 'ZONE') {
        this.displayLvName = getParam[0]['lvName'];
      }
    });

    var tempAgentCode = sessionStorage.getItem('agentCodeSelected');
    var tempAgentLevel = sessionStorage.getItem('agentLevelSelected');

    if (tempAgentCode && tempAgentLevel) {
      this.agentCode = tempAgentCode;
      this.agentLevel = tempAgentLevel;
      this.setAgentFilter(tempAgentLevel);
    } else {
      this.setAgentFilter(this.authService.getPosition());
      this.agentCode = this.authService.getLoginedName();
      // console.log('GET : ', this.authService.getPosition());
    }

    // Set closYM
    this.dataService.getClosYmInfo().subscribe(
      response => {
        try {
          this.closYm = Object.values(response)[0].substring(0,4);
        } catch (error) {
          this.closYm = '';
          console.log('get closYm Error.');
        }
      },
      error => {
        // console.log('getPerformanceDaily: ', error);
      }
    );

    this.titleCode = tempAgentCode;
    this.callAPI_getAgentPersonalInfo();
    if (this.agentLevel == 'AGENT' || this.agentLevel == 'AG')
      this.callAPI_getAgentWholeGroup();
    else 
      this.callAPI_getAgentUnderLine();

    this.prstRte = sessionStorage.getItem('prstRte')
  }

  getAgentLevelFilterBtn(){
    this.mapAgentLevelName = new Map();
    console.log('AGENT LEVEL: ', this.agentLevel);
    
    if (this.agentLevel != 'AGENT') {
      this.dataService.getButtonName_agentLevel().subscribe((res : any[]) => {
        res = res.filter(n => n)
        this.setWholeGroupButton(res);
        this.tmpListAgentLevelBtn = res;
        this.tmpListAgentLevelBtn.forEach(element => {
          this.mapAgentLevelName.set(element.levelName,element.buttonName);
        });
        this.filterAgentLevelBtn();
      })
    }
  }

  filterAgentLevelBtn(){
    var allBtns = this.tmpListAgentLevelBtn;
    this.listAgentLevelBtn = allBtns.filter(data => 
      this.agentLevel == 'ZONE' ||
      (this.agentLevel == 'GM' && ['AVP','AL','AGENT'].includes(data.levelName)) ||
      (this.agentLevel == 'AVP' && ['AL','AGENT'].includes(data.levelName)) ||
      (this.agentLevel == 'AL' && ['AGENT'].includes(data.levelName))
    )
    if(this.listAgentLevelBtn !== undefined && this.agentLevel != 'AGENT') 
      this.table.setFilterAgentLevel(this.listAgentLevelBtn[0].buttonName,this.listAgentLevelBtn[0].levelName);
  }

  // [Calling API Service methods]
  callAPI_getAgentPersonalInfo() {
    let data = {
      "agentCode": this.agentCode
    };
    this.dataService.getAgentStructure(data).subscribe(
      response => {
        // console.log('res. getAgentStructure: ', response);
        this.agentName = response[0].thaiName + ' ' + response[0].thaiSurname;
        this.agentLevel = response[0].agentLevel;
        this.setAgentFilter(response[0].agentLevel);
      },
      error => {
        // console.log('err. getAgentStructure: ', error);
      });
  }
  callAPI_getAgentUnderLine() {
    
    /** spinner starts on init */
    this.spinner.show();
    this.loadingStatus = false;
    this.filterGroupType = 'directGroup';
    let post_agentLevel = this.table.agLevelFilter;
    // console.log('POST AGENT_LEVEL: ', post_agentLevel);
    
    if (post_agentLevel == 'AG') {
      post_agentLevel = 'AGENT';
    }

    let data = {
      "agentCode": this.agentCode,
      "agentLevel": post_agentLevel
    };

    // console.log('DATA: ', data);
    // console.log('AGENT_LEVEL: ', this.agentLevel);

    // Current as AL Level query > AG
    if (this.agentLevel == 'AL') {
      this.dataService.getAgentOverview_underAL(data).subscribe(
      response => {
        this.tmpTableAgent = response;
        this.table.setData(response);
        if(Object.assign(response).length > 0){
          // PC
          this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
          this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
          // ACTIVE
          this.numberActive = this.table.data.map(item => item.active).length
          this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
          this.numberActivePct = this.numberActiveGt0/this.numberActive*100
          // NC
          this.numberNc = this.table.data.map(item => item.nc).length
          this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
          this.numberNcPct = this.numberNcGt0/this.numberNc*100
          // RYP
          this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
          this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
        }

        this.loadingStatus = true;
        this.spinner.hide();
      },
      error => {
        // console.log('get_agPersonalInfo: ', error);
      }
      );
    } 
    else if (this.agentLevel == 'AVP') {
      this.dataService.getAgentOverview_underAVP(data).subscribe(
      response => {
        // console.log('Agent UNDER AVP: ', response);
        this.tmpTableAgent = response;
        this.table.setData(response);
        if(Object.assign(response).length > 0){
          // PC
          this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
          this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
          // ACTIVE
          this.numberActive = this.table.data.map(item => item.active).length
          this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
          this.numberActivePct = this.numberActiveGt0/this.numberActive*100
          // NC
          this.numberNc = this.table.data.map(item => item.nc).length
          this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
          this.numberNcPct = this.numberNcGt0/this.numberNc*100
          // RYP
          this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
          this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
        }

        this.loadingStatus = true;
        this.spinner.hide();
      },
      error => {
        // console.log('get_agPersonalInfo: ', error);
      });
    } 
    else if (this.agentLevel == 'GM') {
      this.dataService.getAgentOverview_underGM(data).subscribe(
      response => {
        console.log('Agent UNDER GM: ', response);
        this.tmpTableAgent = response;
        this.table.setData(response);
        if(Object.assign(response).length > 0){
          // PC
          this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
          this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
          // ACTIVE
          this.numberActive = this.table.data.map(item => item.active).length
          this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
          this.numberActivePct = this.numberActiveGt0/this.numberActive*100
          // NC
          this.numberNc = this.table.data.map(item => item.nc).length
          this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
          this.numberNcPct = this.numberNcGt0/this.numberNc*100
          // RYP
          this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
          this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
        }

        this.loadingStatus = true;
        this.spinner.hide();
      },
      error => {
        // console.log('get_agPersonalInfo: ', error);
      });
    }
    else if (this.agentLevel == 'ZONE' && sessionStorage.getItem('zoneFilterSelected') !== 'COMPANY') {
      data = {
        "agentCode": sessionStorage.getItem('zoneFilterSelected'),
        "agentLevel": post_agentLevel
      };
      this.titleCode = sessionStorage.getItem('zoneTitleName');
      this.dataService.getAgentOverview_underZONE(data).subscribe(
      response => {
        // console.log('Agent UNDER ZONE: ', response);
        this.tmpTableAgent = response;
        this.table.setData(response);
        if(Object.assign(response).length > 0){
          // PC
          this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
          this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
          // ACTIVE
          this.numberActive = this.table.data.map(item => item.active).length
          this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
          this.numberActivePct = this.numberActiveGt0/this.numberActive*100
          // NC
          this.numberNc = this.table.data.map(item => item.nc).length
          this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
          this.numberNcPct = this.numberNcGt0/this.numberNc*100
          // RYP
          this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
          this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
        }

        this.loadingStatus = true;
        this.spinner.hide();
      },
      error => {
        // console.log('get_agPersonalInfo: ', error);
      });
    }
    else if (this.agentLevel == 'ZONE' && sessionStorage.getItem('zoneFilterSelected') === 'COMPANY') {
      // COMPANY LEVEL
      if (sessionStorage.getItem('channelFilterSelected') === 'COMPANY') {
        this.titleCode = '';
        data = {
          "agentCode": '',
          "agentLevel": post_agentLevel
        };

        if (post_agentLevel === 'AGENT') {
          
          if (this.filterKpiType == undefined || this.filterKpiType == 'PC') {
            this.dataService.getAgentOverview_underCOMPANY_agentPC(data).subscribe(
              response => {
                // console.log('Agent UNDER ZONE: ', response);
                this.tmpTableAgent = response;
                this.table.setData(response);
                if(Object.assign(response).length > 0){
                  // PC
                  this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
                  this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
                  // ACTIVE
                  this.numberActive = this.table.data.map(item => item.active).length
                  this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
                  this.numberActivePct = this.numberActiveGt0/this.numberActive*100
                  // NC
                  this.numberNc = this.table.data.map(item => item.nc).length
                  this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
                  this.numberNcPct = this.numberNcGt0/this.numberNc*100
                  // RYP
                  this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
                  this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
                }

                this.loadingStatus = true;
                this.spinner.hide();
                this.topRecords = Object.keys(response).length;
              },
              error => {
                // console.log('get_agPersonalInfo: ', error);
              });
          }
          else if (this.filterKpiType == 'ACTIVE') {
            this.dataService.getAgentOverview_underCOMPANY_agentACTIVE(data).subscribe(
              response => {
                // console.log('Agent UNDER ZONE: ', response);
                this.tmpTableAgent = response;
                this.table.setData(response);
                if(Object.assign(response).length > 0){
                  // PC
                  this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
                  this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
                  // ACTIVE
                  this.numberActive = this.table.data.map(item => item.active).length
                  this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
                  this.numberActivePct = this.numberActiveGt0/this.numberActive*100
                  // NC
                  this.numberNc = this.table.data.map(item => item.nc).length
                  this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
                  this.numberNcPct = this.numberNcGt0/this.numberNc*100
                  // RYP
                  this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
                  this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
                }

                this.loadingStatus = true;
                this.spinner.hide();
                this.topRecords = Object.keys(response).length;
              },
              error => {
                // console.log('get_agPersonalInfo: ', error);
              });
          }
          else if (this.filterKpiType == 'RYP') {
            this.dataService.getAgentOverview_underCOMPANY_agentRYP(data).subscribe(
              response => {
                // console.log('Agent UNDER ZONE: ', response);
                this.tmpTableAgent = response;
                this.table.setData(response);
                if(Object.assign(response).length > 0){
                  // PC
                  this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
                  this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
                  // ACTIVE
                  this.numberActive = this.table.data.map(item => item.active).length
                  this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
                  this.numberActivePct = this.numberActiveGt0/this.numberActive*100
                  // NC
                  this.numberNc = this.table.data.map(item => item.nc).length
                  this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
                  this.numberNcPct = this.numberNcGt0/this.numberNc*100
                  // RYP
                  this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
                  this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
                }

                this.loadingStatus = true;
                this.spinner.hide();
                this.topRecords = Object.keys(response).length;
              },
              error => {
                // console.log('get_agPersonalInfo: ', error);
              });
          }
          
        } else {
          this.dataService.getAgentOverview_underCOMPANY_producer(data).subscribe(
            response => {
              // console.log('Agent UNDER ZONE: ', response);
              this.tmpTableAgent = response;
              this.table.setData(response);
              if(Object.assign(response).length > 0){
                // PC
                this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
                this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
                // ACTIVE
                this.numberActive = this.table.data.map(item => item.active).length
                this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
                this.numberActivePct = this.numberActiveGt0/this.numberActive*100
                // NC
                this.numberNc = this.table.data.map(item => item.nc).length
                this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
                this.numberNcPct = this.numberNcGt0/this.numberNc*100
                // RYP
                this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
                this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
              }

              this.loadingStatus = true;
              this.spinner.hide();
            },
            error => {
              // console.log('get_agPersonalInfo: ', error);
            });
        }
      } 
      // CHANNEL LEVEL
      else {
        this.titleCode = sessionStorage.getItem('channelFilterSelected');
        const postData = {
          "agentCode": '',
          "agentLevel": post_agentLevel,
          "channelCode": sessionStorage.getItem('channelFilterSelected')
        };

        // Query Agent level unde channel
        if (post_agentLevel === 'AGENT') {
          if (this.filterKpiType == undefined || this.filterKpiType == 'PC') {
            this.dataService.getAgentOverview_underChannel_agentPc(postData).subscribe(
              response => {
                this.tmpTableAgent = response;
                this.table.setData(response);
                if (Object.assign(response).length > 0) {
                  this.setTableUnderChannel();
                  this.topRecords = Object.keys(response).length;
                }
                this.loadingStatus = true;
                this.spinner.hide();
              },
              error => {
                // console.log('get_agPersonalInfo: ', error);
              });
          }
          else if (this.filterKpiType == 'ACTIVE') {
            this.dataService.getAgentOverview_underChannel_agentActive(postData).subscribe(
              response => {
                this.tmpTableAgent = response;
                this.table.setData(response);
                if (Object.assign(response).length > 0) {
                  this.setTableUnderChannel();
                  this.topRecords = Object.keys(response).length;
                }
                this.loadingStatus = true;
                this.spinner.hide();
              },
              error => {
                // console.log('get_agPersonalInfo: ', error);
              });
          }
          else if (this.filterKpiType == 'RYP') {
            this.dataService.getAgentOverview_underChannel_agentRyp(postData).subscribe(
              response => {
                this.tmpTableAgent = response;
                this.table.setData(response);
                if (Object.assign(response).length > 0) {
                  this.setTableUnderChannel();
                  this.topRecords = Object.keys(response).length;
                }
                this.loadingStatus = true;
                this.spinner.hide();
              },
              error => {
                // console.log('get_agPersonalInfo: ', error);
              });
          }
        } // End if
        // Query Producer level under channel
        else {
          this.dataService.getAgentOverview_underChannel_producer(postData).subscribe(
            response => {
              this.tmpTableAgent = response;
              this.table.setData(response);
              if (Object.assign(response).length > 0) {
                this.setTableUnderChannel();
              }
              this.loadingStatus = true;
              this.spinner.hide();
            },
            error => {
              // console.log('get_agPersonalInfo: ', error);
            });
        } // End else

        // OLD VERSION HERE
      }
    }
  }
  callAPI_getAgentWholeGroup() {
    /** spinner starts on init */
    this.spinner.show();
    this.loadingStatus = false;
    this.filterGroupType = 'wholeGroup';
    let post_agentLevel = this.table.agLevelFilter;

    if (post_agentLevel == 'AG') {
      post_agentLevel = 'AGENT';
    }

    let data = {
      "filter": this.table.filterKpiType,
      "agentCode": this.agentCode,
      "agentLevel": post_agentLevel
    };

    this.dataService.postTeamdetailBySon(data).subscribe(
      response => {
        console.log('RES: ', Object.assign(response).length);
        this.tmpTableAgent = response;
        this.table.setData(response);

        if(Object.assign(response).length > 0){
          if(Object.assign(response).length > 0){
            // PC
            this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
            this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
            // ACTIVE
            this.numberActive = this.table.data.map(item => item.active).length
            this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
            this.numberActivePct = this.numberActiveGt0/this.numberActive*100
            // NC
            this.numberNc = this.table.data.map(item => item.nc).length
            this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
            this.numberNcPct = this.numberNcGt0/this.numberNc*100
            // RYP
            this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
            this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
          }
        }
        
        this.loadingStatus = true;
        this.spinner.hide();
      },
      error => {
        // console.log('get_agPersonalInfo: ', error);
      }
    );
  }

  // Set Data 
  setTableUnderChannel() {
    // PC
    this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
    this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
    // ACTIVE
    this.numberActive = this.table.data.map(item => item.active).length
    this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
    this.numberActivePct = this.numberActiveGt0 / this.numberActive * 100
    // NC
    this.numberNc = this.table.data.map(item => item.nc).length
    this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
    this.numberNcPct = this.numberNcGt0 / this.numberNc * 100
    // RYP
    this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
    this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
  }

  // get select from Agent Table
  handleAgentSelect(param) {
    // console.log('ITEM: ', param);
    let item = param.item;
    let selectedAgentLv = param.level;
    let agCode = item.agentCode;

    if (param.groupType == 'directGroup') {
      if (selectedAgentLv == 'GM') agCode = item.gmCode;
      if (selectedAgentLv == 'AVP') agCode = item.avpCode;
      if (selectedAgentLv == 'AL') agCode = item.alCode;
      if (selectedAgentLv == 'AG') {
        selectedAgentLv = 'AGENT';
        agCode = item.agentCode;
      }
    }
    else if (param.groupType == 'wholeGroup') {
      agCode = item.sonCode;
    //   console.log('SON CODE: ', item.sonCode);
      
        if (selectedAgentLv == 'AG') {
            selectedAgentLv = 'AGENT';
        }
    }
    
    let redirectionBasedOnRole = '';
    let currentAgentCode = sessionStorage.getItem('agentCodeSelected');
    let currentAgentLevel = sessionStorage.getItem('agentLevelSelected');
    let agentCodeStored = sessionStorage.getItem('agentCodeStored') != null ? JSON.parse(sessionStorage.getItem('agentCodeStored')) : [];
    agentCodeStored.push(currentAgentCode);
    let agentLevelStored = sessionStorage.getItem('agentLevelStored') != null ? JSON.parse(sessionStorage.getItem('agentLevelStored')) : [];
    agentLevelStored.push(currentAgentLevel);
    sessionStorage.setItem('agentLevelStored', JSON.stringify(agentLevelStored));
    sessionStorage.setItem('agentCodeStored',JSON.stringify(agentCodeStored));
    sessionStorage.setItem('agentCodeSelected', agCode);
    sessionStorage.setItem('agentLevelSelected', selectedAgentLv);
    // this.checkSessionStorageService.storeSessionChangePage(agCode,selectedAgentLv);

    // Set agentCodeSelected HERE!!!
    switch (selectedAgentLv) {
      case 'GM': case 'AVP': case 'AL': 
          redirectionBasedOnRole = '/dashboard/producer/';
        break;
      
      case 'AGENT':
          redirectionBasedOnRole = '/dashboard/agent/';
        break;
    
      default:
          redirectionBasedOnRole = '/error/404';
        break;
    }
    this.router.navigate([redirectionBasedOnRole]);
  }
  handleTeamSelect(event) {

    let item = event.detail;
    var levelName = this.table.filterAgentLevel !== undefined ? this.table.filterAgentLevel.split('/')[0] : '';
    this.mapAgentLevelName.forEach((value: string, key: string) => {
      if(value == this.table.filterAgentLevel) levelName = key;
    });
    if(levelName == 'AGENT') levelName = 'AG';
    this.table.setFilterAgentLevel(this.table.filterAgentLevel,levelName);
    let filterAgentLevel = this.table.agLevelFilter;
    let redirectionBasedOnKpiType = '/dashboard/producer/kpidetail/'+filterAgentLevel.toLowerCase();
    var filterKPI = this.table.filterKpiType;
    if(item.agentLevel == 'AL' && this.table.filterKpiType == 'NC') filterKPI = 'PC';
    let queryParams = [
      { kpiType: filterKPI, lvName:item.levelName}
    ];

    let _tempCode = item.agentCode;
    if (event.type == 'wholeGroup') _tempCode = item.sonCode;

    this.checkSessionStorageService.storeSessionChangePage(_tempCode, filterAgentLevel);
    // console.log('handleTeamSelect to: ', redirectionBasedOnKpiType);
    // console.log('handleTeamSelect urlParam: ', queryParams);
    this.router.navigate([redirectionBasedOnKpiType], { 
      queryParams: {
        params: JSON.stringify(queryParams) 
      } 
    });
  }

  // :: call from HTML, button 'HOME'
  goToHome() {
    let redirectionBasedOnRole = '';
    let userRole = this.authService.getPosition();
    let agentCode = this.authService.getLoginedName();
    sessionStorage.setItem('agentCodeSelected', agentCode);
    sessionStorage.setItem('agentLevelSelected', userRole);
    sessionStorage.setItem('channelFilterSelected', 'COMPANY');
    sessionStorage.setItem('zoneFilterSelected', 'COMPANY');
    sessionStorage.removeItem('agentCodeStored');
    sessionStorage.removeItem('agentLevelStored');
    // Set agentCodeSelected == this.authService.getLoginedName() HERE!!!
    
    switch (userRole) {
      case 'GM': case 'AVP': case 'AL': // Set as GM - AL
          redirectionBasedOnRole = '/dashboard/producer/';
        break;
      
      case 'AGENT': // Set as AG
          redirectionBasedOnRole = '/dashboard/agent/';
        break;

      case 'ZONE':
          redirectionBasedOnRole = '/dashboard/staff/';
        break;
    
      default:
          redirectionBasedOnRole = '/error/404';
        break;
    }
    this.router.navigate([redirectionBasedOnRole]);
  }
  goToOverview() {
    // sessionStorage.setItem('agentCodeSelected', this.agentCode);
    // this.dashboardBackRouteService.onClickBack();
    this.location.back();
  }

  back() {
    this.location.back();
  }

  // Controller methods
  handleKPIFilterSelect(kpiSelect: string) {
    if (this.table.filterKpiType != kpiSelect) {
      this.searchKeyword = '';
      this.setDefaultPeriodType();
      this.filterKpiType = kpiSelect;
      var levelAGBtnName = this.mapAgentLevelName.get('AGENT');
      this.setFilterButtons();
      this.table.setKpiFilter(kpiSelect);
      if(kpiSelect == 'NC') {
        var btnToShow = [];
        this.listAgentLevelBtn.forEach(ele => {
          if(ele.buttonName != levelAGBtnName) btnToShow.push(ele);
        });
        this.listAgentLevelBtn = btnToShow;
      }else{
        this.filterAgentLevelBtn();
      }
      this.setAgentFilter(this.agentLevel);
      if (this.agentLevel == 'AGENT' || this.agentLevel == 'AG')
        this.callAPI_getAgentWholeGroup();
      else
        this.callAPI_getAgentUnderLine();
    }
  }
  handleAgentLevelSelect(event) {
    if (this.table.filterAgentLevel != event.agentLevel) {
      this.searchKeyword = '';
      this.setDefaultPeriodType();
      var levelName = event.agentLevel !== undefined ? event.agentLevel.split('/')[0] : '';
      this.mapAgentLevelName.forEach((value: string, key: string) => {
        if (value == event.agentLevel) levelName = key;
      });
      if(levelName == 'AGENT') levelName = 'AG';
      this.table.setFilterAgentLevel(event.agentLevel, levelName);
      
      if (event.type == 'directGroup') {
        this.callAPI_getAgentUnderLine();
      }
      else if (event.type == 'wholeGroup') {
        this.callAPI_getAgentWholeGroup();
      } 

      this.setFilterButtons();
      this.setTableHeaders();
    }
  }

  setAgentFilter(agentLevel) {
    let tmpAgentFilter = this.table.filterAgentLevel;
    if(agentLevel == 'AL') {
      this.table.setFilterAgentLevel(this.mapAgentLevelName.get('AGENT'),'AG');
    } else if(agentLevel == 'AVP') {
      this.table.setFilterAgentLevel(this.mapAgentLevelName.get('AL'),'AL');
    } else if(agentLevel == 'GM') {
      this.table.setFilterAgentLevel(this.mapAgentLevelName.get('AVP'),'AVP');
    } else if(agentLevel == 'ZONE') {
      this.table.setFilterAgentLevel(this.mapAgentLevelName.get('GM'),'GM');
    }
    if(tmpAgentFilter != this.table.filterAgentLevel) this.callAPI_getAgentUnderLine();
    this.setTableHeaders();
    this.setFilterButtons();
  }
  setTableHeaders(){
    if (this.table.agLevelFilter == 'AVP') {
      if(this.agentLevel == 'ZONE') this.table.setAgentLevelHeaders([new HeaderKPIDetailModel(this.mapAgentLevelName.get('GM'),'gmCode'),new HeaderKPIDetailModel(this.mapAgentLevelName.get('AVP'),'avpCode')]);
      else if (this.agentLevel == 'GM' || this.agentLevel == 'AVP') this.table.setAgentLevelHeaders([new HeaderKPIDetailModel(this.mapAgentLevelName.get('AVP'),'avpCode')]);
      else this.table.resetAgentLevelHeaders();
    }
    else if (this.table.agLevelFilter == 'AL') {
      if(this.agentLevel == 'ZONE') this.table.setAgentLevelHeaders([new HeaderKPIDetailModel(this.mapAgentLevelName.get('GM'),'gmCode'),new HeaderKPIDetailModel(this.mapAgentLevelName.get('AVP'),'avpCode'),new HeaderKPIDetailModel(this.table.filterAgentLevel,'alCode')]);
      else if(this.agentLevel == 'GM') this.table.setAgentLevelHeaders([new HeaderKPIDetailModel(this.mapAgentLevelName.get('AVP'),'avpCode'),new HeaderKPIDetailModel(this.table.filterAgentLevel,'alCode')]);
      else this.table.setAgentLevelHeaders([new HeaderKPIDetailModel(this.table.filterAgentLevel,'alCode')]);
    }
    else if (this.table.agLevelFilter == 'AG') {
      if(this.agentLevel == 'ZONE') this.table.setAgentLevelHeaders([new HeaderKPIDetailModel(this.mapAgentLevelName.get('GM'),'gmCode'),new HeaderKPIDetailModel(this.mapAgentLevelName.get('AVP'),'avpCode'),new HeaderKPIDetailModel(this.mapAgentLevelName.get('AL'),'alCode'),new HeaderKPIDetailModel(this.table.filterAgentLevel,'agentCode')]);
      else if(this.agentLevel == 'GM') this.table.setAgentLevelHeaders([new HeaderKPIDetailModel(this.mapAgentLevelName.get('AVP'),'avpCode'),new HeaderKPIDetailModel(this.mapAgentLevelName.get('AL'),'alCode'),new HeaderKPIDetailModel(this.table.filterAgentLevel,'agentCode')]);
      else if(this.agentLevel == 'AVP') this.table.setAgentLevelHeaders([new HeaderKPIDetailModel(this.mapAgentLevelName.get('AL'),'alCode'),new HeaderKPIDetailModel(this.table.filterAgentLevel,'agentCode')]);
      else this.table.setAgentLevelHeaders([new HeaderKPIDetailModel(this.table.filterAgentLevel,'agentCode')]);
    }
    else if (this.table.agLevelFilter == 'GM') {
      if (this.agentLevel == 'ZONE' || this.agentLevel == 'GM') this.table.setAgentLevelHeaders([new HeaderKPIDetailModel(this.mapAgentLevelName.get('GM'),'gmCode')]);
      else this.table.resetAgentLevelHeaders();
    }
    this.table.setSubHeaders(this.table.filterKpiType);
  }
  setFilterButtons(){
    let currentFilterLevel = this.table.filterAgentLevel;
    let levelWithoutNC = this.mapAgentLevelName.get('AGENT');
    
    if(this.filterGroupType == 'wholeGroup' && currentFilterLevel == "AL/FM/BK" ){
      this.listStatusBtn = ['PC','ACTIVE','RYP']
    } else {
      if (currentFilterLevel == levelWithoutNC && this.listStatusBtn.indexOf('NC') != -1){
        this.listStatusBtn.splice(this.listStatusBtn.indexOf('NC'),1);
      } else if (currentFilterLevel == levelWithoutNC){
        this.listStatusBtn = ['PC','ACTIVE','RYP'];
      } else {
        this.listStatusBtn = ['PC','ACTIVE','NC','RYP'];
      }
    }

    
  }
  setWholeGroupButton(btnList: any = []){
    btnList.forEach(element => {
      if (element.levelName == this.agentLevel)
        this.listWholeGroupBtn.push(element);
    });
  }

  filterTable(){
    var allData = this.tmpTableAgent;
    var filteredData = allData.filter(data => this.filteredData(data));
    this.table.setData(filteredData);
  }
  filteredData(data){
    var agentLevelHeaders = this.table.getAgentLevelHeaders();
    var allHeaders = [];
    agentLevelHeaders.forEach(ele => {
      allHeaders.push(ele.name);
    })
    return this.searchKeyword == '' || 
      (allHeaders.includes(this.mapAgentLevelName.get('AGENT')) && (UtilitiesHelper.compareStr(data.agentName,this.searchKeyword) || UtilitiesHelper.compareStr(data.agentCode,this.searchKeyword))) ||
      (allHeaders.includes(this.mapAgentLevelName.get('AVP')) && (UtilitiesHelper.compareStr(data.avpName,this.searchKeyword) || UtilitiesHelper.compareStr(data.avpCode,this.searchKeyword))) ||
      (allHeaders.includes(this.mapAgentLevelName.get('AL')) && (UtilitiesHelper.compareStr(data.alName,this.searchKeyword) || UtilitiesHelper.compareStr(data.alCode,this.searchKeyword))) ||
      (allHeaders.includes(this.mapAgentLevelName.get('GM')) && (UtilitiesHelper.compareStr(data.gmName,this.searchKeyword) || UtilitiesHelper.compareStr(data.gmCode,this.searchKeyword)))
  }
  checkIsShowRecords() {
    if (this.agentLevel == 'ZONE' && sessionStorage.getItem('zoneFilterSelected') === 'COMPANY') {
      if (this.table.agLevelFilter === 'AG') {
        return true;
      }
    }
    return false;
  }

  handlePeriodMode(periodType: string) {
    if (this.currentPeriodType != periodType) {
      this.currentPeriodType = periodType;
      this.spinner.show();
      this.loadingStatus = false;
      this.mtdPeriodMode_status = !this.mtdPeriodMode_status;
      var _postData;
      
      if (periodType == 'YTD') {
        if (this.filterGroupType == 'directGroup') {
          let _postAgentLevel = this.table.agLevelFilter;
          if (_postAgentLevel == 'AG') {
            _postAgentLevel = 'AGENT';
          }
          switch (this.agentLevel) {
            // Producer
            case 'AL': case 'AVP': case 'GM':
              _postData = {
                'filter': this.agentLevel,
                'agentCode': this.agentCode,
                'agentLevel': _postAgentLevel,
                'closYm': this.closYm
              };
              this.dataService.postTeamdetailYtdByAgent(_postData).subscribe(
                response => {
                  this.tmpTableAgent = response;
                  this.table.setData(response);
                  if(Object.assign(response).length > 0){
                    // PC
                    this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
                    this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
                    // ACTIVE
                    this.numberActive = this.table.data.map(item => item.active).length
                    this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
                    this.numberActivePct = this.numberActiveGt0/this.numberActive*100
                    // NC
                    this.numberNc = this.table.data.map(item => item.nc).length
                    this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
                    this.numberNcPct = this.numberNcGt0/this.numberNc*100
                    // RYP
                    this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
                    this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
                  }

                  this.spinner.hide();
                  this.loadingStatus = true;
                },
                error => {
                  console.log('get_ytdAgentDirectGroup - AG: ', error);
                }
              );
              break;
            
            // Company
            case 'ZONE':
              const _channelSelected = sessionStorage.getItem('channelFilterSelected');
              const _zoneSelected = sessionStorage.getItem('zoneFilterSelected');
              // Company
              if (_channelSelected == 'COMPANY' && _zoneSelected == 'COMPANY') {
                _postData = {
                  'filter': this.filterKpiType,
                  'agentLevel': _postAgentLevel,
                  'closYm': this.closYm
                };
                this.dataService.postTeamdetailYtdByCompany(_postData).subscribe(
                  response => {
                    this.tmpTableAgent = response;
                    this.table.setData(response);
                    if(Object.assign(response).length > 0){
                      // PC
                      this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
                      this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
                      // ACTIVE
                      this.numberActive = this.table.data.map(item => item.active).length
                      this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
                      this.numberActivePct = this.numberActiveGt0/this.numberActive*100
                      // NC
                      this.numberNc = this.table.data.map(item => item.nc).length
                      this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
                      this.numberNcPct = this.numberNcGt0/this.numberNc*100
                      // RYP
                      this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
                      this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
                    }

                    this.spinner.hide();
                    this.loadingStatus = true;
                  },
                  error => {
                    console.log('get_ytdAgentDirectGroup - COMPANY: ', error);
                  }
                );
              }
              // Channel
              else if (_channelSelected != 'COMPANY' && _zoneSelected == 'COMPANY') {
                _postData = {
                  'filter': this.filterKpiType,
                  'agentCode': _channelSelected,
                  'agentLevel': _postAgentLevel,
                  'closYm': this.closYm
                };
                this.dataService.postTeamdetailYtdByChannel(_postData).subscribe(
                  response => {
                    this.tmpTableAgent = response;
                    this.table.setData(response);
                    if(Object.assign(response).length > 0){
                      // PC
                      this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
                      this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
                      // ACTIVE
                      this.numberActive = this.table.data.map(item => item.active).length
                      this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
                      this.numberActivePct = this.numberActiveGt0/this.numberActive*100
                      // NC
                      this.numberNc = this.table.data.map(item => item.nc).length
                      this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
                      this.numberNcPct = this.numberNcGt0/this.numberNc*100
                      // RYP
                      this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
                      this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
                    }

                    this.spinner.hide();
                    this.loadingStatus = true;
                  },
                  error => {
                    console.log('get_ytdAgentDirectGroup - CHANNEL: ', error);
                  }
                );
              }
              // Zone
              else if (_channelSelected != 'COMPANY' && _zoneSelected != 'COMPANY') {
                _postData = {
                  'agentCode': _zoneSelected,
                  'agentLevel': _postAgentLevel,
                  'closYm': this.closYm
                };
                this.dataService.postTeamdetailYtdByZone(_postData).subscribe(
                  response => {
                    this.tmpTableAgent = response;
                    this.table.setData(response);
                    if(Object.assign(response).length > 0){
                      // PC
                      this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
                      this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
                      // ACTIVE
                      this.numberActive = this.table.data.map(item => item.active).length
                      this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
                      this.numberActivePct = this.numberActiveGt0/this.numberActive*100
                      // NC
                      this.numberNc = this.table.data.map(item => item.nc).length
                      this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
                      this.numberNcPct = this.numberNcGt0/this.numberNc*100
                      // RYP
                      this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
                      this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
                    }

                    this.spinner.hide();
                    this.loadingStatus = true;
                  },
                  error => {
                    console.log('get_ytdAgentDirectGroup - ZONE: ', error);
                  }
                );
              }
              break;
          
            default:
              this.spinner.hide();
              break;
          }
        }
        else if (this.filterGroupType == 'wholeGroup') {
          _postData = {
            'filter': this.filterKpiType,
            'agentCode': this.agentCode,
            'agentLevel': this.agentLevel,
            'closYm': this.closYm
          };
          this.dataService.postTeamdetailYtdBySon(_postData).subscribe(
            response => {
              this.tmpTableAgent = response;
              this.table.setData(response);
              if(Object.assign(response).length > 0){
                // PC
                this.totalSubmitPc = this.table.data.map(item => item.pcSubmit).reduce((total, pcSubmit) => total + pcSubmit)
                this.totalActualPc = this.table.data.map(item => item.pcApprove).reduce((total, pcApprove) => total + pcApprove)
                // ACTIVE
                this.numberActive = this.table.data.map(item => item.active).length
                this.numberActiveGt0 = this.table.data.filter(item => item.active > 0).length
                this.numberActivePct = this.numberActiveGt0/this.numberActive*100
                // NC
                this.numberNc = this.table.data.map(item => item.nc).length
                this.numberNcGt0 = this.table.data.filter(item => item.nc > 0).length
                this.numberNcPct = this.numberNcGt0/this.numberNc*100
                // RYP
                this.totalSubmitRyp = this.table.data.map(item => item.targetRyp).reduce((total, targetRyp) => total + targetRyp)
                this.totalActualRyp = this.table.data.map(item => item.ryp).reduce((total, ryp) => total + ryp)
              }

              this.spinner.hide();
              this.loadingStatus = true;
            },
            error => {
              console.log('get_ytdAgentWholeGroup - AG: ', error);
            }
          );
        }
      }
      // MTD
      else {
        if (this.filterGroupType == 'directGroup') {
          this.callAPI_getAgentUnderLine();
        }
        else if (this.filterGroupType == 'wholeGroup') {
          this.callAPI_getAgentWholeGroup();
        }
      }
    }
  }
  setDefaultPeriodType() {
    this.mtdPeriodMode_status = true;
    this.currentPeriodType = 'MTD';
  }

  // Back from browser
  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    this.dashboardBackRouteService.onClickBack();
  }


}
