import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadPromotionControllerComponent } from './upload-promotion-controller/upload-promotion-controller.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/core/shared.module';
import { PromotionConfigTableComponent } from './components/promotion-config-table/promotion-config-table.component';
import { SuperUserGuards } from 'src/app/core/security/superuser.guards';

const routes: Routes = [
  {
    path: '',
    canActivate: [SuperUserGuards],
    component: UploadPromotionControllerComponent
  },
  {
    path: '**', redirectTo: 'error', pathMatch: 'full'
  }
]


@NgModule({
  declarations: [
    UploadPromotionControllerComponent,
    PromotionConfigTableComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    CommonModule
  ]
})
export class UploadPromotionModule { }
