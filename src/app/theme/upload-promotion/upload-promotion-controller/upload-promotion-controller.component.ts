import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/security/auth.service';
import { DashboardBackRouteService } from 'src/app/core/service/dashboard-back-route.service';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import * as XLSX from "xlsx";

type AOA = any[][];

@Component({
  selector: 'app-upload-promotion-controller',
  templateUrl: './upload-promotion-controller.component.html',
  styleUrls: ['./upload-promotion-controller.component.scss']
})
export class UploadPromotionControllerComponent implements OnInit {

  public msgSuccess: string = '';
  public msgFail: string = '';
  // V.file
  public fileName: String = '';
  public fileSelected: any;
  public fileBased64: String;
  public msgUploadStatus: String = 'รอการอัปโหลด';
  public uploadStatus = 0;
  public filePromotionType: String = 'RESULT';
  public modalStatus: String = '';

  // V.dataTable
  public tableHeader;
  public tableData;

  // V.tab
  public tabSelectedId: Number = 0;

  // V.promotionConfigTable
  public configIdSelect: Number = 0;
  public configTableHeader: any = [
    'ID', 'ชื่อไฟล์', 'วันที่สร้าง', 'ผู้สร้าง', 'วันที่อัปเดตล่าสุด', 'ผู้อัปเดต', 'ประเภท', 'สถานะ', 'Action'
  ];
  public configTableData: any = [];

  constructor(
    private authService: AuthService,
    private dataService: DataServiceService,
    private dashboardBackRouteService: DashboardBackRouteService,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
  }

  // TAB CONTROLLER
  onChangeTab(tabId: number) {
    if (tabId != this.tabSelectedId) {
      this.tabSelectedId = tabId;

      if (this.tabSelectedId == 1) {
        this.apiGetPromotionConfigList();
      }
    }
  }

  // MODAL CONTROLLER
  closeModalConfig() {
    this.configIdSelect = 0;
    this.modalService.dismissAll();
  }

  // API
  onSubmitFile(content) {
    if (this.fileSelected && this.uploadStatus == 0) {
      this.spinner.show();

      var _postData = {
        promotionType: this.filePromotionType,
        fileName: this.fileSelected.name,
        fileSize: parseInt(this.fileSelected.size),
        fileBased64: this.fileBased64,
        fileType: this.fileSelected.type
      };
      
      this.dataService.postUploadPromotion(_postData).subscribe(
        response => {
          // console.log('RES: ', response[0]);
          this.msgSuccess = 'อัปโหลดไฟล์สำเร็จ';
          this.msgFail = 'อัปโหลดไฟล์ล้มเหลว';
          if (response[0] == 'success') {
            this.msgUploadStatus = 'สำเร็จ';
            this.uploadStatus = 2;
            this.resetFileSelect();
            this.modalStatus = 'success';
          } 
          else {
            this.msgUploadStatus = 'ไม่สำเร็จ';
            this.uploadStatus = 1;
            this.modalStatus = 'fail';
          }
          this.modalService.open(content, { centered: true, windowClass: "modal-status" }).result.then((result) => {
            // this.closeResult = `Closed with: ${result}`;
          }, (reason) => {
            // this.closeResult = `Dismissed`;
          });
          this.spinner.hide();
        },
        error => {
          console.log('post_uploadPromotion Failed: ', error);
        }
      );
    }
  }
  apiGetPromotionConfigList() {
    this.spinner.show();
    let _postData = {
      agentCode: ''
    };
    this.dataService.getPromotionConfigList(_postData).subscribe(
      response => {
        // console.log('RES: ', response);
        this.configTableData = response;
        this.spinner.hide();
      },
      error => {
        console.log('getPromotionConfigList Failed: ', error);
      }
    );
  }
  submitDeleteConfig(contentResult) {
    this.modalService.dismissAll();
    this.spinner.show();
    let _postData = {
      promotionConfigId: this.configIdSelect
    };
    this.dataService.delPromotionConfigList(_postData).subscribe(
      response => {
        // console.log('RES: ', response);
        this.apiGetPromotionConfigList();
        this.msgSuccess = 'ยกเลิกรายการสำเร็จ';
        this.msgFail = 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง';
        if (response['msg'] == 'success') {
          this.modalStatus = 'success';
        }
        else {
          this.modalStatus = 'fail';
        }
        this.spinner.hide();
        this.modalService.open(contentResult, { centered: true, windowClass: "modal-status" });
      },
      error => {
        console.log('delPromotionConfigList Failed: ', error);
      }
    );
  }
  handlerOnDeleteConfig(contentConfirm, configId) {
    this.configIdSelect = configId;
    this.modalService.open(contentConfirm, { centered: true, windowClass: "modal-status" })
  }

  // HANDLER FILE SELECTED
  onFileChange(fileInput) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      this.uploadStatus = 0;
      this.msgUploadStatus = 'รอการอัปโหลด';
      this.fileSelected = fileInput.target.files[0];
      this.fileName = fileInput.target.files[0].name;
      const file = fileInput.target.files[0];
      this.readFile(file).then((base64: string): any => {
        this.fileBased64 = base64;
      });
      
      // To-DataTable
      // const target: DataTransfer = <DataTransfer>fileInput.target;
      // const reader: FileReader = new FileReader();
      // reader.onload = (e: any) => {
      //   /* read workbook */
      //   const bstr: string = e.target.result;
      //   const wb: XLSX.WorkBook = XLSX.read(bstr, { type: "binary", sheetRows: 6 });

      //   /* grab first sheet */
      //   const wsname: string = wb.SheetNames[0];
      //   const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      //   /* get data */
      //   this.tableData = <AOA>(
      //     XLSX.utils.sheet_to_json(ws, { header: 1, raw: false, range: 0 })
      //   );
      //   this.tableHeader = this.tableData[0];
      //   // remove first header record
      //   this.tableData = this.tableData.slice(1); 
      // };
      // reader.readAsBinaryString(target.files[0]);
      
    }
  }
  readFile(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }
  resetFileSelect() {
    // this.fileSelected = null;
    this.fileName = '';
    this.fileBased64 = '';
  }

  // HANDLDER PROMOTION TYPE CHANGE
  // onPromotionTypeChange(even, type) {
  //   console.log(even);
  //   // console.log('TYPE CHANGE: ', type);
  //   if (this.filePromotionType != type) {
  //     this.filePromotionType = type;
  //   }
  // }

  // ROUTE CONTROLLER
  goToHome() {
    let userRole = this.authService.getPosition();
    let agentCode = this.authService.getLoginedName();
    sessionStorage.removeItem('agentCodeStored');
    sessionStorage.removeItem('agentLevelStored');
    this.dashboardBackRouteService.backHome(userRole, agentCode);
  }

}
