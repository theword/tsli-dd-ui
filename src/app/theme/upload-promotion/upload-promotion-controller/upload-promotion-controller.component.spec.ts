import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadPromotionControllerComponent } from './upload-promotion-controller.component';

describe('UploadPromotionControllerComponent', () => {
  let component: UploadPromotionControllerComponent;
  let fixture: ComponentFixture<UploadPromotionControllerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadPromotionControllerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadPromotionControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
