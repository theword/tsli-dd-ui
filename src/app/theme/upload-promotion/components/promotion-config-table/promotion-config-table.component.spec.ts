import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionConfigTableComponent } from './promotion-config-table.component';

describe('PromotionConfigTableComponent', () => {
  let component: PromotionConfigTableComponent;
  let fixture: ComponentFixture<PromotionConfigTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromotionConfigTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionConfigTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
