import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-promotion-config-table',
  templateUrl: './promotion-config-table.component.html',
  styleUrls: ['./promotion-config-table.component.scss']
})
export class PromotionConfigTableComponent implements OnInit {

  @Input() headers: any = [];
  @Input() data: any = [];

  @Output() onDelete = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  // Translate
  translateFileStatus(type: string) {
    if (type == 'F') return 'ใช้งาน';
    else return 'ลบ';
  }

  onClickDeleteConfig(configId) {
    this.onDelete.emit(configId);
  }

}
