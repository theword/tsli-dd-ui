import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-kpi-ryp',
  templateUrl: './kpi-ryp.component.html',
  styleUrls: ['./kpi-ryp.component.scss']
})
export class KpiRypComponent implements OnInit {

  // Data variables
  // currentYear = new Date().getFullYear();
  // currentMonth = new Date().getMonth();
  chartZoom: boolean = true;

  // Input
  @Input() series_target = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() series_cur = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() series_last = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() series_percent = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() sumYTD: object = {
    target: 0,
    approveCur: 0,
    approveLast: 0,
    percent: 0
  };
  @Input() hideLastYear: boolean = true;
  @Input() activeMonth: number = 0;
  @Input() groupType: string = 'INDIVIDUAL';

  constructor() { }

  ngOnInit(): void {
    this.series_percent = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  }

}
