import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { AuthService } from 'src/app/core/security/auth.service';
import { ModelUser } from 'src/app/core/model/model-user';
import { ModelDashboard } from 'src/app/core/model/model-dashboard';
import { DashboardBackRouteService } from 'src/app/core/service/dashboard-back-route.service';
import { SetFormDataService } from 'src/app/core/service/set-form-data.service';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-detail-controller',
  templateUrl: './detail-controller.component.html',
  styleUrls: ['./detail-controller.component.scss']
})
export class DetailControllerComponent implements OnInit {

  // Utilities
  public currentYear = parseInt(sessionStorage.getItem('calendarCurrentYear'));
  public hideLastYear: boolean = true;
  public filterKpiType: string = 'PC';

  public activeMonthNo: number = 0;

  // Data variables
  public modelUser: ModelUser;
  public model_PC: ModelDashboard;
  public model_PCsubmit: ModelDashboard;
  public model_Active: ModelDashboard;
  public model_RYP: ModelDashboard;
  public model_NC: ModelDashboard;

  public noCaseData: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public caseSizeData: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public ytdCaseData: any = {
    noCase: 0,
    caseSize: 0
  };
  public currentMonthNo: number = 1;

  // GroupType Variables
  public isMtdGroup: boolean = true;
  public filterGroupType: string = 'INDIVIDUAL';
  prstRte:any
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private dataService: DataServiceService,
    private setFormDataService: SetFormDataService,
    private dashboardBackRouteService: DashboardBackRouteService,
    private location: Location,
    private spinner: NgxSpinnerService
  ) { 
    this.modelUser = new ModelUser();
    this.model_PC = new ModelDashboard();
    this.model_PCsubmit = new ModelDashboard();
    this.model_Active = new ModelDashboard();
    this.model_RYP = new ModelDashboard();
    this.model_NC = new ModelDashboard();
  }

  ngOnInit(): void {
    // Get KPI filter type from AgentOverview by params URL. 
    this.route.queryParams.subscribe(params => {
      let getParam = JSON.parse(params.params);
      this.filterKpiType = getParam[0]['kpiType'];
    });
    this.modelUser.setAgentCode(sessionStorage.getItem('agentCodeSelected'));
    // this.modelUser.setAgentLevel('AGENT');
    this.modelUser.setAgentLevel(sessionStorage.getItem('agentLevelSelected'));

    this.apiGetClosYm();
    this.getAllData();

    this.prstRte = sessionStorage.getItem('prstRte')
  }

  async getAllData() {
    this.spinner.show();
    this.callAPI_getAgentStructure();
    this.callAPI_getAgentYtdPc();
    this.callAPI_getAgentYtdActive();
    this.callAPI_getAgentYtdRyp();

    if (this.modelUser.getAgentLevel() !== 'AGENT') {
      this.callAPI_getAgentYtdNc();
    }
    const resPerformance = await this.callAPI_getAgentPerformance();
    const resTarget = await this.callAPI_getAgentTarget();
    // console.log("resPerformance", resPerformance);
    
    if (Object.keys(resPerformance).length > 0 && Object.keys(resTarget).length > 0) {

      // Get ActiveMonth from lastMonth Data
      let closYmLastData = resPerformance[Object.keys(resPerformance).length - 1];
      let activeMonth = parseInt(closYmLastData.closYm.substring(4, 6));
      this.activeMonthNo = activeMonth - 1;

      this.model_PC.setSeriesPercent(
        this.setFormDataService.cal_kpiPercent(
          this.model_PC.getSeriesCurrent(), this.model_PC.getSeriesTarget())
      );
      this.model_Active.setSeriesPercent(
        this.setFormDataService.cal_kpiPercent(
          this.model_Active.getSeriesCurrent(), this.model_Active.getSeriesTarget())
      );
      this.model_RYP.setSeriesPercent(
        this.setFormDataService.cal_kpiPercent(
          this.model_RYP.getSeriesCurrent(), this.model_RYP.getSeriesTarget())
      );

      if (this.modelUser.getAgentLevel() !== 'AGENT') {
        this.model_NC.setSeriesPercent(
          this.setFormDataService.cal_kpiPercent(
            this.model_NC.getSeriesCurrent(), this.model_NC.getSeriesTarget())
        );
      }
      this.spinner.hide();
    }
  }

  // Calling API Service methods
  async callAPI_getAgentPerformance() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    let dataRes;
    dataRes = await this.dataService.getAgentPerformance(data).toPromise();
    if (Object.keys(dataRes).length > 0) {
      // console.log('Set Performance: ', dataRes);
      this.setPerformance(dataRes);
    }
    return dataRes;
  }
  async callAPI_getAgentTarget() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    let dataRes;
    dataRes = await this.dataService.getAgentTarget(data).toPromise()
    if (dataRes && Object.keys(dataRes).length > 0) {
      this.model_PC.setSeriesTarget(
        this.setFormDataService.transform_kpiTargetSeries(dataRes, 'PC'));
      this.model_Active.setSeriesTarget(
        this.setFormDataService.transform_kpiTargetSeries(dataRes, 'NEW_ACTIVE_AGENT'));
      this.model_RYP.setSeriesTarget(
        this.setFormDataService.transform_kpiTargetSeries(dataRes, 'RYP'));

      if (this.modelUser.getAgentLevel() !== 'AGENT') {
        this.model_NC.setSeriesTarget(
          this.setFormDataService.transform_kpiTargetSeries(dataRes, 'NEW_CODE'));
      }
    } else {
      if (this.modelUser.getAgentLevel() === 'AGENT') {
        let series_target = [1,1,1,1,1,1,1,1,1,1,1,1];
        this.model_Active.setSeriesTarget(series_target);
      }
    }
    return dataRes;
  }
  callAPI_getAgentYtdPc() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    this.dataService.getAgentYtdPc(data).subscribe(
      response => {
        // console.log('res. getAgentYtdPc: ', response);
        if (Object.keys(response).length > 0) {
          this.model_PC.setSumYtdValue(
            this.setFormDataService.transform_kpiYtd_forAGENT(response, 'PC')
          );
        }
      },
      error => {
        console.log('err. getAgentYtdPc: ', error);
      }
    );
  }
  callAPI_getAgentYtdActive() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    this.dataService.getAgentYtdActive(data).subscribe(
      response => {
        // console.log('res. getAgentYtdActive: ', response);
        if (Object.keys(response).length > 0) {
          this.model_Active.setSumYtdValue(
            this.setFormDataService.transform_kpiYtd_forAGENT(response, 'ACTIVE')
          );
        }
      },
      error => {
        console.log('err. getAgentYtdActive: ', error);
      }
    );
  }
  callAPI_getAgentYtdRyp() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    this.dataService.getAgentYtdRyp(data).subscribe(
      response => {
        // console.log('res. getAgentYtdRyp: ', response);
        if (Object.keys(response).length > 0) {
          this.model_RYP.setSumYtdValue(
            this.setFormDataService.transform_kpiYtd_forAGENT(response, 'RYP')
          );
        }
      },
      error => {
        console.log('err. getAgentYtdRyp: ', error);
      }
    );
  }
  callAPI_getAgentYtdNc() {
    let data = {
      "agentCode": this.modelUser.getAgentCode(),
      "agentLevel": this.modelUser.getAgentLevel()
    }
    this.dataService.getAgentYtdNc(data).subscribe(
      response => {
        // console.log('res. getAgentYtdNc: ', response);
        if (Object.keys(response).length > 0) {
          this.model_NC.setSumYtdValue(
            this.setFormDataService.transform_kpiYtd_forAGENT(response, 'NC')
          );
        }
      },
      error => {
        console.log('err. getAgentYtdNc: ', error);
      }
    );
  }
  callAPI_getAgentStructure() {
    let data = {
      "agentCode": this.modelUser.getAgentCode()
    };
    this.dataService.getAgentStructure(data).subscribe(
      response => {
        // console.log('res. getAgentStructure: ', response);
        if (Object.keys(response).length > 0) {
          let agentName = response[0]['thaiName'] + ' ' + response[0]['thaiSurname'];
          this.modelUser.setAgentName(agentName);
          this.modelUser.setAgentLevel('AGENT');
          if (response[0]['agentLevel'] !== 'AGENT' && sessionStorage.getItem('agentLevelSelected') === 'AGENT') {
            // this.modelUser.setLevelName(response[0]['levelName'] + ' (ผลงานส่วนตัว)');
            if(response[0]['levelName'] == 'BK') {
              this.modelUser.setLevelName('AL/BROKER' + ' (ผลงานส่วนตัว)');
            } else {
              this.modelUser.setLevelName(response[0]['levelName'] + ' (ผลงานส่วนตัว)');
            }
          } else {
            this.modelUser.setLevelName(response[0]['levelName']);
            this.modelUser.setAgentLevel(response[0]['agentLevel']);
            if(response[0]['levelName'] == 'BK') {
              this.modelUser.setLevelName('AL/BROKER');
            } else {
              this.modelUser.setLevelName(response[0]['levelName']);
            }
          }
        }
      },
      error => {
        console.log('getAgentStructure: ', error);
      }
    );
  }
  apiGetClosYm() {
    this.dataService.getClosYmInfo().subscribe(
      response => {
        if (response && Object.keys(response).length > 0) {
          let closYm = Object.values(response)[0];
          this.currentMonthNo = parseInt(closYm.substring(4, 6));
        }
      },
      error => {
        console.log('getClosYmInfo', error);
      }
    );
  }

  // Controller methods
  handleFilterSelect(kpiSelect: string) {
    if (this.filterKpiType != kpiSelect) {
      this.filterKpiType = kpiSelect;
    }
  }
  goToHome() {
    let userRole = this.authService.getPosition();
    let agentCode = this.authService.getLoginedName();
    sessionStorage.removeItem('agentCodeStored');
    sessionStorage.removeItem('agentLevelStored');
    this.dashboardBackRouteService.backHome(userRole, agentCode);
  }
  goToOverview() {
    this.location.back();
  }
  compareYear() {
    this.hideLastYear = !this.hideLastYear;
  }

  // WholeGroup Performance
  async handleGroupType(groupType: string) {
    if (this.filterGroupType != groupType) {
      this.filterGroupType = groupType;
      this.isMtdGroup = !this.isMtdGroup;

      if (groupType == 'INDIVIDUAL') {
        this.getAllData();
      } 
      else if (groupType == 'GROUP') {
        this.spinner.show();
        var _postData = {
          "agentCode": this.modelUser.getAgentCode(),
          "agentLevel": this.modelUser.getAgentLevel()
        };
        const _resWholeGroupMtd = await this.callAPI_wholeGroupMtd(_postData);
        const _resWholeGroupYtd = await this.callAPI_wholeGroupYtd(_postData);
        // console.log("_resWholeGroupMtd", _resWholeGroupMtd);
        
        if (Object.keys(_resWholeGroupMtd).length > 0 && Object.keys(_resWholeGroupYtd).length > 0) {
          
          // SET Percentage
          this.model_PC.setSeriesPercent(
            this.setFormDataService.cal_GrowthPercent(
              this.model_PC.getSeriesCurrent(), this.model_PC.getSeriesLast())
          );
          this.model_Active.setSeriesPercent(
            this.setFormDataService.cal_GrowthPercent(
              this.model_Active.getSeriesCurrent(), this.model_Active.getSeriesLast())
          );
          this.model_RYP.setSeriesPercent(
            this.setFormDataService.cal_GrowthPercent(
              this.model_RYP.getSeriesCurrent(), this.model_RYP.getSeriesLast())
          );
          if (this.modelUser.getAgentLevel() !== 'AGENT') {
            this.model_NC.setSeriesPercent(
              this.setFormDataService.cal_GrowthPercent(
                this.model_NC.getSeriesCurrent(), this.model_NC.getSeriesLast())
            );
          }
          this.spinner.hide();
        } else {
          // let emptyData = [null, null, null, null, null, null, null, null, null, null, null, null];
          // this.model_PC.setSeriesPercent(emptyData);
          // this.model_Active.setSeriesPercent(emptyData);
          // this.model_RYP.setSeriesPercent(emptyData);
          // this.model_NC.setSeriesPercent(emptyData);
          // this.spinner.hide();
          this.getAllData();
        }
        
        // Remove Target
        let series_target = [null, null, null, null, null, null, null, null, null, null, null, null];
        this.model_PC.setSeriesTarget(series_target);
        this.model_Active.setSeriesTarget(series_target);
        this.model_RYP.setSeriesTarget(series_target);
        if (this.modelUser.getAgentLevel() !== 'AGENT') {
          this.model_NC.setSeriesTarget(series_target);
        }
      }
    }
  }
  async callAPI_wholeGroupMtd(_postData: any) {
    var _dataRes = await this.dataService.postKpiDetailWholeGroupMtd(_postData).toPromise();
    if (_dataRes && Object.keys(_dataRes).length > 0) {
      // console.log('RES MTD: ', _dataRes);
      this.setPerformance(_dataRes);
    } else {
      const emptyList = [null, null, null, null, null, null, null, null, null, null, null, null];
      this.model_PC.setCurrentAndLast(emptyList, emptyList);
      this.model_PCsubmit.setCurrentAndLast(emptyList, emptyList);
      this.model_Active.setCurrentAndLast(emptyList, emptyList);
      this.model_RYP.setCurrentAndLast(emptyList, emptyList);
      this.model_NC.setCurrentAndLast(emptyList, emptyList);
      this.noCaseData = emptyList;
      this.caseSizeData = emptyList;
      this.ytdCaseData = {
        'noCase': null,
        'caseSize': null
      };
    }
    return _dataRes;
  }
  async callAPI_wholeGroupYtd(_postData: any) {
    var _dataRes = await this.dataService.postKpiDetailWholeGroupYtd(_postData).toPromise();
    if (_dataRes && Object.keys(_dataRes).length > 0) {
      // console.log('RES YTD: ', _dataRes);
      this.model_PC.setSumYtdValue(
        this.setFormDataService.transform_kpiYtd_forAGENT(_dataRes, 'PC', true)
      );
      this.model_Active.setSumYtdValue(
        this.setFormDataService.transform_kpiYtd_forAGENT(_dataRes, 'ACTIVE', true)
      );
      this.model_RYP.setSumYtdValue(
        this.setFormDataService.transform_kpiYtd_forAGENT(_dataRes, 'RYP', true)
      );
      this.model_NC.setSumYtdValue(
        this.setFormDataService.transform_kpiYtd_forAGENT(_dataRes, 'NC', true)
      );

      let indexRes = Object.values(_dataRes);
      let indexCurrentYear = indexRes.findIndex(x => x.closYm === this.currentYear.toString());
      this.ytdCaseData = {
        'noCase': _dataRes[indexCurrentYear]['noCase'],
        'caseSize': _dataRes[indexCurrentYear]['caseSize']
      };

      if (this.currentMonthNo > 0 && this.currentMonthNo <= 12) {
        let tempSumPcSubmit = this.model_PC.getSumYtdValue();
        this.ytdCaseData['caseSize'] = tempSumPcSubmit['approveCur'] / this.ytdCaseData['noCase'];
      }

    } else {
      this.model_PC.setSumYtdValue(0);
      this.model_Active.setSumYtdValue(0);
      this.model_RYP.setSumYtdValue(0);
      this.model_NC.setSumYtdValue(0);
    }
    return _dataRes;
  }

  // Utilities
  setPerformance(dataRes) {

    let result = this.setFormDataService.transform_kpiChartSeries(dataRes, 'pcApprove');
    this.model_PC.setCurrentAndLast(result.current, result.last);
    var sumCurrentApprove = result.current.reduce((a, b) => a + (b || 0), 0);

    result = this.setFormDataService.transform_kpiChartSeries(dataRes, 'pcSubmit');
    this.model_PCsubmit.setCurrentAndLast(result.current, result.last);

    if (this.modelUser.getAgentLevel() === 'AGENT') {
      result = this.setFormDataService.transform_kpiChartSeries_null(dataRes, 'active');
      this.model_Active.setCurrentAndLast(result.current, result.last);
    } else {
      result = this.setFormDataService.transform_kpiChartSeries(dataRes, 'active');
      this.model_Active.setCurrentAndLast(result.current, result.last);
    }

    result = this.setFormDataService.transform_kpiChartSeries(dataRes, 'ryp');
    this.model_RYP.setCurrentAndLast(result.current, result.last);

    if (this.modelUser.getAgentLevel() !== 'AGENT') {
      result = this.setFormDataService.transform_kpiChartSeries(dataRes, 'nc');
      this.model_NC.setCurrentAndLast(result.current, result.last);
    }

    result = this.setFormDataService.transform_kpiChartSeries(dataRes, 'noCase');
    this.noCaseData = result.current;
    result = this.setFormDataService.transform_kpiChartSeries(dataRes, 'caseSize');
    this.caseSizeData = result.current;

    this.ytdCaseData = {
      'noCase': this.noCaseData.reduce((a, b) => a + b, 0),
      'caseSize': this.caseSizeData.reduce((a, b) => a + b, 0)
    };

    if (this.currentMonthNo > 0 && this.currentMonthNo <= 12) {
      this.ytdCaseData['caseSize'] = sumCurrentApprove / this.ytdCaseData['noCase'];
    }
  }

  // Back from browser
  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    this.dashboardBackRouteService.onClickBack();
  }
}
