import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiNcComponent } from './kpi-nc.component';

describe('KpiNcComponent', () => {
  let component: KpiNcComponent;
  let fixture: ComponentFixture<KpiNcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiNcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiNcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
