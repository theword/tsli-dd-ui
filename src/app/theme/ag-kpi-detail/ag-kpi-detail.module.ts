import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailControllerComponent } from './detail-controller/detail-controller.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../core/shared.module';
import { KpiPcComponent } from './kpi-pc/kpi-pc.component';
import { KpiActComponent } from './kpi-act/kpi-act.component';
import { KpiRypComponent } from './kpi-ryp/kpi-ryp.component';
import { KpiNcComponent } from './kpi-nc/kpi-nc.component';

const routes: Routes = [
  {
    path: 'main',
    component: DetailControllerComponent,
  },
  { path: '**', redirectTo: 'error', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    DetailControllerComponent,
    KpiPcComponent,
    KpiActComponent,
    KpiRypComponent,
    KpiNcComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule
  ]
})
export class AgKpiDetailModule { }
