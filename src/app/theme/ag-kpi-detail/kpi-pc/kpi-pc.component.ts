import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-kpi-pc',
  templateUrl: './kpi-pc.component.html',
  styleUrls: ['./kpi-pc.component.scss']
})
export class KpiPcComponent implements OnInit {

  // Data variables
  // currentYear = new Date().getFullYear();
  // currentMonth = new Date().getMonth();
  chartZoom: boolean = true;
  
  // Input
  @Input() series_target_pc = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() series_cur_pcSubmit = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() series_cur_pc = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() series_last_pc = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() series_percent_pc = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  @Input() sumYTD: object = {
    target: 0,
    submit: 0,
    approveCur: 0,
    approveLast: 0,
    percent: 0
  };
  @Input() hideLastYear: boolean = true;

  @Input() activeMonth: number = 0;
  @Input() groupType: string = 'INDIVIDUAL';

  @Input() noCaseData: any = [];
  @Input() caseSizeData: any = [];
  @Input() ytdData: any;

  constructor() { }

  ngOnInit(): void {
  }

}
