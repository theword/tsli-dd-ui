import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiPcComponent } from './kpi-pc.component';

describe('KpiPcComponent', () => {
  let component: KpiPcComponent;
  let fixture: ComponentFixture<KpiPcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiPcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiPcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
