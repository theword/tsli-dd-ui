import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompensationControllerComponent } from './compensation-controller/compensation-controller.component';
import { CompensationFilterComponent } from './components/compensation-filter/compensation-filter.component';
import { ValidationComponent } from './components/validation/validation.component';
import { PromoteComponent } from './components/promote/promote.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/core/shared.module';
import { TableCompensationComponent } from './components/table-compensation/table-compensation.component';

const routes: Routes = [
  {
    path: '',
    component: CompensationControllerComponent
  },
  {
    path: '**', redirectTo: 'error', pathMatch: 'full'
  }
]

@NgModule({
  declarations: [
    CompensationControllerComponent, 
    CompensationFilterComponent, 
    ValidationComponent, 
    PromoteComponent,  
    TableCompensationComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule
  ]
})
export class CompensationModule { }
