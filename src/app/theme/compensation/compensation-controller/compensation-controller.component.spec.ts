import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompensationControllerComponent } from './compensation-controller.component';

describe('CompensationControllerComponent', () => {
  let component: CompensationControllerComponent;
  let fixture: ComponentFixture<CompensationControllerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompensationControllerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompensationControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
