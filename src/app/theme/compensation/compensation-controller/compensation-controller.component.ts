import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/core/security/auth.service';
import { DashboardBackRouteService } from 'src/app/core/service/dashboard-back-route.service';
import { Location } from '@angular/common';
import { DataServiceService } from 'src/app/core/service/data-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-compensation-controller',
  templateUrl: './compensation-controller.component.html',
  styleUrls: ['./compensation-controller.component.scss'],
})
export class CompensationControllerComponent implements OnInit {

  // V.agentInfomation
  public agentCode: string = 'EMPTY CODE';
  public agentName: string = 'EMPTY NAME';
  public agentLevel: string = 'NA';
  public groupChannel: string = '';
  public titleCode: string = '';
  public displayLvName: any = '';
  public positionCode: string = '';

  filterCompensation: string;
  filterPeriodType: string

  listFilterCompensation: any[];
  listFilterPeriodType: any[];
  listFilterPeriodTypeName: any[]

  loadingStatus: boolean = true;

  // V.utilities
  public closYm: string = '';

  tableHeader: any[];

  compensationConfig: any = [];
  compensationConfigV: any = []
  compensationConfigH: any = []

  mapping: any = [];
  careerPc: any;
  careerNoCase: any;

  validationData: any;
  validationDataV: any;
  validationDataH: any;

  blockName: any;
  blockNameV: any;
  blockNameH: any;

  compensationBlockData: any;
  validationBlockData: any;

  promoteBlockData: any;
  promoteBlockDataV: any;
  promoteBlockDataH: any;

  ovHighlightIndex: any
  pcMtd: any;
  pcQtd: any;
  pcYtd: any;
  currentPosition: any;
  nextPosition: any;
  activeProducerMtd: any;
  remark: any;

  period: any;
  periodH: any;
  periodV: any;
  
  isVetical = true ;

  prstRte:any
  
  closYmTH:any

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private dataService: DataServiceService,
    private dashboardBackRouteService: DashboardBackRouteService,
    private location: Location,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) {
    this.closYmTH = sessionStorage.getItem('calendarCurrentMonthTH')

  }

  ngOnInit(): void {
    // Get KPI filter type from AgentOverview by params URL.
    this.route.queryParams.subscribe((params) => {
      let getParam = JSON.parse(params.params);
      if (getParam[0]['lvName'] !== 'ZONE') {
        this.displayLvName = getParam[0]['lvName'];
      }
    });

    var tempAgentCode = sessionStorage.getItem('agentCodeSelected');
    var tempAgentLevel = sessionStorage.getItem('agentLevelSelected');
    this.agentCode = tempAgentCode;
    this.agentLevel = tempAgentLevel;
    this.titleCode = tempAgentCode;

    this.selectFilter();
    this.apiPrepareData();
    this.setTableHeader(this.filterCompensation);

    this.prstRte = sessionStorage.getItem('prstRte')
    
    // console.log("closYmTH", this.closYmTH);
     
  }

  // Calling API Service methods
  async apiPrepareData() {
    this.spinner.show();
    await this.apiGetAgentPersonalInfo();
    await this.apiGetClosYm();
    // ***** //
    // Don't forget to check channel and level that have those function to use this value
    switch (this.displayLvName) {
      case 'BM':
      case 'FM':
      case 'CM':
        await this.apiGetValidationConfig();
        break;

      case 'AL':
        await this.apiGetOvConfig();
        break;
        
      default:
        await this.apiGetCompensationConfigWithPc();
        break;
    }
    
    this.spinner.hide();
  }
  async apiGetAgentPersonalInfo() {
    let _postData = {
      agentCode: this.agentCode,
    };
    let _dataRes = await this.dataService.getAgentStructure(_postData).toPromise();
    if (_dataRes && Object.keys(_dataRes).length > 0) {
      // console.log('AGENT DETAIL: ', _dataRes);
      this.agentName = _dataRes[0].thaiName + ' ' + _dataRes[0].thaiSurname;
      this.agentLevel = _dataRes[0].agentLevel;
      this.groupChannel = _dataRes[0].groupChannel;
      this.positionCode = _dataRes[0].positionCode;
    }
  }
  async apiGetClosYm() {
    let _dataRes = await this.dataService.getClosYmInfo().toPromise();
    if (_dataRes && Object.keys(_dataRes).length > 0) {
      this.closYm = Object.values(_dataRes)[0];
    }
  }
  // USE FOR CAREER BENEFIT (CONFIG ONLY)
  async apiGetCompensationConfig() {
    this.spinner.show();
    const _postData = {
      'closYm': this.closYm,
      'channelCode': this.groupChannel,
      'agentLevel': this.agentLevel,
      'filter': this.filterCompensation,
    };
    var _dataRes = await this.dataService.postCompensationConfig(_postData).toPromise();
    if (_dataRes && Object.keys(_dataRes).length > 0) {
      // console.log('RES COMPEN CONFIG: ', _dataRes);
      this.compensationConfig = _dataRes
      this.spinner.hide();
    }
  }
  async apiGetValidationConfig() {
    this.spinner.show();
    var positionCode =  null;
    if (this.groupChannel == 'SFC') {
      positionCode = this.positionCode;
    } 
    const _postData = {
      'closYm': this.closYm,
      'channelCode': this.groupChannel,
      'agentCode': this.agentCode,
      'agentLevel': this.agentLevel,
      'positionCode': positionCode,
    };
    // console.log('POST VALIDATION: ', _postData);
    var _dataRes = await this.dataService.postValidationConfig(_postData).toPromise();
    if (_dataRes && Object.keys(_dataRes).length > 0) {
      // console.log('RES VALIDATION CONFIG: ', _dataRes);
      if(this.displayLvName == 'BM'){
        this.compensationConfig = _dataRes['config'];
        this.validationData = _dataRes['data'];
        this.validationBlockData =[ _dataRes['config']['positionCode']]
        this.mapping = _dataRes['mapping']
      } else {
        this.compensationConfig = _dataRes['config'];
        this.validationData = _dataRes['data'];
        this.validationBlockData = null
        this.mapping = _dataRes['mapping']
      }
      // this.compensationConfig = {
      //   'periodStart': '202005',
      //   'periodEnd': '202010'
      // }

      if(this.compensationConfig){
        if(this.groupChannel == "SFC"){
          var setPeriod = this.setperiod6SFC(this.compensationConfig.periodStart, this.compensationConfig.periodEnd)
          this.period = `รอบการประเมิน : ${setPeriod.monthStart} ${setPeriod.yearStart} – ${setPeriod.monthEnd} ${setPeriod.yearEnd} (${setPeriod.duration} เดือน)`  

        } else {
          var setPeriod = this.setperiod6(this.compensationConfig.periodStart, this.compensationConfig.periodEnd)
          this.period = `รอบการประเมิน : ${setPeriod.monthStart} ${setPeriod.yearStart} – ${setPeriod.monthEnd} ${setPeriod.yearEnd} (${setPeriod.duration} เดือน)`  
        }
      }
      
      this.spinner.hide();
    }
  }
  async apiGetPromoteConfig() {
    this.spinner.show(); 
    const _postData = {
      'closYm': this.closYm,
      'channelCode': this.groupChannel,
      'agentCode': this.agentCode,
      'agentLevel': this.agentLevel,
      'periodType': this.filterPeriodType,
      'positionCode': this.positionCode,
    };
    console.log('POST PROMOTE: ', _postData);
    var _dataRes = await this.dataService.postPromoteConfig(_postData).toPromise();
    if (_dataRes && Object.keys(_dataRes).length > 0) {
      console.log('RES PROMOTE CONFIG: ', _dataRes);
      // this.compensationConfig = _dataRes['config'][0]
      if(_dataRes['config']){
        this.compensationConfig = this.compensationConfig = _dataRes['config'][0]
      } else {
        this.compensationConfig = null
      }
        
      if(this.displayLvName == 'AL'){
        this.compensationConfigV = {..._dataRes['config'].find(item => item.promoteType == 'V')}
        this.compensationConfigH = {..._dataRes['config'].find(item => item.promoteType == 'H')}
        
        console.log("compensationConfigV", this.compensationConfigV);
        console.log("compensationConfigH", this.compensationConfigH);
        
        this.validationDataV = {..._dataRes['data'].find(item => item.promoteType == 'V')}
        this.validationDataH = {..._dataRes['data'].find(item => item.promoteType == 'H')}
        
        // console.log("validationDataV", this.validationDataV);
        // console.log("validationDataH", this.validationDataH);
        // console.log("validationDataH", Object.keys(this.validationDataH).length);
        
        this.promoteBlockDataV = [`${this.validationDataV.nextPositionCode}`]
        
        // console.log("promoteBlockDataH", this.promoteBlockDataH);

        if(this.validationDataH && Object.keys(this.validationDataH).length > 0){
          this.promoteBlockDataH = [
            `${this.validationDataH.agentLevel} : ${this.validationDataH.positionCode}`, 
            `${this.validationDataH.agentLevel} : ${this.validationDataH.nextPositionCode}`
          ]
        } else {
          this.promoteBlockDataH = [
            "", ""
          ]
        }
        
        this.mapping = _dataRes['mapping']

        if(this.compensationConfigH && Object.keys(this.compensationConfigH).length > 0){
          var setPeriod = this.setperiod12(this.compensationConfigH.periodStart, this.compensationConfigH.periodEnd)
          this.periodH = `นับผลงาน : ${setPeriod.monthStart} ${setPeriod.yearStart} – ${setPeriod.monthEnd} ${setPeriod.yearEnd} (${setPeriod.duration} เดือน)`
          console.log("compensationConfigH period", this.period);
        }

        if(this.compensationConfigV && Object.keys(this.compensationConfigV).length > 0){
          var setPeriod = this.setperiod12(this.compensationConfigV.periodStart, this.compensationConfigV.periodEnd)
          this.periodV = `นับผลงาน : ${setPeriod.monthStart} ${setPeriod.yearStart} – ${setPeriod.monthEnd} ${setPeriod.yearEnd} (${setPeriod.duration} เดือน)`
          console.log("compensationConfigV period", this.period);
        }
      } 
      else if (this.displayLvName == 'AG') {
        this.validationData = {..._dataRes['data'][0]}
        this.promoteBlockData = [`${this.validationData.nextAgentLevel} : ${this.validationData.nextPositionCode}`]
        this.mapping = _dataRes['mapping']

        if(this.compensationConfig){
          var setPeriod = this.setperiod12(this.compensationConfig.periodStart, this.compensationConfig.periodEnd)
          this.period = `นับผลงาน : ${setPeriod.monthStart} ${setPeriod.yearStart} – ${setPeriod.monthEnd} ${setPeriod.yearEnd} (${setPeriod.duration} เดือน)`
        }
      } 
      else if (this.displayLvName == 'FC') {
        this.validationData = {..._dataRes['data'][0]}
        this.promoteBlockData = [this.validationData.nextPositionCode]
        this.mapping = _dataRes['mapping']

        if(this.compensationConfig){
          var setPeriod = this.setperiod12(this.compensationConfig.periodStart, this.compensationConfig.periodEnd)
          this.period = `นับผลงาน : ${setPeriod.monthStart} ${setPeriod.yearStart} – ${setPeriod.monthEnd} ${setPeriod.yearEnd} (${setPeriod.duration} เดือน)`
        }
      } 
      else {
        this.validationData = {..._dataRes['data'][0]}
        this.promoteBlockData = [this.validationData.positionCode, this.validationData.nextPositionCode]
        this.mapping = _dataRes['mapping']

        if(this.compensationConfig){
          var setPeriod = this.setperiod12(this.compensationConfig.periodStart, this.compensationConfig.periodEnd)
          this.period = `นับผลงาน : ${setPeriod.monthStart} ${setPeriod.yearStart} – ${setPeriod.monthEnd} ${setPeriod.yearEnd} (${setPeriod.duration} เดือน)`
        }
      }
      
      this.spinner.hide();
    }
  }
  async apiGetOvConfig() {
    this.spinner.show();
    const _postData = {
      'closYm': this.closYm,
      'channelCode': this.groupChannel,
      'agentCode': this.agentCode,
      'agentLevel': this.agentLevel,
      'filter': this.filterCompensation,
    };
    var _dataRes = await this.dataService.postCompensationOvConfig(_postData).toPromise();
    if (_dataRes && Object.keys(_dataRes).length > 0) {
      // console.log('RES COMPEN - OV - CONFIG: ', _dataRes);
      this.compensationConfig = _dataRes['config']
      this.compensationBlockData = [_dataRes['pcApproved'], _dataRes['active']]
      for(let i=0; i < this.compensationConfig.length; i++){
        if(_dataRes['active'] >= this.compensationConfig[i].activeProducer){
          this.ovHighlightIndex = i
        }
      }
      this.spinner.hide();
    }
  }
  // USE FOR MONTHLY, QUARTERLY AND YEARLY BONUS (CONFIG WITH DATA)
  async apiGetCompensationConfigWithPc() {
    this.spinner.show()
    const _postData = {
      'closYm': this.closYm,
      'channelCode': this.groupChannel,
      'agentLevel': this.agentLevel,
      'filter': this.filterCompensation,
      'agentCode': this.agentCode
    };
    console.log("apiGetCompensationConfigWithPc", _postData);
    var _dataRes = await this.dataService.postCompensationConfigWithPc(_postData).toPromise();
    if (_dataRes && Object.keys(_dataRes).length > 0) {
      console.log('RES COMPEN CONFIG w PC: ', _dataRes);
      if(this.filterCompensation == 'โบนัสตัวแทนอาชีพ'){
        this.compensationConfig = _dataRes['config'][0]
        this.careerPc = _dataRes['pcApproved']
        this.careerNoCase = _dataRes['noCase']
      } else {
        this.compensationConfig = _dataRes['config']
        this.compensationBlockData = [_dataRes['pcApproved']]
      }
      
      this.spinner.hide();
    }
  }

  // Route Controller
  goToHome() {
    let userRole = this.authService.getPosition();
    let agentCode = this.authService.getLoginedName();
    sessionStorage.removeItem('agentCodeStored');
    sessionStorage.removeItem('agentLevelStored');
    this.dashboardBackRouteService.backHome(userRole, agentCode);
  }
  goToOverview() {
    this.location.back();
  }
  handleFilterCompensation(event) {
    if(this.filterCompensation != event){
      this.filterCompensation = event;
      this.setTableHeader(event)
      console.log('Compensation Filter', this.filterCompensation);
      this.resetComponentValue();

      switch (event.toString()) {
        case 'เงินเดือนผันแปร':
        case 'โบนัสไตรมาส':
        case 'โบนัสรายปี':
        case 'โบนัสตัวแทนอาชีพ':
        case 'โบนัสรายเดือน':
          this.apiGetCompensationConfigWithPc();
          break;
        
        case 'รักษาตำแหน่ง':
          this.apiGetValidationConfig();
          break;
        
        case 'เลื่อนตำแหน่ง':
          this.apiGetPromoteConfig();
          break;

        case 'OV':
          this.apiGetOvConfig();
          break;

        default:
          break;
      }
    }
  }
  handleFilterPeriodType(event){
    if(this.filterPeriodType != event){
      this.filterPeriodType = event;
      this.apiGetPromoteConfig();
    }
  }
  isCompensation(filterCompensation){
    if(
      filterCompensation != 'รักษาตำแหน่ง' &&
      filterCompensation != 'เลื่อนตำแหน่ง' &&
      filterCompensation != 'โบนัสตัวแทนอาชีพ'
    ){
      return true
    } else {
      return false
    }
  }
  resetComponentValue(){
    this.compensationConfig = []
    this.careerPc = ""
    this.careerNoCase = ""
    this.period = ""
  }

  // Utilities
  closYm2Month(closYm) {
    switch (closYm) {
      case 0:
        return 'ธ.ค.'
      case 1:
        return 'ม.ค.'
      case 2:
        return 'ก.พ.'
      case 3:
        return 'มี.ค.'
      case 4:
        return 'เม.ย.'
      case 5:
        return 'พ.ค.'
      case 6:
        return 'มิ.ย.'
      case 7:
        return 'ก.ค.'
      case 8:
        return 'ส.ค.'
      case 9:
        return 'ก.ย.'
      case 10:
        return 'ต.ค.'
      case 11:
        return 'พ.ย.'
      case 12:
        return 'ธ.ค.'
    }
  }
  cd2bd(closYm) {
    return parseInt(closYm.toString().substring(0, 4)) + 543
  }
  parseMonth(str) {
    return str.match(/(\d{4})(\d{2})/).splice(1).map(Number)
  }

  setperiod6SFC(periodStart, periodEnd) {
    var [yStart, mStart] = this.parseMonth(periodStart)
    var [yEnd, mEnd] = this.parseMonth(periodEnd)

    var duration = Math.abs((yStart - yEnd) * 12 + (mStart - mEnd)) + 1

    let monthStart = this.closYm2Month(mStart)
    let yearStart = yStart + 543
    
    if(mEnd == 0){
      yEnd = yEnd - 1
    }

    let monthEnd = this.closYm2Month(mEnd)
    let yearEnd = yEnd + 543

    return { monthStart, yearStart, monthEnd, yearEnd, duration }
  }

  setperiod6(periodStart, periodEnd) {
    var [yStart, mStart] = this.parseMonth(periodStart)
    var [yEnd, mEnd] = this.parseMonth(periodEnd)

    var duration = Math.abs((yStart - yEnd) * 12 + (mStart - mEnd)) + 1

    let monthStart = this.closYm2Month(mStart)
    let yearStart = yStart + 543
    // mEnd = mEnd - 1
    
    if(mEnd == 0){
      yEnd = yEnd - 1
    }

    let monthEnd = this.closYm2Month(mEnd)
    let yearEnd = yEnd + 543

    return { monthStart, yearStart, monthEnd, yearEnd, duration }
  }

  setperiod12(periodStart, periodEnd) {
    var [yStart, mStart] = this.parseMonth(periodStart)
    var [yEnd, mEnd] = this.parseMonth(periodEnd)

    var duration = Math.abs((yStart - yEnd) * 12 + (mStart - mEnd)) + 1

    let monthStart = this.closYm2Month(mStart)
    let yearStart = yStart + 543

    let monthEnd = this.closYm2Month(mEnd)
    let yearEnd = yEnd + 543

    return { monthStart, yearStart, monthEnd, yearEnd, duration }
  }

  async setTableHeader(filterCompensation) {
    
    const YM = await this.dataService.getClosYmInfo().toPromise();
    const YEAR = this.closYear(YM['closYm']);
    const QUARTER = this.quarter(YM['closYm']);
    const MONTH = this.closYm2Month(+this.closMonth(YM['closYm']));

    if (filterCompensation == 'เงินเดือนผันแปร') {
      this.tableHeader = ['อัตราเงินเดือนผันแปร', 'PC ที่ต้องทำ', 'PC ที่ต้องทำเพิ่ม'];
      this.blockName = [`PC สะสมประจำเดือน ${MONTH} ${YEAR} `]
      this.remark = [
        'รายงานนี้เป็นการคาดการณ์การจ่ายผลประโยชน์ / ประเมินรักษาตำแหน่ง / เลื่อนตำแหน่งเท่านั้น อาจมีการเปลี่ยนแปลงได้เมื่อระบบคำนวณตอนสิ้นเดือน',
        'การคำนวณการจ่าย จะนำผลงาน PC x อัตราเงินเดือนผันแปร x Persistency Credit'
      ]
    }
    // Bonus Quarter
    else if (filterCompensation == 'โบนัสไตรมาส') {
      this.tableHeader = ['อัตราโบนัสไตรมาส', 'PC ที่ต้องทำ', 'PC  ที่ต้องทำเพิ่ม'];
      this.blockName = [`PC สะสมประจำไตรมาส ${QUARTER}` ] 
      if (this.displayLvName == 'FC') {
        this.remark = [
          'รายงานนี้เป็นการคาดการณ์การจ่ายผลประโยชน์ / ประเมินรักษาตำแหน่ง / เลื่อนตำแหน่งเท่านั้น อาจมีการเปลี่ยนแปลงได้เมื่อระบบคำนวณตอนสิ้นเดือน',
          'การคำนวณการจ่าย จะนำผลงาน (PC x อัตราโบนัสไตรมาส x Persistency Credit) - โบนัสไตรมาสที่จ่ายไปแล้ว'
        ]
      }
      else {
        this.remark = [
          'รายงานนี้เป็นการคาดการณ์การจ่ายผลประโยชน์ / ประเมินรักษาตำแหน่ง / เลื่อนตำแหน่งเท่านั้น อาจมีการเปลี่ยนแปลงได้เมื่อระบบคำนวณตอนสิ้นเดือน',
          'การคำนวณการจ่าย จะนำผลงาน PC x อัตราโบนัสไตรมาส x Persistency Credit'
        ]
      }
    }
    // Bonus Year
    else if (filterCompensation == 'โบนัสรายปี') {
      this.tableHeader = ['อัตราโบนัสรายปี', 'PC ที่ต้องทำ', 'PC ที่ต้องทำเพิ่ม'];
      this.blockName = [`PC สะสมปี ${YEAR}`]
      this.remark = [
        'รายงานนี้เป็นการคาดการณ์การจ่ายผลประโยชน์ / ประเมินรักษาตำแหน่ง / เลื่อนตำแหน่งเท่านั้น อาจมีการเปลี่ยนแปลงได้เมื่อระบบคำนวณตอนสิ้นเดือน',
        'การคำนวณการจ่าย จะนำผลงาน (PC x อัตราโบนัสรายปี x Persistency Credit) - โบนัสไตรมาสที่จ่ายไปแล้ว'
      ]
    }
    else if (filterCompensation == 'รักษาตำแหน่ง') {
      this.tableHeader = ['กฎเกณฑ์', 'ผลงานปัจจุบัน', 'ต้องทำเพิ่ม'];
      if(this.displayLvName == 'BM'){
        this.blockName = ['ตำแหน่งปัจจุบัน']
      } else {
        this.blockName = null
      }
      
      this.remark = [
        'รายงานนี้เป็นการคาดการณ์การจ่ายผลประโยชน์ / ประเมินรักษาตำแหน่ง / เลื่อนตำแหน่งเท่านั้น อาจมีการเปลี่ยนแปลงได้เมื่อระบบคำนวณตอนสิ้นเดือน'
      ]
    }
    else if (filterCompensation == 'เลื่อนตำแหน่ง') {
      this.tableHeader = ['กฎเกณฑ์', 'ผลงานปัจจุบัน', 'ต้องทำเพิ่ม'];
      if(this.displayLvName == 'AG' || this.displayLvName == 'FC'){
        this.blockName = ['เลื่อนตำแหน่งถัดไป']
      } else {
        this.blockName = ['ตำแหน่งปัจจุบัน', 'เลื่อนตำแหน่งถัดไป']
      }

      if (this.displayLvName == 'AL') {
        this.blockNameH = ['ตำแหน่งปัจจุบัน', 'เลื่อนตำแหน่งถัดไป'];
        this.blockNameV = ['เลื่อนตำแหน่งถัดไป'];
        this.remark = [
          'รายงานนี้เป็นการคาดการณ์การจ่ายผลประโยชน์ / ประเมินรักษาตำแหน่ง / เลื่อนตำแหน่งเท่านั้น อาจมีการเปลี่ยนแปลงได้เมื่อระบบคำนวณตอนสิ้นเดือน',
          'การเลื่อนตำแหน่งภายในระดับเดียวกัน จะต้องเลื่อนตำแหน่งตามที่กำหนดไว้ทีละขั้น และไม่สามารถเลื่อนตำแหน่งโดยการข้ามขั้น',
          'จะไม่มีการนำผลงาน ที่ใช้ในการเลื่อนตำแหน่งก่อนหน้านี้ มานับรวมในการเลื่อนตำแหน่งครั้งต่อไป'
        ]
      }
      else {
        this.remark = [
          'รายงานนี้เป็นการคาดการณ์การจ่ายผลประโยชน์ / ประเมินรักษาตำแหน่ง / เลื่อนตำแหน่งเท่านั้น อาจมีการเปลี่ยนแปลงได้เมื่อระบบคำนวณตอนสิ้นเดือน'
        ]
      }
    }
    else if (filterCompensation == 'OV') {
      this.tableHeader = ['อัตรา OV', 'Active Producer ที่ต้องทำ', 'คาดการณ์ผลประโยชน์ OV'];
      this.blockName = [`PC สะสมประจำเดือน  ${MONTH} ${YEAR} `, `Active Producer ประจำเดือน  ${MONTH} ${YEAR} `]
      this.remark = [
        'รายงานนี้เป็นการคาดการณ์การจ่ายผลประโยชน์ / ประเมินรักษาตำแหน่ง / เลื่อนตำแหน่งเท่านั้น อาจมีการเปลี่ยนแปลงได้เมื่อระบบคำนวณตอนสิ้นเดือน',
        'อัตรา OV ที่แสดงนี้เป็นอัตราการจ่ายตามปกติ',
        'การคำนวณการจ่าย จะนำผลงาน PC x อัตรา OV'
      ]
    }
    else if (filterCompensation == 'โบนัสตัวแทนอาชีพ') {
      this.tableHeader = ['กฏเกณฑ์', 'ผลงานปัจจุบัน', 'ต้องทำเพิ่ม'];
      this.remark = [
        'รายงานนี้เป็นการคาดการณ์การจ่ายผลประโยชน์ / ประเมินรักษาตำแหน่ง / เลื่อนตำแหน่งเท่านั้น อาจมีการเปลี่ยนแปลงได้เมื่อระบบคำนวณตอนสิ้นเดือน',
        'ไม่นับผลงานกรณี Split Case'
      ]
    }
    else if (filterCompensation == 'โบนัสรายเดือน') {
      this.tableHeader = ['อัตราโบนัสเดือน', 'PC ที่ต้องทำ', 'PC ที่ต้องทำเพิ่ม'];
      this.blockName = ['PC : MTD']
      this.remark = [
        'รายงานนี้เป็นการคาดการณ์การจ่ายผลประโยชน์ / ประเมินรักษาตำแหน่ง / เลื่อนตำแหน่งเท่านั้น อาจมีการเปลี่ยนแปลงได้เมื่อระบบคำนวณตอนสิ้นเดือน',
        'การคำนวณการจ่าย จะนำ อัตราโบนัสเดือน x Persistency Credit'
      ]
    }
  }
  selectFilter() {
    if (this.displayLvName == 'AG') {
      this.listFilterCompensation = [
        'โบนัสไตรมาส',
        'โบนัสรายปี',
        'โบนัสตัวแทนอาชีพ',
        'เลื่อนตำแหน่ง',
      ];
      this.listFilterPeriodType = [
        'ROLLBACK12', 'ROLLBACK12_CURRENT'
      ]
      this.listFilterPeriodTypeName = [
        'ผลงานย้อนหลัง 12 เดือน', 'ผลงานย้อนหลัง 12 เดือน (รวมเดือนปัจจุบัน)'
      ]
    } else if (this.displayLvName == 'AL') {
      this.listFilterCompensation = [
        'OV',
        'เงินเดือนผันแปร',
        'โบนัสไตรมาส',
        'โบนัสรายปี',
        'รักษาตำแหน่ง',
        'เลื่อนตำแหน่ง',
      ];
      this.listFilterPeriodType = [
        'ROLLBACK12', 'ROLLBACK12_CURRENT'
      ]
      this.listFilterPeriodTypeName = [
        'ผลงานย้อนหลัง 12 เดือน', 'ผลงานย้อนหลัง 12 เดือน (รวมเดือนปัจจุบัน)'
      ]
    } else if (this.displayLvName == 'VP') {
      this.listFilterCompensation = [
        'เงินเดือนผันแปร',
        'โบนัสไตรมาส',
        'โบนัสรายปี',
        'รักษาตำแหน่ง',
        'เลื่อนตำแหน่ง',
      ];
      this.listFilterPeriodType = [
        'NORMAL'
      ]
    } else if (this.displayLvName == 'GM') {
      this.listFilterCompensation = [
        'เงินเดือนผันแปร',
        'โบนัสไตรมาส',
        'โบนัสรายปี',
        'รักษาตำแหน่ง',
        'เลื่อนตำแหน่ง',
      ];
      this.listFilterPeriodType = [
        'NORMAL'
      ]
    } else if (this.displayLvName == 'BM') {
      this.listFilterCompensation = [
        'รักษาตำแหน่ง',
        'เลื่อนตำแหน่ง'
      ];
      this.listFilterPeriodType = [
        'NORMAL'
      ]
    } else if (this.displayLvName == 'CM') {
      this.listFilterCompensation = [
        'รักษาตำแหน่ง',
        'เลื่อนตำแหน่ง'
      ];
      this.listFilterPeriodType = [
        'NORMAL'
      ]
    } else if (this.displayLvName == 'FM') {
      this.listFilterCompensation = [
        'รักษาตำแหน่ง',
        'เลื่อนตำแหน่ง'
      ];
      this.listFilterPeriodType = [
        'NORMAL'
      ]
    } else if (this.displayLvName == 'FC') {
      this.listFilterCompensation = [
        'โบนัสรายเดือน',
        'โบนัสไตรมาส',
        'เลื่อนตำแหน่ง',
      ];
      this.listFilterPeriodType = [
        'ROLLBACK12', 'ROLLBACK12_CURRENT'
      ]
      this.listFilterPeriodTypeName = [
        'ผลงานย้อนหลัง 4 เดือน', 'ผลงานย้อนหลัง 4 เดือน (รวมเดือนปัจจุบัน)'
      ]
    }
    this.filterCompensation = this.listFilterCompensation[0];
    this.filterPeriodType = this.listFilterPeriodType[0]
  }

  closYear( YM ){
    const YEAR = +(""+YM).slice(0,4) + 543;
   return  YEAR ;
  }
  closMonth( YM ){
    return (""+YM).slice(4,6);
  }

  quarter(YM ){
    const MONTH = this.closMonth(YM);
    return +MONTH/3>>0;
  }
}
