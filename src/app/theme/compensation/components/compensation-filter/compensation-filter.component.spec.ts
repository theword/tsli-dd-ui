import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompensationFilterComponent } from './compensation-filter.component';

describe('CompensationFilterComponent', () => {
  let component: CompensationFilterComponent;
  let fixture: ComponentFixture<CompensationFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompensationFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompensationFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
