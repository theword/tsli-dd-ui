import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-compensation-filter',
  templateUrl: './compensation-filter.component.html',
  styleUrls: ['./compensation-filter.component.scss']
})
export class CompensationFilterComponent implements OnInit {

  @Input() listFilterCompensation = []
  @Input() agentLevel = []
  @Input() filterCompensation: string
  
  @Output() changeFilterCompensation = new EventEmitter();


  constructor() { }

  ngOnInit(): void {
  }

  onChangeFilterCompensation(mode) {
    this.changeFilterCompensation.emit(mode);
  }

}
