import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-compensation',
  templateUrl: './table-compensation.component.html',
  styleUrls: ['./table-compensation.component.scss'],
})
export class TableCompensationComponent implements OnInit {

  @Input() filterCompensation
  @Input() ovHighlightIndex
  @Input() tableHeader
  @Input() compensationConfig
  @Input() blockName
  @Input() blockData
  @Input() remark

  constructor() {}

  ngOnInit(): void {
  }
}