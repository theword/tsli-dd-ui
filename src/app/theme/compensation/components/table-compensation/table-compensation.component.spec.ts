import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableCompensationComponent } from './table-compensation.component';

describe('TableCompensationComponent', () => {
  let component: TableCompensationComponent;
  let fixture: ComponentFixture<TableCompensationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableCompensationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableCompensationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
