import { filter } from 'rxjs/operators';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss']
})
export class ValidationComponent implements OnInit {

  @Input() tableHeader: any[]
  @Input() compensationConfig
  @Input() blockName
  @Input() blockData
  @Input() remark
  @Input() period

  @Input() listFilterPeriodType = []
  @Input() filterPeriodType: string
  @Input() listFilterPeriodTypeName = []

  @Input() validationData: any;
  
  @Input() filterCompensation: any;
  @Input() careerPc: any
  @Input() careerNoCase: any

  @Input() mapping: any

  @Input() isVetical: any

  @Output() changeFilterPeriodType = new EventEmitter();

  closYear = "";
  constructor(private http: HttpClient) { }
  
  ngOnInit(): void {
    this.getClosYmInfo().subscribe( r=>{
      if(r){
        this.closYear = (""+(+r.closYm.substring(0,4) + 543 ));
      }
     
    });
  }

  mappingName(kpi){
    let name = this.mapping.filter( item => item.validateType == kpi )

    if(name.length > 0){
      return name[0].desc
    }
    return kpi
  }

  onChangeFilterPeriodType(mode) {
    this.changeFilterPeriodType.emit(mode);
  }

  cv2FieldName(validateName: string) {
    // PC_DIRECT > pcDirect
    if (!validateName) return '';
    let _s = validateName.toLocaleLowerCase();
    _s = _s.replace(/[-_](.?)/g, function (match, s1) {
      return s1.toUpperCase()
    });
    return _s;
  }

  isNumber(val) {
    // String or Text Value
    if (!isNaN(parseFloat(val))) {
      return true;
    }
    else {
      return false;
    }
  }

  highlight(config, value){
    if(this.isNumber(value)){
      // console.log("value >= config", Number.parseInt(value) >= Number.parseInt(config) ? 'true' : 'false');
      // console.log("value", value );
      // console.log("config", config );
      
      if(value - config >= 0 ){
        return 'collect'
      } else {
        return ''
      }
    } else {
      if(value == 'ใช่'){
        return 'collect'
      } else {
        return ''
      }
    }
  }

  isComplete(config, value){
    if(this.isNumber(value)){
      if(value - config >= 0){
        return true
      } else {
        return false
      }
    } else {
      if(value == 'ใช่'){
        return true
      } else {
        return false
      }
    }
  }

  isShow(config, value){
    if(value == null){
      return ''
    }

    if(this.isNumber(value)){
      if(value - config >= 0){
        return
      } else {
        return Math.abs(value - config)
      }
    } else if (value == 'N/A') {
      return config
    } else {
      if(value == 'ใช่'){
        return
      } else {
        return 'ใช่'
      }
    }
  }

  checkKPI(validateKPI){
    if(validateKPI == 'PERSISTENCY'){
      return false
    } else {
      return true
    }
  }
  
  isPercent(value){
    if(!value){
      return '%'
    } else {
      return ''
    }
  }


  getClosYmInfo(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/info/getInfoClosYm`);
  }
}
