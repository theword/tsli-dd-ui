import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-promote',
  templateUrl: './promote.component.html',
  styleUrls: ['./promote.component.scss']
})
export class PromoteComponent implements OnInit {

  @Input() tableHeader: any[]
  @Input() compensationConfig: any;
  @Input() blockName
  @Input() blockData
  @Input() remark
  @Input() period

  @Input() listFilterPeriodType = []
  @Input() filterPeriodType: string
  @Input() listFilterPeriodTypeName = []

  @Input() validationData: any;
  
  @Output() changeFilterPeriodType = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onChangeFilterPeriodType(mode) {
    this.changeFilterPeriodType.emit(mode);
  }

}
