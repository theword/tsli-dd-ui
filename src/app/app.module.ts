import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import {
  HTTP_INTERCEPTORS,
  HttpClient,
  HttpClientModule,
} from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from './core/translation/http-loader-factory';
import { AuthComponent } from './layout/auth/auth.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { AuthInterceptor } from './core/security/auth-interceptor';
import { HttpErrorHandler } from './core/security/http-error-handler';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { MomentModule } from 'angular2-moment';
import { DashboardComponent } from './layout/dashboard/dashboard.component';
import { ErrorComponent } from './layout/error/error.component';
import { FooterComponent } from './layout/footer/footer.component';
import { SharedModule } from './core/shared.module';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { CryptoService } from './core/security/crypto.service';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { NgxSpinnerModule } from 'ngx-spinner';
import { EDocumentComponent } from './web-agent/e-document/e-document.component';
import { DetailDocumentComponent } from './web-agent/detail-document/detail-document.component';
import { DescriptionEDocComponent } from './web-agent/e-document/components/description-e-doc/description-e-doc.component';
import { FilterEDocComponent } from './web-agent/e-document/components/filter-e-doc/filter-e-doc.component';
import { DummyAuthComponent } from './dummy-auth/dummy-auth.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    DashboardComponent,
    ErrorComponent,
    FooterComponent,
    EDocumentComponent,
    DetailDocumentComponent,
    FilterEDocComponent,
    DescriptionEDocComponent,
    DummyAuthComponent,
  ],

  imports: [
    NgbCollapseModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    SlimLoadingBarModule,
    BrowserAnimationsModule,
    NgxChartsModule,
    MomentModule,
    SharedModule,
    DeviceDetectorModule,
    NgxSpinnerModule,
    NgIdleKeepaliveModule.forRoot(),
    ScrollToModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    CollapseModule.forRoot(),
  ],
  providers: [
    Title
    ,{
      useClass: AuthInterceptor,
      provide: HTTP_INTERCEPTORS,
      multi: true,
    },
    {
      useClass: HttpErrorHandler,
      provide: HTTP_INTERCEPTORS,
      multi: true,
    },
    CryptoService
  ],
  bootstrap: [AppComponent],
  exports: [TranslateModule],
})
export class AppModule {}
