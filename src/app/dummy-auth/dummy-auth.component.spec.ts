import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DummyAuthComponent } from './dummy-auth.component';

describe('DummyAuthComponent', () => {
  let component: DummyAuthComponent;
  let fixture: ComponentFixture<DummyAuthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DummyAuthComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
