import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from '../core/security/auth.service';

@Component({
  selector: 'app-dummy-auth',
  templateUrl: './dummy-auth.component.html',
  styleUrls: ['./dummy-auth.component.scss'],
})
export class DummyAuthComponent implements OnInit {
  dummy = {};
  constructor(
    private auth: AuthService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.spinner.show();
    this.dummy['key'] = this.route.snapshot.queryParamMap.get('key');
    this.dummy['value'] = this.route.snapshot.queryParamMap.get('value');
    this.dummy['redirectUri'] = this.route.snapshot.queryParamMap.get('redirectUri');
  }

  ngOnInit(): void {
    if (this.auth.isLoggedIn()) {
      this.auth.logout();
    }
    this.auth
      .oauthDummyLogin(this.dummy['key'], this.dummy['value'])
      .then((r) => {
        this.router.navigateByUrl(this.dummy['redirectUri']);
      }).finally(()=>{
        this.spinner.hide();
      });
  }
}
