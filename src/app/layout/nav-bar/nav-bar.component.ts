import { Component, OnInit,  AfterViewInit } from '@angular/core';
import { AuthService } from 'src/app/core/security/auth.service';
import { Router } from '@angular/router';
import { CheckSessionStorageService } from 'src/app/core/service/check-session-storage.service';
import { PositionRouterService } from 'src/app/core/service/position-router.service';
import { ModelUser } from 'src/app/core/model/model-user';
import { DashboardBackRouteService } from 'src/app/core/service/dashboard-back-route.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent implements OnInit, AfterViewInit  {


 
  fullname = '';
  cmsUrl = '';
  public modelUser: ModelUser;
  public isCollapsed = false;
  isHidden = false;

  constructor(
    private router: Router,
    private authservice: AuthService,
    private positionRouterService: PositionRouterService,
    private dashboardBackRouteService: DashboardBackRouteService,
  ) {

    this.modelUser = new ModelUser();
    // if( ['prod'].includes(environment.info.env) ){
    //   this.isHidden = true;
    // }else{
    //   this.isHidden = false;
    // }

  }
  
  ngOnInit(): void {
    this.fullname = this.authservice.getName();
    this.cmsUrl = environment.cmsUrl ;
  }

  ngAfterViewInit(): void {
  }
  
  onCollapsed() {
    this.isCollapsed = !this.isCollapsed
  }
  
  accountList() {
    this.router.navigate(['/lock-engine/member']);
  }
  logAccess() {
    this.router.navigate(['/lock-engine/access-log']);
  }
  myDevice() {
    this.router.navigate(['/lock-engine/my-device']);
  }
  targetYear() {
    this.router.navigate(['/targetYear']);
  }
  uploadPromotion() {
    this.router.navigate(['/uploadPromotion']);
  }
  // Web Agent Portal
  webAgent() {
    this.router.navigate(['/web-agent']);
  }
  webAgentInsurance() {
    this.router.navigate(['/web-agent/validate-insurance']);
  }
  dashboardMainPage() {
    this.router.navigate(['/dashboard']);
  }
  webAgentDocument() {
    this.router.navigate(['/web-agent/validate-document']);
  }
  webAgentReport() {
    this.router.navigate(['/web-agent/report']);
  }
  webAgentEDocument() {
    this.router.navigate(['/web-agent/e-document']);
  }
  webAgentERegister() {
    if(this.authservice.isRegisterAdmin() ){
      this.router.navigate(['/web-agent/e-register-admin']);
    }else{
      this.router.navigate(['/web-agent/e-register-agent']);
    }
  }
  webAgentERegisterLICExam() {
    this.router.navigate(['/web-agent/register-upload-lic-exam']);
  }
  // End Web Agent Portal
  myHome() {
    this.positionRouterService.gotoPerformanceDetail();
  }

  goToHome() {
    let userRole = this.authservice.getPosition();
    let agentCode = this.authservice.getLoginedName();
    sessionStorage.removeItem('agentCodeStored');
    sessionStorage.removeItem('agentLevelStored');
    // window.location.reload();
    this.dashboardBackRouteService.backHome(userRole, agentCode);
  }

  isStaff(): boolean {
    return this.positionRouterService.isStaff();
  }

  isSuperUser(): boolean{
    return this.positionRouterService.isSuperUser() ;
  }

  isLICExamAccess(){
    return (this.authservice.isRegisterAdmin() || this.authservice.isRegisterAdmin('3'))
  }
  
  isWebAgent() {
    switch(this.router.url){
      // case "/web-agent" :
      // case "/web-agent/validate-insurance" :
      // case "/web-agent/validate-document" :
      // case "/web-agent/report" :
      case "/web-agent/e-document" :
      // case "/web-agent/e-register-admin" :
      // case "/web-agent/e-register-agent" :
        return false
      default:
        return true
    }
     
  }

  logout() {
  
      setTimeout( () => {
        this.authservice.logout();
        window.location.assign(environment.cmsUrl+"/sso-logout.php");
      }, 400);

  }


}
