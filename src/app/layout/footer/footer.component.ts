import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, interval } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { startWith, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  year = 2020;
  app: any;
  schedule = '';

  prefixClosYm = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  prefixClosYmTH = ['ม.ค.','ก.พ.','มี.ค.','เม.ษ.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ส.ค.','ต.ค.','พ.ย.','ธ.ค.'];
  closYm = '';
  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.app = {
      env: environment.info.env,
      version: environment.info.version,
    };

    interval(60000)
      .pipe(
        startWith(0),
        switchMap(() => this.getInfo())
      )
      .subscribe((res) => {
        // console.log(res);
        this.year = res.year;
        this.schedule = res.schedule;
        // this.closYm = res.
      });
    interval(60000)
    .pipe(
      startWith(0),
      switchMap(() => this.getClosYmInfo())
    )
    .subscribe((res) => {
      // console.log('ClosYM: ', res);
      // console.log('prefixClosYm >>>',this.prefixClosYm[res.closYm.substring(4,6) - 1]);
      
      this.closYm = this.prefixClosYm[res.closYm.substring(4,6) - 1] + '-' + res.closYm.substring(0,4);
      let yearTH = (parseInt(res.closYm.substring(0,4)) + 543).toString().substring(2,4)
      sessionStorage.setItem('calendarCurrentMonthTH',  this.prefixClosYmTH[res.closYm.substring(4,6) - 1] + " " + yearTH )
    });
  }

  getInfo(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/info`);
  }

  getClosYmInfo(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/info/getInfoClosYm`);
  }
}
