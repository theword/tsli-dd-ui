import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  UrlTree,
  ActivatedRoute,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class LoginGuards implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

    canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {


    if( this.authService.isLoggedIn() && route.queryParamMap.get('cms_logout') ){
        this.authService.logout();
        return true;
    }


    if (this.authService.isLoggedIn()) {
      this.authService.isRedirectCms( route.queryParams );
      const returnUrl = route.queryParams.returnUrl || '/dashboard';
      this.router.navigateByUrl(returnUrl,   {queryParams: { returnUrl: state.url }});

      return false;
    }

    this.authService.logout();
    return true;
  }
}
