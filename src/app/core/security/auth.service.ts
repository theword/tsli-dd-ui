import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as moment from 'moment';
import { tap } from 'rxjs/operators';
import { DeviceDetectorService } from 'ngx-device-detector';
import { STORGE_KEY as SKEY } from 'src/app/authentication/storage.key';
import { CryptoService } from './crypto.service';
import { ActivatedRoute } from '@angular/router';


@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private router: ActivatedRoute,
    private http: HttpClient,
    private cryptoService: CryptoService,
    private deviceService: DeviceDetectorService
  ) { }

  initial() {
    if (
      !this.isLoggedIn() ||
      this.deviceService.isMobile ||
      this.deviceService.isTablet
    ) {
      this.logout();
    }
  }

  oauthDummyLogin(username: string, password: string) {
    let header = this.addBasicAuth();
    header['headers'][environment.headerDummy] = environment.headerDummyValue;
    return this.http
      .post(
        `${environment.oauth2.issueUrl}?&grant_type=${environment.oauth2.grantType}&username=${('' + username)}&password=${password}`,
        undefined,
        header
      )
      .toPromise()
      .then((r: any) => {
        // Read jwt token
        this.setSession(r);
      });
  }

  oauthLogin(username: string, password: string) {
    return this.http
      .post(
        `${environment.oauth2.issueUrl}?grant_type=${environment.oauth2.grantType}&username=${('' + username).toUpperCase()}&password=${password}`,
        undefined,
        this.addBasicAuth()
      )
      .toPromise()
      .then((r: any) => {
        // Read jwt token
        this.setSession(r);
      });
  }

  oauthRefreshToken() {
    return this.http
      .post(
        `${environment.oauth2.issueUrl
        }?grant_type=refresh_token&refresh_token=${this.getRefreshToken()}`,
        undefined,
        this.addBasicAuth()
      )
      .pipe(
        tap((r) => {
          this.setSession(r);
        })
      );
  }

  addBasicAuth() {
    const basicSecret = window.btoa(
      `${environment.oauth2.clientId}:${environment.oauth2.secret}`
    );
    return {
      headers: {
        Authorization: `Basic ${basicSecret}`,
      },
    };
  }

  private async setSession(auth: any) {

    const helper = new JwtHelperService();
    const token = helper.decodeToken(auth.access_token);
    const expiresAt = moment().add(auth.expires_in, 'seconds');
    localStorage.setItem(SKEY.ACCESS_TOKEN, auth.access_token);
    localStorage.setItem(SKEY.REFRESH_TOKEN, auth.refresh_token);
    localStorage.setItem(SKEY.EXPIRES_AT, JSON.stringify(expiresAt.valueOf()));
    localStorage.setItem(SKEY.TOKEN_TYPE, auth.token_type);
    localStorage.setItem(SKEY.SCOPE, auth.scope);
    localStorage.setItem(
      SKEY.AGENT_CODE,
      this.cryptoService.encrypt(auth.agentCode)
    );
    localStorage.setItem(
      SKEY.ROLE,
      this.cryptoService.encrypt(('' + token.role).toUpperCase())
    );
    localStorage.setItem(SKEY.NAME, this.cryptoService.encrypt(token.name));
    localStorage.setItem(SKEY.STATUS, this.cryptoService.encrypt(token.status));
    localStorage.setItem(SKEY.IS_SUPER_USER, this.cryptoService.encrypt(token.isSuperUser));
    localStorage.setItem(SKEY.WEB_ROLE, this.cryptoService.encrypt(token['webRoles']));
    const obj = {
      'user_login': token['agentCode'],
      'user_email': token['email'],
      'user_pass': token['email'],   // password will be username always
      'first_name': token['name'],  // first name will be username
      'role': token['role'],     //register the user with subscriber role only
    }

    const str = JSON.stringify(obj);
    let encoded = btoa(((encodeURIComponent(str))));
    encoded = encoded?.replace(/=/g, "");
    localStorage.setItem(SKEY.WP_KEY, encoded);

  }

  logout() {
    //  let rs = await  this.http.get(`${environment.apiUrl}/users/signout`); 
    localStorage.clear();
    sessionStorage.clear();
    // this.deleteAllCookies();
  }

  deleteAllCookies() {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i];
      var eqPos = cookie.indexOf("=");
      var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
  }

  public isLoggedIn(): any {
    return !this.isTokenExpired() && this.getToken();
  }

  clearSession() {
    localStorage.removeItem(SKEY.ACCESS_TOKEN);
    localStorage.removeItem(SKEY.REFRESH_TOKEN);
    localStorage.removeItem(SKEY.EXPIRES_AT);
    localStorage.removeItem(SKEY.TOKEN_TYPE);
    localStorage.removeItem(SKEY.SCOPE);
    localStorage.removeItem(SKEY.AGENT_CODE);
    localStorage.removeItem(SKEY.ROLE);
    localStorage.removeItem(SKEY.DEVICE_ID);
    localStorage.removeItem(SKEY.NAME);
    localStorage.removeItem(SKEY.STATUS);
    localStorage.removeItem(SKEY.DEVICE_STATUS);
    localStorage.removeItem(SKEY.IS_SUPER_USER);
    localStorage.removeItem(SKEY.WEB_ROLE);
    localStorage.removeItem(SKEY.ROLES_LIST);

  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  isTokenExpired() {
    return moment().isAfter(this.getExpiration());
  }

  getExpiration() {
    const expiration = localStorage.getItem(SKEY.EXPIRES_AT);
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }

  getToken() {
    return localStorage.getItem(SKEY.ACCESS_TOKEN);
  }

  getRefreshToken() {
    return localStorage.getItem(SKEY.REFRESH_TOKEN);
  }

  getDeviceId() {
    return this.cryptoService.decrypt(localStorage.getItem(SKEY.DEVICE_ID));
  }
  setDeviceId(deviceId) {
    localStorage.setItem(SKEY.DEVICE_ID, this.cryptoService.encrypt(deviceId));
  }

  getName() {
    return this.cryptoService.decrypt(localStorage.getItem(SKEY.NAME));
  }

  getIsSuperUser(): any {
    return this.cryptoService.decrypt(localStorage.getItem(SKEY.IS_SUPER_USER));
  }
  getPosition() {
    return this.cryptoService.decrypt(localStorage.getItem(SKEY.ROLE));
  }

  getLoginedName(): string {
    return this.cryptoService.decrypt(localStorage.getItem(SKEY.AGENT_CODE));
  }

  setDeviceStatus(status: string) {
    localStorage.setItem(
      SKEY.DEVICE_STATUS,
      this.cryptoService.encrypt(status)
    );
  }
  getDeviceStatus(): string {
    return this.cryptoService.decrypt(localStorage.getItem(SKEY.DEVICE_STATUS));
  }
  getWebRole(): string {
    return this.cryptoService.decrypt(localStorage.getItem(SKEY.WEB_ROLE));
  }
  isRegisterAdmin(activityType='') {
    // console.log("this.getWebRole() ", this.getWebRole().split(',').includes('REGISTER_ADMIN'+ activityType));
    // console.log("isRegisterAdmin", 'REGISTER_ADMIN'+ activityType);
    // console.log("isRegisterAdmin", (',' + this.getWebRole() + ',').includes('REGISTER_ADMIN'+ activityType));
    return this.getWebRole().split(',').includes('REGISTER_ADMIN'+ activityType)
  }

  isLockEngineValid(): boolean {
    return this.getDeviceId() && 'valid' === this.getDeviceStatus();
  }

  // isSuperUser(): boolean {
  //   return this.getIsSuperUser();
  // }


  isRedirectCms(queryParams): boolean {
    const redirect = queryParams['redirect'];
    let redirectUrl = queryParams['redirectUrl'];
    if (redirect && redirectUrl) {
      window.location.replace(`${redirectUrl}?dashboard=${localStorage.getItem(SKEY.WP_KEY)}`);
      return true;
    }
    return false;

  }
  
  setUserRolesList(rolesList) {
    localStorage.setItem(SKEY.ROLES_LIST, this.cryptoService.encrypt(rolesList));
  }
  getUserRolesList() {
    return this.cryptoService.decrypt(localStorage.getItem(SKEY.ROLES_LIST));
  }
  // Mapping Permission for CRUD activity by user roles
  // getActivityByUserRoles(activityType)
  isERegisterAccess(activityType) {
    const userRoles = this.cryptoService.decrypt(localStorage.getItem(SKEY.ROLES_LIST));
    // console.log('userRoles: ', userRoles);
    // console.log('checkPermission: ', userRoles.includes(activityType));
    return userRoles.includes(activityType);
  }

}
