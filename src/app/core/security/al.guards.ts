import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { PositionRouterService } from '../service/position-router.service';

@Injectable()
export class AlGuards implements CanActivate {
  constructor(
    private positionRouterService: PositionRouterService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {

    if (this.positionRouterService.isStaff() || this.positionRouterService.isGm() || this.positionRouterService.isAvp()) {
      return true;
    }

    if (!this.positionRouterService.isAl()) {
      this.positionRouterService.gotoPerformanceDetail();

      return false;
    }

    return true;
  }
}
