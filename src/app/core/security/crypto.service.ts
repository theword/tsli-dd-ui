import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root',
})
export class CryptoService {
  hashCode: any;
  key: any;
  iv: any;

  constructor() {
    this.hashCode = environment.hashCode;
    this.key = CryptoJS.enc.Utf8.parse(this.hashCode);
    this.iv = CryptoJS.enc.Utf8.parse(this.hashCode);
  }

  // The set method is use for encrypt the value.
  encrypt(value) {
    if (!value) {
      return value;
    }
    return value;
    // return this.encryptUsingAES256(value);
  }

  // The get method is use for decrypt the value.
  decrypt(value) {
    if (!value) {
      return value;
    }
    return value;
    // return this.decryptUsingAES256(value);
  }

  encryptUsingAES256(text): string {
    const encrypted = CryptoJS.AES.encrypt(JSON.stringify(text), this.key, {
      keySize: 16,
      iv: this.iv,
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    });
    return encrypted.toString();
  }

  decryptUsingAES256(text): string {
    return CryptoJS.AES.decrypt(text, this.key, {
      keySize: 16,
      iv: this.iv,
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    }).toString(CryptoJS.enc.Utf8);
  }
}
