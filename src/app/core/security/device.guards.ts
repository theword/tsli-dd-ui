import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { DeviceDetectorService } from 'ngx-device-detector';

@Injectable()
export class DeviceGuards implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private deviceService: DeviceDetectorService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    // if (!this.authService.isLockEngineValid()) {
    //   if (this.deviceService.isDesktop()) {
    //     this.router.navigate(['/lock-engine/download-desktop'], {});
    //   }

    //   return false;
    // }

    return true;
  }
}
