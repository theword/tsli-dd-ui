import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  UrlTree,
  Router,
  CanActivateChild,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuards implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {

    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['/auth/login'], {
          queryParams: { returnUrl: state.url,
          redirect: route.queryParamMap.get('redirect'),
          redirectUrl: route.queryParamMap.get('redirectUrl') 
        },
      });
    
      return false;
    }
    this.authService.isRedirectCms(route.queryParams);

    
    return true;
  }
}
