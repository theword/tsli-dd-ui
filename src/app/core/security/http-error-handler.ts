import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class HttpErrorHandler implements HttpInterceptor {
  constructor(private translate: TranslateService, private router: Router) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((event: any) => {
        if (event.status < 200 || event.status >= 300) {
          return Observable.throw(event);
        }
        return event;
      }),
      catchError((response: HttpErrorResponse) => {

    
        switch (response.status) {
          case 400:
            if (response.error) {
              Swal.fire(
                '',
                this.translate.instant('alert.login.uname.invalid',                ),
                'warning'
              );
            }
            break;
          case 401:
            Swal.fire(
              '',
              this.translate.instant('alert.no-authorize'),
              'error'
            ).then(() => {
              this.router.navigate(['/auth/login']);
            });
            break;
          case 403:
            Swal.fire(
              '',
              this.translate.instant('alert.no-authorize'),
              'error'
            ).then(() => {
              this.router.navigate(['/auth/login']);
            });
            break;

          case 504:
            Swal.fire(
              '',
              this.translate.instant('alert.gateway.timeout'),
              'error'
            );
            break;

          default :
            Swal.fire(
              '',
              this.translate.instant('alert.system-error'),
              'error'
            );
            break;

             
        }

        return throwError(response);
      })
    );
  }
}
