import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { PositionRouterService } from '../service/position-router.service';

@Injectable()
export class SuperUserGuards implements CanActivate {
  constructor(private positionRouterService: PositionRouterService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    // if (!this.positionRouterService.isSuperUser()) {
      // this.positionRouterService.gotoPerformanceDetail();
      // return false;
    // }

    return this.positionRouterService.isSuperUser();
  }
}
