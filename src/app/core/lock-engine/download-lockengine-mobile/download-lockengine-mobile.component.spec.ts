import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadLockengineMobileComponent } from './download-lockengine-mobile.component';

describe('DownloadLockengineMobileComponent', () => {
  let component: DownloadLockengineMobileComponent;
  let fixture: ComponentFixture<DownloadLockengineMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadLockengineMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadLockengineMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
