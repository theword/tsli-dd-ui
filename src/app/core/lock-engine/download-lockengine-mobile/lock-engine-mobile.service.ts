import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class LockEngineMobileService {
  constructor(private http: HttpClient) {}

  getDeviceIdLog(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/lock-engine/getDeviceIdLog`);
  }
}
