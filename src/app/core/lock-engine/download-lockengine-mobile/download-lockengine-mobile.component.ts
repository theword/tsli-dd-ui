import { Component, OnInit, Inject } from '@angular/core';
import { LockEngineService } from '../lock.manager.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../security/auth.service';
import { Router } from '@angular/router';
import { PositionRouterService } from '../../service/position-router.service';
import { LockEngineMobileService } from './lock-engine-mobile.service';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-download-lockengine-mobile',
  templateUrl: './download-lockengine-mobile.component.html',
  styleUrls: ['./download-lockengine-mobile.component.scss'],
})
export class DownloadLockengineMobileComponent implements OnInit {
  clientIsDead = false;

  constructor(
    @Inject(DOCUMENT) private readonly document: any,
    // private router: Router,
    private lockEngineService: LockEngineService,
    // private lockManager: LockEngineMobileService,
    private positionRouterService: PositionRouterService // private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.clientIsDead = this.lockEngineService.clientIsDead();

    if ( this.lockEngineService.isDesktop || this.lockEngineService.isWindows ) {
      this.lockEngineService.gotoValidateDesktop();
    }

    if (!this.clientIsDead) {
      this.lockEngineService.addLog('no dead');
      this.lockEngineService.checkMobile();
    }
  }

  getLog(): any {
    return this.lockEngineService.getLog();
  }
}
