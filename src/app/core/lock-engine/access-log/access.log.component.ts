import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-access-log',
  templateUrl: './access.log.component.html',
  styleUrls: ['./access.log.component.scss'],
})
export class AccessLogComponent implements OnInit  {

  paging: { totalPages: 0, totalItems: 0,  data: []};
  criteria: any = {page: 0, size: 150};
  license: any = {};
  constructor(private http: HttpClient, private translate: TranslateService) {}

  ngOnInit(): void {
    this.search();
    this.getLicense();
  }

  search() {

    this.http
      .post(
        `${environment.apiUrl}/lock-engine/searchUserAccessLog`,
        {},
        { params: this.criteria }
      )
      .subscribe((r: any) => {
        this.paging = r;

      });
  }

  getLicense() {
    this.http
      .get(`${environment.apiUrl}/lock-engine/getLicense`)
      .subscribe((r: any) => {
        this.license = r;
      });
  }
  isActiveStatus(status) {
    return 'A' === status;
  }

  userTypeDesc(userType) {
    return 'U' === userType
      ? this.translate.instant('memmber.type.user')
      : this.translate.instant('memmber.type.producer');
  }


  previous() {
    if ( this.criteria.page === 0){
      return;
    }else{
      this.criteria.page--;
    }
    this.search();
  }

  next() {
    if ( this.criteria.page === this.paging?.totalPages - 1 ){
      return;
    }else{
      this.criteria.page++;
    }
    this.search();
  }

  firstPage(){
    this.criteria.page = 0;
    this.search();
  }

  endPage(){
    this.criteria.page = this.paging.totalPages - 1;
    this.search();
  }

  itemInfo(): string {
    const start = this.printSequense(0);
    const end = start + this.criteria.size - 1;
    return '' + start + '-' + (end > this.paging?.totalItems ? this.paging?.totalItems : end ) + ' of ' + this.paging?.totalItems;
  }


  printSequense(seq) {
   return (this.criteria.page * this.criteria.size) + 1 + seq;
  }
}
