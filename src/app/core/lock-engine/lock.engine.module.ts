import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../core/shared.module';
import { DownloadLockengineDesktopComponent } from './download-lockengine-desktop/download-lockengine-desktop.component';
import { DownloadLockengineMobileComponent } from './download-lockengine-mobile/download-lockengine-mobile.component';
import { LockEngineComponent } from './lock-engine/lock-engine.component';
import { ManageDeviceComponent } from './manage-device/manage-device.component';
import { ManageDeviceService } from './manage-device/manage-device.service';
import { LockEngineMobileService } from './download-lockengine-mobile/lock-engine-mobile.service';
import { MemberComponent } from './member/member.component';
import { StaffGuards } from '../security/staff.guards';
import { AccessLogComponent } from './access-log/access.log.component';
import { SuperUserGuards } from '../security/superuser.guards';

const routes: Routes = [
  // {
  //   path: 'download-desktop',
  //   component: DownloadLockengineDesktopComponent,
  //   canActivate: [],
  // },
  // {
  //   path: 'download-mobile',
  //   component: DownloadLockengineMobileComponent,
  //   canActivate: [],
  // },
  // {
  //   path: 'my-device',
  //   component: ManageDeviceComponent,
  //   canActivate: [],
  // },
  {
    path: 'access-log',
    component: AccessLogComponent,
    canActivate: [SuperUserGuards],
  },
  {
    path: 'member',
    component: MemberComponent,
    canActivate: [SuperUserGuards],
  },
  { path: '**', redirectTo: '/error/404', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    DownloadLockengineDesktopComponent,
    DownloadLockengineMobileComponent,
    LockEngineComponent,
    ManageDeviceComponent,
    MemberComponent,
    AccessLogComponent
  ],
  imports: [RouterModule.forChild(routes), SharedModule],
  exports: [],
  providers: [ManageDeviceService, LockEngineMobileService],
})
export class LockEngineModule {}
