import { Injectable, Inject } from '@angular/core';
import { ReplaySubject, Observable, forkJoin, of } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../security/auth.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Router } from '@angular/router';
import { PositionRouterService } from '../service/position-router.service';

const log = [];
@Injectable({
  providedIn: 'root',
})
export class LockEngineService {
  // public log: any[] = [];
  worker: any;
  platform: any;
  isAndroid: any;
  isDesktop: any;
  isTablet: any;
  isMobile: any;
  isWindows: any;
  isIos: any;
  isMac: any;
  userId: any;
  os: any;
  private loadedLibraries: { [url: string]: ReplaySubject<any> } = {};

  constructor(
    @Inject(DOCUMENT) private readonly document: any,
    private router: Router,
    private deviceService: DeviceDetectorService,
    private authService: AuthService,
    private http: HttpClient,
    private positionRouterService: PositionRouterService
  ) {
    this.os = ('' + this.deviceService.getDeviceInfo().os).toLowerCase();
    this.platform = (
      '' + this.deviceService.getDeviceInfo().device
    ).toLowerCase();


    this.isWindows =
      this.os.indexOf('windows') >= 0 && this.platform === 'unknown';
    this.isMac = this.os.indexOf('mac') >= 0 && this.platform === 'unknown';
    this.isAndroid =
      this.os.indexOf('android') >= 0 && this.platform.indexOf('android') >= 0;
    this.isIos = 'ios|iphone|ipod|ipad'.indexOf(this.platform);
    this.isTablet = this.deviceService.isTablet();
    this.isDesktop = this.deviceService.isDesktop();
    this.isMobile = this.deviceService.isMobile();
    this.userId = this.authService.getToken();

    // if ( this.isDesktop ) {
    // this.lazyLoadLib();
    // }
  }

  clientIsDead() {
    return typeof this.document.defaultView.deeseal === 'undefined';
  }

  lazyLoadLib(): Observable<any> {
    return forkJoin([
      this.loadScript(environment.lockEngine.desktopEndpoint),
      this.loadScript(environment.lockEngine.jqueryUrl),
    ]);
  }

  checkDesktop(): Observable<any> {
    // return this.checkDevice(environment.lockEngine.validationDesktopURL);

    const tokenId = this.document.defaultView.deeseal.getAuthToken(this.userId);

    if (!tokenId) {
      return of(undefined);
    }

    // return new Observable((subscriber) => {
    this.http
    .post(environment.lockEngine.validationDesktopURL, {
    token: tokenId,
    })
    .subscribe(
    (r: any) => {
      this.authService.setDeviceId(r.meta);
      this.authService.setDeviceStatus(r.status);
      if (r.status === 'valid') {
        this.positionRouterService.gotoPerformanceDetail();
      } else {
        this.gotoMyDevice();
      }

      // subscriber.next(r);
    },
    (e) => {
      // subscriber.error((e) => {
        if (!this.isDesktop){
          this.document.defaultView.deeseal.log('error: ' + e);
        }
      // })
    });
    // });


  }

  checkMobile() {
    // this.document.defaultView.deeseal.log(this.isAndroid);
    if (this.isAndroid) {
      log.push('1 is android');
      this.checkAndroid();
    } else {
      log.push('1 is ios');
      this.checkIOS();
    }
  }

  checkDevice(url: string): Observable<any> {
    const tokenId = this.document.defaultView.deeseal.getAuthToken(this.userId);

    if (!tokenId) {
      return of(undefined);
    }

    return new Observable((subscriber) => {
      this.http
        .post(url, {
          token: tokenId,
        })
        .subscribe(
          (r: any) => {
            this.authService.setDeviceId(r.meta);
            this.authService.setDeviceStatus(r.status);
            console.log(r);
            if (r.status === 'valid') {
              this.positionRouterService.gotoPerformanceDetail();
            } else {
              this.gotoMyDevice();
            }

            subscriber.next(r);
          },
          () =>
            subscriber.error((e) => {
              if (!this.isDesktop)
                this.document.defaultView.deeseal.log('error: ' + e);
            })
        );
    });
  }

  checkAndroid() {
    // const ref = encodeURI(environment.lockEngine.validationMobileURL);
    // window.location.href = `intent://token?userid=${this.authService.getToken()}&ref=${ref}#Intent;scheme=deeseal;action=android.intent.action.VIEW;package=com.deverhood.deeseal;end;`; //S.browser_fallback_url=
    //  this.checkDevice(environment.lockEngine.validationMobileURL);

    const tokenId = this.document.defaultView.deeseal.getAuthToken(this.userId);
    if (!this.isDesktop) {
      this.document.defaultView.deeseal.log('token ' + tokenId);
    }

    this.mobileTabletPermission(tokenId);
  }

  checkIOS() {
    // const ref = encodeURI(environment.lockEngine.validationMobileURL);
    // window.location.href = `deeseal:token?userid=${this.authService.getToken()}&ref=${ref}`;
    // tslint:disable-next-line: only-arrow-functions
    log.push('2 check ios');

    this.document.defaultView.deeseal.getAuthToken(
      this.userId,
      (tokenId: any) => {
        log.push('4 tokenId: ');
        this.mobileTabletPermission(tokenId);
      }
    );
  }

  mobileTabletPermission(tokenId: any) {
    this.http
      .post(environment.lockEngine.validationMobileURL, {
        token: tokenId,
      })
      .subscribe(
        (r: any) => {
          this.authService.setDeviceId(r.meta);
          this.authService.setDeviceStatus(r.status);

          if (r.status === 'valid') {
            // setTimeout(() => {
              this.positionRouterService.gotoPerformanceDetail();
            // }, 100);
          } else {
            this.gotoMyDevice();
          }
        },
        (e) => {
          if (!this.isDesktop) {
            console.error( e );
          }
        }
      );
  }

  public loadScript(url: string): Observable<any> {
    try {
      if (this.loadedLibraries[url]) {
        const scripts = this.document.getElementsByTagName('script');
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < scripts.length; i++) {
          if (scripts[i].src === environment.lockEngine.desktopEndpoint) {
            scripts[i].parentNode.removeChild(scripts[i]);
          }
        }
      }

      this.loadedLibraries[url] = new ReplaySubject();

      const script = this.document.createElement('script');
      script.type = 'text/javascript';
      script.async = true;
      script.async = true;
      script.src = url;
      script.onload = () => {
        this.loadedLibraries[url].next();
        this.loadedLibraries[url].complete();
      };

      this.document.body.appendChild(script);
    } catch (error) {
      console.error(error);
    }
    return this.loadedLibraries[url].asObservable();
  }

  gotoValidateDesktop() {
    this.router.navigate(['/lock-engine/download-desktop'], {});
  }
  gotoValidateMobile() {
    this.router.navigate(['/lock-engine/download-mobile'], {});
  }
  gotoMyDevice() {
    console.log('/lock-engine/my-device/');
    this.router.navigateByUrl('/lock-engine/my-device');
  }
  getLog(): any {
    return log;
  }

  addLog(msg) {
    log.push(msg);
  }
}
