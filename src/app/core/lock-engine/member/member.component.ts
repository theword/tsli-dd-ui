import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss'],
})
export class MemberComponent implements OnInit  {

  paging: { totalPages: 0, totalItems: 0,  members: []};
  member: any = {page: 0, size: 50};
  license: any = {};
  constructor(private http: HttpClient, private translate: TranslateService) {}

  ngOnInit(): void {
    this.search();
    this.getLicense();
  }

  search() {
    console.log(this.member );
    this.http
      .post(
        `${environment.apiUrl}/lock-engine/searchUser`,
        {},
        { params: this.member }
      )
      .subscribe((r: any) => {
        console.log(r);
        this.paging = r;

        // this.members = r?.members;
      });
  }

  getLicense() {
    this.http
      .get(`${environment.apiUrl}/lock-engine/getLicense`)
      .subscribe((r: any) => {
        this.license = r;
        console.log(r);
      });
  }
  isActiveStatus(status) {
    return 'A' === status;
  }

  userTypeDesc(userType) {
    return 'U' === userType
      ? this.translate.instant('memmber.type.user')
      : this.translate.instant('memmber.type.producer');
  }


  previous() {
    if ( this.member.page === 0){
      return;
    }else{
      this.member.page--;
    }
    this.search();
  }

  next() {
    if ( this.member.page === this.paging?.totalPages - 1 ){
      return;
    }else{
      this.member.page++;
    }
    this.search();
  }

  firstPage(){
    this.member.page = 0;
    this.search();
  }

  endPage(){
    this.member.page = this.paging.totalPages - 1;
    this.search();
  }

  itemInfo(): string {
    const start = this.printSequense(0);
    const end = start + this.member.size - 1;
    return '' + start + '-' + (end > this.paging?.totalItems ? this.paging?.totalItems : end ) + ' of ' + this.paging?.totalItems;
  }


  printSequense(seq) {
   return (this.member.page * this.member.size) + 1 + seq;
  }
}
