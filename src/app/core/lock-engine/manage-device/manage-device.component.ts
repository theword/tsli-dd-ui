import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../security/auth.service';
import { ManageDeviceService } from './manage-device.service';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { PositionRouterService } from '../../service/position-router.service';

@Component({
  selector: 'app-manage-device',
  templateUrl: './manage-device.component.html',
  styleUrls: ['./manage-device.component.scss'],
})
export class ManageDeviceComponent implements OnInit {
  deviceId: any;
  devices: any = new Array();
  deviceName: '';
  constructor(
    private authService: AuthService,
    private translate: TranslateService,
    private manageDeviceService: ManageDeviceService,
    private positionRouterService: PositionRouterService
  ) {}

  ngOnInit(): void {
    this.deviceId = this.authService.getDeviceId();

    this.manageDeviceService.getDeviceInfo(this.deviceId).subscribe( (r: any) => {
      if ( r?.deviceName ){
        this.deviceName = r.deviceName;
      }
    });

    this.manageDeviceService.devicesData.subscribe((r) => {
      setTimeout(() => {}, 1000);
      this.devices = r;
    });
  }

  deleteDevice(deviceId) {
    Swal.fire({
      title: this.translate.instant('alert.delete.title'),
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: this.translate.instant('alert.delete.title.cancel'),
      confirmButtonText: this.translate.instant('alert.delete.title.confirm'),
    }).then((result) => {
      if (result.value) {
        this.manageDeviceService.deleteDevice(deviceId).subscribe(
          (r) => {
            Swal.fire(
              '',
              this.translate.instant('alert.delete.title.success'),
              'success'
            );
            this.manageDeviceService.loadDevices();
          },
          (e) => {}
        );
      }
    });
  }

  isRegistered(deviceId): any {
    return this.devices.some((device) => {
      // tslint:disable-next-line:no-string-literal
      return deviceId === device['deviceId'];
    });
  }

  isHighLight(deviceId): any {
    return deviceId === this.deviceId;
  }

  saveDevice() {

    Swal.fire({
      title: this.translate.instant('alert.save.title'),
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: this.translate.instant('alert.save.title.cancel'),
      confirmButtonText: this.translate.instant('alert.save.title.confirm'),
    }).then((result) => {
      if (result.value) {
        this.manageDeviceService.saveDevice(this.deviceId, this.deviceName ).subscribe(
          (r) => {
            console.log(r);
            if (r?.status === true ){
              Swal.fire(
                '',
                this.translate.instant('alert.save.title.success'),
                'success'
              );
              this.manageDeviceService.loadDevices();
            }else {
              Swal.fire(
                '',
                this.translate.instant('alert.save.title.fail'),
                'error'
              );
            }
          },
          (e) => {}
        );
      }
    });
  }

  goHome() {
    this.positionRouterService.gotoPerformanceDetail();
  }
}
