import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DeviceDetectorService } from 'ngx-device-detector';

@Injectable()
export class ManageDeviceService {
  private dataSource: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  devicesData = this.dataSource.asObservable();

  constructor(
    private http: HttpClient,
    private deviceService: DeviceDetectorService
  ) {
    this.loadDevices();
  }

  getDeviceInfo( deviceId ): Observable<any> {
    return this.http.get(`${environment.apiUrl}/lock-engine/devicesInfo`, { params: {'deviceId': deviceId} });
  }
  getUserDevices(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/lock-engine/devices`);
  }

  deleteDevice(deviceId): Observable<any> {
    return this.http.delete(
      `${environment.apiUrl}/lock-engine/delete-devices/${deviceId}`
    );
  }

  saveDevice(deviceId: string, deviceName: string): Observable<any> {
    return this.http.post(
      `${environment.apiUrl}/lock-engine/save/device`,
      {
        'deviceId': deviceId,
        'deviceName': deviceName
      },
      {
        params: {
          isMobile: '' + !this.deviceService.isDesktop,
        },
      }
    );
  }

  loadDevices() {
    this.getUserDevices().subscribe((r) => {
      this.dataSource.next(r);
    });
  }
}
