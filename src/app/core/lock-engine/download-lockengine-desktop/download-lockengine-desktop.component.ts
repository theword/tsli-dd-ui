import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { LockEngineService } from '../lock.manager.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { AuthService } from '../../security/auth.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { PositionRouterService } from '../../service/position-router.service';

@Component({
  selector: 'app-download-lockengine-desktop',
  templateUrl: './download-lockengine-desktop.component.html',
  styleUrls: ['./download-lockengine-desktop.component.scss'],
})
export class DownloadLockengineDesktopComponent implements OnInit, OnDestroy {
  worker: any;
  isStarted = false;
  imageUrl: any;
  linkDownload: any;
  clientIsDead = false;

  constructor(
    private lockEngineService: LockEngineService,
    private translate: TranslateService,
    private positionRouterService: PositionRouterService,
  ) {}

  ngOnInit(): void {

    this.clientIsDead = this.lockEngineService.clientIsDead();

    if (this.lockEngineService.clientIsDead()) {

      if (this.lockEngineService.isWindows) {
        this.imageUrl = this.translate.instant(`lock.engine.download.pic.win`);
        this.linkDownload = environment.lockEngine.programWinUrl;
      } else if (this.lockEngineService.isMac) {
        this.imageUrl = this.translate.instant(`lock.engine.download.pic.mac`);
        this.linkDownload = environment.lockEngine.programMacUrl;
      }

    }else{

    }

    this.callClientDesktopMode();
  }

  ngOnDestroy() {
    if (this.worker) {
      clearInterval(this.worker);
    }
  }

  startDownload() {
    this.isStarted = true;
    window.open(
      this.linkDownload,
      '_blank' // <- This is what makes it open in a new window.
    );
  }

  callClientDesktopMode() {
    if (this.lockEngineService.clientIsDead()) {
      this.worker = setInterval(() => {
        this.callable();
      }, 3000);
    } else {
      this.callable();
    }
  }

  callable() {
    this.lockEngineService
      .loadScript(environment.lockEngine.desktopEndpoint)
      .subscribe((r) => {
        if (!this.lockEngineService.clientIsDead()) {
          this.lockEngineService
            .checkDesktop();
            // .subscribe((_) => {
            //   clearInterval(this.worker);
            //   this.positionRouterService.gotoPerformanceDetail();
            // });
        }
      });
  }
}
