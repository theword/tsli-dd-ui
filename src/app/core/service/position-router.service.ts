import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../security/auth.service';

@Injectable({
  providedIn: 'root',
})
export class PositionRouterService {

    GM = 'GM';
    AVP = 'AVP,VP';
    AL = 'AL';
    PRODUCER = 'GM,AL,AVP,VP,ZONE';
    STAFF = 'STAFF,ZONE,COMPANY,CHANNEL';
    AGENT = 'AG,AGENT';

  constructor(private router: Router, private authService: AuthService) {

  }

  gotoPerformanceDetail() {
    let url: string;
    if (this.isStaff()) {
      url = '/dashboard/staff';
    }else if ( this.isproducer() ) {
      url = '/dashboard/producer';
    }else if (this.isAgent()) {
        url = '/dashboard/agent';
        // if (this.authService.getLoginedName() != sessionStorage.getItem('agentCodeSelected')) {
        //     sessionStorage.setItem('agentCodeSelected', this.authService.getLoginedName());
        // }
    }else{
        url = '/error/404';
    }
    this.router.navigateByUrl(url);
    // window.location.reload();
  }

  isproducer() {
    const position =  this.authService.getPosition();
    return this.PRODUCER.includes(position);
  }

  isAvp() {
    const position =  this.authService.getPosition();
    return this.AVP.includes(position);
  }
  isAl() {
    const position =  this.authService.getPosition();
    return this.AL.includes(position);
  }
  isGm() {
    const position =  this.authService.getPosition();
    return this.GM.includes(position);
  }
  isAgent() {
    const position =  this.authService.getPosition();
    return this.AGENT.includes(position);
  }
  isStaff() {
    const position =  this.authService.getPosition();
    return this.STAFF.includes(position);
  }

  isSuperUser(): boolean{
    return this.authService.getIsSuperUser() == "true";
  }
}
