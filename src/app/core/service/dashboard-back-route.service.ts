import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../security/auth.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardBackRouteService {

  constructor(
    private router: Router,
    private authService : AuthService
  ) { }

  backHome(userRole, agentCode) {
    console.log('BACK HOME: ', userRole, ' | ', agentCode);
    let redirectionBasedOnRole = '';
    sessionStorage.setItem('agentCodeSelected', agentCode);
    sessionStorage.setItem('agentLevelSelected', userRole);
    switch (userRole) {
      case 'GM': case 'AVP': case 'AV' : case 'AL': // Set as GM - AL
        redirectionBasedOnRole = '/dashboard/producer/';
          // window.location.reload();
        break;
      case 'AGENT': case 'AG': // Set as AG
        redirectionBasedOnRole = '/dashboard/agent/';
        break;

      case 'ZONE': case 'COMPANY':
        sessionStorage.setItem('channelFilterSelected', 'COMPANY');
        sessionStorage.setItem('zoneFilterSelected', 'COMPANY');
        redirectionBasedOnRole = '/dashboard/staff/';
        break;
      default:
        redirectionBasedOnRole = '/error/404';
        break;
    }
    this.router.navigate([redirectionBasedOnRole]);
  }

  onClickBack(){
    let agentCodeStored = sessionStorage.getItem('agentCodeStored') != null ? JSON.parse(sessionStorage.getItem('agentCodeStored')) : [this.authService.getLoginedName()];
    let agentLevelStored = sessionStorage.getItem('agentLevelStored') != null ? JSON.parse(sessionStorage.getItem('agentLevelStored')) : [this.authService.getPosition()];
    let agentCode = this.authService.getLoginedName();
    let userRole = this.authService.getPosition();
    if(agentCodeStored.length > 0){
      agentCode = agentCodeStored.pop();
      sessionStorage.setItem('agentCodeStored', JSON.stringify(agentCodeStored));
    }
    if(agentLevelStored.length > 0){
      userRole = agentLevelStored.pop();
      sessionStorage.setItem('agentLevelStored', JSON.stringify(agentLevelStored));
    }
    sessionStorage.setItem('agentCodeSelected', agentCode);
    sessionStorage.setItem('agentLevelSelected', userRole);
  }

}
