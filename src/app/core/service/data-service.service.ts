import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  constructor(private http: HttpClient) { }

  getClosYmInfo() {
    return this.http.get(`${environment.apiUrl}/info/getInfoClosYm`);
  }


  getParameter(paramName) {
    return this.http.get(`${environment.apiUrl}/info/get-parameter`, 
    {
      responseType: "text",
      params:  {  paramName:paramName }
    });
  }

  // ##### AGENT LEVEL ##### //

  // agent structure - findByAgentCode :: agent/structure/
  getAgentStructure(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/structure`, data);
  }
  // agent overview (performance current month) - findByAgentCode and AgentLevel
  getAgentPerformance(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/performance`, data);
  }
  // agent overview (performance current month) - findByAgentCode and AgentLevel
  getAgentOverview(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/performance/currentmonth`, data);
  }
  // agent overview under AL as AG - findByUnderAlOverview
  getAgentOverview_underAL(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-al`, data);
  }
  // agent overview under AVP as AL and AG - findByUnderAvpOverview
  getAgentOverview_underAVP(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-avp`, data);
  }
  // agent overview under GM as AVP and AL and AG - findByUnderGmOverview
  getAgentOverview_underGM(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-gm`, data);
  }
  // agent overview under ZONE as GM and AVP and AL and AG - findByUnderZoneOverview
  getAgentOverview_underZONE(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-zone`, data);
  }

  // ====================================== //
  //   PHASE 2: Team detail for Comp.&Ch.   //
  // ====================================== //

  // agent overview under COMPANY as GM and AVP and AL - findByUnderCompanyOverviewProducer
  getAgentOverview_underCOMPANY_producer(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-company-producer`, data);
  }

  // agent overview under COMPANY as GM and AVP and AL - findByUnderCompanyOverviewProducer
  getAgentOverview_underCOMPANY_agentPC(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-company-agent-pc`, data);
  }
  getAgentOverview_underCOMPANY_agentACTIVE(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-company-agent-active`, data);
  }
  getAgentOverview_underCOMPANY_agentRYP(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-company-agent-ryp`, data);
  }

  // agent overview under CHANNEL - SFC as GM and AVP and AL - findByUnderChannelSfcOverviewProducer
  getAgentOverview_underCHANNEL_SFC_producer(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-channel-sfc-producer`, data);
  }
  getAgentOverview_underCHANNEL_SFC_agentPC(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-channel-sfc-agent-pc`, data);
  }
  getAgentOverview_underCHANNEL_SFC_agentACTIVE(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-channel-sfc-agent-active`, data);
  }
  getAgentOverview_underCHANNEL_SFC_agentRYP(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-channel-sfc-agent-ryp`, data);
  }

  // agent overview under CHANNEL - GA as GM and AVP and AL - findByUnderChannelGaOverviewProducer
  getAgentOverview_underCHANNEL_GA_producer(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-channel-ga-producer`, data);
  }
  getAgentOverview_underCHANNEL_GA_agentPC(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-channel-ga-agent-pc`, data);
  }
  getAgentOverview_underCHANNEL_GA_agentACTIVE(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-channel-ga-agent-active`, data);
  }
  getAgentOverview_underCHANNEL_GA_agentRYP(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-channel-ga-agent-ryp`, data);
  }

  // ====================================== //
  // CR-Add new Channel: Improve teamDetail //
  // ====================================== //

  getAgentOverview_underChannel_producer(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-channel-producer`, data);
  }
  getAgentOverview_underChannel_agentPc(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-channel-agent-pc`, data);
  }
  getAgentOverview_underChannel_agentActive(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-channel-agent-active`, data);
  }
  getAgentOverview_underChannel_agentRyp(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/overview/under-line/by-channel-agent-ryp`, data);
  }

  // ====================================== //
  // END PHASE 2: Team detail for Comp.&Ch. //
  // ====================================== //

  // agent YTD KPI PC - findByAgentCode and AgentLevel
  getAgentYtdPc(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/ytd/pc`, data);
  }
  // agent YTD KPI ACTIVE - findByAgentCode and AgentLevel
  getAgentYtdActive(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/ytd/active`, data);
  }
  // agent YTD KPI NC - findByAgentCode and AgentLevel
  getAgentYtdNc(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/ytd/nc`, data);
  }
  // agent YTD KPI RYP - findByAgentCode and AgentLevel
  getAgentYtdRyp(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/ytd/ryp`, data);
  }


  // agent YTD all KPI - findByAgentCode and AgentLevel
  getAgentYtdAllKPI(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/ytd/currentyear`, data);
  }
  // agent YTD all KPI - findByAgentCode and AgentLevel
  getAgentYtdCurrentAndLastYear(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/ytd/currentandlastyear`, data);
  }
  // agent target - findByAgentCode
  getAgentTarget(data: object) {
    return this.http.post(`${environment.apiUrl}/agent/target`, data);
  }

  // ##### COMPANY LEVEL ##### //
  // get company overview, all of channel and zone performance current month
  getCompanyAllOverview() {
    return this.http.post(`${environment.apiUrl}/company/overview/all`, null);
  }
  // get company or channel or zone performance is selected
  getCompanyPerformance() {
    return this.http.post(`${environment.apiUrl}/company/performance/company`, null);
  }
  getChannelPerformance(data) {
    return this.http.post(`${environment.apiUrl}/company/performance/channel`, data);
  }
  getZonePerformance(data) {
    return this.http.post(`${environment.apiUrl}/company/performance/zone`, data);
  }
  // get company or channel or zone YTD is selected
  getCompanyYTD(data) {
    return this.http.post(`${environment.apiUrl}/company/ytd`, data);
  }
  // get company or channel or zone Target is selected
  getCompanyTarget(data) {
    return this.http.post(`${environment.apiUrl}/company/target`, data);
  }
  // get company or channel or zone retention MTD
  getCompanyRetention() {
    return this.http.post(`${environment.apiUrl}/company/retention/company`, null);
  }
  getChannelRetention(data) {
    return this.http.post(`${environment.apiUrl}/company/retention/channel`, data);
  }
  getZoneRetention(data) {
    return this.http.post(`${environment.apiUrl}/company/retention/zone`, data);
  }
  // get company or channel retention YTD
  getCompanyYtdRetention(data) {
    return this.http.post(`${environment.apiUrl}/company/ytd/retention/all`, data);
  }
  getChannelYtdRetention(data) {
    return this.http.post(`${environment.apiUrl}/company/ytd/retention/channel`, data);
  }
  getZoneYtdRetention(data) {
    return this.http.post(`${environment.apiUrl}/company/ytd/retention/zone`, data);
  }

  // get buttonName based on agent level
  getButtonName_agentLevel() {
    return this.http.post(`${environment.apiUrl}/utilities/button-agent-name`, null);
  }

  // ====== ADD NEW PHASE 2 PerformanceDaily ====== //
  getPerformanceDaily(data) {
    return this.http.post(`${environment.apiUrl}/agent/performance-daily`, data);
  }
  getPerformanceDailyCompany(data) {
    return this.http.post(`${environment.apiUrl}/agent/performance-daily-company`, data);
  }
  getPerformanceDailyChannel(data) {
    return this.http.post(`${environment.apiUrl}/agent/performance-daily-channel`, data);
  }
  getPerformanceDailyZone(data) {
    return this.http.post(`${environment.apiUrl}/agent/performance-daily-zone`, data);
  }
  getPerformanceWeeklyCompany(data) {
    return this.http.post(`${environment.apiUrl}/performance-weekly/company`, data);
  }
  getPerformanceWeeklyChannel(data) {
    return this.http.post(`${environment.apiUrl}/performance-weekly/channel`, data);
  }
  getPerformanceWeeklyZone(data) {
    return this.http.post(`${environment.apiUrl}/performance-weekly/zone`, data);
  }
  // ====== END NEW PHASE 2 PerformanceDaily ====== //

  // ====== ADD NEW PHASE 2 PolicyDetail - KPIsubDetail ====== //
  postPolicyPcAgent(data) {
    return this.http.post(`${environment.apiUrl}/policy/pc/agent`, data);
  }
  postPolicyPcAl(data) {
    return this.http.post(`${environment.apiUrl}/policy/pc/al`, data);
  }
  postPolicyPcAvp(data) {
    return this.http.post(`${environment.apiUrl}/policy/pc/avp`, data);
  }
  postPolicyPcGm(data) {
    return this.http.post(`${environment.apiUrl}/policy/pc/gm`, data);
  }
  postPolicyPcCompany(data) {
    return this.http.post(`${environment.apiUrl}/policy/pc/company`, data);
  }
  postPolicyPcChannel(data) {
    return this.http.post(`${environment.apiUrl}/policy/pc/channel`, data);
  }
  postPolicyPcZone(data) {
    return this.http.post(`${environment.apiUrl}/policy/pc/zone`, data);
  }

  postPolicyNcAl(data) {
    return this.http.post(`${environment.apiUrl}/policy/nc/al`, data);
  }
  postPolicyNcAvp(data) {
    return this.http.post(`${environment.apiUrl}/policy/nc/avp`, data);
  }
  postPolicyNcGm(data) {
    return this.http.post(`${environment.apiUrl}/policy/nc/gm`, data);
  }
  postPolicyNcCompany(data) {
    return this.http.post(`${environment.apiUrl}/policy/nc/company`, data);
  }
  postPolicyNcChannel(data) {
    return this.http.post(`${environment.apiUrl}/policy/nc/channel`, data);
  }
  postPolicyNcZone(data) {
    return this.http.post(`${environment.apiUrl}/policy/nc/zone`, data);
  }

  postPolicyRypAgent(data) {
    return this.http.post(`${environment.apiUrl}/policy/ryp/agent`, data);
  }
  postPolicyRypAl(data) {
    return this.http.post(`${environment.apiUrl}/policy/ryp/al`, data);
  }
  postPolicyRypAvp(data) {
    return this.http.post(`${environment.apiUrl}/policy/ryp/avp`, data);
  }
  postPolicyRypGm(data) {
    return this.http.post(`${environment.apiUrl}/policy/ryp/gm`, data);
  }
  postPolicyRypCompany(data) {
    return this.http.post(`${environment.apiUrl}/policy/ryp/company`, data);
  }
  postPolicyRypChannel(data) {
    return this.http.post(`${environment.apiUrl}/policy/ryp/channel`, data);
  }
  postPolicyRypZone(data) {
    return this.http.post(`${environment.apiUrl}/policy/ryp/zone`, data);
  }

  postPolicyRetentionCompany(data) {
    return this.http.post(`${environment.apiUrl}/policy/retention/company`, data);
  }
  postPolicyRetentionChannel(data) {
    return this.http.post(`${environment.apiUrl}/policy/retention/channel`, data);
  }
  postPolicyRetentionZone(data) {
    return this.http.post(`${environment.apiUrl}/policy/retention/zone`, data);
  }

  postPolicyRypCountByStatus(data) {
    return this.http.post(`${environment.apiUrl}/policy/ryp/count-ryp-policy-by-status`, data);
  }
  postPolicyRypCompanyPagination(data) {
    return this.http.post(`${environment.apiUrl}/policy/ryp/company-pagination`, data);
  }
  postPolicyRypChannelPagination(data) {
    return this.http.post(`${environment.apiUrl}/policy/ryp/channel-pagination`, data);
  }
  postSearchPolicyRyp(data) {
    return this.http.post(`${environment.apiUrl}/policy/ryp/search-pagination`, data);
  }
  postSearchPolicyPC(data) {
    return this.http.post(`${environment.apiUrl}/policy/pc/search-pagination`, data);
  }
  postNcMemoDetail(data) {
    return this.http.post(`${environment.apiUrl}/policy/nc/memo`, data);
  }
  // ====== END NEW PHASE 2 PolicyDetail - KPIsubDetail ====== //

  // ====================================== //
  //   PHASE 3: Persistency   //
  // ====================================== //
  postPersistencySummary(data) {
    return this.http.post(`${environment.apiUrl}/agent/persistency/summary`, data);
  }
  postPersistencyDetailAg(data) {
    return this.http.post(`${environment.apiUrl}/agent/persistency/detail-agent`, data);
  }
  postPersistencyDetailAl(data) {
    return this.http.post(`${environment.apiUrl}/agent/persistency/detail-al`, data);
  }
  postPersistencyDetailAvp(data) {
    return this.http.post(`${environment.apiUrl}/agent/persistency/detail-avp`, data);
  }
  postPersistencyDetailGm(data) {
    return this.http.post(`${environment.apiUrl}/agent/persistency/detail-gm`, data);
  }
  // Search Persistency to-collect
  searchPersistencyDetailAg(data) {
    return this.http.post(`${environment.apiUrl}/agent/persistency/detail-agent-search`, data);
  }
  searchPersistencyDetailAl(data) {
    return this.http.post(`${environment.apiUrl}/agent/persistency/detail-al-search`, data);
  }
  searchPersistencyDetailAvp(data) {
    return this.http.post(`${environment.apiUrl}/agent/persistency/detail-avp-search`, data);
  }
  searchPersistencyDetailGm(data) {
    return this.http.post(`${environment.apiUrl}/agent/persistency/detail-gm-search`, data);
  }

  // ====================================== //
  //   PHASE 3: Edit Year Target   //
  // ====================================== //
  postTargetAgentListAg(data) {
    return this.http.post(`${environment.apiUrl}/edit/target-list-agent`, data);
  }
  postTargetAgentListAl(data) {
    return this.http.post(`${environment.apiUrl}/edit/target-list-al`, data);
  }
  postTargetAgentListAvp(data) {
    return this.http.post(`${environment.apiUrl}/edit/target-list-avp`, data);
  }
  postTargetAgentListGm(data) {
    return this.http.post(`${environment.apiUrl}/edit/target-list-gm`, data);
  }
  postTargetAgentListZone(data) {
    return this.http.post(`${environment.apiUrl}/edit/target-list-zone`, data);
  }
  // Search EditYear Target AgentList 
  searchTargetAgentListAg(data) {
    return this.http.post(`${environment.apiUrl}/edit/target-list-search-ag`, data);
  }
  searchTargetAgentListAl(data) {
    return this.http.post(`${environment.apiUrl}/edit/target-list-search-al`, data);
  }
  searchTargetAgentListAvp(data) {
    return this.http.post(`${environment.apiUrl}/edit/target-list-search-avp`, data);
  }
  searchTargetAgentListGm(data) {
    return this.http.post(`${environment.apiUrl}/edit/target-list-search-gm`, data);
  }
  searchTargetAgentListZone(data) {
    return this.http.post(`${environment.apiUrl}/edit/target-list-search-zone`, data);
  }
  // Get Target Year by AgentCode
  postTargetYearByAgentCode(data) {
    return this.http.post(`${environment.apiUrl}/edit/target-year-by-agent`, data);
  }
  // Update PC Target Year 
  postUpdateTargetYear(data) {
    return this.http.post(`${environment.apiUrl}/edit/update-pc-target-year`, data);
  }
  // Update All Target Year 
  postUpdateAllTargetYear(data) {
    return this.http.post(`${environment.apiUrl}/edit/update-all-target-year`, data);
  }

  // ====================================== //
  // PHASE 3: Promotion Result and Potential//
  // ====================================== //
  postPromotionList(data) {
    return this.http.post(`${environment.apiUrl}/agent/promotion-list`, data);
  }
  postPromotionDetail(data) {
    return this.http.post(`${environment.apiUrl}/agent/promotion-detail`, data);
  }
  postUploadPromotion(data) {
    return this.http.post(`${environment.apiUrl}/agent/upload-promotion`, data);
  }
  getPromotionConfigList(data) {
    return this.http.post(`${environment.apiUrl}/super-admin/promotion-config-list`, data);
  }
  delPromotionConfigList(data) {
    return this.http.post(`${environment.apiUrl}/super-admin/promotion-config-delete`, data);
  }

  // ====================================== //
  // PHASE 3: TEAMDETAIL BY SON             //
  // ====================================== //
  postTeamdetailBySon(data) {
    return this.http.post(`${environment.apiUrl}/agent/teamdetail-agent-by-son`, data);
  }
  postTeamdetailYtdBySon(data) {
    return this.http.post(`${environment.apiUrl}/agent/teamdetail-agent-ytd-by-son`, data);
  }

  // ====================================== //
  // PHASE 3: TEAMDETAIL BY AGENT YTD       //
  // ====================================== //
  postTeamdetailYtdByAgent(data) {
    return this.http.post(`${environment.apiUrl}/agent/teamdetail-agent-ytd`, data);
  }
  postTeamdetailYtdByZone(data) {
    return this.http.post(`${environment.apiUrl}/agent/teamdetail-zone-ytd`, data);
  }
  postTeamdetailYtdByChannel(data) {
    return this.http.post(`${environment.apiUrl}/agent/teamdetail-channel-ytd`, data);
  }
  postTeamdetailYtdByCompany(data) {
    return this.http.post(`${environment.apiUrl}/agent/teamdetail-company-ytd`, data);
  }

  // ====================================== //
  // PHASE 3: KPI DETAIL WHOLEGROUP         //
  // ====================================== //
  postKpiDetailWholeGroupMtd(data) {
    return this.http.post(`${environment.apiUrl}/agent/kpidetail-wholegroup-mtd`, data);
  }
  postKpiDetailWholeGroupYtd(data) {
    return this.http.post(`${environment.apiUrl}/agent/kpidetail-wholegroup-ytd`, data);
  }

  // ====================================== //
  // PHASE 3: COMPENSATION                  //
  // ====================================== //
  postCompensationConfig(data) {
    return this.http.post(`${environment.apiUrl}/agent/compensation-config`, data);
  }
  postCompensationConfigWithPc(data) {
    return this.http.post(`${environment.apiUrl}/agent/compensation-config-with-pc`, data);
  }
  postAgentPerformanceQuarter(data) {
    return this.http.post(`${environment.apiUrl}/agent/agent-performance-quarter`, data);
  }
  postValidationConfig(data) {
    return this.http.post(`${environment.apiUrl}/agent/validation-config`, data);
  }
  postPromoteConfig(data) {
    return this.http.post(`${environment.apiUrl}/agent/promote-config`, data);
  }
  postCompensationOvConfig(data) {
    return this.http.post(`${environment.apiUrl}/agent/compensation-config-ov`, data);
  }

  // ====================================== //
  // DD & WAR CR                            //
  // ====================================== //
  getUserRolesByUsername(data) {
    return this.http.post(`${environment.apiUrl}/user/roles/roles-by-username`, data);
  }
  getUploadFileHistory(type) {
    return this.http.get(`${environment.apiUrl}/upload-file-hist/get-upload-file-history?type=${type}`);
  }

  /*postUploadActivity(data) {
    return this.http.post(`${environment.apiUrl}/agent/upload-Activity`, data);
  }*/
}
