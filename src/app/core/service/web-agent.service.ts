import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from '../security/auth.service';

@Injectable({
    providedIn: 'root'
})
export class WebAgentService {
    private loginedName = '';
    constructor(
        private http: HttpClient
        , private auth: AuthService
    ) {
        this.loginedName = auth.getLoginedName();

    }

    searchSuperUser() {
        return this.http.get(`${environment.apiWebAgentUrl}/nonsecure/searchSuperUser?username=VISANU`);
    }
    searchEDocumentsByPid(data: any) {
        return this.http.get(`${environment.apiWebAgentUrl}/public/search-edocuments?key=${data}`);
    }
    searchDocument(data: Object) {
        return this.http.get(`${environment.apiWebAgentUrl}/document/search-documents?agentCode=${data['agentCode']}&agentFilter=${data['agentFilter']}&policyCode=${data['policyCode']}&fName=${data['fName']}&lName=${data['lName']}&docCategory=${data['docCategory']}&curPage=${data['curPage']}`);
    }
    getListDocCategory() {
        return this.http.get(`${environment.apiWebAgentUrl}/document/list-doc-category`);
    }
    searchInsurance(params: any) {
        // return this.http.get(`${environment.apiWebAgentUrl}/insurance/search-insurance?agentCode=${data['agentCode']}&policyCode=${data['policyCode']}&policyId=${data['policyId']}&fName=${data['fName']}&lName=${data['lName']}&pid=${data['pid']}&curPage=${data['curPage']}`);
        return this.http.get(`${environment.apiWebAgentUrl}/insurance/search-insurance`, { params: params });
    }
    getPolicyDetail(data: Object) {
        return this.http.get(`${environment.apiWebAgentUrl}/policy/ins-policy-detail?agentCode=${data['agentCode']}&policyCode=${data['policyCode']}&policyId=${data['policyId']}`);
    }

    getActivityList(data: Object) {
        return this.http.get(`${environment.apiWebAgentUrl}/activity/activity-list?webRole=${data['webRole']}&activityType=${data['activityType']}&provinceId=${data['provinceId']}&keywords=${data['keywords']}&curPage=${data['curPage']}&sortingType=${data['sortingType']}`);
    }
    getActivityInfoById(data: Object) {
        return this.http.get(`${environment.apiWebAgentUrl}/activity/activity-info-by-id?activityId=${data['activityId']}`);
    }
    getActivityDetail(data: Object) {
        return this.http.get(`${environment.apiWebAgentUrl}/activity/activity-detail?activityId=${data['activityId']}`);
    }
    getActivityDetailList(data: Object) {
        return this.http.get(`${environment.apiWebAgentUrl}/activity/activity-detail-list?activityId=${data['activityId']}&keywords=${data['keywords']}&curPage=${data['curPage']}`);
    }
    deleteActivityDetail(data: Object) {
        return this.http.get(`${environment.apiWebAgentUrl}/update-activity/delete?activityId=${data['activityId']}`);
    }

    getProvinceList() {
        return this.http.get(`${environment.apiWebAgentUrl}/activity/province-list`);
    }
    getProvinceListWAll() {
        return this.http.get(`${environment.apiWebAgentUrl}/activity/province-list-w-all`);
    }
    getActivityTypeList() {
        return this.http.get(`${environment.apiWebAgentUrl}/activity/activity-type-list`);
    }
    getActivitySubTypeList(activityType) {
        return this.http.get(`${environment.apiWebAgentUrl}/activity/activity-sub-type-list?activityType=${activityType}`);
    }

    getAgentDetail(data: Object) {
        return this.http.get(`${environment.apiWebAgentUrl}/agent/search-agent?agentCode=${data['agentCode']}`);
    }
    getRecommenderAgentDetail(data: Object) {
        return this.http.get(`${environment.apiWebAgentUrl}/agent/search-recommender-agent?agentCode=${data['agentCode']}`);
    }
    getAgentDetailByPid(data: Object) {
        return this.http.get(`${environment.apiWebAgentUrl}/agent/search-agent-by-pid?pid=${data['pid']}&activityId=${data['activityId']}&activityType=${data['activityType']}`);
    }
    checkPayinNumber(data: Object) {
        return this.http.get(`${environment.apiWebAgentUrl}/agent/check-payin-num?payinNum=${data['payinNum']}&activityId=${data['activityId']}&registerId=${data['registerId']}&isAdmin=${data['isAdmin']}`);
    }
    getCheckDocumentList(data: Object) {
        return this.http.get(`${environment.apiWebAgentUrl}/checkdocument/document-list?categoryTypeName=${data['categoryTypeName']}&policyCode=${data['policyCode']}&fName=${data['fName']}&lName=${data['lName']}&curPage=${data['curPage']}`);
    }

    eRegisterActivity(data: Object) {
        return this.http.post(`${environment.apiWebAgentUrl}/register/register-activity`, data);
    }
    eRegisterUpdateAgent(data: Object) {
        return this.http.post(`${environment.apiWebAgentUrl}/register/update-agent-activity`, data);
    }

    downloadEdocument(filePath): any {
        filePath = filePath?.replace("\\", "/");
        return this.http.get(`${environment.apiWebAgentUrl}/public/download?filePath=${filePath}`, { responseType: 'blob' });
    }
    // DOWNLOAD FILE //
    downloadDocument(policyCode): any {
        return this.http.get(`${environment.apiWebAgentUrl}/insurance/download?policyCode=${policyCode}`, { responseType: 'blob' });
    }
    downloadMemoDocument(fileLocation): any {
        return this.http.get(`${environment.apiWebAgentUrl}/document/download?fileName=${fileLocation}`, { responseType: 'blob' });
    }

    insertActivityDetail(data: Object) {
        return this.http.post(`${environment.apiWebAgentUrl}/update-activity/insert-detail`, data);
    }
    updateActivityDetail(data: Object) {
        return this.http.post(`${environment.apiWebAgentUrl}/update-activity/update-detail`, data);
    }
    uploadImage(data: Object) {
        return this.http.post(`${environment.apiWebAgentUrl}/register/upload-file`, data);
    }

    getReportClosYM() {
        return this.http.get(`${environment.apiWebAgentUrl}/report/get-report-closym`);
    }
    getReportMaster() {
        return this.http.get(`${environment.apiWebAgentUrl}/report/get-report-master`);
    }

    getBankMaster() {
        return this.http.get(`${environment.apiWebAgentUrl}/register/get-bank-master`);
    }
    getGenderMaster() {
        return this.http.get(`${environment.apiWebAgentUrl}/register/get-gender-master`);
    }
    getEducationMaster() {
        return this.http.get(`${environment.apiWebAgentUrl}/register/get-education-master`);
    }
    getTitleMaster() {
        return this.http.get(`${environment.apiWebAgentUrl}/register/get-title-master`);
    }

    searchReport(params: any) {
        return this.http.get(`${environment.apiWebAgentUrl}/report/search-report-metadata`, { params: params });
    }


    flagDeleteERegister(registerId, activityId) {
        return this.http.get(`${environment.apiWebAgentUrl}/register/flag-to-delete?editor=${this.loginedName}&registerId=${registerId}&activityId=${activityId}`);
    }
    downloadReportFile(reportPath): any {
        reportPath = ("" + reportPath).replace("\\", "/");
        return this.http.get(`${environment.apiWebAgentUrl}/report/download-report?reportPath=${reportPath}`, { responseType: 'blob' });
    }

    getAgentChild(parentAgentCode) {
        return this.http.get(`${environment.apiWebAgentUrl}/insurance/get-agent-child?agentCode=${parentAgentCode}`);
    }

    getAgentFilterSearchDocument(parentAgentCode) {
        return this.http.get(`${environment.apiWebAgentUrl}/document/get-agent-filter-search-document?agentCode=${parentAgentCode}`);
    }

    postUploadActivity(data: Object) {
        return this.http.post(`${environment.apiWebAgentUrl}/agent/upload-Activity`, data);
    }

    getUploadHistoryByType(type: String) {
        return this.http.get(`${environment.apiWebAgentUrl}/agent/get-upload-file-history?type=${type}`);
    }

  postUploadLicExam(data: Object) {
    return this.http.post(`${environment.apiWebAgentUrl}/agent/upload-LICExam`, data);
  }

  postUploadResult(data: Object) {
    return this.http.post(`${environment.apiWebAgentUrl}/agent/upload-result`, data);
  }
 
}