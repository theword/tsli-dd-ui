import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CheckSessionStorageService {

  constructor() { }

  clearSession() {
    console.log('CLEAR SESSION STORAGE');
    sessionStorage.clear();

  }

  storeSessionChangePage(agentCode,agentLevel){
    /* Add agent for ZONE/GM */
    let currentAgentCode = sessionStorage.getItem('agentCodeSelected');
    let currentAgentLevel = sessionStorage.getItem('agentLevelSelected');
    let agentCodeStored = sessionStorage.getItem('agentCodeStored') != null ? JSON.parse(sessionStorage.getItem('agentCodeStored')) : [];
    agentCodeStored.push(currentAgentCode);
    let agentLevelStored = sessionStorage.getItem('agentLevelStored') != null ? JSON.parse(sessionStorage.getItem('agentLevelStored')) : [];
    agentLevelStored.push(currentAgentLevel);
    sessionStorage.setItem('agentLevelStored', JSON.stringify(agentLevelStored));
    sessionStorage.setItem('agentCodeStored',JSON.stringify(agentCodeStored));
    sessionStorage.setItem('agentCodeSelected', agentCode);
    sessionStorage.setItem('agentLevelSelected', agentLevel);
  }
}
