import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SetFormDataService {

  private currentYear = parseInt(sessionStorage.getItem('calendarCurrentYear'));

  constructor() { }

  /// [Set Form Data]
  // ChartSeries:: call in 'callAPI_getAgentPerformance' Methods
  transform_kpiChartSeries(data, attr: string) {
    
    // !!! WORK ONLY BY QUERY 'ORDER BY' YEAR (CLOS_YM)
    let lastSeries = [];
    let currentSeries = [];
    for (let index = 1; index <= 12; index++) {
      let monthByIndex = data.filter( x => parseInt(x.closYm.substring(4,6)) === index);
      
      // Data available by each month
      if (monthByIndex.length > 0) {

        // Found in current and last year
        if (monthByIndex.length == 2) {
          lastSeries.push(monthByIndex[0][attr]);
          currentSeries.push(monthByIndex[1][attr]);
        }

        // Found only current or last year
        else if (monthByIndex.length == 1) {
          let getElementYear = parseInt(monthByIndex[0]['closYm'].substring(0,4));
          // Current Year only
          if (getElementYear === this.currentYear) {
            currentSeries.push(monthByIndex[0][attr]);
            lastSeries.push(0);
          }
          // Last Year only, current year missing! 
          else if (getElementYear !== this.currentYear) {
            lastSeries.push(monthByIndex[0][attr]);
            currentSeries.push(null);
          }
          // else {
          //   lastSeries.push(0);
          //   currentSeries.push(0);
          // }
        }
      } // end if

      // Data not available, e.g. missing in month both of current and past year
      else {
        lastSeries.push(0);
        currentSeries.push(null);
      }

    } // End for

    return {last: lastSeries, current: currentSeries};
  }
  transform_kpiChartSeries_dyThousand(data, attr: string) {
    // !!! WORK ONLY BY QUERY 'ORDER BY' YEAR (CLOS_YM)
    let lastSeries = [];
    let currentSeries = [];
    for (let index = 1; index <= 12; index++) {
      let monthByIndex = data.filter( x => parseInt(x.closYm.substring(4,6)) === index);

      // Data available by each month
      if (monthByIndex.length > 0) {

        // Found in current and last year
        if (monthByIndex.length == 2) {
          
          lastSeries.push((monthByIndex[0][attr]/1000).toFixed(2));
          currentSeries.push((monthByIndex[1][attr]/1000).toFixed(2));
        }

        // Found only current or last year
        else if (monthByIndex.length == 1) {
          let getElementYear = parseInt(monthByIndex[0]['closYm'].substring(0,4));
          // Current Year only
          if (getElementYear === this.currentYear) {
            currentSeries.push((monthByIndex[0][attr]/1000).toFixed(2));
            lastSeries.push(0);
          }
          // Last Year only, current year missing! 
          else if (getElementYear !== this.currentYear) {
            lastSeries.push((monthByIndex[0][attr]/1000).toFixed(2));
            currentSeries.push(null);
          }
          // else {
          //   lastSeries.push(monthByIndex[0][attr]/1000);
          //   currentSeries.push(0);
          // }
        }
      } // end if

      // Data not available, e.g. missing in month both of current and past year
      else {
        lastSeries.push(0);
        currentSeries.push(null);
      }

    } // End for

    return {last: lastSeries, current: currentSeries};
  }
  transform_kpiChartSeries_null(data, attr: string) {
    // !!! WORK ONLY BY QUERY 'ORDER BY' YEAR (CLOS_YM)
    let lastSeries = [];
    let currentSeries = [];
    for (let index = 1; index <= 12; index++) {
      let monthByIndex = data.filter( x => parseInt(x.closYm.substring(4,6)) === index);

      // Data available by each month
      if (monthByIndex.length > 0) {

        // Found in current and last year
        if (monthByIndex.length == 2) {
          lastSeries.push(monthByIndex[0][attr]);
          currentSeries.push(monthByIndex[1][attr]);
        }

        // Found only current or last year
        else if (monthByIndex.length == 1) {
          let getElementYear = parseInt(monthByIndex[0]['closYm'].substring(0,4));
          // Current Year only
          if (getElementYear === this.currentYear) {
            currentSeries.push(monthByIndex[0][attr]);
            lastSeries.push(null);
          }
          // Last Year only, current year missing! 
          else if (getElementYear !== this.currentYear) {
            lastSeries.push(monthByIndex[0][attr]);
            currentSeries.push(null);
          }
        }
      } // end if

      // Data not available, e.g. missing in month both of current and past year
      else {
        lastSeries.push(null);
        currentSeries.push(null);
      }

    } // End for

    return {last: lastSeries, current: currentSeries};
  }

  // TargetSeries:: call in 'callAPI_getAgentTarget' Methods
  transform_kpiTargetSeries(data, kpiName: string) {
    // Find by index, select by kpiName {PC, NEW_ACTIVE_AGENT, NEW_CODE, RYP}
    let index = data.findIndex( x => x.kpiName == kpiName);
    let resData = [];
    // Check productionYear = currentYear ?
    // if true: assign target from 'targetM#' by # = {1, 2, 3, ..., 12}
    // to storageTarget (array e.g. series_target_pc)
    if(data[index]['productionYear'] == this.currentYear.toString()) {
      for (let i = 1; i <= 12; i++) {
        let targetName = 'targetM' + i;
        if(data[index][targetName]) {
          resData.push(data[index][targetName]);
        } else {
          resData.push(0);
        }
      } // End for
    } // End if

    return resData;
  }
  transform_kpiTargetSeries_dyThousand(data, kpiName: string) {
    // Find by index, select by kpiName {PC, NEW_ACTIVE_AGENT, NEW_CODE, RYP}
    let index = data.findIndex( x => x.kpiName == kpiName);
    let resData = [];
    // Check productionYear = currentYear ?
    // if true: assign target from 'targetM#' by # = {1, 2, 3, ..., 12}
    // to storageTarget (array e.g. series_target_pc)
    if(data[index]['productionYear'] == this.currentYear.toString()) {
      for (let i = 1; i <= 12; i++) {
        let targetName = 'targetM' + i;
        if(data[index][targetName]) {
          resData.push((data[index][targetName]/1000).toFixed(2));
        } else {
          resData.push(0);
        }
      } // End for
    } // End if

    return resData;
  }

  // Set Retention MTD
  transform_MtdRetention(resMtdRT, resYtdRT, resOverview) {
    let dataRes = [];
    var formData = {}, closymRes = {closYm_last: 0, closYm_current: 0};
    var tempName = '';
    var uniqueYear = [...new Set(resMtdRT.map(item => item.closYm))];
    var activeLM = Math.min.apply(null, uniqueYear), activeCM = Math.max.apply(null, uniqueYear);
    closymRes.closYm_last = activeLM.toString().substring(4,6);
    closymRes.closYm_current = activeCM.toString().substring(4,6);

    for (let index = 0; index < resYtdRT.length; index++) {
      let data_lm = [], data_cm = [];
      var elementValue = resYtdRT[index];
      
      // Get LastMonth (lm) and CurrentMonth (cm) Retention
      if (elementValue.agentLevel === 'ZONE') {
        var tempOverview = resOverview.filter( attribute => attribute.code === resYtdRT[index].code);
        tempName = tempOverview[0].zoneName;

        data_lm = resMtdRT.filter( x => parseInt(x.closYm) === activeLM
          && x.zone === resYtdRT[index].code);
        data_cm = resMtdRT.filter( x => parseInt(x.closYm) === activeCM
          && x.zone === resYtdRT[index].code);
      }
      else if (elementValue.agentLevel === 'CHANNEL') {
        var tempOverview = resOverview.filter( attribute => attribute.code === resYtdRT[index].code);
        tempName = tempOverview[0].code;

        data_lm = resMtdRT.filter( x => parseInt(x.closYm) === activeLM
          && x.groupChannel === resYtdRT[index].code);
        data_cm = resMtdRT.filter( x => parseInt(x.closYm) === activeCM
          && x.groupChannel === resYtdRT[index].code);
      }
      else if (elementValue.agentLevel === 'COMPANY') {
        var tempOverview = resOverview.filter( attribute => attribute.code === resYtdRT[index].code);
        tempName = tempOverview[0].code;

        data_lm = resMtdRT.filter( x => parseInt(x.closYm) === activeLM
          && x.agentLevel === resYtdRT[index].code);
        data_cm = resMtdRT.filter( x => parseInt(x.closYm) === activeCM
          && x.agentLevel === resYtdRT[index].code);
      }
      
      
      if (data_lm.length > 0 && data_cm.length > 0) {
        formData = {
          chName: tempName,
          lmRetention: data_lm[0].retention,
          rtMonth01: data_cm[0].retentionMtd1,
          rtMonth02: data_cm[0].retentionMtd2,
          rtMonth03: data_cm[0].retentionMtd3,
          rtMonth04: data_cm[0].retentionMtd4,
          rtMonth05: data_cm[0].retentionMtd5,
          rtMonth06: data_cm[0].retentionMtd6,
          cmRT: data_cm[0].retention
        };
      }
      else if (data_lm.length > 0 && data_cm.length < 1) {
        formData = {
          chName: tempName,
          lmRetention: data_lm[0].retention,
          rtMonth01: 0,
          rtMonth02: 0,
          rtMonth03: 0,
          rtMonth04: 0,
          rtMonth05: 0,
          rtMonth06: 0,
          cmRT: 0
        };
      }
      else if (data_lm.length < 1 && data_cm.length > 0) {
        formData = {
          chName: tempName,
          lmRetention: 0,
          rtMonth01: data_cm[0].retentionMtd1,
          rtMonth02: data_cm[0].retentionMtd2,
          rtMonth03: data_cm[0].retentionMtd3,
          rtMonth04: data_cm[0].retentionMtd4,
          rtMonth05: data_cm[0].retentionMtd5,
          rtMonth06: data_cm[0].retentionMtd6,
          cmRT: data_cm[0].retention
        };
      }
      else {
        formData = {
          chName: tempName,
          lmRetention: 0,
          rtMonth01: 0,
          rtMonth02: 0,
          rtMonth03: 0,
          rtMonth04: 0,
          rtMonth05: 0,
          rtMonth06: 0,
          cmRT: 0
        };
      }
      dataRes.push(formData);
    }
    return {dataRes: dataRes, closYm: closymRes};
    // return dataRes;
  }

  // Set SummaryYTD of each KPI: use in KPI Detail Page (series table and MTD,YTD tables)
  transform_kpiYtd_forAGENT(data, kpiType: string, isWholeGroup: boolean = false) {
    let indexCurrentYear = data.findIndex(x => x.closYm === this.currentYear.toString());
    let indexLastYear = data.findIndex(x => x.closYm === (this.currentYear-1).toString());
    
    let dataRes = {};
    var target = 0;
    var submit = 0;
    var approveCur = 0;
    var approveLast = 0;
    var percent = 0;
    // Set PC
    if (kpiType == 'PC') {
      if (indexCurrentYear != '-1') {
        target = data[indexCurrentYear]['pcTarget'];
        submit = data[indexCurrentYear]['pcSubmit'];
        approveCur = data[indexCurrentYear]['pcApprove'];
        percent = data[indexCurrentYear]['percent'];
      }
      if (indexLastYear != '-1') {
        approveLast = data[indexLastYear]['pcApprove'];
      }
      if (isWholeGroup) {
        if (data[indexCurrentYear] && data[indexLastYear]) {
          percent = ((data[indexCurrentYear]['pcApprove'] - data[indexLastYear]['pcApprove']) / data[indexLastYear]['pcApprove']) * 100;
        }
        else percent = 0;
      }
      dataRes = {
        target: target,
        submit: submit,
        approveCur: approveCur,
        approveLast: approveLast,
        percent: percent
      };
    }

    // Set ACT
    else if (kpiType == 'ACTIVE') {
      if (indexCurrentYear != '-1') {
        target = data[indexCurrentYear]['activeTarget'];
        approveCur = data[indexCurrentYear]['active'];
        percent = data[indexCurrentYear]['percent'];
      }
      if (indexLastYear != '-1') {
        approveLast = data[indexLastYear]['active'];
      }
      if (isWholeGroup) {
        if (data[indexCurrentYear] && data[indexLastYear]) {
          percent = ((data[indexCurrentYear]['active'] - data[indexLastYear]['active']) / data[indexLastYear]['active']) * 100;
        }
        else percent = 0;
      }
      dataRes = {
        target: target,
        approveCur: approveCur,
        approveLast: approveLast,
        percent: percent
      };
    }

    // Set NC
    else if (kpiType == 'NC') {
      if (indexCurrentYear != '-1') {
        target = data[indexCurrentYear]['ncTarget'];
        approveCur = data[indexCurrentYear]['nc'];
        percent = data[indexCurrentYear]['percent'];
      }
      if (indexLastYear != '-1') {
        approveLast = data[indexLastYear]['nc'];
      }
      if (isWholeGroup) {
        if (data[indexCurrentYear] && data[indexLastYear]) {
          percent = ((data[indexCurrentYear]['nc'] - data[indexLastYear]['nc']) / data[indexLastYear]['nc']) * 100;
        }
        else percent = 0;
      }
      dataRes = {
        target: target,
        approveCur: approveCur,
        approveLast: approveLast,
        percent: percent
      };
      
    }

    // Set RYP
    else if (kpiType == 'RYP') {
      if (indexCurrentYear != '-1') {
        target = data[indexCurrentYear]['rypTarget'];
        approveCur = data[indexCurrentYear]['ryp'];
        percent = data[indexCurrentYear]['percent'];
      }
      if (indexLastYear != '-1') {
        approveLast = data[indexLastYear]['ryp'];
      }
      if (isWholeGroup) {
        if (data[indexCurrentYear] && data[indexLastYear]) {
          percent = ((data[indexCurrentYear]['ryp'] - data[indexLastYear]['ryp']) / data[indexLastYear]['ryp']) * 100;
        }
        else percent = 0;
      }
      dataRes = {
        target: target,
        approveCur: approveCur,
        approveLast: approveLast,
        percent: percent
      };
    }
    return dataRes;
  }
  transform_kpiYtd_forSTAFF(data, kpiType: string) {
    let indexCurrentYear = data.findIndex(x => x.closYm === this.currentYear.toString());
    let indexLastYear = data.findIndex(x => x.closYm === (this.currentYear-1).toString());
    let dataRes = {};
    var target = 0;
    var submit = 0;
    var approveCur = 0;
    var approveLast = 0;
    var percent = 0;
    // Set PC
    if (kpiType == 'PC') {
      if (indexCurrentYear != '-1') {
        target = data[indexCurrentYear]['pcTarget'];
        submit = data[indexCurrentYear]['pcSubmit'];
        approveCur = data[indexCurrentYear]['pcApprove'];
        percent = data[indexCurrentYear]['pcPercent'];
      }
      if (indexLastYear != '-1') {
        approveLast = data[indexLastYear]['pcApprove'];
      }
      dataRes = {
        target: target,
        submit: submit,
        approveCur: approveCur,
        approveLast: approveLast,
        percent: percent
      };
    }

    // Set ACT
    else if (kpiType == 'ACTIVE') {
      if (indexCurrentYear != '-1') {
        target = data[indexCurrentYear]['activeTarget'];
        approveCur = data[indexCurrentYear]['active'];
        percent = data[indexCurrentYear]['activePercent'];
      }
      if (indexLastYear != '-1') {
        approveLast = data[indexLastYear]['active'];
      }
      dataRes = {
        target: target,
        approveCur: approveCur,
        approveLast: approveLast,
        percent: percent
      };
    }

    // Set NC
    else if (kpiType == 'NC') {
      if (indexCurrentYear != '-1') {
        target = data[indexCurrentYear]['ncTarget'];
        approveCur = data[indexCurrentYear]['nc'];
        percent = data[indexCurrentYear]['ncPercent'];
      }
      if (indexLastYear != '-1') {
        approveLast = data[indexLastYear]['nc'];
      }
      dataRes = {
        target: target,
        approveCur: approveCur,
        approveLast: approveLast,
        percent: percent
      };
    }

    // Set RYP
    else if (kpiType == 'RYP') {
      if (indexCurrentYear != '-1') {
        target = data[indexCurrentYear]['rypTarget'];
        approveCur = data[indexCurrentYear]['ryp'];
        percent = data[indexCurrentYear]['rypPercent'];
      }
      if (indexLastYear != '-1') {
        approveLast = data[indexLastYear]['ryp'];
      }
      dataRes = {
        target: target,
        approveCur: approveCur,
        approveLast: approveLast,
        percent: percent
      };
    }
    return dataRes;
  }

  // [Calculation]
  cal_kpiPercent(dataApprove, dataTarget) {
    let dataRes = [];
    for (let index = 0; index < dataApprove.length; index++) {
      let percent = 0;
      if (dataTarget[index] > 0) {
        percent = (dataApprove[index]/dataTarget[index])*100;
      }
      dataRes[index] = percent;
    }
    return dataRes;
  }
  cal_GrowthPercent(dataApprove, dataTarget) {
    let dataRes = [];
    for (let index = 0; index < dataApprove.length; index++) {
      let percent = 0;
      if (dataTarget[index] > 0 && dataApprove[index] > 0) {
        percent = ((dataApprove[index] - dataTarget[index]) / dataTarget[index]) * 100;
      }
      dataRes[index] = percent;
    }
    return dataRes;
  }

  // Set PerformanceTable
  transform_performanceData(data, closYm) {
    let dailyList = [];
    let totalList = {
      pcActual: 0,
      pcSubmit: 0,
      active: 0,
      nc: 0,
      retention: 0,
      ryp: 0,
    };
    for (let index = 0; index < Object.keys(data).length; index++) {
      if (data[index].closYm == closYm) {
        dailyList.push(data[index]);
        totalList = {
          pcActual: totalList.pcActual + data[index].pcActual,
          pcSubmit: totalList.pcSubmit + data[index].pcSubmit,
          active: totalList.active + data[index].active,
          nc: totalList.nc + data[index].nc,
          retention: totalList.retention + data[index].retention,
          ryp: totalList.ryp + data[index].ryp,
        }
      }
    }
    return {list: dailyList, total: totalList};
  }

  // Set PerformanceTable SelectionCalendar
  getSelectionCalendar(data) {
    let calendarList = [];
    let _closYm = '';
    for (let index = 0; index < Object.keys(data).length; index++) {
      if (_closYm != data[index].closYm) {
        _closYm = data[index].closYm;
        let _viewValue = data[index].closYm.substring(4,6) + '/' + data[index].closYm.substring(0,4);
        let _list = {
          value: _closYm,
          viewValue: _viewValue
        };
        calendarList.push(_list);
      }
    }
    return calendarList;
  }

  getSelectionCalendarYM(data) {
    let _tempList = [];
    let _month = parseInt(data.substring(4,6));
    let _year = parseInt(data.substring(0,4));
    let _monthChoice = '';
    let _list = {
      value: '',
      viewValue: ''
    };
    if (data) {
      for (let index = 0; index <= 2; index++) {
        let _label = '';
        let _value = '';
        if (_month - index <= 0) {
          _monthChoice = (12 + (_month - index)).toString().padStart(2, '0');
          _value = (_year - 1).toString() + _monthChoice;
          _label = _monthChoice + '/' + (_year - 1);

          _list = {
            value: _value,
            viewValue: _label
          };
        } else {
          _monthChoice = (_month - index).toString().padStart(2, '0');
          _value = _year.toString() + _monthChoice;
          _label = _monthChoice + '/' + _year;
          _list = {
            value: _value,
            viewValue: _label
          };
        }
        
        _tempList.push(_list);
      }
    }
    return _tempList;
  }
  
}
