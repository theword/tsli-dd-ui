import { Directive, forwardRef, HostListener } from '@angular/core';
import { DefaultValueAccessor, NG_VALUE_ACCESSOR,  } from '@angular/forms';


export const FIXED_NUMBER_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CurrencyMaskDirective),
  multi: true
};


@Directive({
  selector: 'input [type=text][currencyMask]',
  providers: [FIXED_NUMBER_VALUE_ACCESSOR]
})
export class CurrencyMaskDirective extends DefaultValueAccessor {

  private lastValue: string;


 
  @HostListener('keydown.backspace', ['$event'])
  keydownBackspace(event) {
    this.onInputChange(event.target.value, true);
  }

  @HostListener('keydown.backspace', ['$event'])
  onInput(event) {
    this.onInputChange(event.target.value, true);
  }

    
  @HostListener('change', ['$event']) handleOnChange(value) {
    this.onInputChange(value.target.value, false);
  }

  @HostListener('input', ['$event']) handleOnInput(value) {
     this.onInputChange(value.target.value , false );
  }


  onInputChange(event, backspace) {
  
    this.lastValue = event.replace(/\D/g, '');
    if (backspace && this.lastValue.length <= 15) {
      this.lastValue = event.substring(0, event.length);
    } 
   
    this.onChange(''+this.lastValue);
    this.writeValue( ''+Number(this.lastValue).toLocaleString());
  }


}
