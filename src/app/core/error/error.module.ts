import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PageServerErrorComponent } from './page-server-error/page-server-error.component';

const routes: Routes = [
  {
    path: '404',
    component: PageNotFoundComponent,
  },
  { path: '500', component: PageServerErrorComponent },
  { path: '**', redirectTo: '404', pathMatch: 'full' },
];

@NgModule({
  declarations: [PageNotFoundComponent, PageServerErrorComponent],
  imports: [RouterModule.forChild(routes), SharedModule],
  exports: [],
  providers: [],
})
export class ErrorModule {}
