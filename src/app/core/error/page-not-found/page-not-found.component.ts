import { Component, OnInit } from '@angular/core';
import { PositionRouterService } from '../../service/position-router.service';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {

  constructor( private positionRouterService: PositionRouterService) { }

  ngOnInit(): void {
  }

  gotoHome() {
    this.positionRouterService.gotoPerformanceDetail();
  }
}
