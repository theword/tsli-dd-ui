import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarModule,
} from 'ngx-perfect-scrollbar';
import { TranslateModule } from '@ngx-translate/core';
import { ChartComponent } from '../theme/components/chart/chart.component';
import { ChartActComponent } from '../theme/components/chart-act/chart-act.component';
import { ChartNcComponent } from '../theme/components/chart-nc/chart-nc.component';
import { ChartRypComponent } from '../theme/components/chart-ryp/chart-ryp.component';
import { ChartRtComponent } from '../theme/components/chart-rt/chart-rt.component';
import { NavBarComponent } from '../layout/nav-bar/nav-bar.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { MinorTableComponent } from '../dashboard/shared/components/minor-table/minor-table.component';
import { MtdYtdTableComponent } from '../dashboard/shared/components/mtd-ytd-table/mtd-ytd-table.component';
import { MFlippedComponent} from '../dashboard/shared/components/m-flipped/m-flipped.component';
import { RetantionTableComponent } from '../dashboard/shared/components/retantion-table/retantion-table.component';
import { FilterRetentionPipe } from '../dashboard/shared/pipe/filter-retention.pipe';
import { DisplayAgentNamePipe } from '../dashboard/shared/pipe/display-agentname.pipe';
import { DisplayAgentCodePipe } from '../dashboard/shared/pipe/display-agentcode.pipe';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SubDetailFilterComponent } from '../dashboard/shared/components/sub-detail-filter/sub-detail-filter.component';
import { SubDetailTableComponent } from '../dashboard/shared/components/sub-detail-table/sub-detail-table.component';
import { FilterPolicyPcPipe } from '../dashboard/shared/pipe/filter-policy-pc.pipe';
import { ModalMemoComponent } from '../dashboard/shared/components/modal-memo/modal-memo.component';
import { CurrencyMaskDirective } from './currency-mask-directive';
import { ModalEditStatusComponent } from '../theme/target-year/components/modal-edit-status/modal-edit-status.component';
import { CaseTableComponent } from '../dashboard/shared/components/case-table/case-table.component';
import { KUnitPipe } from '../dashboard/shared/pipe/k-unit.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NumberDirective } from '../dashboard/shared/pipe/NumberDirective';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

@NgModule({
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    NgxSpinnerModule,
    MDBBootstrapModule.forRoot(),
    CollapseModule.forRoot(),
    NgbModule
  ],
  declarations: [
    ChartComponent,
    ChartActComponent,
    ChartNcComponent,
    ChartRypComponent,
    NavBarComponent,
    ChartRtComponent,
    MinorTableComponent,
    MtdYtdTableComponent,
    MFlippedComponent,
    RetantionTableComponent,
    FilterRetentionPipe,
    DisplayAgentNamePipe,
    DisplayAgentCodePipe,
    SubDetailFilterComponent,
    SubDetailTableComponent,
    FilterPolicyPcPipe,
    ModalMemoComponent,
    CurrencyMaskDirective,
    ModalEditStatusComponent,
    CaseTableComponent,
    KUnitPipe,
    NumberDirective
  ],
  exports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    ChartComponent,
    ChartActComponent,
    ChartNcComponent,
    ChartRypComponent,
    NavBarComponent,
    MDBBootstrapModule,
    ChartRtComponent,
    CollapseModule,
    MinorTableComponent,
    MtdYtdTableComponent,
    MFlippedComponent,
    RetantionTableComponent,
    FilterRetentionPipe,
    DisplayAgentNamePipe,
    DisplayAgentCodePipe,
    NgxSpinnerModule,
    SubDetailFilterComponent,
    SubDetailTableComponent,
    FilterPolicyPcPipe,
    ModalMemoComponent,
    CurrencyMaskDirective,
    ModalEditStatusComponent,
    CaseTableComponent,
    KUnitPipe,
    NumberDirective,
    NgbModule
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    }
  ],
})
export class SharedModule {}
