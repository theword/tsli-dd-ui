import Swal from 'sweetalert2';

export class UtilitiesHelper {
    static newGuid() {
        const dt = new Date().getTime();
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            // tslint:disable-next-line:no-bitwise
            const r = (dt + Math.random() * 16) % 16 | 0;
            // tslint:disable-next-line:no-bitwise
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }

    static convertDataTableParam(params) {
        if (params.columns) {
            params.columns.forEach((column: any, index: any) => {
                params['columns[' + index + '].data'] = column.data || "";
                params['columns[' + index + '].name'] = column.name || "";
                params['columns[' + index + '].searchable'] = column.searchable || true;
                params['columns[' + index + '].orderable'] = column.orderable || false;

                if (column.search) {
                    params['columns[' + index + '].search.regex'] = column.search.regex || "";
                    params['columns[' + index + '].search.value'] = column.search.value || "";
                }
            });
            delete params.columns;
        }

        if (params.order) {
            params.order.forEach((order: any, index: any) => {
                params['order[' + index + '].column'] = order.column;
                params['order[' + index + '].dir'] = order.dir;
            });
            delete params.order;
        }

        if (params.search) {
            params['search.regex'] = params.search.regex || "";
            params['search.value'] = params.search.value || "";
            delete params.search;
        }

        return params ; // $.param(params);
    }

    static parseDateDDMMYYYY(date) {
        const mm = date.getMonth() + 1;
        const dd = date.getDate();

        return [(dd > 9 ? '' : '0') + dd + '/',
        (mm > 9 ? '' : '0') + mm + '/',
        date.getFullYear()
        ].join('');
    }

    // static parseDate(str: string): Date {
    //     return moment(str, "DD/MM/YYYY").toDate();
    // }

    // static displayDate(date): string {
    //     return moment(date).format("DD/MM/YYYY");
    // }

    // static displayDateTime(date): string {
    //     return moment(date).format("DD/MM/YYYY HH:mm");
    // }

    // static getDateNowDisplay(): string {
    //     return UtilitiesHelper.displayDate(new Date());
    // }

    static getDateTimeNow(): Date {
        return new Date();
    }


    // static getDateNow(): Date {
    //     return moment(new Date()).toDate();
    // }

    static alert(title: string, text: string = '', type: any = 'error') {
        return Swal.fire(title, text, type);
    }

    static pad(num: number, size: number): string {
      let s = num + '';
      while (s.length < size) { s = '0' + s; }
      return s;
    }

    static compareStr(str,keyword){
        str = str +'';
        keyword = keyword + '';
        return str != null ? str.toLowerCase().includes(keyword.toLowerCase()) : false; 
    }

    static convertToRawDate(searchText) { 
        let rawDate = searchText.substring(6, 10) + searchText.substring(3, 5) + searchText.substring(0, 2);
        return rawDate;
    }
}
