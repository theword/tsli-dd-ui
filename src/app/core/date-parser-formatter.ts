import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Injectable } from '@angular/core';
import { isNumber } from 'util';

@Injectable()

export class DateParserFormatter extends NgbDateParserFormatter {
    parse(value: string): NgbDateStruct {
        if (value) {
            const dateParts = value.trim().split('/');
            if (dateParts.length === 1 && isNumber(dateParts[0])) {
                return { day: parseInt(dateParts[0], 2), month: null, year: null };
            } else if (dateParts.length === 2 && isNumber(dateParts[0]) && isNumber(dateParts[1])) {
                return { day: parseInt(dateParts[0], 2), month: parseInt(dateParts[1], 2), year: null };
            } else if (dateParts.length === 3 && isNumber(dateParts[0]) && isNumber(dateParts[1]) && isNumber(dateParts[2])) {
                return { day: parseInt(dateParts[0], 2), month: parseInt(dateParts[1], 2), year: parseInt(dateParts[2], 2) };
            }
        }
        return null;
    }

    format(date: NgbDateStruct): string {
        return date ?
            `${isNumber(date.day) ? this.padNumber(date.day) : ''}/${isNumber(date.month) ? this.padNumber(date.month) : ''}/${date.year}` :
            '';
    }

    padNumber(n: number) {
        let str = n.toString();
        const padStr = '0';
        const len = 2;
        while (str.length < len) {
            str = padStr + str;
        }
        return str;
    }
}
