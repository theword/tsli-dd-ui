export class ModelUser {

  levelName: string = 'NA';
  agentName: string = 'NA';
  agentLevel: string = 'NA';
  agentCode: string = 'NA';

  tableOverview: any = [
    {
      agentCode: 0,
      targetPc: 0,
      pcSubmit: 0,
      pcApprove: 0,
      pcPercent: 0,
      targetAct: 0,
      active: 0,
      activePercent: 0,
      targetNc: 0,
      nc: 0,
      ncPercent: 0,
      targetRt: 0,
      retention: 0,
      targetRyp: 0,
      ryp: 0
    }
  ];

  tablePerformanceDaily: any = [];
  performanceDailyTotal: any = {
    pcActual: 0,
    pcSubmit: 0,
    active: 0,
    nc: 0,
    retention: 0,
    ryp: 0,
  };
  performanceWeekly: any = [];
  
  setDefaultTableOverview(agentCode) {
    let tempData = {
      agentCode: agentCode,
      targetPc: 0,
      pcSubmit: 0,
      pcApprove: 0,
      pcPercent: 0,
      targetAct: 0,
      active: 0,
      activePercent: 0,
      targetNc: 0,
      nc: 0,
      ncPercent: 0,
      targetRt: 0,
      retention: 0,
      targetRyp: 0,
      ryp: 0
    };
    this.tableOverview = [];
    this.tableOverview.push(tempData);
  }

  getLevelName() {
    return this.levelName;
  }
  setLevelName(levelName: string) {
    this.levelName = levelName;
  }
  getAgentName() {
    return this.agentName;
  }
  setAgentName(agentName: string) {
    this.agentName = agentName;
  }
  getAgentLevel() {
    return this.agentLevel;
  }
  setAgentLevel(agentLevel: string) {
    this.agentLevel = agentLevel;
  }
  getAgentCode() {
    return this.agentCode;
  }
  setAgentCode(agentCode: string) {
    this.agentCode = agentCode;
  }
  getTableOverview() {
    return this.tableOverview;
  }
  setTableOverview(data) {
    this.tableOverview = data;
  }
  getTablePerformanceDaily() {
    return this.tablePerformanceDaily;
  }
  setTablePerformanceDaily(data) {
    this.tablePerformanceDaily = data;
  }
  getPerformanceDailyTotal() {
    return this.performanceDailyTotal;
  }
  setPerformanceDailyTotal(data) {
    this.performanceDailyTotal = data;
  }
  getPerformanceWeekly() {
    return this.performanceWeekly;
  }
  setPerformanceWeekly(data) {
    this.performanceWeekly = data;
  }
}
