export class ModelDashboard {

  kpiBoxValue: object = {
    name: 'Empty', 
    target: 0, 
    total: 0,
    total_percent: 0
  };
  sumYtdValue: object = {
    target: 0,
    submit: 0,
    approveCur: 0,
    approveLast: 0,
    percent: 0
  };
  series_target = [0,0,0,0,0,0,0,0,0,0,0,0];
  series_current = [0,0,0,0,0,0,0,0,0,0,0,0];
  series_last = [0,0,0,0,0,0,0,0,0,0,0,0];
  series_percentage = [0,0,0,0,0,0,0,0,0,0,0,0];

  setCurrentAndLast(data_cur, data_last) {
    this.series_current = data_cur;
    this.series_last = data_last;
  }

  getKpiBoxValue() {
    return this.kpiBoxValue;
  }
  setKpiBoxValue(name: string, target: number, total: number, percent: number) {
    this.kpiBoxValue = {
      name: name, 
      target: target, 
      total: total,
      total_percent: percent
    };
  }
  getSumYtdValue() {
    return this.sumYtdValue;
  }
  setSumYtdValue(data) {
    this.sumYtdValue = data;
  }
  getSeriesTarget() {
    return this.series_target;
  }
  setSeriesTarget(data) {
    this.series_target = data;
  }
  getSeriesCurrent() {
    return this.series_current;
  }
  setSeriesCurrent(data) {
    this.series_current = data;
  }
  getSeriesLast() {
    return this.series_last;
  }
  setSeriesLast(data) {
    this.series_last = data;
  }
  getSeriesPercent() {
    return this.series_percentage;
  }
  setSeriesPercent(data) {
    this.series_percentage = data;
  }
}
