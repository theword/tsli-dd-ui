export class ModelTempKpi {
  masterPcMtd: {
    target: 0,
    value: 0,
    percent: 0
  };
  masterPcYtd: {
    target: 0,
    value: 0,
    percent: 0
  };

  masterNcMtd: {
    target: 0,
    value: 0,
    percent: 0
  };
  masterNcYtd: {
    target: 0,
    value: 0,
    percent: 0
  };

  masterActiveMtd: {
    target: 0,
    value: 0,
    percent: 0
  };
  masterActiveYtd: {
    target: 0,
    value: 0,
    percent: 0
  };

  masterRypMtd: {
    target: 0,
    value: 0,
    percent: 0
  };
  masterRypYtd: {
    target: 0,
    value: 0,
    percent: 0
  };

  masterRetentionMtd: {
    target: 0,
    value: 0,
    percent: 0
  };
  masterRetentionYtd: {
    target: 0,
    value: 0,
    percent: 0
  };

  setMasterPcMtd(target, value, percent) {
    this.masterPcMtd = {
      target: target,
      value: value,
      percent: percent
    };
  }
  getMasterPcMtd() {
    return this.masterPcMtd;
  }
  setMasterPcYtd(target, value, percent) {
    this.masterPcYtd = {
      target: target,
      value: value,
      percent: percent
    };
  }
  getMasterPcYtd() {
    return this.masterPcYtd;
  }

  setMasterNcMtd(target, value, percent) {
    this.masterNcMtd = {
      target: target,
      value: value,
      percent: percent
    };
  }
  getMasterNcMtd() {
    return this.masterNcMtd;
  }
  setMasterNcYtd(target, value, percent) {
    this.masterNcYtd = {
      target: target,
      value: value,
      percent: percent
    };
  }
  getMasterNcYtd() {
    return this.masterNcYtd;
  }

  setMasterActiveMtd(target, value, percent) {
    this.masterActiveMtd = {
      target: target,
      value: value,
      percent: percent
    };
  }
  getMasterActiveMtd() {
    return this.masterActiveMtd;
  }
  setMasterActiveYtd(target, value, percent) {
    this.masterActiveYtd = {
      target: target,
      value: value,
      percent: percent
    };
  }
  getMasterActiveYtd() {
    return this.masterActiveYtd;
  }

  setMasterRypMtd(target, value, percent) {
    this.masterRypMtd = {
      target: target,
      value: value,
      percent: percent
    };
  }
  getMasterRypMtd() {
    return this.masterRypMtd;
  }
  setMasterRypYtd(target, value, percent) {
    this.masterRypYtd = {
      target: target,
      value: value,
      percent: percent
    };
  }
  getMasterRypYtd() {
    return this.masterRypYtd;
  }

  setMasterRetentionMtd(target, value, percent) {
    this.masterRetentionMtd = {
      target: target,
      value: value,
      percent: percent
    };
  }
  getMasterRetentionMtd() {
    return this.masterRetentionMtd;
  }
  setMasterRetentionYtd(target, value, percent) {
    this.masterRetentionYtd = {
      target: target,
      value: value,
      percent: percent
    };
  }
  getMasterRetentionYtd() {
    return this.masterRetentionYtd;
  }
}