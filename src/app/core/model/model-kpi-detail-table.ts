export class KPIDetailTableModel {
    agentLevelHeaders : HeaderKPIDetailModel[];
    filterKpiType : string;
    filterAgentLevel : string;
    agLevelFilter : string;
    headerStyleClass : string;
    subHeaderStyleClass : string;
    subHeaders : SubHeaderKPIDetailModel[];
    data : any[];
    tmpData : any[];
    mapLevelBtn : any = new Map();

    setKpiFilter(filterKpiType){
        this.filterKpiType = filterKpiType;
        this.headerStyleClass = this.getHeaderStyleByKpiFilter(filterKpiType);
        this.subHeaderStyleClass = this.getSubHeaderStyleByKpiFilter(filterKpiType);
    }

    setData(data){
        this.data = data;
    }

    setAgentLevelHeaders(agentLevelHeaders){
        this.agentLevelHeaders = agentLevelHeaders;
    }

    getAgentLevelHeaders(){
        return this.agentLevelHeaders;
    }

    resetAgentLevelHeaders(){
        this.agentLevelHeaders = [];
    }

    setFilterAgentLevel(filterAgentLevel,levelName){
        this.filterAgentLevel = filterAgentLevel;
        this.agLevelFilter = levelName;
    }

    setSubHeaders(filter){
        if(filter == 'PC'){
            this.subHeaders = [new SubHeaderKPIDetailModel('Target','targetPc'),new SubHeaderKPIDetailModel('Submit','pcSubmit'),new SubHeaderKPIDetailModel('Actual','pcApprove'),new SubHeaderKPIDetailModel('%Target','pcPercent')];
        }
        if(filter == 'ACTIVE'){
            this.subHeaders = [new SubHeaderKPIDetailModel('Target','targetAct'),new SubHeaderKPIDetailModel('Actual','active'),new SubHeaderKPIDetailModel('%Target','activePercent')];
        }
        if(filter == 'NC'){
            this.subHeaders = [new SubHeaderKPIDetailModel('Target','targetNc'),new SubHeaderKPIDetailModel('Actual','nc'),new SubHeaderKPIDetailModel('%Target','ncPercent')];
        }
        if(filter == 'RYP'){
            this.subHeaders = [new SubHeaderKPIDetailModel('Pers 13','prstRte'),new SubHeaderKPIDetailModel('Target','targetRyp'),new SubHeaderKPIDetailModel('Actual','ryp'),new SubHeaderKPIDetailModel('%Target','rypPercent')];
        }
    }

    getHeaderStyleByKpiFilter(filter){
        var styleClass = 'table-bg-header';
        if(filter == 'PC') styleClass += '-pc';
        if(filter == 'ACTIVE') styleClass += '-act';
        if(filter == 'NC') styleClass += '-nc';
        if(filter == 'RYP') styleClass += '-ryp';
        return styleClass;
    }

    getSubHeaderStyleByKpiFilter(filter){
        var styleClass = 'table-bg-subheader';
        if(filter == 'PC') styleClass += '-pc';
        if(filter == 'ACTIVE') styleClass += '-act';
        if(filter == 'NC') styleClass += '-nc';
        if(filter == 'RYP') styleClass += '-ryp';
        return styleClass;
    }
}

export class HeaderKPIDetailModel{
    name : string;
    fieldName : string;

    constructor(name,fieldName){
        this.name = name;
        this.fieldName = fieldName;
    }
}

export class SubHeaderKPIDetailModel{
    name : string;
    fieldName : string;

    constructor(name,fieldName){
        this.name = name;
        this.fieldName = fieldName;
    }
}