export class ModelAgentDetail {

  // Declare variables
  public agentDetail = {
    levelName: 'NA'
    , code: 'NA'
    , fullName: 'NA'
  };

  // Declare function/method
  setAgentDetail(level, code, name) {
    this.agentDetail = {
      levelName: level
      , code: code
      , fullName: name
    }
  }

  getAgentDetail() {
    return this.agentDetail;
  }
}