import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './core/security/auth.service';
import { AuthComponent } from './layout/auth/auth.component';
import { LoginGuards } from './core/security/login.guards';
import { DashboardComponent } from './layout/dashboard/dashboard.component';
import { AuthGuards } from './core/security/auth.guards';
import { ErrorComponent } from './layout/error/error.component';
import { LockEngineComponent } from './core/lock-engine/lock-engine/lock-engine.component';
import { DeviceGuards } from './core/security/device.guards';
import { GmGuards } from './core/security/gm.guards';
import { ProducerGuards } from './core/security/producer.guards';
import { StaffGuards } from './core/security/staff.guards';
import { AlGuards } from './core/security/al.guards';
import { AvpGuards } from './core/security/avp.guards';
import { SuperUserGuards } from './core/security/superuser.guards';
import { ERegisterAdminGuards } from './core/security/e-register.guards';
import { GuestGuards } from './core/security/guest.guards';
import { EDocumentComponent } from './web-agent/e-document/e-document.component';
import { DummyAuthComponent } from './dummy-auth/dummy-auth.component';

const routes: Routes = [
  {
    path: 'auth',
    component: AuthComponent,
    canActivate: [LoginGuards],
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./authentication/authentication.module').then(
            (m) => m.AuthenticationModule
          ),
      },
    ],
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuards, DeviceGuards],
    children: [
      {
        path: '',
        loadChildren: () =>
        import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },

    ],
  },

  {
    path: 'agent/kpidetail',
    component: DashboardComponent,
    canActivate: [AuthGuards],
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./theme/ag-kpi-detail/ag-kpi-detail.module').then((m) => m.AgKpiDetailModule),
      },
    ],
  },
  {
    path: 'kpi/subdetail',
    component: DashboardComponent,
    canActivate: [AuthGuards],
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./theme/kpi-sub-detail/kpi-sub-detail.module').then((m) => m.KpiSubDetailModule)
        
      }
    ]
  },
  // END AG DASHBOARD
  {
    path: 'web-agent',
    component: DashboardComponent,
    children: [
      {
        path: '', 
        canActivate: [AuthGuards],
        loadChildren: () => 
          import('./web-agent/web-agent.module').then((m) => m.WebAgentModule)
      }
    ]
  },
  {
    path: 'persistency',
    component: DashboardComponent,
    canActivate: [AuthGuards],
    children: [
      {
        path: '', 
        loadChildren: () => 
          import('./theme/persistency/persistency.module').then((m) => m.PersistencyModule)
      }
    ]
  },
  {
    path: 'targetYear',
    component: DashboardComponent,
    canActivate: [AuthGuards],
    children: [
      {
        path: '', 
        loadChildren: () => 
          import('./theme/target-year/target-year.module').then((m) => m.TargetYearModule)
      }
    ]
  },
  {
    path: 'uploadPromotion',
    component: DashboardComponent,
    canActivate: [AuthGuards],
    children: [
      {
        path: '', 
        loadChildren: () => 
          import('./theme/upload-promotion/upload-promotion.module').then((m) => m.UploadPromotionModule)
      }
    ]
  },
  {
    path: 'compensation',
    component: DashboardComponent,
    canActivate: [AuthGuards],
    children: [
      {
        path: '', 
        loadChildren: () => 
          import('./theme/compensation/compensation.module').then((m) => m.CompensationModule)
      }
    ]
  },
  {
    path: 'promotion',
    component: DashboardComponent,
    canActivate: [AuthGuards],
    children: [
      {
        path: '', 
        loadChildren: () => 
          import('./theme/promotion/promotion.module').then((m) => m.PromotionModule)
      }
    ]
  },
  {
    path: 'company/kpidetail',
    component: DashboardComponent,
    canActivate: [AuthGuards, DeviceGuards, StaffGuards],
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./theme/company-kpi-detail/company-kpi-detail.module').then((m) => m.CompanyKpiDetailModule),
      },
    ],
  },
  // END COMPANY AND CHANNEL KPI Detail

  {
    path: 'error', // Wildcard route for a 505 page
    component: ErrorComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./core/error/error.module').then((m) => m.ErrorModule),
      },
    ],
  },
  {
    path: 'lock-engine',
    component: LockEngineComponent,
    canActivate: [AuthGuards],
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./core/lock-engine/lock.engine.module').then((m) => m.LockEngineModule),
      },
    ],
  },
    
  {
    path: 'e-document',
    component: EDocumentComponent,
    canActivate: [GuestGuards]
  },
  { path: 'dummy-auth', component: DummyAuthComponent }, // redirect to home
  { path: '', redirectTo: 'dashboard', pathMatch: 'full'}, // redirect to home
  { path: '**', redirectTo: 'auth', pathMatch: 'full' }, // Wildcard route for a 404 page


];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
  providers: [AuthGuards, GuestGuards, LoginGuards, AuthService, DeviceGuards, GmGuards, ProducerGuards, StaffGuards, AvpGuards, AlGuards, SuperUserGuards , ERegisterAdminGuards],
})
export class AppRoutingModule {}
