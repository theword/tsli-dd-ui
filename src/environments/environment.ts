// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  info: {
    env: '',
    version: '0.0.3'
  },
  edocumentSecretKey: 'b63d4ed4f7f7c682eb1913aad1f2827ccd4ce2c96b364e5308927f22e791ccd4',
  headerDummy: 'aGVhZGVyRHVtbXk',
  headerDummyValue: 'b914540e5d719d341fc45d4d318b898c757e65cd45fab1f68b30026d49c8cbff',
  cmsUrl: 'https://dd.samsunglife.co.th/agency',
  registerUrl: 'https://digital.samsunglife.co.th/agent-service/form-register',
  forgetPasswordUrl: 'https://digital.samsunglife.co.th/agent-service/forgot-pwd',
  hashCode: '123456$#@$^@1ERF',
  apiUrl: '/api',
  apiWebAgentUrl: '/agency-api',
  oauth2: {
    issueUrl: '/oauth/token',
    revokeUrl: '/oauth/revoke',
    clientId: 'clientId',
    secret: 'secret',
    grantType: 'password'
  },
  lockEngine: {
    validationMobileURL: '/api/lock-engine/deviceValidation/mobile',
    validationDesktopURL: '/api/lock-engine/deviceValidation/desktop',
    desktopEndpoint: 'http://localhost:65080/deeseal.js',
    jqueryUrl: 'assets/lib/jquery/jquery.min.js',
    programWinUrl: 'http://35.202.125.35:9000/seal-setup.exe',
    programMacUrl: 'http://35.202.125.35:9000/mac-deeseal-1.0.pkg'
  }
};

