// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  info: {
    env: 'vm',
    version: '0.0.2'
  },
  hashCode: '',
  apiUrl: '/api',
  apiWebAgentUrl: '/resources',
  oauth2: {
    issueUrl: '/oauth/token',
    revokeUrl: '/oauth/revoke',
    clientId: 'clientId',
    secret: 'secret',
    grantType: 'password'
  },
  lockEngine: {
    validationMobileURL: '/api/lock-engine/deviceValidation/mobile',
    validationDesktopURL: '/api/lock-engine/deviceValidation/desktop',
    desktopEndpoint: 'http://localhost:65080/deeseal.js',
    jqueryUrl: 'assets/lib/jquery/jquery.min.js',
    programWinUrl: 'http://35.202.125.35:9000/seal-setup.exe',
    programMacUrl: 'http://35.202.125.35:9000/mac-deeseal-1.0.pkg'
  }
};

