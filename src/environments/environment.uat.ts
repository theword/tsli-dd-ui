// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  info: {
    env: 'uat',
    version: '0.0.4'
  },
  edocumentSecretKey: '65d9a10bbb11e3ea36fe64724a3fca02a542ac68749254518370d6e41f620490',
  headerDummy: 'aGVhZGVyRHVtbXk',
  headerDummyValue: 'b914540e5d719d341fc45d4d318b898c757e65cd45fab1f68b30026d49c8cbff',
  cmsUrl: 'https://dd-uat.samsunglife.co.th/agency',
  registerUrl: 'https://digital-uat.samsunglife.co.th/agent-service/form-register',
  forgetPasswordUrl: 'https://digital-uat.samsunglife.co.th/agent-service/forgot-pwd',
  hashCode: '123456$#@$^@1ERF',
  apiUrl: '/api',
  apiWebAgentUrl: '/agency-api',
  oauth2: {
    issueUrl: '/oauth/token',
    revokeUrl: '/oauth/revoke',
    clientId: 'clientId',
    secret: 'secret',
    grantType: 'password'
  },
  lockEngine: {
    validationMobileURL: '/api/lock-engine/deviceValidation/mobile',
    validationDesktopURL: '/api/lock-engine/deviceValidation/desktop',
    desktopEndpoint: 'http://localhost:65080/deeseal.js',
    jqueryUrl: 'assets/lib/jquery/jquery.min.js',
    programWinUrl: 'http://35.202.125.35:9000/seal-setup.exe',
    programMacUrl: 'http://35.202.125.35:9000/mac-deeseal-1.0.pkg'
  }
};

